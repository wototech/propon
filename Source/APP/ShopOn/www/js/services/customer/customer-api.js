﻿function CustomerAPI() {
    var self = this;

    this.searchValues = function (product, start_record, end_record, filterExpression, sort_expression, callback) {
        //todo filter expression -> user_id, comp_prod_id, instance_id, prod_id, comp_prod_name, comp_id, email_id, user_name, phone_no, full_name, city, state, country
        //$.server.webMethodGET("shopon/customer/?start_record=" + start_record + "&end_record=" + end_record + "&filter_expression=" + encodeURIComponent(filterExpression) + "&sort_expression=" + sort_expression + "&store_no=" + localStorage.getItem("user_store_no"), callback);
        var url = "shopon/customer/?start_record=" + start_record + "&end_record=" + end_record + "&filter_expression=" + encodeURIComponent(filterExpression) + "&sort_expression=" + sort_expression + "&store_no=" + localStorage.getItem("user_store_no");
        if (LCACHE.isEmpty(url)) {
            $.server.webMethodGET(url, function (result, obj) {
                LCACHE.set(url, result);
                callback(result, obj);
            });
        } else {
            callback(LCACHE.get(url));
        }
    }

    this.getValue = function (product, id, callback) {
        $.server.webMethodGET(product, "shopon/customer/" + id.cust_no, callback, callback);
    }

    this.postValue = function (product,data, callback, errorCallback) {
        $.server.webMethodPOST(product,"shopon/customer/", data, callback, errorCallback);
    }

    this.putValue = function (product, id, data, callback, errorCallback) {
        $.server.webMethodPUT(product, "shopon/customer/" + id, data, callback, errorCallback);
    }

    this.deleteValue = function (product, id, data, callback, errorCallback) {
        $.server.webMethodDELETE(product, "shopon/customer/" + id, data, callback, callback, callback);
    }
}