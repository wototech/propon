/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); +function (a) { "use strict"; var b = a.fn.jquery.split(" ")[0].split("."); if (b[0] < 2 && b[1] < 9 || 1 == b[0] && 9 == b[1] && b[2] < 1 || b[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4") }(jQuery), +function (a) { "use strict"; function b() { var a = document.createElement("bootstrap"), b = { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd otransitionend", transition: "transitionend" }; for (var c in b) if (void 0 !== a.style[c]) return { end: b[c] }; return !1 } a.fn.emulateTransitionEnd = function (b) { var c = !1, d = this; a(this).one("bsTransitionEnd", function () { c = !0 }); var e = function () { c || a(d).trigger(a.support.transition.end) }; return setTimeout(e, b), this }, a(function () { a.support.transition = b(), a.support.transition && (a.event.special.bsTransitionEnd = { bindType: a.support.transition.end, delegateType: a.support.transition.end, handle: function (b) { if (a(b.target).is(this)) return b.handleObj.handler.apply(this, arguments) } }) }) }(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var c = a(this), e = c.data("bs.alert"); e || c.data("bs.alert", e = new d(this)), "string" == typeof b && e[b].call(c) }) } var c = '[data-dismiss="alert"]', d = function (b) { a(b).on("click", c, this.close) }; d.VERSION = "3.3.7", d.TRANSITION_DURATION = 150, d.prototype.close = function (b) { function c() { g.detach().trigger("closed.bs.alert").remove() } var e = a(this), f = e.attr("data-target"); f || (f = e.attr("href"), f = f && f.replace(/.*(?=#[^\s]*$)/, "")); var g = a("#" === f ? [] : f); b && b.preventDefault(), g.length || (g = e.closest(".alert")), g.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (g.removeClass("in"), a.support.transition && g.hasClass("fade") ? g.one("bsTransitionEnd", c).emulateTransitionEnd(d.TRANSITION_DURATION) : c()) }; var e = a.fn.alert; a.fn.alert = b, a.fn.alert.Constructor = d, a.fn.alert.noConflict = function () { return a.fn.alert = e, this }, a(document).on("click.bs.alert.data-api", c, d.prototype.close) }(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var d = a(this), e = d.data("bs.button"), f = "object" == typeof b && b; e || d.data("bs.button", e = new c(this, f)), "toggle" == b ? e.toggle() : b && e.setState(b) }) } var c = function (b, d) { this.$element = a(b), this.options = a.extend({}, c.DEFAULTS, d), this.isLoading = !1 }; c.VERSION = "3.3.7", c.DEFAULTS = { loadingText: "loading..." }, c.prototype.setState = function (b) { var c = "disabled", d = this.$element, e = d.is("input") ? "val" : "html", f = d.data(); b += "Text", null == f.resetText && d.data("resetText", d[e]()), setTimeout(a.proxy(function () { d[e](null == f[b] ? this.options[b] : f[b]), "loadingText" == b ? (this.isLoading = !0, d.addClass(c).attr(c, c).prop(c, !0)) : this.isLoading && (this.isLoading = !1, d.removeClass(c).removeAttr(c).prop(c, !1)) }, this), 0) }, c.prototype.toggle = function () { var a = !0, b = this.$element.closest('[data-toggle="buttons"]'); if (b.length) { var c = this.$element.find("input"); "radio" == c.prop("type") ? (c.prop("checked") && (a = !1), b.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == c.prop("type") && (c.prop("checked") !== this.$element.hasClass("active") && (a = !1), this.$element.toggleClass("active")), c.prop("checked", this.$element.hasClass("active")), a && c.trigger("change") } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active") }; var d = a.fn.button; a.fn.button = b, a.fn.button.Constructor = c, a.fn.button.noConflict = function () { return a.fn.button = d, this }, a(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (c) { var d = a(c.target).closest(".btn"); b.call(d, "toggle"), a(c.target).is('input[type="radio"], input[type="checkbox"]') || (c.preventDefault(), d.is("input,button") ? d.trigger("focus") : d.find("input:visible,button:visible").first().trigger("focus")) }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (b) { a(b.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(b.type)) }) }(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var d = a(this), e = d.data("bs.carousel"), f = a.extend({}, c.DEFAULTS, d.data(), "object" == typeof b && b), g = "string" == typeof b ? b : f.slide; e || d.data("bs.carousel", e = new c(this, f)), "number" == typeof b ? e.to(b) : g ? e[g]() : f.interval && e.pause().cycle() }) } var c = function (b, c) { this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), this.options = c, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", a.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", a.proxy(this.pause, this)).on("mouseleave.bs.carousel", a.proxy(this.cycle, this)) }; c.VERSION = "3.3.7", c.TRANSITION_DURATION = 600, c.DEFAULTS = { interval: 5e3, pause: "hover", wrap: !0, keyboard: !0 }, c.prototype.keydown = function (a) { if (!/input|textarea/i.test(a.target.tagName)) { switch (a.which) { case 37: this.prev(); break; case 39: this.next(); break; default: return } a.preventDefault() } }, c.prototype.cycle = function (b) { return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), this }, c.prototype.getItemIndex = function (a) { return this.$items = a.parent().children(".item"), this.$items.index(a || this.$active) }, c.prototype.getItemForDirection = function (a, b) { var c = this.getItemIndex(b), d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1; if (d && !this.options.wrap) return b; var e = "prev" == a ? -1 : 1, f = (c + e) % this.$items.length; return this.$items.eq(f) }, c.prototype.to = function (a) { var b = this, c = this.getItemIndex(this.$active = this.$element.find(".item.active")); if (!(a > this.$items.length - 1 || a < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function () { b.to(a) }) : c == a ? this.pause().cycle() : this.slide(a > c ? "next" : "prev", this.$items.eq(a)) }, c.prototype.pause = function (b) { return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this }, c.prototype.next = function () { if (!this.sliding) return this.slide("next") }, c.prototype.prev = function () { if (!this.sliding) return this.slide("prev") }, c.prototype.slide = function (b, d) { var e = this.$element.find(".item.active"), f = d || this.getItemForDirection(b, e), g = this.interval, h = "next" == b ? "left" : "right", i = this; if (f.hasClass("active")) return this.sliding = !1; var j = f[0], k = a.Event("slide.bs.carousel", { relatedTarget: j, direction: h }); if (this.$element.trigger(k), !k.isDefaultPrevented()) { if (this.sliding = !0, g && this.pause(), this.$indicators.length) { this.$indicators.find(".active").removeClass("active"); var l = a(this.$indicators.children()[this.getItemIndex(f)]); l && l.addClass("active") } var m = a.Event("slid.bs.carousel", { relatedTarget: j, direction: h }); return a.support.transition && this.$element.hasClass("slide") ? (f.addClass(b), f[0].offsetWidth, e.addClass(h), f.addClass(h), e.one("bsTransitionEnd", function () { f.removeClass([b, h].join(" ")).addClass("active"), e.removeClass(["active", h].join(" ")), i.sliding = !1, setTimeout(function () { i.$element.trigger(m) }, 0) }).emulateTransitionEnd(c.TRANSITION_DURATION)) : (e.removeClass("active"), f.addClass("active"), this.sliding = !1, this.$element.trigger(m)), g && this.cycle(), this } }; var d = a.fn.carousel; a.fn.carousel = b, a.fn.carousel.Constructor = c, a.fn.carousel.noConflict = function () { return a.fn.carousel = d, this }; var e = function (c) { var d, e = a(this), f = a(e.attr("data-target") || (d = e.attr("href")) && d.replace(/.*(?=#[^\s]+$)/, "")); if (f.hasClass("carousel")) { var g = a.extend({}, f.data(), e.data()), h = e.attr("data-slide-to"); h && (g.interval = !1), b.call(f, g), h && f.data("bs.carousel").to(h), c.preventDefault() } }; a(document).on("click.bs.carousel.data-api", "[data-slide]", e).on("click.bs.carousel.data-api", "[data-slide-to]", e), a(window).on("load", function () { a('[data-ride="carousel"]').each(function () { var c = a(this); b.call(c, c.data()) }) }) }(jQuery), +function (a) { "use strict"; function b(b) { var c, d = b.attr("data-target") || (c = b.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, ""); return a(d) } function c(b) { return this.each(function () { var c = a(this), e = c.data("bs.collapse"), f = a.extend({}, d.DEFAULTS, c.data(), "object" == typeof b && b); !e && f.toggle && /show|hide/.test(b) && (f.toggle = !1), e || c.data("bs.collapse", e = new d(this, f)), "string" == typeof b && e[b]() }) } var d = function (b, c) { this.$element = a(b), this.options = a.extend({}, d.DEFAULTS, c), this.$trigger = a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle() }; d.VERSION = "3.3.7", d.TRANSITION_DURATION = 350, d.DEFAULTS = { toggle: !0 }, d.prototype.dimension = function () { var a = this.$element.hasClass("width"); return a ? "width" : "height" }, d.prototype.show = function () { if (!this.transitioning && !this.$element.hasClass("in")) { var b, e = this.$parent && this.$parent.children(".panel").children(".in, .collapsing"); if (!(e && e.length && (b = e.data("bs.collapse"), b && b.transitioning))) { var f = a.Event("show.bs.collapse"); if (this.$element.trigger(f), !f.isDefaultPrevented()) { e && e.length && (c.call(e, "hide"), b || e.data("bs.collapse", null)); var g = this.dimension(); this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1; var h = function () { this.$element.removeClass("collapsing").addClass("collapse in")[g](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse") }; if (!a.support.transition) return h.call(this); var i = a.camelCase(["scroll", g].join("-")); this.$element.one("bsTransitionEnd", a.proxy(h, this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i]) } } } }, d.prototype.hide = function () { if (!this.transitioning && this.$element.hasClass("in")) { var b = a.Event("hide.bs.collapse"); if (this.$element.trigger(b), !b.isDefaultPrevented()) { var c = this.dimension(); this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1; var e = function () { this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse") }; return a.support.transition ? void this.$element[c](0).one("bsTransitionEnd", a.proxy(e, this)).emulateTransitionEnd(d.TRANSITION_DURATION) : e.call(this) } } }, d.prototype.toggle = function () { this[this.$element.hasClass("in") ? "hide" : "show"]() }, d.prototype.getParent = function () { return a(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(a.proxy(function (c, d) { var e = a(d); this.addAriaAndCollapsedClass(b(e), e) }, this)).end() }, d.prototype.addAriaAndCollapsedClass = function (a, b) { var c = a.hasClass("in"); a.attr("aria-expanded", c), b.toggleClass("collapsed", !c).attr("aria-expanded", c) }; var e = a.fn.collapse; a.fn.collapse = c, a.fn.collapse.Constructor = d, a.fn.collapse.noConflict = function () { return a.fn.collapse = e, this }, a(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (d) { var e = a(this); e.attr("data-target") || d.preventDefault(); var f = b(e), g = f.data("bs.collapse"), h = g ? "toggle" : e.data(); c.call(f, h) }) }(jQuery), +function (a) { "use strict"; function b(b) { var c = b.attr("data-target"); c || (c = b.attr("href"), c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, "")); var d = c && a(c); return d && d.length ? d : b.parent() } function c(c) { c && 3 === c.which || (a(e).remove(), a(f).each(function () { var d = a(this), e = b(d), f = { relatedTarget: this }; e.hasClass("open") && (c && "click" == c.type && /input|textarea/i.test(c.target.tagName) && a.contains(e[0], c.target) || (e.trigger(c = a.Event("hide.bs.dropdown", f)), c.isDefaultPrevented() || (d.attr("aria-expanded", "false"), e.removeClass("open").trigger(a.Event("hidden.bs.dropdown", f))))) })) } function d(b) { return this.each(function () { var c = a(this), d = c.data("bs.dropdown"); d || c.data("bs.dropdown", d = new g(this)), "string" == typeof b && d[b].call(c) }) } var e = ".dropdown-backdrop", f = '[data-toggle="dropdown"]', g = function (b) { a(b).on("click.bs.dropdown", this.toggle) }; g.VERSION = "3.3.7", g.prototype.toggle = function (d) { var e = a(this); if (!e.is(".disabled, :disabled")) { var f = b(e), g = f.hasClass("open"); if (c(), !g) { "ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click", c); var h = { relatedTarget: this }; if (f.trigger(d = a.Event("show.bs.dropdown", h)), d.isDefaultPrevented()) return; e.trigger("focus").attr("aria-expanded", "true"), f.toggleClass("open").trigger(a.Event("shown.bs.dropdown", h)) } return !1 } }, g.prototype.keydown = function (c) { if (/(38|40|27|32)/.test(c.which) && !/input|textarea/i.test(c.target.tagName)) { var d = a(this); if (c.preventDefault(), c.stopPropagation(), !d.is(".disabled, :disabled")) { var e = b(d), g = e.hasClass("open"); if (!g && 27 != c.which || g && 27 == c.which) return 27 == c.which && e.find(f).trigger("focus"), d.trigger("click"); var h = " li:not(.disabled):visible a", i = e.find(".dropdown-menu" + h); if (i.length) { var j = i.index(c.target); 38 == c.which && j > 0 && j--, 40 == c.which && j < i.length - 1 && j++, ~j || (j = 0), i.eq(j).trigger("focus") } } } }; var h = a.fn.dropdown; a.fn.dropdown = d, a.fn.dropdown.Constructor = g, a.fn.dropdown.noConflict = function () { return a.fn.dropdown = h, this }, a(document).on("click.bs.dropdown.data-api", c).on("click.bs.dropdown.data-api", ".dropdown form", function (a) { a.stopPropagation() }).on("click.bs.dropdown.data-api", f, g.prototype.toggle).on("keydown.bs.dropdown.data-api", f, g.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown) }(jQuery), +function (a) { "use strict"; function b(b, d) { return this.each(function () { var e = a(this), f = e.data("bs.modal"), g = a.extend({}, c.DEFAULTS, e.data(), "object" == typeof b && b); f || e.data("bs.modal", f = new c(this, g)), "string" == typeof b ? f[b](d) : g.show && f.show(d) }) } var c = function (b, c) { this.options = c, this.$body = a(document.body), this.$element = a(b), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function () { this.$element.trigger("loaded.bs.modal") }, this)) }; c.VERSION = "3.3.7", c.TRANSITION_DURATION = 300, c.BACKDROP_TRANSITION_DURATION = 150, c.DEFAULTS = { backdrop: !0, keyboard: !0, show: !0 }, c.prototype.toggle = function (a) { return this.isShown ? this.hide() : this.show(a) }, c.prototype.show = function (b) { var d = this, e = a.Event("show.bs.modal", { relatedTarget: b }); this.$element.trigger(e), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () { d.$element.one("mouseup.dismiss.bs.modal", function (b) { a(b.target).is(d.$element) && (d.ignoreBackdropClick = !0) }) }), this.backdrop(function () { var e = a.support.transition && d.$element.hasClass("fade"); d.$element.parent().length || d.$element.appendTo(d.$body), d.$element.show().scrollTop(0), d.adjustDialog(), e && d.$element[0].offsetWidth, d.$element.addClass("in"), d.enforceFocus(); var f = a.Event("shown.bs.modal", { relatedTarget: b }); e ? d.$dialog.one("bsTransitionEnd", function () { d.$element.trigger("focus").trigger(f) }).emulateTransitionEnd(c.TRANSITION_DURATION) : d.$element.trigger("focus").trigger(f) })) }, c.prototype.hide = function (b) { b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), a(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), a.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", a.proxy(this.hideModal, this)).emulateTransitionEnd(c.TRANSITION_DURATION) : this.hideModal()) }, c.prototype.enforceFocus = function () { a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function (a) { document === a.target || this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.trigger("focus") }, this)) }, c.prototype.escape = function () { this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", a.proxy(function (a) { 27 == a.which && this.hide() }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal") }, c.prototype.resize = function () { this.isShown ? a(window).on("resize.bs.modal", a.proxy(this.handleUpdate, this)) : a(window).off("resize.bs.modal") }, c.prototype.hideModal = function () { var a = this; this.$element.hide(), this.backdrop(function () { a.$body.removeClass("modal-open"), a.resetAdjustments(), a.resetScrollbar(), a.$element.trigger("hidden.bs.modal") }) }, c.prototype.removeBackdrop = function () { this.$backdrop && this.$backdrop.remove(), this.$backdrop = null }, c.prototype.backdrop = function (b) { var d = this, e = this.$element.hasClass("fade") ? "fade" : ""; if (this.isShown && this.options.backdrop) { var f = a.support.transition && e; if (this.$backdrop = a(document.createElement("div")).addClass("modal-backdrop " + e).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", a.proxy(function (a) { return this.ignoreBackdropClick ? void (this.ignoreBackdropClick = !1) : void (a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide())) }, this)), f && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b) return; f ? this.$backdrop.one("bsTransitionEnd", b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : b() } else if (!this.isShown && this.$backdrop) { this.$backdrop.removeClass("in"); var g = function () { d.removeBackdrop(), b && b() }; a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : g() } else b && b() }, c.prototype.handleUpdate = function () { this.adjustDialog() }, c.prototype.adjustDialog = function () { var a = this.$element[0].scrollHeight > document.documentElement.clientHeight; this.$element.css({ paddingLeft: !this.bodyIsOverflowing && a ? this.scrollbarWidth : "", paddingRight: this.bodyIsOverflowing && !a ? this.scrollbarWidth : "" }) }, c.prototype.resetAdjustments = function () { this.$element.css({ paddingLeft: "", paddingRight: "" }) }, c.prototype.checkScrollbar = function () { var a = window.innerWidth; if (!a) { var b = document.documentElement.getBoundingClientRect(); a = b.right - Math.abs(b.left) } this.bodyIsOverflowing = document.body.clientWidth < a, this.scrollbarWidth = this.measureScrollbar() }, c.prototype.setScrollbar = function () { var a = parseInt(this.$body.css("padding-right") || 0, 10); this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", a + this.scrollbarWidth) }, c.prototype.resetScrollbar = function () { this.$body.css("padding-right", this.originalBodyPad) }, c.prototype.measureScrollbar = function () { var a = document.createElement("div"); a.className = "modal-scrollbar-measure", this.$body.append(a); var b = a.offsetWidth - a.clientWidth; return this.$body[0].removeChild(a), b }; var d = a.fn.modal; a.fn.modal = b, a.fn.modal.Constructor = c, a.fn.modal.noConflict = function () { return a.fn.modal = d, this }, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (c) { var d = a(this), e = d.attr("href"), f = a(d.attr("data-target") || e && e.replace(/.*(?=#[^\s]+$)/, "")), g = f.data("bs.modal") ? "toggle" : a.extend({ remote: !/#/.test(e) && e }, f.data(), d.data()); d.is("a") && c.preventDefault(), f.one("show.bs.modal", function (a) { a.isDefaultPrevented() || f.one("hidden.bs.modal", function () { d.is(":visible") && d.trigger("focus") }) }), b.call(f, g, this) }) }(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var d = a(this), e = d.data("bs.tooltip"), f = "object" == typeof b && b; !e && /destroy|hide/.test(b) || (e || d.data("bs.tooltip", e = new c(this, f)), "string" == typeof b && e[b]()) }) } var c = function (a, b) { this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", a, b) }; c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.DEFAULTS = { animation: !0, placement: "top", selector: !1, template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover focus", title: "", delay: 0, html: !1, container: !1, viewport: { selector: "body", padding: 0 } }, c.prototype.init = function (b, c, d) { if (this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d), this.$viewport = this.options.viewport && a(a.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = { click: !1, hover: !1, focus: !1 }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!"); for (var e = this.options.trigger.split(" "), f = e.length; f--;) { var g = e[f]; if ("click" == g) this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this)); else if ("manual" != g) { var h = "hover" == g ? "mouseenter" : "focusin", i = "hover" == g ? "mouseleave" : "focusout"; this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this)) } } this.options.selector ? this._options = a.extend({}, this.options, { trigger: "manual", selector: "" }) : this.fixTitle() }, c.prototype.getDefaults = function () { return c.DEFAULTS }, c.prototype.getOptions = function (b) { return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = { show: b.delay, hide: b.delay }), b }, c.prototype.getDelegateOptions = function () { var b = {}, c = this.getDefaults(); return this._options && a.each(this._options, function (a, d) { c[a] != d && (b[a] = d) }), b }, c.prototype.enter = function (b) { var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type); return c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusin" == b.type ? "focus" : "hover"] = !0), c.tip().hasClass("in") || "in" == c.hoverState ? void (c.hoverState = "in") : (clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void (c.timeout = setTimeout(function () { "in" == c.hoverState && c.show() }, c.options.delay.show)) : c.show()) }, c.prototype.isInStateTrue = function () { for (var a in this.inState) if (this.inState[a]) return !0; return !1 }, c.prototype.leave = function (b) { var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type); if (c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusout" == b.type ? "focus" : "hover"] = !1), !c.isInStateTrue()) return clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void (c.timeout = setTimeout(function () { "out" == c.hoverState && c.hide() }, c.options.delay.hide)) : c.hide() }, c.prototype.show = function () { var b = a.Event("show.bs." + this.type); if (this.hasContent() && this.enabled) { this.$element.trigger(b); var d = a.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]); if (b.isDefaultPrevented() || !d) return; var e = this, f = this.tip(), g = this.getUID(this.type); this.setContent(), f.attr("id", g), this.$element.attr("aria-describedby", g), this.options.animation && f.addClass("fade"); var h = "function" == typeof this.options.placement ? this.options.placement.call(this, f[0], this.$element[0]) : this.options.placement, i = /\s?auto?\s?/i, j = i.test(h); j && (h = h.replace(i, "") || "top"), f.detach().css({ top: 0, left: 0, display: "block" }).addClass(h).data("bs." + this.type, this), this.options.container ? f.appendTo(this.options.container) : f.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type); var k = this.getPosition(), l = f[0].offsetWidth, m = f[0].offsetHeight; if (j) { var n = h, o = this.getPosition(this.$viewport); h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h, f.removeClass(n).addClass(h) } var p = this.getCalculatedOffset(h, k, l, m); this.applyPlacement(p, h); var q = function () { var a = e.hoverState; e.$element.trigger("shown.bs." + e.type), e.hoverState = null, "out" == a && e.leave(e) }; a.support.transition && this.$tip.hasClass("fade") ? f.one("bsTransitionEnd", q).emulateTransitionEnd(c.TRANSITION_DURATION) : q() } }, c.prototype.applyPlacement = function (b, c) { var d = this.tip(), e = d[0].offsetWidth, f = d[0].offsetHeight, g = parseInt(d.css("margin-top"), 10), h = parseInt(d.css("margin-left"), 10); isNaN(g) && (g = 0), isNaN(h) && (h = 0), b.top += g, b.left += h, a.offset.setOffset(d[0], a.extend({ using: function (a) { d.css({ top: Math.round(a.top), left: Math.round(a.left) }) } }, b), 0), d.addClass("in"); var i = d[0].offsetWidth, j = d[0].offsetHeight; "top" == c && j != f && (b.top = b.top + f - j); var k = this.getViewportAdjustedDelta(c, b, i, j); k.left ? b.left += k.left : b.top += k.top; var l = /top|bottom/.test(c), m = l ? 2 * k.left - e + i : 2 * k.top - f + j, n = l ? "offsetWidth" : "offsetHeight"; d.offset(b), this.replaceArrow(m, d[0][n], l) }, c.prototype.replaceArrow = function (a, b, c) { this.arrow().css(c ? "left" : "top", 50 * (1 - a / b) + "%").css(c ? "top" : "left", "") }, c.prototype.setContent = function () { var a = this.tip(), b = this.getTitle(); a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right") }, c.prototype.hide = function (b) { function d() { "in" != e.hoverState && f.detach(), e.$element && e.$element.removeAttr("aria-describedby").trigger("hidden.bs." + e.type), b && b() } var e = this, f = a(this.$tip), g = a.Event("hide.bs." + this.type); if (this.$element.trigger(g), !g.isDefaultPrevented()) return f.removeClass("in"), a.support.transition && f.hasClass("fade") ? f.one("bsTransitionEnd", d).emulateTransitionEnd(c.TRANSITION_DURATION) : d(), this.hoverState = null, this }, c.prototype.fixTitle = function () { var a = this.$element; (a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "") }, c.prototype.hasContent = function () { return this.getTitle() }, c.prototype.getPosition = function (b) { b = b || this.$element; var c = b[0], d = "BODY" == c.tagName, e = c.getBoundingClientRect(); null == e.width && (e = a.extend({}, e, { width: e.right - e.left, height: e.bottom - e.top })); var f = window.SVGElement && c instanceof window.SVGElement, g = d ? { top: 0, left: 0 } : f ? null : b.offset(), h = { scroll: d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop() }, i = d ? { width: a(window).width(), height: a(window).height() } : null; return a.extend({}, e, h, i, g) }, c.prototype.getCalculatedOffset = function (a, b, c, d) { return "bottom" == a ? { top: b.top + b.height, left: b.left + b.width / 2 - c / 2 } : "top" == a ? { top: b.top - d, left: b.left + b.width / 2 - c / 2 } : "left" == a ? { top: b.top + b.height / 2 - d / 2, left: b.left - c } : { top: b.top + b.height / 2 - d / 2, left: b.left + b.width } }, c.prototype.getViewportAdjustedDelta = function (a, b, c, d) { var e = { top: 0, left: 0 }; if (!this.$viewport) return e; var f = this.options.viewport && this.options.viewport.padding || 0, g = this.getPosition(this.$viewport); if (/right|left/.test(a)) { var h = b.top - f - g.scroll, i = b.top + f - g.scroll + d; h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i) } else { var j = b.left - f, k = b.left + f + c; j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k) } return e }, c.prototype.getTitle = function () { var a, b = this.$element, c = this.options; return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title) }, c.prototype.getUID = function (a) { do a += ~~(1e6 * Math.random()); while (document.getElementById(a)); return a }, c.prototype.tip = function () { if (!this.$tip && (this.$tip = a(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!"); return this.$tip }, c.prototype.arrow = function () { return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow") }, c.prototype.enable = function () { this.enabled = !0 }, c.prototype.disable = function () { this.enabled = !1 }, c.prototype.toggleEnabled = function () { this.enabled = !this.enabled }, c.prototype.toggle = function (b) { var c = this; b && (c = a(b.currentTarget).data("bs." + this.type), c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c))), b ? (c.inState.click = !c.inState.click, c.isInStateTrue() ? c.enter(c) : c.leave(c)) : c.tip().hasClass("in") ? c.leave(c) : c.enter(c) }, c.prototype.destroy = function () { var a = this; clearTimeout(this.timeout), this.hide(function () { a.$element.off("." + a.type).removeData("bs." + a.type), a.$tip && a.$tip.detach(), a.$tip = null, a.$arrow = null, a.$viewport = null, a.$element = null }) }; var d = a.fn.tooltip; a.fn.tooltip = b, a.fn.tooltip.Constructor = c, a.fn.tooltip.noConflict = function () { return a.fn.tooltip = d, this } }(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var d = a(this), e = d.data("bs.popover"), f = "object" == typeof b && b; !e && /destroy|hide/.test(b) || (e || d.data("bs.popover", e = new c(this, f)), "string" == typeof b && e[b]()) }) } var c = function (a, b) { this.init("popover", a, b) }; if (!a.fn.tooltip) throw new Error("Popover requires tooltip.js"); c.VERSION = "3.3.7", c.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, { placement: "right", trigger: "click", content: "", template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>' }), c.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), c.prototype.constructor = c, c.prototype.getDefaults = function () { return c.DEFAULTS }, c.prototype.setContent = function () { var a = this.tip(), b = this.getTitle(), c = this.getContent(); a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c), a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide() }, c.prototype.hasContent = function () { return this.getTitle() || this.getContent() }, c.prototype.getContent = function () { var a = this.$element, b = this.options; return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content) }, c.prototype.arrow = function () { return this.$arrow = this.$arrow || this.tip().find(".arrow") }; var d = a.fn.popover; a.fn.popover = b, a.fn.popover.Constructor = c, a.fn.popover.noConflict = function () { return a.fn.popover = d, this } }(jQuery), +function (a) {
    "use strict"; function b(c, d) { this.$body = a(document.body), this.$scrollElement = a(a(c).is(document.body) ? window : c), this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", a.proxy(this.process, this)), this.refresh(), this.process() } function c(c) { return this.each(function () { var d = a(this), e = d.data("bs.scrollspy"), f = "object" == typeof c && c; e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]() }) } b.VERSION = "3.3.7", b.DEFAULTS = { offset: 10 }, b.prototype.getScrollHeight = function () { return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight) }, b.prototype.refresh = function () { var b = this, c = "offset", d = 0; this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), a.isWindow(this.$scrollElement[0]) || (c = "position", d = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () { var b = a(this), e = b.data("target") || b.attr("href"), f = /^#./.test(e) && a(e); return f && f.length && f.is(":visible") && [[f[c]().top + d, e]] || null }).sort(function (a, b) { return a[0] - b[0] }).each(function () { b.offsets.push(this[0]), b.targets.push(this[1]) }) }, b.prototype.process = function () { var a, b = this.$scrollElement.scrollTop() + this.options.offset, c = this.getScrollHeight(), d = this.options.offset + c - this.$scrollElement.height(), e = this.offsets, f = this.targets, g = this.activeTarget; if (this.scrollHeight != c && this.refresh(), b >= d) return g != (a = f[f.length - 1]) && this.activate(a); if (g && b < e[0]) return this.activeTarget = null, this.clear(); for (a = e.length; a--;) g != f[a] && b >= e[a] && (void 0 === e[a + 1] || b < e[a + 1]) && this.activate(f[a]) }, b.prototype.activate = function (b) {
        this.activeTarget = b, this.clear(); var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]', d = a(c).parents("li").addClass("active"); d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), d.trigger("activate.bs.scrollspy")
    }, b.prototype.clear = function () { a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active") }; var d = a.fn.scrollspy; a.fn.scrollspy = c, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function () { return a.fn.scrollspy = d, this }, a(window).on("load.bs.scrollspy.data-api", function () { a('[data-spy="scroll"]').each(function () { var b = a(this); c.call(b, b.data()) }) })
}(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var d = a(this), e = d.data("bs.tab"); e || d.data("bs.tab", e = new c(this)), "string" == typeof b && e[b]() }) } var c = function (b) { this.element = a(b) }; c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.prototype.show = function () { var b = this.element, c = b.closest("ul:not(.dropdown-menu)"), d = b.data("target"); if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) { var e = c.find(".active:last a"), f = a.Event("hide.bs.tab", { relatedTarget: b[0] }), g = a.Event("show.bs.tab", { relatedTarget: e[0] }); if (e.trigger(f), b.trigger(g), !g.isDefaultPrevented() && !f.isDefaultPrevented()) { var h = a(d); this.activate(b.closest("li"), c), this.activate(h, h.parent(), function () { e.trigger({ type: "hidden.bs.tab", relatedTarget: b[0] }), b.trigger({ type: "shown.bs.tab", relatedTarget: e[0] }) }) } } }, c.prototype.activate = function (b, d, e) { function f() { g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), h ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), b.parent(".dropdown-menu").length && b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), e && e() } var g = d.find("> .active"), h = e && a.support.transition && (g.length && g.hasClass("fade") || !!d.find("> .fade").length); g.length && h ? g.one("bsTransitionEnd", f).emulateTransitionEnd(c.TRANSITION_DURATION) : f(), g.removeClass("in") }; var d = a.fn.tab; a.fn.tab = b, a.fn.tab.Constructor = c, a.fn.tab.noConflict = function () { return a.fn.tab = d, this }; var e = function (c) { c.preventDefault(), b.call(a(this), "show") }; a(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', e).on("click.bs.tab.data-api", '[data-toggle="pill"]', e) }(jQuery), +function (a) { "use strict"; function b(b) { return this.each(function () { var d = a(this), e = d.data("bs.affix"), f = "object" == typeof b && b; e || d.data("bs.affix", e = new c(this, f)), "string" == typeof b && e[b]() }) } var c = function (b, d) { this.options = a.extend({}, c.DEFAULTS, d), this.$target = a(this.options.target).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), this.$element = a(b), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition() }; c.VERSION = "3.3.7", c.RESET = "affix affix-top affix-bottom", c.DEFAULTS = { offset: 0, target: window }, c.prototype.getState = function (a, b, c, d) { var e = this.$target.scrollTop(), f = this.$element.offset(), g = this.$target.height(); if (null != c && "top" == this.affixed) return e < c && "top"; if ("bottom" == this.affixed) return null != c ? !(e + this.unpin <= f.top) && "bottom" : !(e + g <= a - d) && "bottom"; var h = null == this.affixed, i = h ? e : f.top, j = h ? g : b; return null != c && e <= c ? "top" : null != d && i + j >= a - d && "bottom" }, c.prototype.getPinnedOffset = function () { if (this.pinnedOffset) return this.pinnedOffset; this.$element.removeClass(c.RESET).addClass("affix"); var a = this.$target.scrollTop(), b = this.$element.offset(); return this.pinnedOffset = b.top - a }, c.prototype.checkPositionWithEventLoop = function () { setTimeout(a.proxy(this.checkPosition, this), 1) }, c.prototype.checkPosition = function () { if (this.$element.is(":visible")) { var b = this.$element.height(), d = this.options.offset, e = d.top, f = d.bottom, g = Math.max(a(document).height(), a(document.body).height()); "object" != typeof d && (f = e = d), "function" == typeof e && (e = d.top(this.$element)), "function" == typeof f && (f = d.bottom(this.$element)); var h = this.getState(g, b, e, f); if (this.affixed != h) { null != this.unpin && this.$element.css("top", ""); var i = "affix" + (h ? "-" + h : ""), j = a.Event(i + ".bs.affix"); if (this.$element.trigger(j), j.isDefaultPrevented()) return; this.affixed = h, this.unpin = "bottom" == h ? this.getPinnedOffset() : null, this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix", "affixed") + ".bs.affix") } "bottom" == h && this.$element.offset({ top: g - b - f }) } }; var d = a.fn.affix; a.fn.affix = b, a.fn.affix.Constructor = c, a.fn.affix.noConflict = function () { return a.fn.affix = d, this }, a(window).on("load", function () { a('[data-spy="affix"]').each(function () { var c = a(this), d = c.data(); d.offset = d.offset || {}, null != d.offsetBottom && (d.offset.bottom = d.offsetBottom), null != d.offsetTop && (d.offset.top = d.offsetTop), b.call(c, d) }) }) }(jQuery);


//JQuery UI
!function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t(jQuery)}(function(P){var t,e,i,s;function n(t,e){var i,s,n,o=t.nodeName.toLowerCase();return"area"===o?(s=(i=t.parentNode).name,!(!t.href||!s||"map"!==i.nodeName.toLowerCase())&&(!!(n=P("img[usemap='#"+s+"']")[0])&&a(n))):(/input|select|textarea|button|object/.test(o)?!t.disabled:"a"===o&&t.href||e)&&a(t)}function a(t){return P.expr.filters.visible(t)&&!P(t).parents().addBack().filter(function(){return"hidden"===P.css(this,"visibility")}).length}P.ui=P.ui||{},P.extend(P.ui,{version:"1.11.2",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),P.fn.extend({scrollParent:function(t){var e=this.css("position"),i="absolute"===e,s=t?/(auto|scroll|hidden)/:/(auto|scroll)/,n=this.parents().filter(function(){var t=P(this);return(!i||"static"!==t.css("position"))&&s.test(t.css("overflow")+t.css("overflow-y")+t.css("overflow-x"))}).eq(0);return"fixed"!==e&&n.length?n:P(this[0].ownerDocument||document)},uniqueId:(t=0,function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++t)})}),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&P(this).removeAttr("id")})}}),P.extend(P.expr[":"],{data:P.expr.createPseudo?P.expr.createPseudo(function(e){return function(t){return!!P.data(t,e)}}):function(t,e,i){return!!P.data(t,i[3])},focusable:function(t){return n(t,!isNaN(P.attr(t,"tabindex")))},tabbable:function(t){var e=P.attr(t,"tabindex"),i=isNaN(e);return(i||0<=e)&&n(t,!i)}}),P("<a>").outerWidth(1).jquery||P.each(["Width","Height"],function(t,i){var n="Width"===i?["Left","Right"]:["Top","Bottom"],s=i.toLowerCase(),o={innerWidth:P.fn.innerWidth,innerHeight:P.fn.innerHeight,outerWidth:P.fn.outerWidth,outerHeight:P.fn.outerHeight};function a(t,e,i,s){return P.each(n,function(){e-=parseFloat(P.css(t,"padding"+this))||0,i&&(e-=parseFloat(P.css(t,"border"+this+"Width"))||0),s&&(e-=parseFloat(P.css(t,"margin"+this))||0)}),e}P.fn["inner"+i]=function(t){return void 0===t?o["inner"+i].call(this):this.each(function(){P(this).css(s,a(this,t)+"px")})},P.fn["outer"+i]=function(t,e){return"number"!=typeof t?o["outer"+i].call(this,t):this.each(function(){P(this).css(s,a(this,t,!0,e)+"px")})}}),P.fn.addBack||(P.fn.addBack=function(t){return this.add(null==t?this.prevObject:this.prevObject.filter(t))}),P("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(P.fn.removeData=(e=P.fn.removeData,function(t){return arguments.length?e.call(this,P.camelCase(t)):e.call(this)})),P.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),P.fn.extend({focus:(s=P.fn.focus,function(e,i){return"number"==typeof e?this.each(function(){var t=this;setTimeout(function(){P(t).focus(),i&&i.call(t)},e)}):s.apply(this,arguments)}),disableSelection:(i="onselectstart"in document.createElement("div")?"selectstart":"mousedown",function(){return this.bind(i+".ui-disableSelection",function(t){t.preventDefault()})}),enableSelection:function(){return this.unbind(".ui-disableSelection")},zIndex:function(t){if(void 0!==t)return this.css("zIndex",t);if(this.length)for(var e,i,s=P(this[0]);s.length&&s[0]!==document;){if(("absolute"===(e=s.css("position"))||"relative"===e||"fixed"===e)&&(i=parseInt(s.css("zIndex"),10),!isNaN(i)&&0!==i))return i;s=s.parent()}return 0}}),P.ui.plugin={add:function(t,e,i){var s,n=P.ui[t].prototype;for(s in i)n.plugins[s]=n.plugins[s]||[],n.plugins[s].push([e,i[s]])},call:function(t,e,i,s){var n,o=t.plugins[e];if(o&&(s||t.element[0].parentNode&&11!==t.element[0].parentNode.nodeType))for(n=0;n<o.length;n++)t.options[o[n][0]]&&o[n][1].apply(t.element,i)}};var o,r=0,h=Array.prototype.slice;P.cleanData=(o=P.cleanData,function(t){var e,i,s;for(s=0;null!=(i=t[s]);s++)try{(e=P._data(i,"events"))&&e.remove&&P(i).triggerHandler("remove")}catch(t){}o(t)}),P.widget=function(t,i,e){var s,n,o,a,r={},h=t.split(".")[0];return t=t.split(".")[1],s=h+"-"+t,e||(e=i,i=P.Widget),P.expr[":"][s.toLowerCase()]=function(t){return!!P.data(t,s)},P[h]=P[h]||{},n=P[h][t],o=P[h][t]=function(t,e){if(!this._createWidget)return new o(t,e);arguments.length&&this._createWidget(t,e)},P.extend(o,n,{version:e.version,_proto:P.extend({},e),_childConstructors:[]}),(a=new i).options=P.widget.extend({},a.options),P.each(e,function(e,s){var n,o;P.isFunction(s)?r[e]=(n=function(){return i.prototype[e].apply(this,arguments)},o=function(t){return i.prototype[e].apply(this,t)},function(){var t,e=this._super,i=this._superApply;return this._super=n,this._superApply=o,t=s.apply(this,arguments),this._super=e,this._superApply=i,t}):r[e]=s}),o.prototype=P.widget.extend(a,{widgetEventPrefix:n&&a.widgetEventPrefix||t},r,{constructor:o,namespace:h,widgetName:t,widgetFullName:s}),n?(P.each(n._childConstructors,function(t,e){var i=e.prototype;P.widget(i.namespace+"."+i.widgetName,o,e._proto)}),delete n._childConstructors):i._childConstructors.push(o),P.widget.bridge(t,o),o},P.widget.extend=function(t){for(var e,i,s=h.call(arguments,1),n=0,o=s.length;n<o;n++)for(e in s[n])i=s[n][e],s[n].hasOwnProperty(e)&&void 0!==i&&(P.isPlainObject(i)?t[e]=P.isPlainObject(t[e])?P.widget.extend({},t[e],i):P.widget.extend({},i):t[e]=i);return t},P.widget.bridge=function(o,e){var a=e.prototype.widgetFullName||o;P.fn[o]=function(i){var t="string"==typeof i,s=h.call(arguments,1),n=this;return i=!t&&s.length?P.widget.extend.apply(null,[i].concat(s)):i,t?this.each(function(){var t,e=P.data(this,a);return"instance"===i?(n=e,!1):e?P.isFunction(e[i])&&"_"!==i.charAt(0)?(t=e[i].apply(e,s))!==e&&void 0!==t?(n=t&&t.jquery?n.pushStack(t.get()):t,!1):void 0:P.error("no such method '"+i+"' for "+o+" widget instance"):P.error("cannot call methods on "+o+" prior to initialization; attempted to call method '"+i+"'")}):this.each(function(){var t=P.data(this,a);t?(t.option(i||{}),t._init&&t._init()):P.data(this,a,new e(i,this))}),n}},P.Widget=function(){},P.Widget._childConstructors=[],P.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,e){e=P(e||this.defaultElement||this)[0],this.element=P(e),this.uuid=r++,this.eventNamespace="."+this.widgetName+this.uuid,this.bindings=P(),this.hoverable=P(),this.focusable=P(),e!==this&&(P.data(e,this.widgetFullName,this),this._on(!0,this.element,{remove:function(t){t.target===e&&this.destroy()}}),this.document=P(e.style?e.ownerDocument:e.document||e),this.window=P(this.document[0].defaultView||this.document[0].parentWindow)),this.options=P.widget.extend({},this.options,this._getCreateOptions(),t),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:P.noop,_getCreateEventData:P.noop,_create:P.noop,_init:P.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(P.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:P.noop,widget:function(){return this.element},option:function(t,e){var i,s,n,o=t;if(0===arguments.length)return P.widget.extend({},this.options);if("string"==typeof t)if(o={},t=(i=t.split(".")).shift(),i.length){for(s=o[t]=P.widget.extend({},this.options[t]),n=0;n<i.length-1;n++)s[i[n]]=s[i[n]]||{},s=s[i[n]];if(t=i.pop(),1===arguments.length)return void 0===s[t]?null:s[t];s[t]=e}else{if(1===arguments.length)return void 0===this.options[t]?null:this.options[t];o[t]=e}return this._setOptions(o),this},_setOptions:function(t){var e;for(e in t)this._setOption(e,t[e]);return this},_setOption:function(t,e){return this.options[t]=e,"disabled"===t&&(this.widget().toggleClass(this.widgetFullName+"-disabled",!!e),e&&(this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus"))),this},enable:function(){return this._setOptions({disabled:!1})},disable:function(){return this._setOptions({disabled:!0})},_on:function(a,r,t){var h,l=this;"boolean"!=typeof a&&(t=r,r=a,a=!1),t?(r=h=P(r),this.bindings=this.bindings.add(r)):(t=r,r=this.element,h=this.widget()),P.each(t,function(t,e){function i(){if(a||!0!==l.options.disabled&&!P(this).hasClass("ui-state-disabled"))return("string"==typeof e?l[e]:e).apply(l,arguments)}"string"!=typeof e&&(i.guid=e.guid=e.guid||i.guid||P.guid++);var s=t.match(/^([\w:-]*)\s*(.*)$/),n=s[1]+l.eventNamespace,o=s[2];o?h.delegate(o,n,i):r.bind(n,i)})},_off:function(t,e){e=(e||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,t.unbind(e).undelegate(e),this.bindings=P(this.bindings.not(t).get()),this.focusable=P(this.focusable.not(t).get()),this.hoverable=P(this.hoverable.not(t).get())},_delay:function(t,e){var i=this;return setTimeout(function(){return("string"==typeof t?i[t]:t).apply(i,arguments)},e||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){P(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){P(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){P(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){P(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,e,i){var s,n,o=this.options[t];if(i=i||{},(e=P.Event(e)).type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),e.target=this.element[0],n=e.originalEvent)for(s in n)s in e||(e[s]=n[s]);return this.element.trigger(e,i),!(P.isFunction(o)&&!1===o.apply(this.element[0],[e].concat(i))||e.isDefaultPrevented())}},P.each({show:"fadeIn",hide:"fadeOut"},function(o,a){P.Widget.prototype["_"+o]=function(e,t,i){"string"==typeof t&&(t={effect:t});var s,n=t?!0===t||"number"==typeof t?a:t.effect||a:o;"number"==typeof(t=t||{})&&(t={duration:t}),s=!P.isEmptyObject(t),t.complete=i,t.delay&&e.delay(t.delay),s&&P.effects&&P.effects.effect[n]?e[o](t):n!==o&&e[n]?e[n](t.duration,t.easing,i):e.queue(function(t){P(this)[o](),i&&i.call(e[0]),t()})}});P.widget;var l=!1;P(document).mouseup(function(){l=!1});P.widget("ui.mouse",{version:"1.11.2",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var e=this;this.element.bind("mousedown."+this.widgetName,function(t){return e._mouseDown(t)}).bind("click."+this.widgetName,function(t){if(!0===P.data(t.target,e.widgetName+".preventClickEvent"))return P.removeData(t.target,e.widgetName+".preventClickEvent"),t.stopImmediatePropagation(),!1}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&this.document.unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(t){if(!l){this._mouseMoved=!1,this._mouseStarted&&this._mouseUp(t),this._mouseDownEvent=t;var e=this,i=1===t.which,s=!("string"!=typeof this.options.cancel||!t.target.nodeName)&&P(t.target).closest(this.options.cancel).length;return!(i&&!s&&this._mouseCapture(t))||(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){e.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=!1!==this._mouseStart(t),!this._mouseStarted)?(t.preventDefault(),!0):(!0===P.data(t.target,this.widgetName+".preventClickEvent")&&P.removeData(t.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(t){return e._mouseMove(t)},this._mouseUpDelegate=function(t){return e._mouseUp(t)},this.document.bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),t.preventDefault(),l=!0))}},_mouseMove:function(t){if(this._mouseMoved){if(P.ui.ie&&(!document.documentMode||document.documentMode<9)&&!t.button)return this._mouseUp(t);if(!t.which)return this._mouseUp(t)}return(t.which||t.button)&&(this._mouseMoved=!0),this._mouseStarted?(this._mouseDrag(t),t.preventDefault()):(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=!1!==this._mouseStart(this._mouseDownEvent,t),this._mouseStarted?this._mouseDrag(t):this._mouseUp(t)),!this._mouseStarted)},_mouseUp:function(t){return this.document.unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,t.target===this._mouseDownEvent.target&&P.data(t.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(t)),l=!1},_mouseDistanceMet:function(t){return Math.max(Math.abs(this._mouseDownEvent.pageX-t.pageX),Math.abs(this._mouseDownEvent.pageY-t.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}});!function(){P.ui=P.ui||{};var n,x,k=Math.max,C=Math.abs,D=Math.round,s=/left|center|right/,o=/top|center|bottom/,a=/[\+\-]\d+(\.[\d]+)?%?/,r=/^\w+/,h=/%$/,l=P.fn.position;function I(t,e,i){return[parseFloat(t[0])*(h.test(t[0])?e/100:1),parseFloat(t[1])*(h.test(t[1])?i/100:1)]}function T(t,e){return parseInt(P.css(t,e),10)||0}P.position={scrollbarWidth:function(){if(void 0!==n)return n;var t,e,i=P("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),s=i.children()[0];return P("body").append(i),t=s.offsetWidth,i.css("overflow","scroll"),t===(e=s.offsetWidth)&&(e=i[0].clientWidth),i.remove(),n=t-e},getScrollInfo:function(t){var e=t.isWindow||t.isDocument?"":t.element.css("overflow-x"),i=t.isWindow||t.isDocument?"":t.element.css("overflow-y"),s="scroll"===e||"auto"===e&&t.width<t.element[0].scrollWidth;return{width:"scroll"===i||"auto"===i&&t.height<t.element[0].scrollHeight?P.position.scrollbarWidth():0,height:s?P.position.scrollbarWidth():0}},getWithinInfo:function(t){var e=P(t||window),i=P.isWindow(e[0]),s=!!e[0]&&9===e[0].nodeType;return{element:e,isWindow:i,isDocument:s,offset:e.offset()||{left:0,top:0},scrollLeft:e.scrollLeft(),scrollTop:e.scrollTop(),width:i||s?e.width():e.outerWidth(),height:i||s?e.height():e.outerHeight()}}},P.fn.position=function(u){if(!u||!u.of)return l.apply(this,arguments);u=P.extend({},u);var d,p,f,m,g,t,e,i,v=P(u.of),_=P.position.getWithinInfo(u.within),b=P.position.getScrollInfo(_),y=(u.collision||"flip").split(" "),w={};return t=9===(i=(e=v)[0]).nodeType?{width:e.width(),height:e.height(),offset:{top:0,left:0}}:P.isWindow(i)?{width:e.width(),height:e.height(),offset:{top:e.scrollTop(),left:e.scrollLeft()}}:i.preventDefault?{width:0,height:0,offset:{top:i.pageY,left:i.pageX}}:{width:e.outerWidth(),height:e.outerHeight(),offset:e.offset()},v[0].preventDefault&&(u.at="left top"),p=t.width,f=t.height,m=t.offset,g=P.extend({},m),P.each(["my","at"],function(){var t,e,i=(u[this]||"").split(" ");1===i.length&&(i=s.test(i[0])?i.concat(["center"]):o.test(i[0])?["center"].concat(i):["center","center"]),i[0]=s.test(i[0])?i[0]:"center",i[1]=o.test(i[1])?i[1]:"center",t=a.exec(i[0]),e=a.exec(i[1]),w[this]=[t?t[0]:0,e?e[0]:0],u[this]=[r.exec(i[0])[0],r.exec(i[1])[0]]}),1===y.length&&(y[1]=y[0]),"right"===u.at[0]?g.left+=p:"center"===u.at[0]&&(g.left+=p/2),"bottom"===u.at[1]?g.top+=f:"center"===u.at[1]&&(g.top+=f/2),d=I(w.at,p,f),g.left+=d[0],g.top+=d[1],this.each(function(){var i,t,a=P(this),r=a.outerWidth(),h=a.outerHeight(),e=T(this,"marginLeft"),s=T(this,"marginTop"),n=r+e+T(this,"marginRight")+b.width,o=h+s+T(this,"marginBottom")+b.height,l=P.extend({},g),c=I(w.my,a.outerWidth(),a.outerHeight());"right"===u.my[0]?l.left-=r:"center"===u.my[0]&&(l.left-=r/2),"bottom"===u.my[1]?l.top-=h:"center"===u.my[1]&&(l.top-=h/2),l.left+=c[0],l.top+=c[1],x||(l.left=D(l.left),l.top=D(l.top)),i={marginLeft:e,marginTop:s},P.each(["left","top"],function(t,e){P.ui.position[y[t]]&&P.ui.position[y[t]][e](l,{targetWidth:p,targetHeight:f,elemWidth:r,elemHeight:h,collisionPosition:i,collisionWidth:n,collisionHeight:o,offset:[d[0]+c[0],d[1]+c[1]],my:u.my,at:u.at,within:_,elem:a})}),u.using&&(t=function(t){var e=m.left-l.left,i=e+p-r,s=m.top-l.top,n=s+f-h,o={target:{element:v,left:m.left,top:m.top,width:p,height:f},element:{element:a,left:l.left,top:l.top,width:r,height:h},horizontal:i<0?"left":0<e?"right":"center",vertical:n<0?"top":0<s?"bottom":"middle"};p<r&&C(e+i)<p&&(o.horizontal="center"),f<h&&C(s+n)<f&&(o.vertical="middle"),k(C(e),C(i))>k(C(s),C(n))?o.important="horizontal":o.important="vertical",u.using.call(this,t,o)}),a.offset(P.extend(l,{using:t}))})},P.ui.position={fit:{left:function(t,e){var i,s=e.within,n=s.isWindow?s.scrollLeft:s.offset.left,o=s.width,a=t.left-e.collisionPosition.marginLeft,r=n-a,h=a+e.collisionWidth-o-n;e.collisionWidth>o?0<r&&h<=0?(i=t.left+r+e.collisionWidth-o-n,t.left+=r-i):t.left=0<h&&r<=0?n:h<r?n+o-e.collisionWidth:n:0<r?t.left+=r:0<h?t.left-=h:t.left=k(t.left-a,t.left)},top:function(t,e){var i,s=e.within,n=s.isWindow?s.scrollTop:s.offset.top,o=e.within.height,a=t.top-e.collisionPosition.marginTop,r=n-a,h=a+e.collisionHeight-o-n;e.collisionHeight>o?0<r&&h<=0?(i=t.top+r+e.collisionHeight-o-n,t.top+=r-i):t.top=0<h&&r<=0?n:h<r?n+o-e.collisionHeight:n:0<r?t.top+=r:0<h?t.top-=h:t.top=k(t.top-a,t.top)}},flip:{left:function(t,e){var i,s,n=e.within,o=n.offset.left+n.scrollLeft,a=n.width,r=n.isWindow?n.scrollLeft:n.offset.left,h=t.left-e.collisionPosition.marginLeft,l=h-r,c=h+e.collisionWidth-a-r,u="left"===e.my[0]?-e.elemWidth:"right"===e.my[0]?e.elemWidth:0,d="left"===e.at[0]?e.targetWidth:"right"===e.at[0]?-e.targetWidth:0,p=-2*e.offset[0];l<0?((i=t.left+u+d+p+e.collisionWidth-a-o)<0||i<C(l))&&(t.left+=u+d+p):0<c&&(0<(s=t.left-e.collisionPosition.marginLeft+u+d+p-r)||C(s)<c)&&(t.left+=u+d+p)},top:function(t,e){var i,s,n=e.within,o=n.offset.top+n.scrollTop,a=n.height,r=n.isWindow?n.scrollTop:n.offset.top,h=t.top-e.collisionPosition.marginTop,l=h-r,c=h+e.collisionHeight-a-r,u="top"===e.my[1]?-e.elemHeight:"bottom"===e.my[1]?e.elemHeight:0,d="top"===e.at[1]?e.targetHeight:"bottom"===e.at[1]?-e.targetHeight:0,p=-2*e.offset[1];l<0?(s=t.top+u+d+p+e.collisionHeight-a-o,t.top+u+d+p>l&&(s<0||s<C(l))&&(t.top+=u+d+p)):0<c&&(i=t.top-e.collisionPosition.marginTop+u+d+p-r,t.top+u+d+p>c&&(0<i||C(i)<c)&&(t.top+=u+d+p))}},flipfit:{left:function(){P.ui.position.flip.left.apply(this,arguments),P.ui.position.fit.left.apply(this,arguments)},top:function(){P.ui.position.flip.top.apply(this,arguments),P.ui.position.fit.top.apply(this,arguments)}}},function(){var t,e,i,s,n,o=document.getElementsByTagName("body")[0],a=document.createElement("div");for(n in t=document.createElement(o?"div":"body"),i={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},o&&P.extend(i,{position:"absolute",left:"-1000px",top:"-1000px"}),i)t.style[n]=i[n];t.appendChild(a),(e=o||document.documentElement).insertBefore(t,e.firstChild),a.style.cssText="position: absolute; left: 10.7432222px;",s=P(a).offset().left,x=10<s&&s<11,t.innerHTML="",e.removeChild(t)}()}();P.ui.position;P.widget("ui.draggable",P.ui.mouse,{version:"1.11.2",widgetEventPrefix:"drag",options:{addClasses:!0,appendTo:"parent",axis:!1,connectToSortable:!1,containment:!1,cursor:"auto",cursorAt:!1,grid:!1,handle:!1,helper:"original",iframeFix:!1,opacity:!1,refreshPositions:!1,revert:!1,revertDuration:500,scope:"default",scroll:!0,scrollSensitivity:20,scrollSpeed:20,snap:!1,snapMode:"both",snapTolerance:20,stack:!1,zIndex:!1,drag:null,start:null,stop:null},_create:function(){"original"===this.options.helper&&this._setPositionRelative(),this.options.addClasses&&this.element.addClass("ui-draggable"),this.options.disabled&&this.element.addClass("ui-draggable-disabled"),this._setHandleClassName(),this._mouseInit()},_setOption:function(t,e){this._super(t,e),"handle"===t&&(this._removeHandleClassName(),this._setHandleClassName())},_destroy:function(){(this.helper||this.element).is(".ui-draggable-dragging")?this.destroyOnClear=!0:(this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"),this._removeHandleClassName(),this._mouseDestroy())},_mouseCapture:function(t){var e=this.options;return this._blurActiveElement(t),!(this.helper||e.disabled||0<P(t.target).closest(".ui-resizable-handle").length)&&(this.handle=this._getHandle(t),!!this.handle&&(this._blockFrames(!0===e.iframeFix?"iframe":e.iframeFix),!0))},_blockFrames:function(t){this.iframeBlocks=this.document.find(t).map(function(){var t=P(this);return P("<div>").css("position","absolute").appendTo(t.parent()).outerWidth(t.outerWidth()).outerHeight(t.outerHeight()).offset(t.offset())[0]})},_unblockFrames:function(){this.iframeBlocks&&(this.iframeBlocks.remove(),delete this.iframeBlocks)},_blurActiveElement:function(t){var e=this.document[0];if(this.handleElement.is(t.target))try{e.activeElement&&"body"!==e.activeElement.nodeName.toLowerCase()&&P(e.activeElement).blur()}catch(t){}},_mouseStart:function(t){var e=this.options;return this.helper=this._createHelper(t),this.helper.addClass("ui-draggable-dragging"),this._cacheHelperProportions(),P.ui.ddmanager&&(P.ui.ddmanager.current=this),this._cacheMargins(),this.cssPosition=this.helper.css("position"),this.scrollParent=this.helper.scrollParent(!0),this.offsetParent=this.helper.offsetParent(),this.hasFixedAncestor=0<this.helper.parents().filter(function(){return"fixed"===P(this).css("position")}).length,this.positionAbs=this.element.offset(),this._refreshOffsets(t),this.originalPosition=this.position=this._generatePosition(t,!1),this.originalPageX=t.pageX,this.originalPageY=t.pageY,e.cursorAt&&this._adjustOffsetFromHelper(e.cursorAt),this._setContainment(),!1===this._trigger("start",t)?(this._clear(),!1):(this._cacheHelperProportions(),P.ui.ddmanager&&!e.dropBehaviour&&P.ui.ddmanager.prepareOffsets(this,t),this._normalizeRightBottom(),this._mouseDrag(t,!0),P.ui.ddmanager&&P.ui.ddmanager.dragStart(this,t),!0)},_refreshOffsets:function(t){this.offset={top:this.positionAbs.top-this.margins.top,left:this.positionAbs.left-this.margins.left,scroll:!1,parent:this._getParentOffset(),relative:this._getRelativeOffset()},this.offset.click={left:t.pageX-this.offset.left,top:t.pageY-this.offset.top}},_mouseDrag:function(t,e){if(this.hasFixedAncestor&&(this.offset.parent=this._getParentOffset()),this.position=this._generatePosition(t,!0),this.positionAbs=this._convertPositionTo("absolute"),!e){var i=this._uiHash();if(!1===this._trigger("drag",t,i))return this._mouseUp({}),!1;this.position=i.position}return this.helper[0].style.left=this.position.left+"px",this.helper[0].style.top=this.position.top+"px",P.ui.ddmanager&&P.ui.ddmanager.drag(this,t),!1},_mouseStop:function(t){var e=this,i=!1;return P.ui.ddmanager&&!this.options.dropBehaviour&&(i=P.ui.ddmanager.drop(this,t)),this.dropped&&(i=this.dropped,this.dropped=!1),"invalid"===this.options.revert&&!i||"valid"===this.options.revert&&i||!0===this.options.revert||P.isFunction(this.options.revert)&&this.options.revert.call(this.element,i)?P(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){!1!==e._trigger("stop",t)&&e._clear()}):!1!==this._trigger("stop",t)&&this._clear(),!1},_mouseUp:function(t){return this._unblockFrames(),P.ui.ddmanager&&P.ui.ddmanager.dragStop(this,t),this.handleElement.is(t.target)&&this.element.focus(),P.ui.mouse.prototype._mouseUp.call(this,t)},cancel:function(){return this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear(),this},_getHandle:function(t){return!this.options.handle||!!P(t.target).closest(this.element.find(this.options.handle)).length},_setHandleClassName:function(){this.handleElement=this.options.handle?this.element.find(this.options.handle):this.element,this.handleElement.addClass("ui-draggable-handle")},_removeHandleClassName:function(){this.handleElement.removeClass("ui-draggable-handle")},_createHelper:function(t){var e=this.options,i=P.isFunction(e.helper),s=i?P(e.helper.apply(this.element[0],[t])):"clone"===e.helper?this.element.clone().removeAttr("id"):this.element;return s.parents("body").length||s.appendTo("parent"===e.appendTo?this.element[0].parentNode:e.appendTo),i&&s[0]===this.element[0]&&this._setPositionRelative(),s[0]===this.element[0]||/(fixed|absolute)/.test(s.css("position"))||s.css("position","absolute"),s},_setPositionRelative:function(){/^(?:r|a|f)/.test(this.element.css("position"))||(this.element[0].style.position="relative")},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),P.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_isRootNode:function(t){return/(html|body)/i.test(t.tagName)||t===this.document[0]},_getParentOffset:function(){var t=this.offsetParent.offset(),e=this.document[0];return"absolute"===this.cssPosition&&this.scrollParent[0]!==e&&P.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),this._isRootNode(this.offsetParent[0])&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"!==this.cssPosition)return{top:0,left:0};var t=this.element.position(),e=this._isRootNode(this.scrollParent[0]);return{top:t.top-(parseInt(this.helper.css("top"),10)||0)+(e?0:this.scrollParent.scrollTop()),left:t.left-(parseInt(this.helper.css("left"),10)||0)+(e?0:this.scrollParent.scrollLeft())}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,e,i,s=this.options,n=this.document[0];this.relativeContainer=null,s.containment?"window"!==s.containment?"document"!==s.containment?s.containment.constructor!==Array?("parent"===s.containment&&(s.containment=this.helper[0].parentNode),(i=(e=P(s.containment))[0])&&(t=/(scroll|auto)/.test(e.css("overflow")),this.containment=[(parseInt(e.css("borderLeftWidth"),10)||0)+(parseInt(e.css("paddingLeft"),10)||0),(parseInt(e.css("borderTopWidth"),10)||0)+(parseInt(e.css("paddingTop"),10)||0),(t?Math.max(i.scrollWidth,i.offsetWidth):i.offsetWidth)-(parseInt(e.css("borderRightWidth"),10)||0)-(parseInt(e.css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(t?Math.max(i.scrollHeight,i.offsetHeight):i.offsetHeight)-(parseInt(e.css("borderBottomWidth"),10)||0)-(parseInt(e.css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom],this.relativeContainer=e)):this.containment=s.containment:this.containment=[0,0,P(n).width()-this.helperProportions.width-this.margins.left,(P(n).height()||n.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]:this.containment=[P(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,P(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,P(window).scrollLeft()+P(window).width()-this.helperProportions.width-this.margins.left,P(window).scrollTop()+(P(window).height()||n.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]:this.containment=null},_convertPositionTo:function(t,e){e||(e=this.position);var i="absolute"===t?1:-1,s=this._isRootNode(this.scrollParent[0]);return{top:e.top+this.offset.relative.top*i+this.offset.parent.top*i-("fixed"===this.cssPosition?-this.offset.scroll.top:s?0:this.offset.scroll.top)*i,left:e.left+this.offset.relative.left*i+this.offset.parent.left*i-("fixed"===this.cssPosition?-this.offset.scroll.left:s?0:this.offset.scroll.left)*i}},_generatePosition:function(t,e){var i,s,n,o,a=this.options,r=this._isRootNode(this.scrollParent[0]),h=t.pageX,l=t.pageY;return r&&this.offset.scroll||(this.offset.scroll={top:this.scrollParent.scrollTop(),left:this.scrollParent.scrollLeft()}),e&&(this.containment&&(i=this.relativeContainer?(s=this.relativeContainer.offset(),[this.containment[0]+s.left,this.containment[1]+s.top,this.containment[2]+s.left,this.containment[3]+s.top]):this.containment,t.pageX-this.offset.click.left<i[0]&&(h=i[0]+this.offset.click.left),t.pageY-this.offset.click.top<i[1]&&(l=i[1]+this.offset.click.top),t.pageX-this.offset.click.left>i[2]&&(h=i[2]+this.offset.click.left),t.pageY-this.offset.click.top>i[3]&&(l=i[3]+this.offset.click.top)),a.grid&&(n=a.grid[1]?this.originalPageY+Math.round((l-this.originalPageY)/a.grid[1])*a.grid[1]:this.originalPageY,l=i?n-this.offset.click.top>=i[1]||n-this.offset.click.top>i[3]?n:n-this.offset.click.top>=i[1]?n-a.grid[1]:n+a.grid[1]:n,o=a.grid[0]?this.originalPageX+Math.round((h-this.originalPageX)/a.grid[0])*a.grid[0]:this.originalPageX,h=i?o-this.offset.click.left>=i[0]||o-this.offset.click.left>i[2]?o:o-this.offset.click.left>=i[0]?o-a.grid[0]:o+a.grid[0]:o),"y"===a.axis&&(h=this.originalPageX),"x"===a.axis&&(l=this.originalPageY)),{top:l-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.offset.scroll.top:r?0:this.offset.scroll.top),left:h-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.offset.scroll.left:r?0:this.offset.scroll.left)}},_clear:function(){this.helper.removeClass("ui-draggable-dragging"),this.helper[0]===this.element[0]||this.cancelHelperRemoval||this.helper.remove(),this.helper=null,this.cancelHelperRemoval=!1,this.destroyOnClear&&this.destroy()},_normalizeRightBottom:function(){"y"!==this.options.axis&&"auto"!==this.helper.css("right")&&(this.helper.width(this.helper.width()),this.helper.css("right","auto")),"x"!==this.options.axis&&"auto"!==this.helper.css("bottom")&&(this.helper.height(this.helper.height()),this.helper.css("bottom","auto"))},_trigger:function(t,e,i){return i=i||this._uiHash(),P.ui.plugin.call(this,t,[e,i,this],!0),/^(drag|start|stop)/.test(t)&&(this.positionAbs=this._convertPositionTo("absolute"),i.offset=this.positionAbs),P.Widget.prototype._trigger.call(this,t,e,i)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}}),P.ui.plugin.add("draggable","connectToSortable",{start:function(e,t,i){var s=P.extend({},t,{item:i.element});i.sortables=[],P(i.options.connectToSortable).each(function(){var t=P(this).sortable("instance");t&&!t.options.disabled&&(i.sortables.push(t),t.refreshPositions(),t._trigger("activate",e,s))})},stop:function(e,t,i){var s=P.extend({},t,{item:i.element});i.cancelHelperRemoval=!1,P.each(i.sortables,function(){var t=this;t.isOver?(t.isOver=0,i.cancelHelperRemoval=!0,t.cancelHelperRemoval=!1,t._storedCSS={position:t.placeholder.css("position"),top:t.placeholder.css("top"),left:t.placeholder.css("left")},t._mouseStop(e),t.options.helper=t.options._helper):(t.cancelHelperRemoval=!0,t._trigger("deactivate",e,s))})},drag:function(i,s,n){P.each(n.sortables,function(){var t=!1,e=this;e.positionAbs=n.positionAbs,e.helperProportions=n.helperProportions,e.offset.click=n.offset.click,e._intersectsWith(e.containerCache)&&(t=!0,P.each(n.sortables,function(){return this.positionAbs=n.positionAbs,this.helperProportions=n.helperProportions,this.offset.click=n.offset.click,this!==e&&this._intersectsWith(this.containerCache)&&P.contains(e.element[0],this.element[0])&&(t=!1),t})),t?(e.isOver||(e.isOver=1,e.currentItem=s.helper.appendTo(e.element).data("ui-sortable-item",!0),e.options._helper=e.options.helper,e.options.helper=function(){return s.helper[0]},i.target=e.currentItem[0],e._mouseCapture(i,!0),e._mouseStart(i,!0,!0),e.offset.click.top=n.offset.click.top,e.offset.click.left=n.offset.click.left,e.offset.parent.left-=n.offset.parent.left-e.offset.parent.left,e.offset.parent.top-=n.offset.parent.top-e.offset.parent.top,n._trigger("toSortable",i),n.dropped=e.element,P.each(n.sortables,function(){this.refreshPositions()}),n.currentItem=n.element,e.fromOutside=n),e.currentItem&&(e._mouseDrag(i),s.position=e.position)):e.isOver&&(e.isOver=0,e.cancelHelperRemoval=!0,e.options._revert=e.options.revert,e.options.revert=!1,e._trigger("out",i,e._uiHash(e)),e._mouseStop(i,!0),e.options.revert=e.options._revert,e.options.helper=e.options._helper,e.placeholder&&e.placeholder.remove(),n._refreshOffsets(i),s.position=n._generatePosition(i,!0),n._trigger("fromSortable",i),n.dropped=!1,P.each(n.sortables,function(){this.refreshPositions()}))})}}),P.ui.plugin.add("draggable","cursor",{start:function(t,e,i){var s=P("body"),n=i.options;s.css("cursor")&&(n._cursor=s.css("cursor")),s.css("cursor",n.cursor)},stop:function(t,e,i){var s=i.options;s._cursor&&P("body").css("cursor",s._cursor)}}),P.ui.plugin.add("draggable","opacity",{start:function(t,e,i){var s=P(e.helper),n=i.options;s.css("opacity")&&(n._opacity=s.css("opacity")),s.css("opacity",n.opacity)},stop:function(t,e,i){var s=i.options;s._opacity&&P(e.helper).css("opacity",s._opacity)}}),P.ui.plugin.add("draggable","scroll",{start:function(t,e,i){i.scrollParentNotHidden||(i.scrollParentNotHidden=i.helper.scrollParent(!1)),i.scrollParentNotHidden[0]!==i.document[0]&&"HTML"!==i.scrollParentNotHidden[0].tagName&&(i.overflowOffset=i.scrollParentNotHidden.offset())},drag:function(t,e,i){var s=i.options,n=!1,o=i.scrollParentNotHidden[0],a=i.document[0];o!==a&&"HTML"!==o.tagName?(s.axis&&"x"===s.axis||(i.overflowOffset.top+o.offsetHeight-t.pageY<s.scrollSensitivity?o.scrollTop=n=o.scrollTop+s.scrollSpeed:t.pageY-i.overflowOffset.top<s.scrollSensitivity&&(o.scrollTop=n=o.scrollTop-s.scrollSpeed)),s.axis&&"y"===s.axis||(i.overflowOffset.left+o.offsetWidth-t.pageX<s.scrollSensitivity?o.scrollLeft=n=o.scrollLeft+s.scrollSpeed:t.pageX-i.overflowOffset.left<s.scrollSensitivity&&(o.scrollLeft=n=o.scrollLeft-s.scrollSpeed))):(s.axis&&"x"===s.axis||(t.pageY-P(a).scrollTop()<s.scrollSensitivity?n=P(a).scrollTop(P(a).scrollTop()-s.scrollSpeed):P(window).height()-(t.pageY-P(a).scrollTop())<s.scrollSensitivity&&(n=P(a).scrollTop(P(a).scrollTop()+s.scrollSpeed))),s.axis&&"y"===s.axis||(t.pageX-P(a).scrollLeft()<s.scrollSensitivity?n=P(a).scrollLeft(P(a).scrollLeft()-s.scrollSpeed):P(window).width()-(t.pageX-P(a).scrollLeft())<s.scrollSensitivity&&(n=P(a).scrollLeft(P(a).scrollLeft()+s.scrollSpeed)))),!1!==n&&P.ui.ddmanager&&!s.dropBehaviour&&P.ui.ddmanager.prepareOffsets(i,t)}}),P.ui.plugin.add("draggable","snap",{start:function(t,e,i){var s=i.options;i.snapElements=[],P(s.snap.constructor!==String?s.snap.items||":data(ui-draggable)":s.snap).each(function(){var t=P(this),e=t.offset();this!==i.element[0]&&i.snapElements.push({item:this,width:t.outerWidth(),height:t.outerHeight(),top:e.top,left:e.left})})},drag:function(t,e,i){var s,n,o,a,r,h,l,c,u,d,p=i.options,f=p.snapTolerance,m=e.offset.left,g=m+i.helperProportions.width,v=e.offset.top,_=v+i.helperProportions.height;for(u=i.snapElements.length-1;0<=u;u--)h=(r=i.snapElements[u].left-i.margins.left)+i.snapElements[u].width,c=(l=i.snapElements[u].top-i.margins.top)+i.snapElements[u].height,g<r-f||h+f<m||_<l-f||c+f<v||!P.contains(i.snapElements[u].item.ownerDocument,i.snapElements[u].item)?(i.snapElements[u].snapping&&i.options.snap.release&&i.options.snap.release.call(i.element,t,P.extend(i._uiHash(),{snapItem:i.snapElements[u].item})),i.snapElements[u].snapping=!1):("inner"!==p.snapMode&&(s=Math.abs(l-_)<=f,n=Math.abs(c-v)<=f,o=Math.abs(r-g)<=f,a=Math.abs(h-m)<=f,s&&(e.position.top=i._convertPositionTo("relative",{top:l-i.helperProportions.height,left:0}).top),n&&(e.position.top=i._convertPositionTo("relative",{top:c,left:0}).top),o&&(e.position.left=i._convertPositionTo("relative",{top:0,left:r-i.helperProportions.width}).left),a&&(e.position.left=i._convertPositionTo("relative",{top:0,left:h}).left)),d=s||n||o||a,"outer"!==p.snapMode&&(s=Math.abs(l-v)<=f,n=Math.abs(c-_)<=f,o=Math.abs(r-m)<=f,a=Math.abs(h-g)<=f,s&&(e.position.top=i._convertPositionTo("relative",{top:l,left:0}).top),n&&(e.position.top=i._convertPositionTo("relative",{top:c-i.helperProportions.height,left:0}).top),o&&(e.position.left=i._convertPositionTo("relative",{top:0,left:r}).left),a&&(e.position.left=i._convertPositionTo("relative",{top:0,left:h-i.helperProportions.width}).left)),!i.snapElements[u].snapping&&(s||n||o||a||d)&&i.options.snap.snap&&i.options.snap.snap.call(i.element,t,P.extend(i._uiHash(),{snapItem:i.snapElements[u].item})),i.snapElements[u].snapping=s||n||o||a||d)}}),P.ui.plugin.add("draggable","stack",{start:function(t,e,i){var s,n=i.options,o=P.makeArray(P(n.stack)).sort(function(t,e){return(parseInt(P(t).css("zIndex"),10)||0)-(parseInt(P(e).css("zIndex"),10)||0)});o.length&&(s=parseInt(P(o[0]).css("zIndex"),10)||0,P(o).each(function(t){P(this).css("zIndex",s+t)}),this.css("zIndex",s+o.length))}}),P.ui.plugin.add("draggable","zIndex",{start:function(t,e,i){var s=P(e.helper),n=i.options;s.css("zIndex")&&(n._zIndex=s.css("zIndex")),s.css("zIndex",n.zIndex)},stop:function(t,e,i){var s=i.options;s._zIndex&&P(e.helper).css("zIndex",s._zIndex)}});P.ui.draggable;P.widget("ui.droppable",{version:"1.11.2",widgetEventPrefix:"drop",options:{accept:"*",activeClass:!1,addClasses:!0,greedy:!1,hoverClass:!1,scope:"default",tolerance:"intersect",activate:null,deactivate:null,drop:null,out:null,over:null},_create:function(){var t,e=this.options,i=e.accept;this.isover=!1,this.isout=!0,this.accept=P.isFunction(i)?i:function(t){return t.is(i)},this.proportions=function(){if(!arguments.length)return t||(t={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight});t=arguments[0]},this._addToManager(e.scope),e.addClasses&&this.element.addClass("ui-droppable")},_addToManager:function(t){P.ui.ddmanager.droppables[t]=P.ui.ddmanager.droppables[t]||[],P.ui.ddmanager.droppables[t].push(this)},_splice:function(t){for(var e=0;e<t.length;e++)t[e]===this&&t.splice(e,1)},_destroy:function(){var t=P.ui.ddmanager.droppables[this.options.scope];this._splice(t),this.element.removeClass("ui-droppable ui-droppable-disabled")},_setOption:function(t,e){if("accept"===t)this.accept=P.isFunction(e)?e:function(t){return t.is(e)};else if("scope"===t){var i=P.ui.ddmanager.droppables[this.options.scope];this._splice(i),this._addToManager(e)}this._super(t,e)},_activate:function(t){var e=P.ui.ddmanager.current;this.options.activeClass&&this.element.addClass(this.options.activeClass),e&&this._trigger("activate",t,this.ui(e))},_deactivate:function(t){var e=P.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass),e&&this._trigger("deactivate",t,this.ui(e))},_over:function(t){var e=P.ui.ddmanager.current;e&&(e.currentItem||e.element)[0]!==this.element[0]&&this.accept.call(this.element[0],e.currentItem||e.element)&&(this.options.hoverClass&&this.element.addClass(this.options.hoverClass),this._trigger("over",t,this.ui(e)))},_out:function(t){var e=P.ui.ddmanager.current;e&&(e.currentItem||e.element)[0]!==this.element[0]&&this.accept.call(this.element[0],e.currentItem||e.element)&&(this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("out",t,this.ui(e)))},_drop:function(e,t){var i=t||P.ui.ddmanager.current,s=!1;return!(!i||(i.currentItem||i.element)[0]===this.element[0])&&(this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function(){var t=P(this).droppable("instance");if(t.options.greedy&&!t.options.disabled&&t.options.scope===i.options.scope&&t.accept.call(t.element[0],i.currentItem||i.element)&&P.ui.intersect(i,P.extend(t,{offset:t.element.offset()}),t.options.tolerance,e))return!(s=!0)}),!s&&(!!this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.activeClass&&this.element.removeClass(this.options.activeClass),this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("drop",e,this.ui(i)),this.element)))},ui:function(t){return{draggable:t.currentItem||t.element,helper:t.helper,position:t.position,offset:t.positionAbs}}}),P.ui.intersect=function(){function d(t,e,i){return e<=t&&t<e+i}return function(t,e,i,s){if(!e.offset)return!1;var n=(t.positionAbs||t.position.absolute).left+t.margins.left,o=(t.positionAbs||t.position.absolute).top+t.margins.top,a=n+t.helperProportions.width,r=o+t.helperProportions.height,h=e.offset.left,l=e.offset.top,c=h+e.proportions().width,u=l+e.proportions().height;switch(i){case"fit":return h<=n&&a<=c&&l<=o&&r<=u;case"intersect":return h<n+t.helperProportions.width/2&&a-t.helperProportions.width/2<c&&l<o+t.helperProportions.height/2&&r-t.helperProportions.height/2<u;case"pointer":return d(s.pageY,l,e.proportions().height)&&d(s.pageX,h,e.proportions().width);case"touch":return(l<=o&&o<=u||l<=r&&r<=u||o<l&&u<r)&&(h<=n&&n<=c||h<=a&&a<=c||n<h&&c<a);default:return!1}}}(),P.ui.ddmanager={current:null,droppables:{default:[]},prepareOffsets:function(t,e){var i,s,n=P.ui.ddmanager.droppables[t.options.scope]||[],o=e?e.type:null,a=(t.currentItem||t.element).find(":data(ui-droppable)").addBack();t:for(i=0;i<n.length;i++)if(!(n[i].options.disabled||t&&!n[i].accept.call(n[i].element[0],t.currentItem||t.element))){for(s=0;s<a.length;s++)if(a[s]===n[i].element[0]){n[i].proportions().height=0;continue t}n[i].visible="none"!==n[i].element.css("display"),n[i].visible&&("mousedown"===o&&n[i]._activate.call(n[i],e),n[i].offset=n[i].element.offset(),n[i].proportions({width:n[i].element[0].offsetWidth,height:n[i].element[0].offsetHeight}))}},drop:function(t,e){var i=!1;return P.each((P.ui.ddmanager.droppables[t.options.scope]||[]).slice(),function(){this.options&&(!this.options.disabled&&this.visible&&P.ui.intersect(t,this,this.options.tolerance,e)&&(i=this._drop.call(this,e)||i),!this.options.disabled&&this.visible&&this.accept.call(this.element[0],t.currentItem||t.element)&&(this.isout=!0,this.isover=!1,this._deactivate.call(this,e)))}),i},dragStart:function(t,e){t.element.parentsUntil("body").bind("scroll.droppable",function(){t.options.refreshPositions||P.ui.ddmanager.prepareOffsets(t,e)})},drag:function(o,a){o.options.refreshPositions&&P.ui.ddmanager.prepareOffsets(o,a),P.each(P.ui.ddmanager.droppables[o.options.scope]||[],function(){if(!this.options.disabled&&!this.greedyChild&&this.visible){var t,e,i,s=P.ui.intersect(o,this,this.options.tolerance,a),n=!s&&this.isover?"isout":s&&!this.isover?"isover":null;n&&(this.options.greedy&&(e=this.options.scope,(i=this.element.parents(":data(ui-droppable)").filter(function(){return P(this).droppable("instance").options.scope===e})).length&&((t=P(i[0]).droppable("instance")).greedyChild="isover"===n)),t&&"isover"===n&&(t.isover=!1,t.isout=!0,t._out.call(t,a)),this[n]=!0,this["isout"===n?"isover":"isout"]=!1,this["isover"===n?"_over":"_out"].call(this,a),t&&"isout"===n&&(t.isout=!1,t.isover=!0,t._over.call(t,a)))}})},dragStop:function(t,e){t.element.parentsUntil("body").unbind("scroll.droppable"),t.options.refreshPositions||P.ui.ddmanager.prepareOffsets(t,e)}};P.ui.droppable;P.widget("ui.resizable",P.ui.mouse,{version:"1.11.2",widgetEventPrefix:"resize",options:{alsoResize:!1,animate:!1,animateDuration:"slow",animateEasing:"swing",aspectRatio:!1,autoHide:!1,containment:!1,ghost:!1,grid:!1,handles:"e,s,se",helper:!1,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:90,resize:null,start:null,stop:null},_num:function(t){return parseInt(t,10)||0},_isNumber:function(t){return!isNaN(parseInt(t,10))},_hasScroll:function(t,e){if("hidden"===P(t).css("overflow"))return!1;var i,s=e&&"left"===e?"scrollLeft":"scrollTop";return 0<t[s]||(t[s]=1,i=0<t[s],t[s]=0,i)},_create:function(){var t,e,i,s,n=this,o=this.options;if(this.element.addClass("ui-resizable"),P.extend(this,{_aspectRatio:!!o.aspectRatio,aspectRatio:o.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:o.helper||o.ghost||o.animate?o.helper||"ui-resizable-helper":null}),this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)&&(this.element.wrap(P("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")})),this.element=this.element.parent().data("ui-resizable",this.element.resizable("instance")),this.elementIsWrapper=!0,this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")}),this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0}),this.originalResizeStyle=this.originalElement.css("resize"),this.originalElement.css("resize","none"),this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"})),this.originalElement.css({margin:this.originalElement.css("margin")}),this._proportionallyResize()),this.handles=o.handles||(P(".ui-resizable-handle",this.element).length?{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"}:"e,s,se"),this.handles.constructor===String)for("all"===this.handles&&(this.handles="n,e,s,w,se,sw,ne,nw"),t=this.handles.split(","),this.handles={},e=0;e<t.length;e++)i=P.trim(t[e]),(s=P("<div class='ui-resizable-handle "+("ui-resizable-"+i)+"'></div>")).css({zIndex:o.zIndex}),"se"===i&&s.addClass("ui-icon ui-icon-gripsmall-diagonal-se"),this.handles[i]=".ui-resizable-"+i,this.element.append(s);this._renderAxis=function(t){var e,i,s,n;for(e in t=t||this.element,this.handles)this.handles[e].constructor===String&&(this.handles[e]=this.element.children(this.handles[e]).first().show()),this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)&&(i=P(this.handles[e],this.element),n=/sw|ne|nw|se|n|s/.test(e)?i.outerHeight():i.outerWidth(),s=["padding",/ne|nw|n/.test(e)?"Top":/se|sw|s/.test(e)?"Bottom":/^e$/.test(e)?"Right":"Left"].join(""),t.css(s,n),this._proportionallyResize()),P(this.handles[e]).length},this._renderAxis(this.element),this._handles=P(".ui-resizable-handle",this.element).disableSelection(),this._handles.mouseover(function(){n.resizing||(this.className&&(s=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)),n.axis=s&&s[1]?s[1]:"se")}),o.autoHide&&(this._handles.hide(),P(this.element).addClass("ui-resizable-autohide").mouseenter(function(){o.disabled||(P(this).removeClass("ui-resizable-autohide"),n._handles.show())}).mouseleave(function(){o.disabled||n.resizing||(P(this).addClass("ui-resizable-autohide"),n._handles.hide())})),this._mouseInit()},_destroy:function(){this._mouseDestroy();var t,e=function(t){P(t).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()};return this.elementIsWrapper&&(e(this.element),t=this.element,this.originalElement.css({position:t.css("position"),width:t.outerWidth(),height:t.outerHeight(),top:t.css("top"),left:t.css("left")}).insertAfter(t),t.remove()),this.originalElement.css("resize",this.originalResizeStyle),e(this.originalElement),this},_mouseCapture:function(t){var e,i,s=!1;for(e in this.handles)((i=P(this.handles[e])[0])===t.target||P.contains(i,t.target))&&(s=!0);return!this.options.disabled&&s},_mouseStart:function(t){var e,i,s,n=this.options,o=this.element;return this.resizing=!0,this._renderProxy(),e=this._num(this.helper.css("left")),i=this._num(this.helper.css("top")),n.containment&&(e+=P(n.containment).scrollLeft()||0,i+=P(n.containment).scrollTop()||0),this.offset=this.helper.offset(),this.position={left:e,top:i},this.size=this._helper?{width:this.helper.width(),height:this.helper.height()}:{width:o.width(),height:o.height()},this.originalSize=this._helper?{width:o.outerWidth(),height:o.outerHeight()}:{width:o.width(),height:o.height()},this.sizeDiff={width:o.outerWidth()-o.width(),height:o.outerHeight()-o.height()},this.originalPosition={left:e,top:i},this.originalMousePosition={left:t.pageX,top:t.pageY},this.aspectRatio="number"==typeof n.aspectRatio?n.aspectRatio:this.originalSize.width/this.originalSize.height||1,s=P(".ui-resizable-"+this.axis).css("cursor"),P("body").css("cursor","auto"===s?this.axis+"-resize":s),o.addClass("ui-resizable-resizing"),this._propagate("start",t),!0},_mouseDrag:function(t){var e,i,s=this.originalMousePosition,n=this.axis,o=t.pageX-s.left||0,a=t.pageY-s.top||0,r=this._change[n];return this._updatePrevProperties(),r&&(e=r.apply(this,[t,o,a]),this._updateVirtualBoundaries(t.shiftKey),(this._aspectRatio||t.shiftKey)&&(e=this._updateRatio(e,t)),e=this._respectSize(e,t),this._updateCache(e),this._propagate("resize",t),i=this._applyChanges(),!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize(),P.isEmptyObject(i)||(this._updatePrevProperties(),this._trigger("resize",t,this.ui()),this._applyChanges())),!1},_mouseStop:function(t){this.resizing=!1;var e,i,s,n,o,a,r,h=this.options,l=this;return this._helper&&(s=(i=(e=this._proportionallyResizeElements).length&&/textarea/i.test(e[0].nodeName))&&this._hasScroll(e[0],"left")?0:l.sizeDiff.height,n=i?0:l.sizeDiff.width,o={width:l.helper.width()-n,height:l.helper.height()-s},a=parseInt(l.element.css("left"),10)+(l.position.left-l.originalPosition.left)||null,r=parseInt(l.element.css("top"),10)+(l.position.top-l.originalPosition.top)||null,h.animate||this.element.css(P.extend(o,{top:r,left:a})),l.helper.height(l.size.height),l.helper.width(l.size.width),this._helper&&!h.animate&&this._proportionallyResize()),P("body").css("cursor","auto"),this.element.removeClass("ui-resizable-resizing"),this._propagate("stop",t),this._helper&&this.helper.remove(),!1},_updatePrevProperties:function(){this.prevPosition={top:this.position.top,left:this.position.left},this.prevSize={width:this.size.width,height:this.size.height}},_applyChanges:function(){var t={};return this.position.top!==this.prevPosition.top&&(t.top=this.position.top+"px"),this.position.left!==this.prevPosition.left&&(t.left=this.position.left+"px"),this.size.width!==this.prevSize.width&&(t.width=this.size.width+"px"),this.size.height!==this.prevSize.height&&(t.height=this.size.height+"px"),this.helper.css(t),t},_updateVirtualBoundaries:function(t){var e,i,s,n,o,a=this.options;o={minWidth:this._isNumber(a.minWidth)?a.minWidth:0,maxWidth:this._isNumber(a.maxWidth)?a.maxWidth:1/0,minHeight:this._isNumber(a.minHeight)?a.minHeight:0,maxHeight:this._isNumber(a.maxHeight)?a.maxHeight:1/0},(this._aspectRatio||t)&&(e=o.minHeight*this.aspectRatio,s=o.minWidth/this.aspectRatio,i=o.maxHeight*this.aspectRatio,n=o.maxWidth/this.aspectRatio,e>o.minWidth&&(o.minWidth=e),s>o.minHeight&&(o.minHeight=s),i<o.maxWidth&&(o.maxWidth=i),n<o.maxHeight&&(o.maxHeight=n)),this._vBoundaries=o},_updateCache:function(t){this.offset=this.helper.offset(),this._isNumber(t.left)&&(this.position.left=t.left),this._isNumber(t.top)&&(this.position.top=t.top),this._isNumber(t.height)&&(this.size.height=t.height),this._isNumber(t.width)&&(this.size.width=t.width)},_updateRatio:function(t){var e=this.position,i=this.size,s=this.axis;return this._isNumber(t.height)?t.width=t.height*this.aspectRatio:this._isNumber(t.width)&&(t.height=t.width/this.aspectRatio),"sw"===s&&(t.left=e.left+(i.width-t.width),t.top=null),"nw"===s&&(t.top=e.top+(i.height-t.height),t.left=e.left+(i.width-t.width)),t},_respectSize:function(t){var e=this._vBoundaries,i=this.axis,s=this._isNumber(t.width)&&e.maxWidth&&e.maxWidth<t.width,n=this._isNumber(t.height)&&e.maxHeight&&e.maxHeight<t.height,o=this._isNumber(t.width)&&e.minWidth&&e.minWidth>t.width,a=this._isNumber(t.height)&&e.minHeight&&e.minHeight>t.height,r=this.originalPosition.left+this.originalSize.width,h=this.position.top+this.size.height,l=/sw|nw|w/.test(i),c=/nw|ne|n/.test(i);return o&&(t.width=e.minWidth),a&&(t.height=e.minHeight),s&&(t.width=e.maxWidth),n&&(t.height=e.maxHeight),o&&l&&(t.left=r-e.minWidth),s&&l&&(t.left=r-e.maxWidth),a&&c&&(t.top=h-e.minHeight),n&&c&&(t.top=h-e.maxHeight),t.width||t.height||t.left||!t.top?t.width||t.height||t.top||!t.left||(t.left=null):t.top=null,t},_getPaddingPlusBorderDimensions:function(t){for(var e=0,i=[],s=[t.css("borderTopWidth"),t.css("borderRightWidth"),t.css("borderBottomWidth"),t.css("borderLeftWidth")],n=[t.css("paddingTop"),t.css("paddingRight"),t.css("paddingBottom"),t.css("paddingLeft")];e<4;e++)i[e]=parseInt(s[e],10)||0,i[e]+=parseInt(n[e],10)||0;return{height:i[0]+i[2],width:i[1]+i[3]}},_proportionallyResize:function(){if(this._proportionallyResizeElements.length)for(var t,e=0,i=this.helper||this.element;e<this._proportionallyResizeElements.length;e++)t=this._proportionallyResizeElements[e],this.outerDimensions||(this.outerDimensions=this._getPaddingPlusBorderDimensions(t)),t.css({height:i.height()-this.outerDimensions.height||0,width:i.width()-this.outerDimensions.width||0})},_renderProxy:function(){var t=this.element,e=this.options;this.elementOffset=t.offset(),this._helper?(this.helper=this.helper||P("<div style='overflow:hidden;'></div>"),this.helper.addClass(this._helper).css({width:this.element.outerWidth()-1,height:this.element.outerHeight()-1,position:"absolute",left:this.elementOffset.left+"px",top:this.elementOffset.top+"px",zIndex:++e.zIndex}),this.helper.appendTo("body").disableSelection()):this.helper=this.element},_change:{e:function(t,e){return{width:this.originalSize.width+e}},w:function(t,e){var i=this.originalSize;return{left:this.originalPosition.left+e,width:i.width-e}},n:function(t,e,i){var s=this.originalSize;return{top:this.originalPosition.top+i,height:s.height-i}},s:function(t,e,i){return{height:this.originalSize.height+i}},se:function(t,e,i){return P.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[t,e,i]))},sw:function(t,e,i){return P.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[t,e,i]))},ne:function(t,e,i){return P.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[t,e,i]))},nw:function(t,e,i){return P.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[t,e,i]))}},_propagate:function(t,e){P.ui.plugin.call(this,t,[e,this.ui()]),"resize"!==t&&this._trigger(t,e,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}}}),P.ui.plugin.add("resizable","animate",{stop:function(e){var i=P(this).resizable("instance"),t=i.options,s=i._proportionallyResizeElements,n=s.length&&/textarea/i.test(s[0].nodeName),o=n&&i._hasScroll(s[0],"left")?0:i.sizeDiff.height,a=n?0:i.sizeDiff.width,r={width:i.size.width-a,height:i.size.height-o},h=parseInt(i.element.css("left"),10)+(i.position.left-i.originalPosition.left)||null,l=parseInt(i.element.css("top"),10)+(i.position.top-i.originalPosition.top)||null;i.element.animate(P.extend(r,l&&h?{top:l,left:h}:{}),{duration:t.animateDuration,easing:t.animateEasing,step:function(){var t={width:parseInt(i.element.css("width"),10),height:parseInt(i.element.css("height"),10),top:parseInt(i.element.css("top"),10),left:parseInt(i.element.css("left"),10)};s&&s.length&&P(s[0]).css({width:t.width,height:t.height}),i._updateCache(t),i._propagate("resize",e)}})}}),P.ui.plugin.add("resizable","containment",{start:function(){var i,s,t,e,n,o,a,r=P(this).resizable("instance"),h=r.options,l=r.element,c=h.containment,u=c instanceof P?c.get(0):/parent/.test(c)?l.parent().get(0):c;u&&(r.containerElement=P(u),/document/.test(c)||c===document?(r.containerOffset={left:0,top:0},r.containerPosition={left:0,top:0},r.parentData={element:P(document),left:0,top:0,width:P(document).width(),height:P(document).height()||document.body.parentNode.scrollHeight}):(i=P(u),s=[],P(["Top","Right","Left","Bottom"]).each(function(t,e){s[t]=r._num(i.css("padding"+e))}),r.containerOffset=i.offset(),r.containerPosition=i.position(),r.containerSize={height:i.innerHeight()-s[3],width:i.innerWidth()-s[1]},t=r.containerOffset,e=r.containerSize.height,n=r.containerSize.width,o=r._hasScroll(u,"left")?u.scrollWidth:n,a=r._hasScroll(u)?u.scrollHeight:e,r.parentData={element:u,left:t.left,top:t.top,width:o,height:a}))},resize:function(t){var e,i,s,n,o=P(this).resizable("instance"),a=o.options,r=o.containerOffset,h=o.position,l=o._aspectRatio||t.shiftKey,c={top:0,left:0},u=o.containerElement,d=!0;u[0]!==document&&/static/.test(u.css("position"))&&(c=r),h.left<(o._helper?r.left:0)&&(o.size.width=o.size.width+(o._helper?o.position.left-r.left:o.position.left-c.left),l&&(o.size.height=o.size.width/o.aspectRatio,d=!1),o.position.left=a.helper?r.left:0),h.top<(o._helper?r.top:0)&&(o.size.height=o.size.height+(o._helper?o.position.top-r.top:o.position.top),l&&(o.size.width=o.size.height*o.aspectRatio,d=!1),o.position.top=o._helper?r.top:0),s=o.containerElement.get(0)===o.element.parent().get(0),n=/relative|absolute/.test(o.containerElement.css("position")),o.offset.top=s&&n?(o.offset.left=o.parentData.left+o.position.left,o.parentData.top+o.position.top):(o.offset.left=o.element.offset().left,o.element.offset().top),e=Math.abs(o.sizeDiff.width+(o._helper?o.offset.left-c.left:o.offset.left-r.left)),i=Math.abs(o.sizeDiff.height+(o._helper?o.offset.top-c.top:o.offset.top-r.top)),e+o.size.width>=o.parentData.width&&(o.size.width=o.parentData.width-e,l&&(o.size.height=o.size.width/o.aspectRatio,d=!1)),i+o.size.height>=o.parentData.height&&(o.size.height=o.parentData.height-i,l&&(o.size.width=o.size.height*o.aspectRatio,d=!1)),d||(o.position.left=o.prevPosition.left,o.position.top=o.prevPosition.top,o.size.width=o.prevSize.width,o.size.height=o.prevSize.height)},stop:function(){var t=P(this).resizable("instance"),e=t.options,i=t.containerOffset,s=t.containerPosition,n=t.containerElement,o=P(t.helper),a=o.offset(),r=o.outerWidth()-t.sizeDiff.width,h=o.outerHeight()-t.sizeDiff.height;t._helper&&!e.animate&&/relative/.test(n.css("position"))&&P(this).css({left:a.left-s.left-i.left,width:r,height:h}),t._helper&&!e.animate&&/static/.test(n.css("position"))&&P(this).css({left:a.left-s.left-i.left,width:r,height:h})}}),P.ui.plugin.add("resizable","alsoResize",{start:function(){var t=P(this).resizable("instance").options,e=function(t){P(t).each(function(){var t=P(this);t.data("ui-resizable-alsoresize",{width:parseInt(t.width(),10),height:parseInt(t.height(),10),left:parseInt(t.css("left"),10),top:parseInt(t.css("top"),10)})})};"object"!=typeof t.alsoResize||t.alsoResize.parentNode?e(t.alsoResize):t.alsoResize.length?(t.alsoResize=t.alsoResize[0],e(t.alsoResize)):P.each(t.alsoResize,function(t){e(t)})},resize:function(t,o){var e=P(this).resizable("instance"),i=e.options,s=e.originalSize,n=e.originalPosition,a={height:e.size.height-s.height||0,width:e.size.width-s.width||0,top:e.position.top-n.top||0,left:e.position.left-n.left||0},r=function(t,i){P(t).each(function(){var t=P(this),s=P(this).data("ui-resizable-alsoresize"),n={},e=i&&i.length?i:t.parents(o.originalElement[0]).length?["width","height"]:["width","height","top","left"];P.each(e,function(t,e){var i=(s[e]||0)+(a[e]||0);i&&0<=i&&(n[e]=i||null)}),t.css(n)})};"object"!=typeof i.alsoResize||i.alsoResize.nodeType?r(i.alsoResize):P.each(i.alsoResize,function(t,e){r(t,e)})},stop:function(){P(this).removeData("resizable-alsoresize")}}),P.ui.plugin.add("resizable","ghost",{start:function(){var t=P(this).resizable("instance"),e=t.options,i=t.size;t.ghost=t.originalElement.clone(),t.ghost.css({opacity:.25,display:"block",position:"relative",height:i.height,width:i.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass("string"==typeof e.ghost?e.ghost:""),t.ghost.appendTo(t.helper)},resize:function(){var t=P(this).resizable("instance");t.ghost&&t.ghost.css({position:"relative",height:t.size.height,width:t.size.width})},stop:function(){var t=P(this).resizable("instance");t.ghost&&t.helper&&t.helper.get(0).removeChild(t.ghost.get(0))}}),P.ui.plugin.add("resizable","grid",{resize:function(){var t,e=P(this).resizable("instance"),i=e.options,s=e.size,n=e.originalSize,o=e.originalPosition,a=e.axis,r="number"==typeof i.grid?[i.grid,i.grid]:i.grid,h=r[0]||1,l=r[1]||1,c=Math.round((s.width-n.width)/h)*h,u=Math.round((s.height-n.height)/l)*l,d=n.width+c,p=n.height+u,f=i.maxWidth&&i.maxWidth<d,m=i.maxHeight&&i.maxHeight<p,g=i.minWidth&&i.minWidth>d,v=i.minHeight&&i.minHeight>p;i.grid=r,g&&(d+=h),v&&(p+=l),f&&(d-=h),m&&(p-=l),/^(se|s|e)$/.test(a)?(e.size.width=d,e.size.height=p):/^(ne)$/.test(a)?(e.size.width=d,e.size.height=p,e.position.top=o.top-u):/^(sw)$/.test(a)?(e.size.width=d,e.size.height=p,e.position.left=o.left-c):((p-l<=0||d-h<=0)&&(t=e._getPaddingPlusBorderDimensions(this)),e.position.top=0<p-l?(e.size.height=p,o.top-u):(p=l-t.height,e.size.height=p,o.top+n.height-p),e.position.left=0<d-h?(e.size.width=d,o.left-c):(d=l-t.height,e.size.width=d,o.left+n.width-d))}});P.ui.resizable,P.widget("ui.selectable",P.ui.mouse,{version:"1.11.2",options:{appendTo:"body",autoRefresh:!0,distance:0,filter:"*",tolerance:"touch",selected:null,selecting:null,start:null,stop:null,unselected:null,unselecting:null},_create:function(){var t,e=this;this.element.addClass("ui-selectable"),this.dragged=!1,this.refresh=function(){(t=P(e.options.filter,e.element[0])).addClass("ui-selectee"),t.each(function(){var t=P(this),e=t.offset();P.data(this,"selectable-item",{element:this,$element:t,left:e.left,top:e.top,right:e.left+t.outerWidth(),bottom:e.top+t.outerHeight(),startselected:!1,selected:t.hasClass("ui-selected"),selecting:t.hasClass("ui-selecting"),unselecting:t.hasClass("ui-unselecting")})})},this.refresh(),this.selectees=t.addClass("ui-selectee"),this._mouseInit(),this.helper=P("<div class='ui-selectable-helper'></div>")},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item"),this.element.removeClass("ui-selectable ui-selectable-disabled"),this._mouseDestroy()},_mouseStart:function(i){var s=this,t=this.options;this.opos=[i.pageX,i.pageY],this.options.disabled||(this.selectees=P(t.filter,this.element[0]),this._trigger("start",i),P(t.appendTo).append(this.helper),this.helper.css({left:i.pageX,top:i.pageY,width:0,height:0}),t.autoRefresh&&this.refresh(),this.selectees.filter(".ui-selected").each(function(){var t=P.data(this,"selectable-item");t.startselected=!0,i.metaKey||i.ctrlKey||(t.$element.removeClass("ui-selected"),t.selected=!1,t.$element.addClass("ui-unselecting"),t.unselecting=!0,s._trigger("unselecting",i,{unselecting:t.element}))}),P(i.target).parents().addBack().each(function(){var t,e=P.data(this,"selectable-item");if(e)return t=!i.metaKey&&!i.ctrlKey||!e.$element.hasClass("ui-selected"),e.$element.removeClass(t?"ui-unselecting":"ui-selected").addClass(t?"ui-selecting":"ui-unselecting"),e.unselecting=!t,e.selecting=t,(e.selected=t)?s._trigger("selecting",i,{selecting:e.element}):s._trigger("unselecting",i,{unselecting:e.element}),!1}))},_mouseDrag:function(i){if(this.dragged=!0,!this.options.disabled){var t,s=this,n=this.options,o=this.opos[0],a=this.opos[1],r=i.pageX,h=i.pageY;return r<o&&(t=r,r=o,o=t),h<a&&(t=h,h=a,a=t),this.helper.css({left:o,top:a,width:r-o,height:h-a}),this.selectees.each(function(){var t=P.data(this,"selectable-item"),e=!1;t&&t.element!==s.element[0]&&("touch"===n.tolerance?e=!(t.left>r||t.right<o||t.top>h||t.bottom<a):"fit"===n.tolerance&&(e=t.left>o&&t.right<r&&t.top>a&&t.bottom<h),e?(t.selected&&(t.$element.removeClass("ui-selected"),t.selected=!1),t.unselecting&&(t.$element.removeClass("ui-unselecting"),t.unselecting=!1),t.selecting||(t.$element.addClass("ui-selecting"),t.selecting=!0,s._trigger("selecting",i,{selecting:t.element}))):(t.selecting&&((i.metaKey||i.ctrlKey)&&t.startselected?(t.$element.removeClass("ui-selecting"),t.selecting=!1,t.$element.addClass("ui-selected"),t.selected=!0):(t.$element.removeClass("ui-selecting"),t.selecting=!1,t.startselected&&(t.$element.addClass("ui-unselecting"),t.unselecting=!0),s._trigger("unselecting",i,{unselecting:t.element}))),t.selected&&(i.metaKey||i.ctrlKey||t.startselected||(t.$element.removeClass("ui-selected"),t.selected=!1,t.$element.addClass("ui-unselecting"),t.unselecting=!0,s._trigger("unselecting",i,{unselecting:t.element})))))}),!1}},_mouseStop:function(e){var i=this;return this.dragged=!1,P(".ui-unselecting",this.element[0]).each(function(){var t=P.data(this,"selectable-item");t.$element.removeClass("ui-unselecting"),t.unselecting=!1,t.startselected=!1,i._trigger("unselected",e,{unselected:t.element})}),P(".ui-selecting",this.element[0]).each(function(){var t=P.data(this,"selectable-item");t.$element.removeClass("ui-selecting").addClass("ui-selected"),t.selecting=!1,t.selected=!0,t.startselected=!0,i._trigger("selected",e,{selected:t.element})}),this._trigger("stop",e),this.helper.remove(),!1}}),P.widget("ui.sortable",P.ui.mouse,{version:"1.11.2",widgetEventPrefix:"sort",ready:!1,options:{appendTo:"parent",axis:!1,connectWith:!1,containment:!1,cursor:"auto",cursorAt:!1,dropOnEmpty:!0,forcePlaceholderSize:!1,forceHelperSize:!1,grid:!1,handle:!1,helper:"original",items:"> *",opacity:!1,placeholder:!1,revert:!1,scroll:!0,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_isOverAxis:function(t,e,i){return e<=t&&t<e+i},_isFloating:function(t){return/left|right/.test(t.css("float"))||/inline|table-cell/.test(t.css("display"))},_create:function(){var t=this.options;this.containerCache={},this.element.addClass("ui-sortable"),this.refresh(),this.floating=!!this.items.length&&("x"===t.axis||this._isFloating(this.items[0].item)),this.offset=this.element.offset(),this._mouseInit(),this._setHandleClassName(),this.ready=!0},_setOption:function(t,e){this._super(t,e),"handle"===t&&this._setHandleClassName()},_setHandleClassName:function(){this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle"),P.each(this.items,function(){(this.instance.options.handle?this.item.find(this.instance.options.handle):this.item).addClass("ui-sortable-handle")})},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle"),this._mouseDestroy();for(var t=this.items.length-1;0<=t;t--)this.items[t].item.removeData(this.widgetName+"-item");return this},_mouseCapture:function(t,e){var i=null,s=!1,n=this;return!this.reverting&&(!this.options.disabled&&"static"!==this.options.type&&(this._refreshItems(t),P(t.target).parents().each(function(){if(P.data(this,n.widgetName+"-item")===n)return i=P(this),!1}),P.data(t.target,n.widgetName+"-item")===n&&(i=P(t.target)),!!i&&(!(this.options.handle&&!e&&(P(this.options.handle,i).find("*").addBack().each(function(){this===t.target&&(s=!0)}),!s))&&(this.currentItem=i,this._removeCurrentsFromItems(),!0))))},_mouseStart:function(t,e,i){var s,n,o=this.options;if((this.currentContainer=this).refreshPositions(),this.helper=this._createHelper(t),this._cacheHelperProportions(),this._cacheMargins(),this.scrollParent=this.helper.scrollParent(),this.offset=this.currentItem.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},P.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.helper.css("position","absolute"),this.cssPosition=this.helper.css("position"),this.originalPosition=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,o.cursorAt&&this._adjustOffsetFromHelper(o.cursorAt),this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]},this.helper[0]!==this.currentItem[0]&&this.currentItem.hide(),this._createPlaceholder(),o.containment&&this._setContainment(),o.cursor&&"auto"!==o.cursor&&(n=this.document.find("body"),this.storedCursor=n.css("cursor"),n.css("cursor",o.cursor),this.storedStylesheet=P("<style>*{ cursor: "+o.cursor+" !important; }</style>").appendTo(n)),o.opacity&&(this.helper.css("opacity")&&(this._storedOpacity=this.helper.css("opacity")),this.helper.css("opacity",o.opacity)),o.zIndex&&(this.helper.css("zIndex")&&(this._storedZIndex=this.helper.css("zIndex")),this.helper.css("zIndex",o.zIndex)),this.scrollParent[0]!==document&&"HTML"!==this.scrollParent[0].tagName&&(this.overflowOffset=this.scrollParent.offset()),this._trigger("start",t,this._uiHash()),this._preserveHelperProportions||this._cacheHelperProportions(),!i)for(s=this.containers.length-1;0<=s;s--)this.containers[s]._trigger("activate",t,this._uiHash(this));return P.ui.ddmanager&&(P.ui.ddmanager.current=this),P.ui.ddmanager&&!o.dropBehaviour&&P.ui.ddmanager.prepareOffsets(this,t),this.dragging=!0,this.helper.addClass("ui-sortable-helper"),this._mouseDrag(t),!0},_mouseDrag:function(t){var e,i,s,n,o=this.options,a=!1;for(this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute"),this.lastPositionAbs||(this.lastPositionAbs=this.positionAbs),this.options.scroll&&(this.scrollParent[0]!==document&&"HTML"!==this.scrollParent[0].tagName?(this.overflowOffset.top+this.scrollParent[0].offsetHeight-t.pageY<o.scrollSensitivity?this.scrollParent[0].scrollTop=a=this.scrollParent[0].scrollTop+o.scrollSpeed:t.pageY-this.overflowOffset.top<o.scrollSensitivity&&(this.scrollParent[0].scrollTop=a=this.scrollParent[0].scrollTop-o.scrollSpeed),this.overflowOffset.left+this.scrollParent[0].offsetWidth-t.pageX<o.scrollSensitivity?this.scrollParent[0].scrollLeft=a=this.scrollParent[0].scrollLeft+o.scrollSpeed:t.pageX-this.overflowOffset.left<o.scrollSensitivity&&(this.scrollParent[0].scrollLeft=a=this.scrollParent[0].scrollLeft-o.scrollSpeed)):(t.pageY-P(document).scrollTop()<o.scrollSensitivity?a=P(document).scrollTop(P(document).scrollTop()-o.scrollSpeed):P(window).height()-(t.pageY-P(document).scrollTop())<o.scrollSensitivity&&(a=P(document).scrollTop(P(document).scrollTop()+o.scrollSpeed)),t.pageX-P(document).scrollLeft()<o.scrollSensitivity?a=P(document).scrollLeft(P(document).scrollLeft()-o.scrollSpeed):P(window).width()-(t.pageX-P(document).scrollLeft())<o.scrollSensitivity&&(a=P(document).scrollLeft(P(document).scrollLeft()+o.scrollSpeed))),!1!==a&&P.ui.ddmanager&&!o.dropBehaviour&&P.ui.ddmanager.prepareOffsets(this,t)),this.positionAbs=this._convertPositionTo("absolute"),this.options.axis&&"y"===this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"===this.options.axis||(this.helper[0].style.top=this.position.top+"px"),e=this.items.length-1;0<=e;e--)if(s=(i=this.items[e]).item[0],(n=this._intersectsWithPointer(i))&&i.instance===this.currentContainer&&!(s===this.currentItem[0]||this.placeholder[1===n?"next":"prev"]()[0]===s||P.contains(this.placeholder[0],s)||"semi-dynamic"===this.options.type&&P.contains(this.element[0],s))){if(this.direction=1===n?"down":"up","pointer"!==this.options.tolerance&&!this._intersectsWithSides(i))break;this._rearrange(t,i),this._trigger("change",t,this._uiHash());break}return this._contactContainers(t),P.ui.ddmanager&&P.ui.ddmanager.drag(this,t),this._trigger("sort",t,this._uiHash()),this.lastPositionAbs=this.positionAbs,!1},_mouseStop:function(t,e){if(t){if(P.ui.ddmanager&&!this.options.dropBehaviour&&P.ui.ddmanager.drop(this,t),this.options.revert){var i=this,s=this.placeholder.offset(),n=this.options.axis,o={};n&&"x"!==n||(o.left=s.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollLeft)),n&&"y"!==n||(o.top=s.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollTop)),this.reverting=!0,P(this.helper).animate(o,parseInt(this.options.revert,10)||500,function(){i._clear(t)})}else this._clear(t,e);return!1}},cancel:function(){if(this.dragging){this._mouseUp({target:null}),"original"===this.options.helper?this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"):this.currentItem.show();for(var t=this.containers.length-1;0<=t;t--)this.containers[t]._trigger("deactivate",null,this._uiHash(this)),this.containers[t].containerCache.over&&(this.containers[t]._trigger("out",null,this._uiHash(this)),this.containers[t].containerCache.over=0)}return this.placeholder&&(this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]),"original"!==this.options.helper&&this.helper&&this.helper[0].parentNode&&this.helper.remove(),P.extend(this,{helper:null,dragging:!1,reverting:!1,_noFinalSort:null}),this.domPosition.prev?P(this.domPosition.prev).after(this.currentItem):P(this.domPosition.parent).prepend(this.currentItem)),this},serialize:function(e){var t=this._getItemsAsjQuery(e&&e.connected),i=[];return e=e||{},P(t).each(function(){var t=(P(e.item||this).attr(e.attribute||"id")||"").match(e.expression||/(.+)[\-=_](.+)/);t&&i.push((e.key||t[1]+"[]")+"="+(e.key&&e.expression?t[1]:t[2]))}),!i.length&&e.key&&i.push(e.key+"="),i.join("&")},toArray:function(t){var e=this._getItemsAsjQuery(t&&t.connected),i=[];return t=t||{},e.each(function(){i.push(P(t.item||this).attr(t.attribute||"id")||"")}),i},_intersectsWith:function(t){var e=this.positionAbs.left,i=e+this.helperProportions.width,s=this.positionAbs.top,n=s+this.helperProportions.height,o=t.left,a=o+t.width,r=t.top,h=r+t.height,l=this.offset.click.top,c=this.offset.click.left,u="x"===this.options.axis||r<s+l&&s+l<h,d="y"===this.options.axis||o<e+c&&e+c<a,p=u&&d;return"pointer"===this.options.tolerance||this.options.forcePointerForContainers||"pointer"!==this.options.tolerance&&this.helperProportions[this.floating?"width":"height"]>t[this.floating?"width":"height"]?p:o<e+this.helperProportions.width/2&&i-this.helperProportions.width/2<a&&r<s+this.helperProportions.height/2&&n-this.helperProportions.height/2<h},_intersectsWithPointer:function(t){var e="x"===this.options.axis||this._isOverAxis(this.positionAbs.top+this.offset.click.top,t.top,t.height),i="y"===this.options.axis||this._isOverAxis(this.positionAbs.left+this.offset.click.left,t.left,t.width),s=e&&i,n=this._getDragVerticalDirection(),o=this._getDragHorizontalDirection();return!!s&&(this.floating?o&&"right"===o||"down"===n?2:1:n&&("down"===n?2:1))},_intersectsWithSides:function(t){var e=this._isOverAxis(this.positionAbs.top+this.offset.click.top,t.top+t.height/2,t.height),i=this._isOverAxis(this.positionAbs.left+this.offset.click.left,t.left+t.width/2,t.width),s=this._getDragVerticalDirection(),n=this._getDragHorizontalDirection();return this.floating&&n?"right"===n&&i||"left"===n&&!i:s&&("down"===s&&e||"up"===s&&!e)},_getDragVerticalDirection:function(){var t=this.positionAbs.top-this.lastPositionAbs.top;return 0!==t&&(0<t?"down":"up")},_getDragHorizontalDirection:function(){var t=this.positionAbs.left-this.lastPositionAbs.left;return 0!==t&&(0<t?"right":"left")},refresh:function(t){return this._refreshItems(t),this._setHandleClassName(),this.refreshPositions(),this},_connectWith:function(){var t=this.options;return t.connectWith.constructor===String?[t.connectWith]:t.connectWith},_getItemsAsjQuery:function(t){var e,i,s,n,o=[],a=[],r=this._connectWith();if(r&&t)for(e=r.length-1;0<=e;e--)for(i=(s=P(r[e])).length-1;0<=i;i--)(n=P.data(s[i],this.widgetFullName))&&n!==this&&!n.options.disabled&&a.push([P.isFunction(n.options.items)?n.options.items.call(n.element):P(n.options.items,n.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),n]);function h(){o.push(this)}for(a.push([P.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):P(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]),e=a.length-1;0<=e;e--)a[e][0].each(h);return P(o)},_removeCurrentsFromItems:function(){var i=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=P.grep(this.items,function(t){for(var e=0;e<i.length;e++)if(i[e]===t.item[0])return!1;return!0})},_refreshItems:function(t){this.items=[],this.containers=[this];var e,i,s,n,o,a,r,h,l=this.items,c=[[P.isFunction(this.options.items)?this.options.items.call(this.element[0],t,{item:this.currentItem}):P(this.options.items,this.element),this]],u=this._connectWith();if(u&&this.ready)for(e=u.length-1;0<=e;e--)for(i=(s=P(u[e])).length-1;0<=i;i--)(n=P.data(s[i],this.widgetFullName))&&n!==this&&!n.options.disabled&&(c.push([P.isFunction(n.options.items)?n.options.items.call(n.element[0],t,{item:this.currentItem}):P(n.options.items,n.element),n]),this.containers.push(n));for(e=c.length-1;0<=e;e--)for(o=c[e][1],i=0,h=(a=c[e][0]).length;i<h;i++)(r=P(a[i])).data(this.widgetName+"-item",o),l.push({item:r,instance:o,width:0,height:0,left:0,top:0})},refreshPositions:function(t){var e,i,s,n;for(this.offsetParent&&this.helper&&(this.offset.parent=this._getParentOffset()),e=this.items.length-1;0<=e;e--)(i=this.items[e]).instance!==this.currentContainer&&this.currentContainer&&i.item[0]!==this.currentItem[0]||(s=this.options.toleranceElement?P(this.options.toleranceElement,i.item):i.item,t||(i.width=s.outerWidth(),i.height=s.outerHeight()),n=s.offset(),i.left=n.left,i.top=n.top);if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(e=this.containers.length-1;0<=e;e--)n=this.containers[e].element.offset(),this.containers[e].containerCache.left=n.left,this.containers[e].containerCache.top=n.top,this.containers[e].containerCache.width=this.containers[e].element.outerWidth(),this.containers[e].containerCache.height=this.containers[e].element.outerHeight();return this},_createPlaceholder:function(i){var s,n=(i=i||this).options;n.placeholder&&n.placeholder.constructor!==String||(s=n.placeholder,n.placeholder={element:function(){var t=i.currentItem[0].nodeName.toLowerCase(),e=P("<"+t+">",i.document[0]).addClass(s||i.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper");return"tr"===t?i.currentItem.children().each(function(){P("<td>&#160;</td>",i.document[0]).attr("colspan",P(this).attr("colspan")||1).appendTo(e)}):"img"===t&&e.attr("src",i.currentItem.attr("src")),s||e.css("visibility","hidden"),e},update:function(t,e){s&&!n.forcePlaceholderSize||(e.height()||e.height(i.currentItem.innerHeight()-parseInt(i.currentItem.css("paddingTop")||0,10)-parseInt(i.currentItem.css("paddingBottom")||0,10)),e.width()||e.width(i.currentItem.innerWidth()-parseInt(i.currentItem.css("paddingLeft")||0,10)-parseInt(i.currentItem.css("paddingRight")||0,10)))}}),i.placeholder=P(n.placeholder.element.call(i.element,i.currentItem)),i.currentItem.after(i.placeholder),n.placeholder.update(i,i.placeholder)},_contactContainers:function(t){var e,i,s,n,o,a,r,h,l,c,u=null,d=null;for(e=this.containers.length-1;0<=e;e--)if(!P.contains(this.currentItem[0],this.containers[e].element[0]))if(this._intersectsWith(this.containers[e].containerCache)){if(u&&P.contains(this.containers[e].element[0],u.element[0]))continue;u=this.containers[e],d=e}else this.containers[e].containerCache.over&&(this.containers[e]._trigger("out",t,this._uiHash(this)),this.containers[e].containerCache.over=0);if(u)if(1===this.containers.length)this.containers[d].containerCache.over||(this.containers[d]._trigger("over",t,this._uiHash(this)),this.containers[d].containerCache.over=1);else{for(s=1e4,n=null,o=(l=u.floating||this._isFloating(this.currentItem))?"left":"top",a=l?"width":"height",c=l?"clientX":"clientY",i=this.items.length-1;0<=i;i--)P.contains(this.containers[d].element[0],this.items[i].item[0])&&this.items[i].item[0]!==this.currentItem[0]&&(r=this.items[i].item.offset()[o],h=!1,t[c]-r>this.items[i][a]/2&&(h=!0),Math.abs(t[c]-r)<s&&(s=Math.abs(t[c]-r),n=this.items[i],this.direction=h?"up":"down"));if(!n&&!this.options.dropOnEmpty)return;if(this.currentContainer===this.containers[d])return void(this.currentContainer.containerCache.over||(this.containers[d]._trigger("over",t,this._uiHash()),this.currentContainer.containerCache.over=1));n?this._rearrange(t,n,null,!0):this._rearrange(t,null,this.containers[d].element,!0),this._trigger("change",t,this._uiHash()),this.containers[d]._trigger("change",t,this._uiHash(this)),this.currentContainer=this.containers[d],this.options.placeholder.update(this.currentContainer,this.placeholder),this.containers[d]._trigger("over",t,this._uiHash(this)),this.containers[d].containerCache.over=1}},_createHelper:function(t){var e=this.options,i=P.isFunction(e.helper)?P(e.helper.apply(this.element[0],[t,this.currentItem])):"clone"===e.helper?this.currentItem.clone():this.currentItem;return i.parents("body").length||P("parent"!==e.appendTo?e.appendTo:this.currentItem[0].parentNode)[0].appendChild(i[0]),i[0]===this.currentItem[0]&&(this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}),i[0].style.width&&!e.forceHelperSize||i.width(this.currentItem.width()),i[0].style.height&&!e.forceHelperSize||i.height(this.currentItem.height()),i},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),P.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var t=this.offsetParent.offset();return"absolute"===this.cssPosition&&this.scrollParent[0]!==document&&P.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&"html"===this.offsetParent[0].tagName.toLowerCase()&&P.ui.ie)&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"!==this.cssPosition)return{top:0,left:0};var t=this.currentItem.position();return{top:t.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:t.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,e,i,s=this.options;"parent"===s.containment&&(s.containment=this.helper[0].parentNode),"document"!==s.containment&&"window"!==s.containment||(this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,P("document"===s.containment?document:window).width()-this.helperProportions.width-this.margins.left,(P("document"===s.containment?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]),/^(document|window|parent)$/.test(s.containment)||(t=P(s.containment)[0],e=P(s.containment).offset(),i="hidden"!==P(t).css("overflow"),this.containment=[e.left+(parseInt(P(t).css("borderLeftWidth"),10)||0)+(parseInt(P(t).css("paddingLeft"),10)||0)-this.margins.left,e.top+(parseInt(P(t).css("borderTopWidth"),10)||0)+(parseInt(P(t).css("paddingTop"),10)||0)-this.margins.top,e.left+(i?Math.max(t.scrollWidth,t.offsetWidth):t.offsetWidth)-(parseInt(P(t).css("borderLeftWidth"),10)||0)-(parseInt(P(t).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,e.top+(i?Math.max(t.scrollHeight,t.offsetHeight):t.offsetHeight)-(parseInt(P(t).css("borderTopWidth"),10)||0)-(parseInt(P(t).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top])},_convertPositionTo:function(t,e){e||(e=this.position);var i="absolute"===t?1:-1,s="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&P.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,n=/(html|body)/i.test(s[0].tagName);return{top:e.top+this.offset.relative.top*i+this.offset.parent.top*i-("fixed"===this.cssPosition?-this.scrollParent.scrollTop():n?0:s.scrollTop())*i,left:e.left+this.offset.relative.left*i+this.offset.parent.left*i-("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():n?0:s.scrollLeft())*i}},_generatePosition:function(t){var e,i,s=this.options,n=t.pageX,o=t.pageY,a="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&P.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,r=/(html|body)/i.test(a[0].tagName);return"relative"!==this.cssPosition||this.scrollParent[0]!==document&&this.scrollParent[0]!==this.offsetParent[0]||(this.offset.relative=this._getRelativeOffset()),this.originalPosition&&(this.containment&&(t.pageX-this.offset.click.left<this.containment[0]&&(n=this.containment[0]+this.offset.click.left),t.pageY-this.offset.click.top<this.containment[1]&&(o=this.containment[1]+this.offset.click.top),t.pageX-this.offset.click.left>this.containment[2]&&(n=this.containment[2]+this.offset.click.left),t.pageY-this.offset.click.top>this.containment[3]&&(o=this.containment[3]+this.offset.click.top)),s.grid&&(e=this.originalPageY+Math.round((o-this.originalPageY)/s.grid[1])*s.grid[1],o=this.containment?e-this.offset.click.top>=this.containment[1]&&e-this.offset.click.top<=this.containment[3]?e:e-this.offset.click.top>=this.containment[1]?e-s.grid[1]:e+s.grid[1]:e,i=this.originalPageX+Math.round((n-this.originalPageX)/s.grid[0])*s.grid[0],n=this.containment?i-this.offset.click.left>=this.containment[0]&&i-this.offset.click.left<=this.containment[2]?i:i-this.offset.click.left>=this.containment[0]?i-s.grid[0]:i+s.grid[0]:i)),{top:o-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.scrollParent.scrollTop():r?0:a.scrollTop()),left:n-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():r?0:a.scrollLeft())}},_rearrange:function(t,e,i,s){i?i[0].appendChild(this.placeholder[0]):e.item[0].parentNode.insertBefore(this.placeholder[0],"down"===this.direction?e.item[0]:e.item[0].nextSibling),this.counter=this.counter?++this.counter:1;var n=this.counter;this._delay(function(){n===this.counter&&this.refreshPositions(!s)})},_clear:function(t,e){this.reverting=!1;var i,s=[];if(!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem),this._noFinalSort=null,this.helper[0]===this.currentItem[0]){for(i in this._storedCSS)"auto"!==this._storedCSS[i]&&"static"!==this._storedCSS[i]||(this._storedCSS[i]="");this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();function n(e,i,s){return function(t){s._trigger(e,t,i._uiHash(i))}}for(this.fromOutside&&!e&&s.push(function(t){this._trigger("receive",t,this._uiHash(this.fromOutside))}),!this.fromOutside&&this.domPosition.prev===this.currentItem.prev().not(".ui-sortable-helper")[0]&&this.domPosition.parent===this.currentItem.parent()[0]||e||s.push(function(t){this._trigger("update",t,this._uiHash())}),this!==this.currentContainer&&(e||(s.push(function(t){this._trigger("remove",t,this._uiHash())}),s.push(function(e){return function(t){e._trigger("receive",t,this._uiHash(this))}}.call(this,this.currentContainer)),s.push(function(e){return function(t){e._trigger("update",t,this._uiHash(this))}}.call(this,this.currentContainer)))),i=this.containers.length-1;0<=i;i--)e||s.push(n("deactivate",this,this.containers[i])),this.containers[i].containerCache.over&&(s.push(n("out",this,this.containers[i])),this.containers[i].containerCache.over=0);if(this.storedCursor&&(this.document.find("body").css("cursor",this.storedCursor),this.storedStylesheet.remove()),this._storedOpacity&&this.helper.css("opacity",this._storedOpacity),this._storedZIndex&&this.helper.css("zIndex","auto"===this._storedZIndex?"":this._storedZIndex),this.dragging=!1,e||this._trigger("beforeStop",t,this._uiHash()),this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.cancelHelperRemoval||(this.helper[0]!==this.currentItem[0]&&this.helper.remove(),this.helper=null),!e){for(i=0;i<s.length;i++)s[i].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!this.cancelHelperRemoval},_trigger:function(){!1===P.Widget.prototype._trigger.apply(this,arguments)&&this.cancel()},_uiHash:function(t){var e=t||this;return{helper:e.helper,placeholder:e.placeholder||P([]),position:e.position,originalPosition:e.originalPosition,offset:e.positionAbs,item:e.currentItem,sender:t?t.element:null}}}),P.widget("ui.accordion",{version:"1.11.2",options:{active:0,animate:{},collapsible:!1,event:"click",header:"> li > :first-child,> :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},hideProps:{borderTopWidth:"hide",borderBottomWidth:"hide",paddingTop:"hide",paddingBottom:"hide",height:"hide"},showProps:{borderTopWidth:"show",borderBottomWidth:"show",paddingTop:"show",paddingBottom:"show",height:"show"},_create:function(){var t=this.options;this.prevShow=this.prevHide=P(),this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role","tablist"),t.collapsible||!1!==t.active&&null!=t.active||(t.active=0),this._processPanels(),t.active<0&&(t.active+=this.headers.length),this._refresh()},_getCreateEventData:function(){return{header:this.active,panel:this.active.length?this.active.next():P()}},_createIcons:function(){var t=this.options.icons;t&&(P("<span>").addClass("ui-accordion-header-icon ui-icon "+t.header).prependTo(this.headers),this.active.children(".ui-accordion-header-icon").removeClass(t.header).addClass(t.activeHeader),this.headers.addClass("ui-accordion-icons"))},_destroyIcons:function(){this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()},_destroy:function(){var t;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"),this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").removeUniqueId(),this._destroyIcons(),t=this.headers.next().removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").css("display","").removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeUniqueId(),"content"!==this.options.heightStyle&&t.css("height","")},_setOption:function(t,e){"active"!==t?("event"===t&&(this.options.event&&this._off(this.headers,this.options.event),this._setupEvents(e)),this._super(t,e),"collapsible"!==t||e||!1!==this.options.active||this._activate(0),"icons"===t&&(this._destroyIcons(),e&&this._createIcons()),"disabled"===t&&(this.element.toggleClass("ui-state-disabled",!!e).attr("aria-disabled",e),this.headers.add(this.headers.next()).toggleClass("ui-state-disabled",!!e))):this._activate(e)},_keydown:function(t){if(!t.altKey&&!t.ctrlKey){var e=P.ui.keyCode,i=this.headers.length,s=this.headers.index(t.target),n=!1;switch(t.keyCode){case e.RIGHT:case e.DOWN:n=this.headers[(s+1)%i];break;case e.LEFT:case e.UP:n=this.headers[(s-1+i)%i];break;case e.SPACE:case e.ENTER:this._eventHandler(t);break;case e.HOME:n=this.headers[0];break;case e.END:n=this.headers[i-1]}n&&(P(t.target).attr("tabIndex",-1),P(n).attr("tabIndex",0),n.focus(),t.preventDefault())}},_panelKeyDown:function(t){t.keyCode===P.ui.keyCode.UP&&t.ctrlKey&&P(t.currentTarget).prev().focus()},refresh:function(){var t=this.options;this._processPanels(),!1===t.active&&!0===t.collapsible||!this.headers.length?(t.active=!1,this.active=P()):!1===t.active?this._activate(0):this.active.length&&!P.contains(this.element[0],this.active[0])?this.headers.length===this.headers.find(".ui-state-disabled").length?(t.active=!1,this.active=P()):this._activate(Math.max(0,t.active-1)):t.active=this.headers.index(this.active),this._destroyIcons(),this._refresh()},_processPanels:function(){var t=this.headers,e=this.panels;this.headers=this.element.find(this.options.header).addClass("ui-accordion-header ui-state-default ui-corner-all"),this.panels=this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide(),e&&(this._off(t.not(this.headers)),this._off(e.not(this.panels)))},_refresh:function(){var i,t=this.options,e=t.heightStyle,s=this.element.parent();this.active=this._findActive(t.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"),this.active.next().addClass("ui-accordion-content-active").show(),this.headers.attr("role","tab").each(function(){var t=P(this),e=t.uniqueId().attr("id"),i=t.next(),s=i.uniqueId().attr("id");t.attr("aria-controls",s),i.attr("aria-labelledby",e)}).next().attr("role","tabpanel"),this.headers.not(this.active).attr({"aria-selected":"false","aria-expanded":"false",tabIndex:-1}).next().attr({"aria-hidden":"true"}).hide(),this.active.length?this.active.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0}).next().attr({"aria-hidden":"false"}):this.headers.eq(0).attr("tabIndex",0),this._createIcons(),this._setupEvents(t.event),"fill"===e?(i=s.height(),this.element.siblings(":visible").each(function(){var t=P(this),e=t.css("position");"absolute"!==e&&"fixed"!==e&&(i-=t.outerHeight(!0))}),this.headers.each(function(){i-=P(this).outerHeight(!0)}),this.headers.next().each(function(){P(this).height(Math.max(0,i-P(this).innerHeight()+P(this).height()))}).css("overflow","auto")):"auto"===e&&(i=0,this.headers.next().each(function(){i=Math.max(i,P(this).css("height","").height())}).height(i))},_activate:function(t){var e=this._findActive(t)[0];e!==this.active[0]&&(e=e||this.active[0],this._eventHandler({target:e,currentTarget:e,preventDefault:P.noop}))},_findActive:function(t){return"number"==typeof t?this.headers.eq(t):P()},_setupEvents:function(t){var i={keydown:"_keydown"};t&&P.each(t.split(" "),function(t,e){i[e]="_eventHandler"}),this._off(this.headers.add(this.headers.next())),this._on(this.headers,i),this._on(this.headers.next(),{keydown:"_panelKeyDown"}),this._hoverable(this.headers),this._focusable(this.headers)},_eventHandler:function(t){var e=this.options,i=this.active,s=P(t.currentTarget),n=s[0]===i[0],o=n&&e.collapsible,a=o?P():s.next(),r=i.next(),h={oldHeader:i,oldPanel:r,newHeader:o?P():s,newPanel:a};t.preventDefault(),n&&!e.collapsible||!1===this._trigger("beforeActivate",t,h)||(e.active=!o&&this.headers.index(s),this.active=n?P():s,this._toggle(h),i.removeClass("ui-accordion-header-active ui-state-active"),e.icons&&i.children(".ui-accordion-header-icon").removeClass(e.icons.activeHeader).addClass(e.icons.header),n||(s.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"),e.icons&&s.children(".ui-accordion-header-icon").removeClass(e.icons.header).addClass(e.icons.activeHeader),s.next().addClass("ui-accordion-content-active")))},_toggle:function(t){var e=t.newPanel,i=this.prevShow.length?this.prevShow:t.oldPanel;this.prevShow.add(this.prevHide).stop(!0,!0),this.prevShow=e,this.prevHide=i,this.options.animate?this._animate(e,i,t):(i.hide(),e.show(),this._toggleComplete(t)),i.attr({"aria-hidden":"true"}),i.prev().attr("aria-selected","false"),e.length&&i.length?i.prev().attr({tabIndex:-1,"aria-expanded":"false"}):e.length&&this.headers.filter(function(){return 0===P(this).attr("tabIndex")}).attr("tabIndex",-1),e.attr("aria-hidden","false").prev().attr({"aria-selected":"true",tabIndex:0,"aria-expanded":"true"})},_animate:function(t,i,e){var s,n,o,a=this,r=0,h=t.length&&(!i.length||t.index()<i.index()),l=this.options.animate||{},c=h&&l.down||l,u=function(){a._toggleComplete(e)};return"number"==typeof c&&(o=c),"string"==typeof c&&(n=c),n=n||c.easing||l.easing,o=o||c.duration||l.duration,i.length?t.length?(s=t.show().outerHeight(),i.animate(this.hideProps,{duration:o,easing:n,step:function(t,e){e.now=Math.round(t)}}),void t.hide().animate(this.showProps,{duration:o,easing:n,complete:u,step:function(t,e){e.now=Math.round(t),"height"!==e.prop?r+=e.now:"content"!==a.options.heightStyle&&(e.now=Math.round(s-i.outerHeight()-r),r=0)}})):i.animate(this.hideProps,o,n,u):t.animate(this.showProps,o,n,u)},_toggleComplete:function(t){var e=t.oldPanel;e.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"),e.length&&(e.parent()[0].className=e.parent()[0].className),this._trigger("activate",null,t)}}),P.widget("ui.menu",{version:"1.11.2",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},items:"> *",menus:"ul",position:{my:"left-1 top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}),this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true"),this._on({"mousedown .ui-menu-item":function(t){t.preventDefault()},"click .ui-menu-item":function(t){var e=P(t.target);!this.mouseHandled&&e.not(".ui-state-disabled").length&&(this.select(t),t.isPropagationStopped()||(this.mouseHandled=!0),e.has(".ui-menu").length?this.expand(t):!this.element.is(":focus")&&P(this.document[0].activeElement).closest(".ui-menu").length&&(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(t){if(!this.previousFilter){var e=P(t.currentTarget);e.siblings(".ui-state-active").removeClass("ui-state-active"),this.focus(t,e)}},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(t,e){var i=this.active||this.element.find(this.options.items).eq(0);e||this.focus(t,i)},blur:function(t){this._delay(function(){P.contains(this.element[0],this.document[0].activeElement)||this.collapseAll(t)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(t){this._closeOnDocumentClick(t)&&this.collapseAll(t),this.mouseHandled=!1}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(),this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var t=P(this);t.data("ui-menu-submenu-carat")&&t.remove()}),this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(t){var e,i,s,n,o=!0;switch(t.keyCode){case P.ui.keyCode.PAGE_UP:this.previousPage(t);break;case P.ui.keyCode.PAGE_DOWN:this.nextPage(t);break;case P.ui.keyCode.HOME:this._move("first","first",t);break;case P.ui.keyCode.END:this._move("last","last",t);break;case P.ui.keyCode.UP:this.previous(t);break;case P.ui.keyCode.DOWN:this.next(t);break;case P.ui.keyCode.LEFT:this.collapse(t);break;case P.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(t);break;case P.ui.keyCode.ENTER:case P.ui.keyCode.SPACE:this._activate(t);break;case P.ui.keyCode.ESCAPE:this.collapse(t);break;default:o=!1,i=this.previousFilter||"",s=String.fromCharCode(t.keyCode),n=!1,clearTimeout(this.filterTimer),s===i?n=!0:s=i+s,e=this._filterMenuItems(s),(e=n&&-1!==e.index(this.active.next())?this.active.nextAll(".ui-menu-item"):e).length||(s=String.fromCharCode(t.keyCode),e=this._filterMenuItems(s)),e.length?(this.focus(t,e),this.previousFilter=s,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter}o&&t.preventDefault()},_activate:function(t){this.active.is(".ui-state-disabled")||(this.active.is("[aria-haspopup='true']")?this.expand(t):this.select(t))},refresh:function(){var t,e=this,s=this.options.icons.submenu,i=this.element.find(this.options.menus);this.element.toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length),i.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var t=P(this),e=t.parent(),i=P("<span>").addClass("ui-menu-icon ui-icon "+s).data("ui-menu-submenu-carat",!0);e.attr("aria-haspopup","true").prepend(i),t.attr("aria-labelledby",e.attr("id"))}),(t=i.add(this.element).find(this.options.items)).not(".ui-menu-item").each(function(){var t=P(this);e._isDivider(t)&&t.addClass("ui-widget-content ui-menu-divider")}),t.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({tabIndex:-1,role:this._itemRole()}),t.filter(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!P.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},_setOption:function(t,e){"icons"===t&&this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(e.submenu),"disabled"===t&&this.element.toggleClass("ui-state-disabled",!!e).attr("aria-disabled",e),this._super(t,e)},focus:function(t,e){var i,s;this.blur(t,t&&"focus"===t.type),this._scrollIntoView(e),this.active=e.first(),s=this.active.addClass("ui-state-focus").removeClass("ui-state-active"),this.options.role&&this.element.attr("aria-activedescendant",s.attr("id")),this.active.parent().closest(".ui-menu-item").addClass("ui-state-active"),t&&"keydown"===t.type?this._close():this.timer=this._delay(function(){this._close()},this.delay),(i=e.children(".ui-menu")).length&&t&&/^mouse/.test(t.type)&&this._startOpening(i),this.activeMenu=e.parent(),this._trigger("focus",t,{item:e})},_scrollIntoView:function(t){var e,i,s,n,o,a;this._hasScroll()&&(e=parseFloat(P.css(this.activeMenu[0],"borderTopWidth"))||0,i=parseFloat(P.css(this.activeMenu[0],"paddingTop"))||0,s=t.offset().top-this.activeMenu.offset().top-e-i,n=this.activeMenu.scrollTop(),o=this.activeMenu.height(),a=t.outerHeight(),s<0?this.activeMenu.scrollTop(n+s):o<s+a&&this.activeMenu.scrollTop(n+s-o+a))},blur:function(t,e){e||clearTimeout(this.timer),this.active&&(this.active.removeClass("ui-state-focus"),this.active=null,this._trigger("blur",t,{item:this.active}))},_startOpening:function(t){clearTimeout(this.timer),"true"===t.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(t)},this.delay))},_open:function(t){var e=P.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden","true"),t.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(e)},collapseAll:function(e,i){clearTimeout(this.timer),this.timer=this._delay(function(){var t=i?this.element:P(e&&e.target).closest(this.element.find(".ui-menu"));t.length||(t=this.element),this._close(t),this.blur(e),this.activeMenu=t},this.delay)},_close:function(t){t||(t=this.active?this.active.parent():this.element),t.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active")},_closeOnDocumentClick:function(t){return!P(t.target).closest(".ui-menu").length},_isDivider:function(t){return!/[^\-\u2014\u2013\s]/.test(t.text())},collapse:function(t){var e=this.active&&this.active.parent().closest(".ui-menu-item",this.element);e&&e.length&&(this._close(),this.focus(t,e))},expand:function(t){var e=this.active&&this.active.children(".ui-menu ").find(this.options.items).first();e&&e.length&&(this._open(e.parent()),this._delay(function(){this.focus(t,e)}))},next:function(t){this._move("next","first",t)},previous:function(t){this._move("prev","last",t)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(t,e,i){var s;this.active&&(s="first"===t||"last"===t?this.active["first"===t?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[t+"All"](".ui-menu-item").eq(0)),s&&s.length&&this.active||(s=this.activeMenu.find(this.options.items)[e]()),this.focus(i,s)},nextPage:function(t){var e,i,s;this.active?this.isLastItem()||(this._hasScroll()?(i=this.active.offset().top,s=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return(e=P(this)).offset().top-i-s<0}),this.focus(t,e)):this.focus(t,this.activeMenu.find(this.options.items)[this.active?"last":"first"]())):this.next(t)},previousPage:function(t){var e,i,s;this.active?this.isFirstItem()||(this._hasScroll()?(i=this.active.offset().top,s=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return 0<(e=P(this)).offset().top-i+s}),this.focus(t,e)):this.focus(t,this.activeMenu.find(this.options.items).first())):this.next(t)},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(t){this.active=this.active||P(t.target).closest(".ui-menu-item");var e={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(t,!0),this._trigger("select",t,e)},_filterMenuItems:function(t){var e=t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&"),i=new RegExp("^"+e,"i");return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function(){return i.test(P.trim(P(this).text()))})}});P.widget("ui.autocomplete",{version:"1.11.2",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},requestIndex:0,pending:0,_create:function(){var i,s,n,t=this.element[0].nodeName.toLowerCase(),e="textarea"===t,o="input"===t;this.isMultiLine=!!e||!o&&this.element.prop("isContentEditable"),this.valueMethod=this.element[e||o?"val":"text"],this.isNewMenu=!0,this.element.addClass("ui-autocomplete-input").attr("autocomplete","off"),this._on(this.element,{keydown:function(t){if(this.element.prop("readOnly"))s=n=i=!0;else{s=n=i=!1;var e=P.ui.keyCode;switch(t.keyCode){case e.PAGE_UP:i=!0,this._move("previousPage",t);break;case e.PAGE_DOWN:i=!0,this._move("nextPage",t);break;case e.UP:i=!0,this._keyEvent("previous",t);break;case e.DOWN:i=!0,this._keyEvent("next",t);break;case e.ENTER:this.menu.active&&(i=!0,t.preventDefault(),this.menu.select(t));break;case e.TAB:this.menu.active&&this.menu.select(t);break;case e.ESCAPE:this.menu.element.is(":visible")&&(this.isMultiLine||this._value(this.term),this.close(t),t.preventDefault());break;default:s=!0,this._searchTimeout(t)}}},keypress:function(t){if(i)return i=!1,void(this.isMultiLine&&!this.menu.element.is(":visible")||t.preventDefault());if(!s){var e=P.ui.keyCode;switch(t.keyCode){case e.PAGE_UP:this._move("previousPage",t);break;case e.PAGE_DOWN:this._move("nextPage",t);break;case e.UP:this._keyEvent("previous",t);break;case e.DOWN:this._keyEvent("next",t)}}},input:function(t){if(n)return n=!1,void t.preventDefault();this._searchTimeout(t)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(t){this.cancelBlur?delete this.cancelBlur:(clearTimeout(this.searching),this.close(t),this._change(t))}}),this._initSource(),this.menu=P("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({role:null}).hide().menu("instance"),this._on(this.menu.element,{mousedown:function(t){t.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur});var i=this.menu.element[0];P(t.target).closest(".ui-menu-item").length||this._delay(function(){var e=this;this.document.one("mousedown",function(t){t.target===e.element[0]||t.target===i||P.contains(i,t.target)||e.close()})})},menufocus:function(t,e){var i,s;if(this.isNewMenu&&(this.isNewMenu=!1,t.originalEvent&&/^mouse/.test(t.originalEvent.type)))return this.menu.blur(),void this.document.one("mousemove",function(){P(t.target).trigger(t.originalEvent)});s=e.item.data("ui-autocomplete-item"),!1!==this._trigger("focus",t,{item:s})&&t.originalEvent&&/^key/.test(t.originalEvent.type)&&this._value(s.value),(i=e.item.attr("aria-label")||s.value)&&P.trim(i).length&&(this.liveRegion.children().hide(),P("<div>").text(i).appendTo(this.liveRegion))},menuselect:function(t,e){var i=e.item.data("ui-autocomplete-item"),s=this.previous;this.element[0]!==this.document[0].activeElement&&(this.element.focus(),this.previous=s,this._delay(function(){this.previous=s,this.selectedItem=i})),!1!==this._trigger("select",t,{item:i})&&this._value(i.value),this.term=this._value(),this.close(t),this.selectedItem=i}}),this.liveRegion=P("<span>",{role:"status","aria-live":"assertive","aria-relevant":"additions"}).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(t,e){this._super(t,e),"source"===t&&this._initSource(),"appendTo"===t&&this.menu.element.appendTo(this._appendTo()),"disabled"===t&&e&&this.xhr&&this.xhr.abort()},_appendTo:function(){var t=this.options.appendTo;return t&&(t=t.jquery||t.nodeType?P(t):this.document.find(t).eq(0)),t&&t[0]||(t=this.element.closest(".ui-front")),t.length||(t=this.document[0].body),t},_initSource:function(){var i,s,n=this;P.isArray(this.options.source)?(i=this.options.source,this.source=function(t,e){e(P.ui.autocomplete.filter(i,t.term))}):"string"==typeof this.options.source?(s=this.options.source,this.source=function(t,e){n.xhr&&n.xhr.abort(),n.xhr=P.ajax({url:s,data:t,dataType:"json",success:function(t){e(t)},error:function(){e([])}})}):this.source=this.options.source},_searchTimeout:function(s){clearTimeout(this.searching),this.searching=this._delay(function(){var t=this.term===this._value(),e=this.menu.element.is(":visible"),i=s.altKey||s.ctrlKey||s.metaKey||s.shiftKey;t&&(!t||e||i)||(this.selectedItem=null,this.search(null,s))},this.options.delay)},search:function(t,e){return t=null!=t?t:this._value(),this.term=this._value(),t.length<this.options.minLength?this.close(e):!1!==this._trigger("search",e)?this._search(t):void 0},_search:function(t){this.pending++,this.element.addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:t},this._response())},_response:function(){var e=++this.requestIndex;return P.proxy(function(t){e===this.requestIndex&&this.__response(t),this.pending--,this.pending||this.element.removeClass("ui-autocomplete-loading")},this)},__response:function(t){t&&(t=this._normalize(t)),this._trigger("response",null,{content:t}),!this.options.disabled&&t&&t.length&&!this.cancelSearch?(this._suggest(t),this._trigger("open")):this._close()},close:function(t){this.cancelSearch=!0,this._close(t)},_close:function(t){this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",t))},_change:function(t){this.previous!==this._value()&&this._trigger("change",t,{item:this.selectedItem})},_normalize:function(t){return t.length&&t[0].label&&t[0].value?t:P.map(t,function(t){return"string"==typeof t?{label:t,value:t}:P.extend({},t,{label:t.label||t.value,value:t.value||t.label})})},_suggest:function(t){var e=this.menu.element.empty();this._renderMenu(e,t),this.isNewMenu=!0,this.menu.refresh(),e.show(),this._resizeMenu(),e.position(P.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var t=this.menu.element;t.outerWidth(Math.max(t.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(i,t){var s=this;P.each(t,function(t,e){s._renderItemData(i,e)})},_renderItemData:function(t,e){return this._renderItem(t,e).data("ui-autocomplete-item",e)},_renderItem:function(t,e){return P("<li>").text(e.label).appendTo(t)},_move:function(t,e){if(this.menu.element.is(":visible"))return this.menu.isFirstItem()&&/^previous/.test(t)||this.menu.isLastItem()&&/^next/.test(t)?(this.isMultiLine||this._value(this.term),void this.menu.blur()):void this.menu[t](e);this.search(null,e)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(t,e){this.isMultiLine&&!this.menu.element.is(":visible")||(this._move(t,e),e.preventDefault())}}),P.extend(P.ui.autocomplete,{escapeRegex:function(t){return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(t,e){var i=new RegExp(P.ui.autocomplete.escapeRegex(e),"i");return P.grep(t,function(t){return i.test(t.label||t.value||t)})}}),P.widget("ui.autocomplete",P.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(t){return t+(1<t?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(t){var e;this._superApply(arguments),this.options.disabled||this.cancelSearch||(e=t&&t.length?this.options.messages.results(t.length):this.options.messages.noResults,this.liveRegion.children().hide(),P("<div>").text(e).appendTo(this.liveRegion))}});P.ui.autocomplete;var c,u="ui-button ui-widget ui-state-default ui-corner-all",d="ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",p=function(){var t=P(this);setTimeout(function(){t.find(":ui-button").button("refresh")},1)},f=function(t){var e=t.name,i=t.form,s=P([]);return e&&(e=e.replace(/'/g,"\\'"),s=i?P(i).find("[name='"+e+"'][type=radio]"):P("[name='"+e+"'][type=radio]",t.ownerDocument).filter(function(){return!this.form})),s};P.widget("ui.button",{version:"1.11.2",defaultElement:"<button>",options:{disabled:null,text:!0,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset"+this.eventNamespace).bind("reset"+this.eventNamespace,p),"boolean"!=typeof this.options.disabled?this.options.disabled=!!this.element.prop("disabled"):this.element.prop("disabled",this.options.disabled),this._determineButtonType(),this.hasTitle=!!this.buttonElement.attr("title");var e=this,i=this.options,t="checkbox"===this.type||"radio"===this.type,s=t?"":"ui-state-active";null===i.label&&(i.label="input"===this.type?this.buttonElement.val():this.buttonElement.html()),this._hoverable(this.buttonElement),this.buttonElement.addClass(u).attr("role","button").bind("mouseenter"+this.eventNamespace,function(){i.disabled||this===c&&P(this).addClass("ui-state-active")}).bind("mouseleave"+this.eventNamespace,function(){i.disabled||P(this).removeClass(s)}).bind("click"+this.eventNamespace,function(t){i.disabled&&(t.preventDefault(),t.stopImmediatePropagation())}),this._on({focus:function(){this.buttonElement.addClass("ui-state-focus")},blur:function(){this.buttonElement.removeClass("ui-state-focus")}}),t&&this.element.bind("change"+this.eventNamespace,function(){e.refresh()}),"checkbox"===this.type?this.buttonElement.bind("click"+this.eventNamespace,function(){if(i.disabled)return!1}):"radio"===this.type?this.buttonElement.bind("click"+this.eventNamespace,function(){if(i.disabled)return!1;P(this).addClass("ui-state-active"),e.buttonElement.attr("aria-pressed","true");var t=e.element[0];f(t).not(t).map(function(){return P(this).button("widget")[0]}).removeClass("ui-state-active").attr("aria-pressed","false")}):(this.buttonElement.bind("mousedown"+this.eventNamespace,function(){if(i.disabled)return!1;P(this).addClass("ui-state-active"),c=this,e.document.one("mouseup",function(){c=null})}).bind("mouseup"+this.eventNamespace,function(){if(i.disabled)return!1;P(this).removeClass("ui-state-active")}).bind("keydown"+this.eventNamespace,function(t){if(i.disabled)return!1;t.keyCode!==P.ui.keyCode.SPACE&&t.keyCode!==P.ui.keyCode.ENTER||P(this).addClass("ui-state-active")}).bind("keyup"+this.eventNamespace+" blur"+this.eventNamespace,function(){P(this).removeClass("ui-state-active")}),this.buttonElement.is("a")&&this.buttonElement.keyup(function(t){t.keyCode===P.ui.keyCode.SPACE&&P(this).click()})),this._setOption("disabled",i.disabled),this._resetButton()},_determineButtonType:function(){var t,e,i;this.element.is("[type=checkbox]")?this.type="checkbox":this.element.is("[type=radio]")?this.type="radio":this.element.is("input")?this.type="input":this.type="button","checkbox"===this.type||"radio"===this.type?(t=this.element.parents().last(),e="label[for='"+this.element.attr("id")+"']",this.buttonElement=t.find(e),this.buttonElement.length||(t=t.length?t.siblings():this.element.siblings(),this.buttonElement=t.filter(e),this.buttonElement.length||(this.buttonElement=t.find(e))),this.element.addClass("ui-helper-hidden-accessible"),(i=this.element.is(":checked"))&&this.buttonElement.addClass("ui-state-active"),this.buttonElement.prop("aria-pressed",i)):this.buttonElement=this.element},widget:function(){return this.buttonElement},_destroy:function(){this.element.removeClass("ui-helper-hidden-accessible"),this.buttonElement.removeClass(u+" ui-state-active "+d).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()),this.hasTitle||this.buttonElement.removeAttr("title")},_setOption:function(t,e){if(this._super(t,e),"disabled"===t)return this.widget().toggleClass("ui-state-disabled",!!e),this.element.prop("disabled",!!e),void(e&&("checkbox"===this.type||"radio"===this.type?this.buttonElement.removeClass("ui-state-focus"):this.buttonElement.removeClass("ui-state-focus ui-state-active")));this._resetButton()},refresh:function(){var t=this.element.is("input, button")?this.element.is(":disabled"):this.element.hasClass("ui-button-disabled");t!==this.options.disabled&&this._setOption("disabled",t),"radio"===this.type?f(this.element[0]).each(function(){P(this).is(":checked")?P(this).button("widget").addClass("ui-state-active").attr("aria-pressed","true"):P(this).button("widget").removeClass("ui-state-active").attr("aria-pressed","false")}):"checkbox"===this.type&&(this.element.is(":checked")?this.buttonElement.addClass("ui-state-active").attr("aria-pressed","true"):this.buttonElement.removeClass("ui-state-active").attr("aria-pressed","false"))},_resetButton:function(){if("input"!==this.type){var t=this.buttonElement.removeClass(d),e=P("<span></span>",this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(t.empty()).text(),i=this.options.icons,s=i.primary&&i.secondary,n=[];i.primary||i.secondary?(this.options.text&&n.push("ui-button-text-icon"+(s?"s":i.primary?"-primary":"-secondary")),i.primary&&t.prepend("<span class='ui-button-icon-primary ui-icon "+i.primary+"'></span>"),i.secondary&&t.append("<span class='ui-button-icon-secondary ui-icon "+i.secondary+"'></span>"),this.options.text||(n.push(s?"ui-button-icons-only":"ui-button-icon-only"),this.hasTitle||t.attr("title",P.trim(e)))):n.push("ui-button-text-only"),t.addClass(n.join(" "))}else this.options.label&&this.element.val(this.options.label)}}),P.widget("ui.buttonset",{version:"1.11.2",options:{items:"button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"},_create:function(){this.element.addClass("ui-buttonset")},_init:function(){this.refresh()},_setOption:function(t,e){"disabled"===t&&this.buttons.button("option",t,e),this._super(t,e)},refresh:function(){var t="rtl"===this.element.css("direction"),e=this.element.find(this.options.items),i=e.filter(":ui-button");e.not(":ui-button").button(),i.button("refresh"),this.buttons=e.map(function(){return P(this).button("widget")[0]}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(t?"ui-corner-right":"ui-corner-left").end().filter(":last").addClass(t?"ui-corner-left":"ui-corner-right").end().end()},_destroy:function(){this.element.removeClass("ui-buttonset"),this.buttons.map(function(){return P(this).button("widget")[0]}).removeClass("ui-corner-left ui-corner-right").end().button("destroy")}});var m;P.ui.button;function g(){this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},P.extend(this._defaults,this.regional[""]),this.regional.en=P.extend(!0,{},this.regional[""]),this.regional["en-US"]=P.extend(!0,{},this.regional.en),this.dpDiv=v(P("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}function v(t){var e="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return t.delegate(e,"mouseout",function(){P(this).removeClass("ui-state-hover"),-1!==this.className.indexOf("ui-datepicker-prev")&&P(this).removeClass("ui-datepicker-prev-hover"),-1!==this.className.indexOf("ui-datepicker-next")&&P(this).removeClass("ui-datepicker-next-hover")}).delegate(e,"mouseover",_)}function _(){P.datepicker._isDisabledDatepicker(m.inline?m.dpDiv.parent()[0]:m.input[0])||(P(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),P(this).addClass("ui-state-hover"),-1!==this.className.indexOf("ui-datepicker-prev")&&P(this).addClass("ui-datepicker-prev-hover"),-1!==this.className.indexOf("ui-datepicker-next")&&P(this).addClass("ui-datepicker-next-hover"))}function b(t,e){for(var i in P.extend(t,e),e)null==e[i]&&(t[i]=e[i]);return t}P.extend(P.ui,{datepicker:{version:"1.11.2"}}),P.extend(g.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(t){return b(this._defaults,t||{}),this},_attachDatepicker:function(t,e){var i,s,n;s="div"===(i=t.nodeName.toLowerCase())||"span"===i,t.id||(this.uuid+=1,t.id="dp"+this.uuid),(n=this._newInst(P(t),s)).settings=P.extend({},e||{}),"input"===i?this._connectDatepicker(t,n):s&&this._inlineDatepicker(t,n)},_newInst:function(t,e){return{id:t[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1"),input:t,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:e,dpDiv:e?v(P("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")):this.dpDiv}},_connectDatepicker:function(t,e){var i=P(t);e.append=P([]),e.trigger=P([]),i.hasClass(this.markerClassName)||(this._attachments(i,e),i.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp),this._autoSize(e),P.data(t,"datepicker",e),e.settings.disabled&&this._disableDatepicker(t))},_attachments:function(t,e){var i,s,n,o=this._get(e,"appendText"),a=this._get(e,"isRTL");e.append&&e.append.remove(),o&&(e.append=P("<span class='"+this._appendClass+"'>"+o+"</span>"),t[a?"before":"after"](e.append)),t.unbind("focus",this._showDatepicker),e.trigger&&e.trigger.remove(),"focus"!==(i=this._get(e,"showOn"))&&"both"!==i||t.focus(this._showDatepicker),"button"!==i&&"both"!==i||(s=this._get(e,"buttonText"),n=this._get(e,"buttonImage"),e.trigger=P(this._get(e,"buttonImageOnly")?P("<img/>").addClass(this._triggerClass).attr({src:n,alt:s,title:s}):P("<button type='button'></button>").addClass(this._triggerClass).html(n?P("<img/>").attr({src:n,alt:s,title:s}):s)),t[a?"before":"after"](e.trigger),e.trigger.click(function(){return P.datepicker._datepickerShowing&&P.datepicker._lastInput===t[0]?P.datepicker._hideDatepicker():(P.datepicker._datepickerShowing&&P.datepicker._lastInput!==t[0]&&P.datepicker._hideDatepicker(),P.datepicker._showDatepicker(t[0])),!1}))},_autoSize:function(t){if(this._get(t,"autoSize")&&!t.inline){var e,i,s,n,o=new Date(2009,11,20),a=this._get(t,"dateFormat");a.match(/[DM]/)&&(e=function(t){for(n=s=i=0;n<t.length;n++)t[n].length>i&&(i=t[n].length,s=n);return s},o.setMonth(e(this._get(t,a.match(/MM/)?"monthNames":"monthNamesShort"))),o.setDate(e(this._get(t,a.match(/DD/)?"dayNames":"dayNamesShort"))+20-o.getDay())),t.input.attr("size",this._formatDate(t,o).length)}},_inlineDatepicker:function(t,e){var i=P(t);i.hasClass(this.markerClassName)||(i.addClass(this.markerClassName).append(e.dpDiv),P.data(t,"datepicker",e),this._setDate(e,this._getDefaultDate(e),!0),this._updateDatepicker(e),this._updateAlternate(e),e.settings.disabled&&this._disableDatepicker(t),e.dpDiv.css("display","block"))},_dialogDatepicker:function(t,e,i,s,n){var o,a,r,h,l,c=this._dialogInst;return c||(this.uuid+=1,o="dp"+this.uuid,this._dialogInput=P("<input type='text' id='"+o+"' style='position: absolute; top: -100px; width: 0px;'/>"),this._dialogInput.keydown(this._doKeyDown),P("body").append(this._dialogInput),(c=this._dialogInst=this._newInst(this._dialogInput,!1)).settings={},P.data(this._dialogInput[0],"datepicker",c)),b(c.settings,s||{}),e=e&&e.constructor===Date?this._formatDate(c,e):e,this._dialogInput.val(e),this._pos=n?n.length?n:[n.pageX,n.pageY]:null,this._pos||(a=document.documentElement.clientWidth,r=document.documentElement.clientHeight,h=document.documentElement.scrollLeft||document.body.scrollLeft,l=document.documentElement.scrollTop||document.body.scrollTop,this._pos=[a/2-100+h,r/2-150+l]),this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),c.settings.onSelect=i,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),P.blockUI&&P.blockUI(this.dpDiv),P.data(this._dialogInput[0],"datepicker",c),this},_destroyDatepicker:function(t){var e,i=P(t),s=P.data(t,"datepicker");i.hasClass(this.markerClassName)&&(e=t.nodeName.toLowerCase(),P.removeData(t,"datepicker"),"input"===e?(s.append.remove(),s.trigger.remove(),i.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)):"div"!==e&&"span"!==e||i.removeClass(this.markerClassName).empty())},_enableDatepicker:function(e){var t,i,s=P(e),n=P.data(e,"datepicker");s.hasClass(this.markerClassName)&&("input"===(t=e.nodeName.toLowerCase())?(e.disabled=!1,n.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""})):"div"!==t&&"span"!==t||((i=s.children("."+this._inlineClass)).children().removeClass("ui-state-disabled"),i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1)),this._disabledInputs=P.map(this._disabledInputs,function(t){return t===e?null:t}))},_disableDatepicker:function(e){var t,i,s=P(e),n=P.data(e,"datepicker");s.hasClass(this.markerClassName)&&("input"===(t=e.nodeName.toLowerCase())?(e.disabled=!0,n.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"})):"div"!==t&&"span"!==t||((i=s.children("."+this._inlineClass)).children().addClass("ui-state-disabled"),i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0)),this._disabledInputs=P.map(this._disabledInputs,function(t){return t===e?null:t}),this._disabledInputs[this._disabledInputs.length]=e)},_isDisabledDatepicker:function(t){if(!t)return!1;for(var e=0;e<this._disabledInputs.length;e++)if(this._disabledInputs[e]===t)return!0;return!1},_getInst:function(t){try{return P.data(t,"datepicker")}catch(t){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(t,e,i){var s,n,o,a,r=this._getInst(t);if(2===arguments.length&&"string"==typeof e)return"defaults"===e?P.extend({},P.datepicker._defaults):r?"all"===e?P.extend({},r.settings):this._get(r,e):null;s=e||{},"string"==typeof e&&((s={})[e]=i),r&&(this._curInst===r&&this._hideDatepicker(),n=this._getDateDatepicker(t,!0),o=this._getMinMaxDate(r,"min"),a=this._getMinMaxDate(r,"max"),b(r.settings,s),null!==o&&void 0!==s.dateFormat&&void 0===s.minDate&&(r.settings.minDate=this._formatDate(r,o)),null!==a&&void 0!==s.dateFormat&&void 0===s.maxDate&&(r.settings.maxDate=this._formatDate(r,a)),"disabled"in s&&(s.disabled?this._disableDatepicker(t):this._enableDatepicker(t)),this._attachments(P(t),r),this._autoSize(r),this._setDate(r,n),this._updateAlternate(r),this._updateDatepicker(r))},_changeDatepicker:function(t,e,i){this._optionDatepicker(t,e,i)},_refreshDatepicker:function(t){var e=this._getInst(t);e&&this._updateDatepicker(e)},_setDateDatepicker:function(t,e){var i=this._getInst(t);i&&(this._setDate(i,e),this._updateDatepicker(i),this._updateAlternate(i))},_getDateDatepicker:function(t,e){var i=this._getInst(t);return i&&!i.inline&&this._setDateFromField(i,e),i?this._getDate(i):null},_doKeyDown:function(t){var e,i,s,n=P.datepicker._getInst(t.target),o=!0,a=n.dpDiv.is(".ui-datepicker-rtl");if(n._keyEvent=!0,P.datepicker._datepickerShowing)switch(t.keyCode){case 9:P.datepicker._hideDatepicker(),o=!1;break;case 13:return(s=P("td."+P.datepicker._dayOverClass+":not(."+P.datepicker._currentClass+")",n.dpDiv))[0]&&P.datepicker._selectDay(t.target,n.selectedMonth,n.selectedYear,s[0]),(e=P.datepicker._get(n,"onSelect"))?(i=P.datepicker._formatDate(n),e.apply(n.input?n.input[0]:null,[i,n])):P.datepicker._hideDatepicker(),!1;case 27:P.datepicker._hideDatepicker();break;case 33:P.datepicker._adjustDate(t.target,t.ctrlKey?-P.datepicker._get(n,"stepBigMonths"):-P.datepicker._get(n,"stepMonths"),"M");break;case 34:P.datepicker._adjustDate(t.target,t.ctrlKey?+P.datepicker._get(n,"stepBigMonths"):+P.datepicker._get(n,"stepMonths"),"M");break;case 35:(t.ctrlKey||t.metaKey)&&P.datepicker._clearDate(t.target),o=t.ctrlKey||t.metaKey;break;case 36:(t.ctrlKey||t.metaKey)&&P.datepicker._gotoToday(t.target),o=t.ctrlKey||t.metaKey;break;case 37:(t.ctrlKey||t.metaKey)&&P.datepicker._adjustDate(t.target,a?1:-1,"D"),o=t.ctrlKey||t.metaKey,t.originalEvent.altKey&&P.datepicker._adjustDate(t.target,t.ctrlKey?-P.datepicker._get(n,"stepBigMonths"):-P.datepicker._get(n,"stepMonths"),"M");break;case 38:(t.ctrlKey||t.metaKey)&&P.datepicker._adjustDate(t.target,-7,"D"),o=t.ctrlKey||t.metaKey;break;case 39:(t.ctrlKey||t.metaKey)&&P.datepicker._adjustDate(t.target,a?-1:1,"D"),o=t.ctrlKey||t.metaKey,t.originalEvent.altKey&&P.datepicker._adjustDate(t.target,t.ctrlKey?+P.datepicker._get(n,"stepBigMonths"):+P.datepicker._get(n,"stepMonths"),"M");break;case 40:(t.ctrlKey||t.metaKey)&&P.datepicker._adjustDate(t.target,7,"D"),o=t.ctrlKey||t.metaKey;break;default:o=!1}else 36===t.keyCode&&t.ctrlKey?P.datepicker._showDatepicker(this):o=!1;o&&(t.preventDefault(),t.stopPropagation())},_doKeyPress:function(t){var e,i,s=P.datepicker._getInst(t.target);if(P.datepicker._get(s,"constrainInput"))return e=P.datepicker._possibleChars(P.datepicker._get(s,"dateFormat")),i=String.fromCharCode(null==t.charCode?t.keyCode:t.charCode),t.ctrlKey||t.metaKey||i<" "||!e||-1<e.indexOf(i)},_doKeyUp:function(t){var e=P.datepicker._getInst(t.target);if(e.input.val()!==e.lastVal)try{P.datepicker.parseDate(P.datepicker._get(e,"dateFormat"),e.input?e.input.val():null,P.datepicker._getFormatConfig(e))&&(P.datepicker._setDateFromField(e),P.datepicker._updateAlternate(e),P.datepicker._updateDatepicker(e))}catch(t){}return!0},_showDatepicker:function(t){var e,i,s,n,o,a,r;("input"!==(t=t.target||t).nodeName.toLowerCase()&&(t=P("input",t.parentNode)[0]),P.datepicker._isDisabledDatepicker(t)||P.datepicker._lastInput===t)||(e=P.datepicker._getInst(t),P.datepicker._curInst&&P.datepicker._curInst!==e&&(P.datepicker._curInst.dpDiv.stop(!0,!0),e&&P.datepicker._datepickerShowing&&P.datepicker._hideDatepicker(P.datepicker._curInst.input[0])),!1!==(s=(i=P.datepicker._get(e,"beforeShow"))?i.apply(t,[t,e]):{})&&(b(e.settings,s),e.lastVal=null,P.datepicker._lastInput=t,P.datepicker._setDateFromField(e),P.datepicker._inDialog&&(t.value=""),P.datepicker._pos||(P.datepicker._pos=P.datepicker._findPos(t),P.datepicker._pos[1]+=t.offsetHeight),n=!1,P(t).parents().each(function(){return!(n|="fixed"===P(this).css("position"))}),o={left:P.datepicker._pos[0],top:P.datepicker._pos[1]},P.datepicker._pos=null,e.dpDiv.empty(),e.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),P.datepicker._updateDatepicker(e),o=P.datepicker._checkOffset(e,o,n),e.dpDiv.css({position:P.datepicker._inDialog&&P.blockUI?"static":n?"fixed":"absolute",display:"none",left:o.left+"px",top:o.top+"px"}),e.inline||(a=P.datepicker._get(e,"showAnim"),r=P.datepicker._get(e,"duration"),e.dpDiv.css("z-index",function(t){for(var e,i;t.length&&t[0]!==document;){if(("absolute"===(e=t.css("position"))||"relative"===e||"fixed"===e)&&(i=parseInt(t.css("zIndex"),10),!isNaN(i)&&0!==i))return i;t=t.parent()}return 0}(P(t))+1),P.datepicker._datepickerShowing=!0,P.effects&&P.effects.effect[a]?e.dpDiv.show(a,P.datepicker._get(e,"showOptions"),r):e.dpDiv[a||"show"](a?r:null),P.datepicker._shouldFocusInput(e)&&e.input.focus(),P.datepicker._curInst=e)))},_updateDatepicker:function(t){this.maxRows=4,(m=t).dpDiv.empty().append(this._generateHTML(t)),this._attachHandlers(t);var e,i=this._getNumberOfMonths(t),s=i[1],n=t.dpDiv.find("."+this._dayOverClass+" a");0<n.length&&_.apply(n.get(0)),t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),1<s&&t.dpDiv.addClass("ui-datepicker-multi-"+s).css("width",17*s+"em"),t.dpDiv[(1!==i[0]||1!==i[1]?"add":"remove")+"Class"]("ui-datepicker-multi"),t.dpDiv[(this._get(t,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),t===P.datepicker._curInst&&P.datepicker._datepickerShowing&&P.datepicker._shouldFocusInput(t)&&t.input.focus(),t.yearshtml&&(e=t.yearshtml,setTimeout(function(){e===t.yearshtml&&t.yearshtml&&t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml),e=t.yearshtml=null},0))},_shouldFocusInput:function(t){return t.input&&t.input.is(":visible")&&!t.input.is(":disabled")&&!t.input.is(":focus")},_checkOffset:function(t,e,i){var s=t.dpDiv.outerWidth(),n=t.dpDiv.outerHeight(),o=t.input?t.input.outerWidth():0,a=t.input?t.input.outerHeight():0,r=document.documentElement.clientWidth+(i?0:P(document).scrollLeft()),h=document.documentElement.clientHeight+(i?0:P(document).scrollTop());return e.left-=this._get(t,"isRTL")?s-o:0,e.left-=i&&e.left===t.input.offset().left?P(document).scrollLeft():0,e.top-=i&&e.top===t.input.offset().top+a?P(document).scrollTop():0,e.left-=Math.min(e.left,e.left+s>r&&s<r?Math.abs(e.left+s-r):0),e.top-=Math.min(e.top,e.top+n>h&&n<h?Math.abs(n+a):0),e},_findPos:function(t){for(var e,i=this._getInst(t),s=this._get(i,"isRTL");t&&("hidden"===t.type||1!==t.nodeType||P.expr.filters.hidden(t));)t=t[s?"previousSibling":"nextSibling"];return[(e=P(t).offset()).left,e.top]},_hideDatepicker:function(t){var e,i,s,n,o=this._curInst;!o||t&&o!==P.data(t,"datepicker")||this._datepickerShowing&&(e=this._get(o,"showAnim"),i=this._get(o,"duration"),s=function(){P.datepicker._tidyDialog(o)},P.effects&&(P.effects.effect[e]||P.effects[e])?o.dpDiv.hide(e,P.datepicker._get(o,"showOptions"),i,s):o.dpDiv["slideDown"===e?"slideUp":"fadeIn"===e?"fadeOut":"hide"](e?i:null,s),e||s(),this._datepickerShowing=!1,(n=this._get(o,"onClose"))&&n.apply(o.input?o.input[0]:null,[o.input?o.input.val():"",o]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),P.blockUI&&(P.unblockUI(),P("body").append(this.dpDiv))),this._inDialog=!1)},_tidyDialog:function(t){t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(t){if(P.datepicker._curInst){var e=P(t.target),i=P.datepicker._getInst(e[0]);(e[0].id===P.datepicker._mainDivId||0!==e.parents("#"+P.datepicker._mainDivId).length||e.hasClass(P.datepicker.markerClassName)||e.closest("."+P.datepicker._triggerClass).length||!P.datepicker._datepickerShowing||P.datepicker._inDialog&&P.blockUI)&&(!e.hasClass(P.datepicker.markerClassName)||P.datepicker._curInst===i)||P.datepicker._hideDatepicker()}},_adjustDate:function(t,e,i){var s=P(t),n=this._getInst(s[0]);this._isDisabledDatepicker(s[0])||(this._adjustInstDate(n,e+("M"===i?this._get(n,"showCurrentAtPos"):0),i),this._updateDatepicker(n))},_gotoToday:function(t){var e,i=P(t),s=this._getInst(i[0]);this._get(s,"gotoCurrent")&&s.currentDay?(s.selectedDay=s.currentDay,s.drawMonth=s.selectedMonth=s.currentMonth,s.drawYear=s.selectedYear=s.currentYear):(e=new Date,s.selectedDay=e.getDate(),s.drawMonth=s.selectedMonth=e.getMonth(),s.drawYear=s.selectedYear=e.getFullYear()),this._notifyChange(s),this._adjustDate(i)},_selectMonthYear:function(t,e,i){var s=P(t),n=this._getInst(s[0]);n["selected"+("M"===i?"Month":"Year")]=n["draw"+("M"===i?"Month":"Year")]=parseInt(e.options[e.selectedIndex].value,10),this._notifyChange(n),this._adjustDate(s)},_selectDay:function(t,e,i,s){var n,o=P(t);P(s).hasClass(this._unselectableClass)||this._isDisabledDatepicker(o[0])||((n=this._getInst(o[0])).selectedDay=n.currentDay=P("a",s).html(),n.selectedMonth=n.currentMonth=e,n.selectedYear=n.currentYear=i,this._selectDate(t,this._formatDate(n,n.currentDay,n.currentMonth,n.currentYear)))},_clearDate:function(t){var e=P(t);this._selectDate(e,"")},_selectDate:function(t,e){var i,s=P(t),n=this._getInst(s[0]);e=null!=e?e:this._formatDate(n),n.input&&n.input.val(e),this._updateAlternate(n),(i=this._get(n,"onSelect"))?i.apply(n.input?n.input[0]:null,[e,n]):n.input&&n.input.trigger("change"),n.inline?this._updateDatepicker(n):(this._hideDatepicker(),this._lastInput=n.input[0],"object"!=typeof n.input[0]&&n.input.focus(),this._lastInput=null)},_updateAlternate:function(t){var e,i,s,n=this._get(t,"altField");n&&(e=this._get(t,"altFormat")||this._get(t,"dateFormat"),i=this._getDate(t),s=this.formatDate(e,i,this._getFormatConfig(t)),P(n).each(function(){P(this).val(s)}))},noWeekends:function(t){var e=t.getDay();return[0<e&&e<6,""]},iso8601Week:function(t){var e,i=new Date(t.getTime());return i.setDate(i.getDate()+4-(i.getDay()||7)),e=i.getTime(),i.setMonth(0),i.setDate(1),Math.floor(Math.round((e-i)/864e5)/7)+1},parseDate:function(i,o,t){if(null==i||null==o)throw"Invalid arguments";if(""===(o="object"==typeof o?o.toString():o+""))return null;var s,e,n,a,r=0,h=(t?t.shortYearCutoff:null)||this._defaults.shortYearCutoff,l="string"!=typeof h?h:(new Date).getFullYear()%100+parseInt(h,10),c=(t?t.dayNamesShort:null)||this._defaults.dayNamesShort,u=(t?t.dayNames:null)||this._defaults.dayNames,d=(t?t.monthNamesShort:null)||this._defaults.monthNamesShort,p=(t?t.monthNames:null)||this._defaults.monthNames,f=-1,m=-1,g=-1,v=-1,_=!1,b=function(t){var e=s+1<i.length&&i.charAt(s+1)===t;return e&&s++,e},y=function(t){var e=b(t),i="@"===t?14:"!"===t?20:"y"===t&&e?4:"o"===t?3:2,s=new RegExp("^\\d{"+("y"===t?i:1)+","+i+"}"),n=o.substring(r).match(s);if(!n)throw"Missing number at position "+r;return r+=n[0].length,parseInt(n[0],10)},w=function(t,e,i){var s=-1,n=P.map(b(t)?i:e,function(t,e){return[[e,t]]}).sort(function(t,e){return-(t[1].length-e[1].length)});if(P.each(n,function(t,e){var i=e[1];if(o.substr(r,i.length).toLowerCase()===i.toLowerCase())return s=e[0],r+=i.length,!1}),-1!==s)return s+1;throw"Unknown name at position "+r},x=function(){if(o.charAt(r)!==i.charAt(s))throw"Unexpected literal at position "+r;r++};for(s=0;s<i.length;s++)if(_)"'"!==i.charAt(s)||b("'")?x():_=!1;else switch(i.charAt(s)){case"d":g=y("d");break;case"D":w("D",c,u);break;case"o":v=y("o");break;case"m":m=y("m");break;case"M":m=w("M",d,p);break;case"y":f=y("y");break;case"@":f=(a=new Date(y("@"))).getFullYear(),m=a.getMonth()+1,g=a.getDate();break;case"!":f=(a=new Date((y("!")-this._ticksTo1970)/1e4)).getFullYear(),m=a.getMonth()+1,g=a.getDate();break;case"'":b("'")?x():_=!0;break;default:x()}if(r<o.length&&(n=o.substr(r),!/^\s+/.test(n)))throw"Extra/unparsed characters found in date: "+n;if(-1===f?f=(new Date).getFullYear():f<100&&(f+=(new Date).getFullYear()-(new Date).getFullYear()%100+(f<=l?0:-100)),-1<v)for(m=1,g=v;;){if(g<=(e=this._getDaysInMonth(f,m-1)))break;m++,g-=e}if((a=this._daylightSavingAdjust(new Date(f,m-1,g))).getFullYear()!==f||a.getMonth()+1!==m||a.getDate()!==g)throw"Invalid date";return a},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:24*(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925))*60*60*1e7,formatDate:function(i,t,e){if(!t)return"";var s,n=(e?e.dayNamesShort:null)||this._defaults.dayNamesShort,o=(e?e.dayNames:null)||this._defaults.dayNames,a=(e?e.monthNamesShort:null)||this._defaults.monthNamesShort,r=(e?e.monthNames:null)||this._defaults.monthNames,h=function(t){var e=s+1<i.length&&i.charAt(s+1)===t;return e&&s++,e},l=function(t,e,i){var s=""+e;if(h(t))for(;s.length<i;)s="0"+s;return s},c=function(t,e,i,s){return h(t)?s[e]:i[e]},u="",d=!1;if(t)for(s=0;s<i.length;s++)if(d)"'"!==i.charAt(s)||h("'")?u+=i.charAt(s):d=!1;else switch(i.charAt(s)){case"d":u+=l("d",t.getDate(),2);break;case"D":u+=c("D",t.getDay(),n,o);break;case"o":u+=l("o",Math.round((new Date(t.getFullYear(),t.getMonth(),t.getDate()).getTime()-new Date(t.getFullYear(),0,0).getTime())/864e5),3);break;case"m":u+=l("m",t.getMonth()+1,2);break;case"M":u+=c("M",t.getMonth(),a,r);break;case"y":u+=h("y")?t.getFullYear():(t.getYear()%100<10?"0":"")+t.getYear()%100;break;case"@":u+=t.getTime();break;case"!":u+=1e4*t.getTime()+this._ticksTo1970;break;case"'":h("'")?u+="'":d=!0;break;default:u+=i.charAt(s)}return u},_possibleChars:function(i){var s,t="",e=!1,n=function(t){var e=s+1<i.length&&i.charAt(s+1)===t;return e&&s++,e};for(s=0;s<i.length;s++)if(e)"'"!==i.charAt(s)||n("'")?t+=i.charAt(s):e=!1;else switch(i.charAt(s)){case"d":case"m":case"y":case"@":t+="0123456789";break;case"D":case"M":return null;case"'":n("'")?t+="'":e=!0;break;default:t+=i.charAt(s)}return t},_get:function(t,e){return void 0!==t.settings[e]?t.settings[e]:this._defaults[e]},_setDateFromField:function(t,e){if(t.input.val()!==t.lastVal){var i=this._get(t,"dateFormat"),s=t.lastVal=t.input?t.input.val():null,n=this._getDefaultDate(t),o=n,a=this._getFormatConfig(t);try{o=this.parseDate(i,s,a)||n}catch(t){s=e?"":s}t.selectedDay=o.getDate(),t.drawMonth=t.selectedMonth=o.getMonth(),t.drawYear=t.selectedYear=o.getFullYear(),t.currentDay=s?o.getDate():0,t.currentMonth=s?o.getMonth():0,t.currentYear=s?o.getFullYear():0,this._adjustInstDate(t)}},_getDefaultDate:function(t){return this._restrictMinMax(t,this._determineDate(t,this._get(t,"defaultDate"),new Date))},_determineDate:function(r,t,e){var i,s,n=null==t||""===t?e:"string"==typeof t?function(t){try{return P.datepicker.parseDate(P.datepicker._get(r,"dateFormat"),t,P.datepicker._getFormatConfig(r))}catch(t){}for(var e=(t.toLowerCase().match(/^c/)?P.datepicker._getDate(r):null)||new Date,i=e.getFullYear(),s=e.getMonth(),n=e.getDate(),o=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,a=o.exec(t);a;){switch(a[2]||"d"){case"d":case"D":n+=parseInt(a[1],10);break;case"w":case"W":n+=7*parseInt(a[1],10);break;case"m":case"M":s+=parseInt(a[1],10),n=Math.min(n,P.datepicker._getDaysInMonth(i,s));break;case"y":case"Y":i+=parseInt(a[1],10),n=Math.min(n,P.datepicker._getDaysInMonth(i,s))}a=o.exec(t)}return new Date(i,s,n)}(t):"number"==typeof t?isNaN(t)?e:(i=t,(s=new Date).setDate(s.getDate()+i),s):new Date(t.getTime());return(n=n&&"Invalid Date"===n.toString()?e:n)&&(n.setHours(0),n.setMinutes(0),n.setSeconds(0),n.setMilliseconds(0)),this._daylightSavingAdjust(n)},_daylightSavingAdjust:function(t){return t?(t.setHours(12<t.getHours()?t.getHours()+2:0),t):null},_setDate:function(t,e,i){var s=!e,n=t.selectedMonth,o=t.selectedYear,a=this._restrictMinMax(t,this._determineDate(t,e,new Date));t.selectedDay=t.currentDay=a.getDate(),t.drawMonth=t.selectedMonth=t.currentMonth=a.getMonth(),t.drawYear=t.selectedYear=t.currentYear=a.getFullYear(),n===t.selectedMonth&&o===t.selectedYear||i||this._notifyChange(t),this._adjustInstDate(t),t.input&&t.input.val(s?"":this._formatDate(t))},_getDate:function(t){return!t.currentYear||t.input&&""===t.input.val()?null:this._daylightSavingAdjust(new Date(t.currentYear,t.currentMonth,t.currentDay))},_attachHandlers:function(t){var e=this._get(t,"stepMonths"),i="#"+t.id.replace(/\\\\/g,"\\");t.dpDiv.find("[data-handler]").map(function(){var t={prev:function(){P.datepicker._adjustDate(i,-e,"M")},next:function(){P.datepicker._adjustDate(i,+e,"M")},hide:function(){P.datepicker._hideDatepicker()},today:function(){P.datepicker._gotoToday(i)},selectDay:function(){return P.datepicker._selectDay(i,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return P.datepicker._selectMonthYear(i,this,"M"),!1},selectYear:function(){return P.datepicker._selectMonthYear(i,this,"Y"),!1}};P(this).bind(this.getAttribute("data-event"),t[this.getAttribute("data-handler")])})},_generateHTML:function(t){var e,i,s,n,o,a,r,h,l,c,u,d,p,f,m,g,v,_,b,y,w,x,k,C,D,I,T,P,M,S,z,A,H,N,E,W,O,F,R,L=new Date,Y=this._daylightSavingAdjust(new Date(L.getFullYear(),L.getMonth(),L.getDate())),B=this._get(t,"isRTL"),j=this._get(t,"showButtonPanel"),q=this._get(t,"hideIfNoPrevNext"),K=this._get(t,"navigationAsDateFormat"),U=this._getNumberOfMonths(t),V=this._get(t,"showCurrentAtPos"),X=this._get(t,"stepMonths"),$=1!==U[0]||1!==U[1],G=this._daylightSavingAdjust(t.currentDay?new Date(t.currentYear,t.currentMonth,t.currentDay):new Date(9999,9,9)),Q=this._getMinMaxDate(t,"min"),J=this._getMinMaxDate(t,"max"),Z=t.drawMonth-V,tt=t.drawYear;if(Z<0&&(Z+=12,tt--),J)for(e=this._daylightSavingAdjust(new Date(J.getFullYear(),J.getMonth()-U[0]*U[1]+1,J.getDate())),e=Q&&e<Q?Q:e;this._daylightSavingAdjust(new Date(tt,Z,1))>e;)--Z<0&&(Z=11,tt--);for(t.drawMonth=Z,t.drawYear=tt,i=this._get(t,"prevText"),i=K?this.formatDate(i,this._daylightSavingAdjust(new Date(tt,Z-X,1)),this._getFormatConfig(t)):i,s=this._canAdjustMonth(t,-1,tt,Z)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(B?"e":"w")+"'>"+i+"</span></a>":q?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(B?"e":"w")+"'>"+i+"</span></a>",n=this._get(t,"nextText"),n=K?this.formatDate(n,this._daylightSavingAdjust(new Date(tt,Z+X,1)),this._getFormatConfig(t)):n,o=this._canAdjustMonth(t,1,tt,Z)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(B?"w":"e")+"'>"+n+"</span></a>":q?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(B?"w":"e")+"'>"+n+"</span></a>",a=this._get(t,"currentText"),r=this._get(t,"gotoCurrent")&&t.currentDay?G:Y,a=K?this.formatDate(a,r,this._getFormatConfig(t)):a,h=t.inline?"":"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(t,"closeText")+"</button>",l=j?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(B?h:"")+(this._isInRange(t,r)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+a+"</button>":"")+(B?"":h)+"</div>":"",c=parseInt(this._get(t,"firstDay"),10),c=isNaN(c)?0:c,u=this._get(t,"showWeek"),d=this._get(t,"dayNames"),p=this._get(t,"dayNamesMin"),f=this._get(t,"monthNames"),m=this._get(t,"monthNamesShort"),g=this._get(t,"beforeShowDay"),v=this._get(t,"showOtherMonths"),_=this._get(t,"selectOtherMonths"),b=this._getDefaultDate(t),y="",x=0;x<U[0];x++){for(k="",this.maxRows=4,C=0;C<U[1];C++){if(D=this._daylightSavingAdjust(new Date(tt,Z,t.selectedDay)),I=" ui-corner-all",T="",$){if(T+="<div class='ui-datepicker-group",1<U[1])switch(C){case 0:T+=" ui-datepicker-group-first",I=" ui-corner-"+(B?"right":"left");break;case U[1]-1:T+=" ui-datepicker-group-last",I=" ui-corner-"+(B?"left":"right");break;default:T+=" ui-datepicker-group-middle",I=""}T+="'>"}for(T+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+I+"'>"+(/all|left/.test(I)&&0===x?B?o:s:"")+(/all|right/.test(I)&&0===x?B?s:o:"")+this._generateMonthYearHeader(t,Z,tt,Q,J,0<x||0<C,f,m)+"</div><table class='ui-datepicker-calendar'><thead><tr>",P=u?"<th class='ui-datepicker-week-col'>"+this._get(t,"weekHeader")+"</th>":"",w=0;w<7;w++)P+="<th scope='col'"+(5<=(w+c+6)%7?" class='ui-datepicker-week-end'":"")+"><span title='"+d[M=(w+c)%7]+"'>"+p[M]+"</span></th>";for(T+=P+"</tr></thead><tbody>",S=this._getDaysInMonth(tt,Z),tt===t.selectedYear&&Z===t.selectedMonth&&(t.selectedDay=Math.min(t.selectedDay,S)),z=(this._getFirstDayOfMonth(tt,Z)-c+7)%7,A=Math.ceil((z+S)/7),H=$&&this.maxRows>A?this.maxRows:A,this.maxRows=H,N=this._daylightSavingAdjust(new Date(tt,Z,1-z)),E=0;E<H;E++){for(T+="<tr>",W=u?"<td class='ui-datepicker-week-col'>"+this._get(t,"calculateWeek")(N)+"</td>":"",w=0;w<7;w++)O=g?g.apply(t.input?t.input[0]:null,[N]):[!0,""],R=(F=N.getMonth()!==Z)&&!_||!O[0]||Q&&N<Q||J&&J<N,W+="<td class='"+(5<=(w+c+6)%7?" ui-datepicker-week-end":"")+(F?" ui-datepicker-other-month":"")+(N.getTime()===D.getTime()&&Z===t.selectedMonth&&t._keyEvent||b.getTime()===N.getTime()&&b.getTime()===D.getTime()?" "+this._dayOverClass:"")+(R?" "+this._unselectableClass+" ui-state-disabled":"")+(F&&!v?"":" "+O[1]+(N.getTime()===G.getTime()?" "+this._currentClass:"")+(N.getTime()===Y.getTime()?" ui-datepicker-today":""))+"'"+(F&&!v||!O[2]?"":" title='"+O[2].replace(/'/g,"&#39;")+"'")+(R?"":" data-handler='selectDay' data-event='click' data-month='"+N.getMonth()+"' data-year='"+N.getFullYear()+"'")+">"+(F&&!v?"&#xa0;":R?"<span class='ui-state-default'>"+N.getDate()+"</span>":"<a class='ui-state-default"+(N.getTime()===Y.getTime()?" ui-state-highlight":"")+(N.getTime()===G.getTime()?" ui-state-active":"")+(F?" ui-priority-secondary":"")+"' href='#'>"+N.getDate()+"</a>")+"</td>",N.setDate(N.getDate()+1),N=this._daylightSavingAdjust(N);T+=W+"</tr>"}11<++Z&&(Z=0,tt++),k+=T+="</tbody></table>"+($?"</div>"+(0<U[0]&&C===U[1]-1?"<div class='ui-datepicker-row-break'></div>":""):"")}y+=k}return y+=l,t._keyEvent=!1,y},_generateMonthYearHeader:function(t,e,i,s,n,o,a,r){var h,l,c,u,d,p,f,m,g=this._get(t,"changeMonth"),v=this._get(t,"changeYear"),_=this._get(t,"showMonthAfterYear"),b="<div class='ui-datepicker-title'>",y="";if(o||!g)y+="<span class='ui-datepicker-month'>"+a[e]+"</span>";else{for(h=s&&s.getFullYear()===i,l=n&&n.getFullYear()===i,y+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>",c=0;c<12;c++)(!h||c>=s.getMonth())&&(!l||c<=n.getMonth())&&(y+="<option value='"+c+"'"+(c===e?" selected='selected'":"")+">"+r[c]+"</option>");y+="</select>"}if(_||(b+=y+(!o&&g&&v?"":"&#xa0;")),!t.yearshtml)if(t.yearshtml="",o||!v)b+="<span class='ui-datepicker-year'>"+i+"</span>";else{for(u=this._get(t,"yearRange").split(":"),d=(new Date).getFullYear(),f=(p=function(t){var e=t.match(/c[+\-].*/)?i+parseInt(t.substring(1),10):t.match(/[+\-].*/)?d+parseInt(t,10):parseInt(t,10);return isNaN(e)?d:e})(u[0]),m=Math.max(f,p(u[1]||"")),f=s?Math.max(f,s.getFullYear()):f,m=n?Math.min(m,n.getFullYear()):m,t.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";f<=m;f++)t.yearshtml+="<option value='"+f+"'"+(f===i?" selected='selected'":"")+">"+f+"</option>";t.yearshtml+="</select>",b+=t.yearshtml,t.yearshtml=null}return b+=this._get(t,"yearSuffix"),_&&(b+=(!o&&g&&v?"":"&#xa0;")+y),b+="</div>"},_adjustInstDate:function(t,e,i){var s=t.drawYear+("Y"===i?e:0),n=t.drawMonth+("M"===i?e:0),o=Math.min(t.selectedDay,this._getDaysInMonth(s,n))+("D"===i?e:0),a=this._restrictMinMax(t,this._daylightSavingAdjust(new Date(s,n,o)));t.selectedDay=a.getDate(),t.drawMonth=t.selectedMonth=a.getMonth(),t.drawYear=t.selectedYear=a.getFullYear(),"M"!==i&&"Y"!==i||this._notifyChange(t)},_restrictMinMax:function(t,e){var i=this._getMinMaxDate(t,"min"),s=this._getMinMaxDate(t,"max"),n=i&&e<i?i:e;return s&&s<n?s:n},_notifyChange:function(t){var e=this._get(t,"onChangeMonthYear");e&&e.apply(t.input?t.input[0]:null,[t.selectedYear,t.selectedMonth+1,t])},_getNumberOfMonths:function(t){var e=this._get(t,"numberOfMonths");return null==e?[1,1]:"number"==typeof e?[1,e]:e},_getMinMaxDate:function(t,e){return this._determineDate(t,this._get(t,e+"Date"),null)},_getDaysInMonth:function(t,e){return 32-this._daylightSavingAdjust(new Date(t,e,32)).getDate()},_getFirstDayOfMonth:function(t,e){return new Date(t,e,1).getDay()},_canAdjustMonth:function(t,e,i,s){var n=this._getNumberOfMonths(t),o=this._daylightSavingAdjust(new Date(i,s+(e<0?e:n[0]*n[1]),1));return e<0&&o.setDate(this._getDaysInMonth(o.getFullYear(),o.getMonth())),this._isInRange(t,o)},_isInRange:function(t,e){var i,s,n=this._getMinMaxDate(t,"min"),o=this._getMinMaxDate(t,"max"),a=null,r=null,h=this._get(t,"yearRange");return h&&(i=h.split(":"),s=(new Date).getFullYear(),a=parseInt(i[0],10),r=parseInt(i[1],10),i[0].match(/[+\-].*/)&&(a+=s),i[1].match(/[+\-].*/)&&(r+=s)),(!n||e.getTime()>=n.getTime())&&(!o||e.getTime()<=o.getTime())&&(!a||e.getFullYear()>=a)&&(!r||e.getFullYear()<=r)},_getFormatConfig:function(t){var e=this._get(t,"shortYearCutoff");return{shortYearCutoff:e="string"!=typeof e?e:(new Date).getFullYear()%100+parseInt(e,10),dayNamesShort:this._get(t,"dayNamesShort"),dayNames:this._get(t,"dayNames"),monthNamesShort:this._get(t,"monthNamesShort"),monthNames:this._get(t,"monthNames")}},_formatDate:function(t,e,i,s){e||(t.currentDay=t.selectedDay,t.currentMonth=t.selectedMonth,t.currentYear=t.selectedYear);var n=e?"object"==typeof e?e:this._daylightSavingAdjust(new Date(s,i,e)):this._daylightSavingAdjust(new Date(t.currentYear,t.currentMonth,t.currentDay));return this.formatDate(this._get(t,"dateFormat"),n,this._getFormatConfig(t))}}),P.fn.datepicker=function(t){if(!this.length)return this;P.datepicker.initialized||(P(document).mousedown(P.datepicker._checkExternalClick),P.datepicker.initialized=!0),0===P("#"+P.datepicker._mainDivId).length&&P("body").append(P.datepicker.dpDiv);var e=Array.prototype.slice.call(arguments,1);return"string"!=typeof t||"isDisabled"!==t&&"getDate"!==t&&"widget"!==t?"option"===t&&2===arguments.length&&"string"==typeof arguments[1]?P.datepicker["_"+t+"Datepicker"].apply(P.datepicker,[this[0]].concat(e)):this.each(function(){"string"==typeof t?P.datepicker["_"+t+"Datepicker"].apply(P.datepicker,[this].concat(e)):P.datepicker._attachDatepicker(this,t)}):P.datepicker["_"+t+"Datepicker"].apply(P.datepicker,[this[0]].concat(e))},P.datepicker=new g,P.datepicker.initialized=!1,P.datepicker.uuid=(new Date).getTime(),P.datepicker.version="1.11.2";P.datepicker,P.widget("ui.dialog",{version:"1.11.2",options:{appendTo:"body",autoOpen:!0,buttons:[],closeOnEscape:!0,closeText:"Close",dialogClass:"",draggable:!0,hide:null,height:"auto",maxHeight:null,maxWidth:null,minHeight:150,minWidth:150,modal:!1,position:{my:"center",at:"center",of:window,collision:"fit",using:function(t){var e=P(this).css(t).offset().top;e<0&&P(this).css("top",t.top-e)}},resizable:!0,show:null,title:null,width:300,beforeClose:null,close:null,drag:null,dragStart:null,dragStop:null,focus:null,open:null,resize:null,resizeStart:null,resizeStop:null},sizeRelatedOptions:{buttons:!0,height:!0,maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0,width:!0},resizableRelatedOptions:{maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0},_create:function(){this.originalCss={display:this.element[0].style.display,width:this.element[0].style.width,minHeight:this.element[0].style.minHeight,maxHeight:this.element[0].style.maxHeight,height:this.element[0].style.height},this.originalPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)},this.originalTitle=this.element.attr("title"),this.options.title=this.options.title||this.originalTitle,this._createWrapper(),this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog),this._createTitlebar(),this._createButtonPane(),this.options.draggable&&P.fn.draggable&&this._makeDraggable(),this.options.resizable&&P.fn.resizable&&this._makeResizable(),this._isOpen=!1,this._trackFocus()},_init:function(){this.options.autoOpen&&this.open()},_appendTo:function(){var t=this.options.appendTo;return t&&(t.jquery||t.nodeType)?P(t):this.document.find(t||"body").eq(0)},_destroy:function(){var t,e=this.originalPosition;this._destroyOverlay(),this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(),this.uiDialog.stop(!0,!0).remove(),this.originalTitle&&this.element.attr("title",this.originalTitle),(t=e.parent.children().eq(e.index)).length&&t[0]!==this.element[0]?t.before(this.element):e.parent.append(this.element)},widget:function(){return this.uiDialog},disable:P.noop,enable:P.noop,close:function(t){var e,i=this;if(this._isOpen&&!1!==this._trigger("beforeClose",t)){if(this._isOpen=!1,this._focusedElement=null,this._destroyOverlay(),this._untrackInstance(),!this.opener.filter(":focusable").focus().length)try{(e=this.document[0].activeElement)&&"body"!==e.nodeName.toLowerCase()&&P(e).blur()}catch(t){}this._hide(this.uiDialog,this.options.hide,function(){i._trigger("close",t)})}},isOpen:function(){return this._isOpen},moveToTop:function(){this._moveToTop()},_moveToTop:function(t,e){var i=!1,s=this.uiDialog.siblings(".ui-front:visible").map(function(){return+P(this).css("z-index")}).get(),n=Math.max.apply(null,s);return n>=+this.uiDialog.css("z-index")&&(this.uiDialog.css("z-index",n+1),i=!0),i&&!e&&this._trigger("focus",t),i},open:function(){var t=this;this._isOpen?this._moveToTop()&&this._focusTabbable():(this._isOpen=!0,this.opener=P(this.document[0].activeElement),this._size(),this._position(),this._createOverlay(),this._moveToTop(null,!0),this.overlay&&this.overlay.css("z-index",this.uiDialog.css("z-index")-1),this._show(this.uiDialog,this.options.show,function(){t._focusTabbable(),t._trigger("focus")}),this._makeFocusTarget(),this._trigger("open"))},_focusTabbable:function(){var t=this._focusedElement;t||(t=this.element.find("[autofocus]")),t.length||(t=this.element.find(":tabbable")),t.length||(t=this.uiDialogButtonPane.find(":tabbable")),t.length||(t=this.uiDialogTitlebarClose.filter(":tabbable")),t.length||(t=this.uiDialog),t.eq(0).focus()},_keepFocus:function(t){function e(){var t=this.document[0].activeElement;this.uiDialog[0]===t||P.contains(this.uiDialog[0],t)||this._focusTabbable()}t.preventDefault(),e.call(this),this._delay(e)},_createWrapper:function(){this.uiDialog=P("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front "+this.options.dialogClass).hide().attr({tabIndex:-1,role:"dialog"}).appendTo(this._appendTo()),this._on(this.uiDialog,{keydown:function(t){if(this.options.closeOnEscape&&!t.isDefaultPrevented()&&t.keyCode&&t.keyCode===P.ui.keyCode.ESCAPE)return t.preventDefault(),void this.close(t);if(t.keyCode===P.ui.keyCode.TAB&&!t.isDefaultPrevented()){var e=this.uiDialog.find(":tabbable"),i=e.filter(":first"),s=e.filter(":last");t.target!==s[0]&&t.target!==this.uiDialog[0]||t.shiftKey?t.target!==i[0]&&t.target!==this.uiDialog[0]||!t.shiftKey||(this._delay(function(){s.focus()}),t.preventDefault()):(this._delay(function(){i.focus()}),t.preventDefault())}},mousedown:function(t){this._moveToTop(t)&&this._focusTabbable()}}),this.element.find("[aria-describedby]").length||this.uiDialog.attr({"aria-describedby":this.element.uniqueId().attr("id")})},_createTitlebar:function(){var t;this.uiDialogTitlebar=P("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog),this._on(this.uiDialogTitlebar,{mousedown:function(t){P(t.target).closest(".ui-dialog-titlebar-close")||this.uiDialog.focus()}}),this.uiDialogTitlebarClose=P("<button type='button'></button>").button({label:this.options.closeText,icons:{primary:"ui-icon-closethick"},text:!1}).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar),this._on(this.uiDialogTitlebarClose,{click:function(t){t.preventDefault(),this.close(t)}}),t=P("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar),this._title(t),this.uiDialog.attr({"aria-labelledby":t.attr("id")})},_title:function(t){this.options.title||t.html("&#160;"),t.text(this.options.title)},_createButtonPane:function(){this.uiDialogButtonPane=P("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),this.uiButtonSet=P("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane),this._createButtons()},_createButtons:function(){var n=this,t=this.options.buttons;this.uiDialogButtonPane.remove(),this.uiButtonSet.empty(),P.isEmptyObject(t)||P.isArray(t)&&!t.length?this.uiDialog.removeClass("ui-dialog-buttons"):(P.each(t,function(t,e){var i,s;e=P.isFunction(e)?{click:e,text:t}:e,e=P.extend({type:"button"},e),i=e.click,e.click=function(){i.apply(n.element[0],arguments)},s={icons:e.icons,text:e.showText},delete e.icons,delete e.showText,P("<button></button>",e).button(s).appendTo(n.uiButtonSet)}),this.uiDialog.addClass("ui-dialog-buttons"),this.uiDialogButtonPane.appendTo(this.uiDialog))},_makeDraggable:function(){var n=this,o=this.options;function a(t){return{position:t.position,offset:t.offset}}this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(t,e){P(this).addClass("ui-dialog-dragging"),n._blockFrames(),n._trigger("dragStart",t,a(e))},drag:function(t,e){n._trigger("drag",t,a(e))},stop:function(t,e){var i=e.offset.left-n.document.scrollLeft(),s=e.offset.top-n.document.scrollTop();o.position={my:"left top",at:"left"+(0<=i?"+":"")+i+" top"+(0<=s?"+":"")+s,of:n.window},P(this).removeClass("ui-dialog-dragging"),n._unblockFrames(),n._trigger("dragStop",t,a(e))}})},_makeResizable:function(){var o=this,a=this.options,t=a.resizable,e=this.uiDialog.css("position"),i="string"==typeof t?t:"n,e,s,w,se,sw,ne,nw";function r(t){return{originalPosition:t.originalPosition,originalSize:t.originalSize,position:t.position,size:t.size}}this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:a.maxWidth,maxHeight:a.maxHeight,minWidth:a.minWidth,minHeight:this._minHeight(),handles:i,start:function(t,e){P(this).addClass("ui-dialog-resizing"),o._blockFrames(),o._trigger("resizeStart",t,r(e))},resize:function(t,e){o._trigger("resize",t,r(e))},stop:function(t,e){var i=o.uiDialog.offset(),s=i.left-o.document.scrollLeft(),n=i.top-o.document.scrollTop();a.height=o.uiDialog.height(),a.width=o.uiDialog.width(),a.position={my:"left top",at:"left"+(0<=s?"+":"")+s+" top"+(0<=n?"+":"")+n,of:o.window},P(this).removeClass("ui-dialog-resizing"),o._unblockFrames(),o._trigger("resizeStop",t,r(e))}}).css("position",e)},_trackFocus:function(){this._on(this.widget(),{focusin:function(t){this._makeFocusTarget(),this._focusedElement=P(t.target)}})},_makeFocusTarget:function(){this._untrackInstance(),this._trackingInstances().unshift(this)},_untrackInstance:function(){var t=this._trackingInstances(),e=P.inArray(this,t);-1!==e&&t.splice(e,1)},_trackingInstances:function(){var t=this.document.data("ui-dialog-instances");return t||(t=[],this.document.data("ui-dialog-instances",t)),t},_minHeight:function(){var t=this.options;return"auto"===t.height?t.minHeight:Math.min(t.minHeight,t.height)},_position:function(){var t=this.uiDialog.is(":visible");t||this.uiDialog.show(),this.uiDialog.position(this.options.position),t||this.uiDialog.hide()},_setOptions:function(t){var i=this,s=!1,n={};P.each(t,function(t,e){i._setOption(t,e),t in i.sizeRelatedOptions&&(s=!0),t in i.resizableRelatedOptions&&(n[t]=e)}),s&&(this._size(),this._position()),this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option",n)},_setOption:function(t,e){var i,s,n=this.uiDialog;"dialogClass"===t&&n.removeClass(this.options.dialogClass).addClass(e),"disabled"!==t&&(this._super(t,e),"appendTo"===t&&this.uiDialog.appendTo(this._appendTo()),"buttons"===t&&this._createButtons(),"closeText"===t&&this.uiDialogTitlebarClose.button({label:""+e}),"draggable"===t&&((i=n.is(":data(ui-draggable)"))&&!e&&n.draggable("destroy"),!i&&e&&this._makeDraggable()),"position"===t&&this._position(),"resizable"===t&&((s=n.is(":data(ui-resizable)"))&&!e&&n.resizable("destroy"),s&&"string"==typeof e&&n.resizable("option","handles",e),s||!1===e||this._makeResizable()),"title"===t&&this._title(this.uiDialogTitlebar.find(".ui-dialog-title")))},_size:function(){var t,e,i,s=this.options;this.element.show().css({width:"auto",minHeight:0,maxHeight:"none",height:0}),s.minWidth>s.width&&(s.width=s.minWidth),t=this.uiDialog.css({height:"auto",width:s.width}).outerHeight(),e=Math.max(0,s.minHeight-t),i="number"==typeof s.maxHeight?Math.max(0,s.maxHeight-t):"none","auto"===s.height?this.element.css({minHeight:e,maxHeight:i,height:"auto"}):this.element.height(Math.max(0,s.height-t)),this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())},_blockFrames:function(){this.iframeBlocks=this.document.find("iframe").map(function(){var t=P(this);return P("<div>").css({position:"absolute",width:t.outerWidth(),height:t.outerHeight()}).appendTo(t.parent()).offset(t.offset())[0]})},_unblockFrames:function(){this.iframeBlocks&&(this.iframeBlocks.remove(),delete this.iframeBlocks)},_allowInteraction:function(t){return!!P(t.target).closest(".ui-dialog").length||!!P(t.target).closest(".ui-datepicker").length},_createOverlay:function(){if(this.options.modal){var e=!0;this._delay(function(){e=!1}),this.document.data("ui-dialog-overlays")||this._on(this.document,{focusin:function(t){e||this._allowInteraction(t)||(t.preventDefault(),this._trackingInstances()[0]._focusTabbable())}}),this.overlay=P("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()),this._on(this.overlay,{mousedown:"_keepFocus"}),this.document.data("ui-dialog-overlays",(this.document.data("ui-dialog-overlays")||0)+1)}},_destroyOverlay:function(){if(this.options.modal&&this.overlay){var t=this.document.data("ui-dialog-overlays")-1;t?this.document.data("ui-dialog-overlays",t):this.document.unbind("focusin").removeData("ui-dialog-overlays"),this.overlay.remove(),this.overlay=null}}}),P.widget("ui.progressbar",{version:"1.11.2",options:{max:100,value:0,change:null,complete:null},min:0,_create:function(){this.oldValue=this.options.value=this._constrainedValue(),this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min}),this.valueDiv=P("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element),this._refreshValue()},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.valueDiv.remove()},value:function(t){if(void 0===t)return this.options.value;this.options.value=this._constrainedValue(t),this._refreshValue()},_constrainedValue:function(t){return void 0===t&&(t=this.options.value),this.indeterminate=!1===t,"number"!=typeof t&&(t=0),!this.indeterminate&&Math.min(this.options.max,Math.max(this.min,t))},_setOptions:function(t){var e=t.value;delete t.value,this._super(t),this.options.value=this._constrainedValue(e),this._refreshValue()},_setOption:function(t,e){"max"===t&&(e=Math.max(this.min,e)),"disabled"===t&&this.element.toggleClass("ui-state-disabled",!!e).attr("aria-disabled",e),this._super(t,e)},_percentage:function(){return this.indeterminate?100:100*(this.options.value-this.min)/(this.options.max-this.min)},_refreshValue:function(){var t=this.options.value,e=this._percentage();this.valueDiv.toggle(this.indeterminate||t>this.min).toggleClass("ui-corner-right",t===this.options.max).width(e.toFixed(0)+"%"),this.element.toggleClass("ui-progressbar-indeterminate",this.indeterminate),this.indeterminate?(this.element.removeAttr("aria-valuenow"),this.overlayDiv||(this.overlayDiv=P("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))):(this.element.attr({"aria-valuemax":this.options.max,"aria-valuenow":t}),this.overlayDiv&&(this.overlayDiv.remove(),this.overlayDiv=null)),this.oldValue!==t&&(this.oldValue=t,this._trigger("change")),t===this.options.max&&this._trigger("complete")}}),P.widget("ui.selectmenu",{version:"1.11.2",defaultElement:"<select>",options:{appendTo:null,disabled:null,icons:{button:"ui-icon-triangle-1-s"},position:{my:"left top",at:"left bottom",collision:"none"},width:null,change:null,close:null,focus:null,open:null,select:null},_create:function(){var t=this.element.uniqueId().attr("id");this.ids={element:t,button:t+"-button",menu:t+"-menu"},this._drawButton(),this._drawMenu(),this.options.disabled&&this.disable()},_drawButton:function(){var t=this,e=this.element.attr("tabindex");this.label=P("label[for='"+this.ids.element+"']").attr("for",this.ids.button),this._on(this.label,{click:function(t){this.button.focus(),t.preventDefault()}}),this.element.hide(),this.button=P("<span>",{class:"ui-selectmenu-button ui-widget ui-state-default ui-corner-all",tabindex:e||this.options.disabled?-1:0,id:this.ids.button,role:"combobox","aria-expanded":"false","aria-autocomplete":"list","aria-owns":this.ids.menu,"aria-haspopup":"true"}).insertAfter(this.element),P("<span>",{class:"ui-icon "+this.options.icons.button}).prependTo(this.button),this.buttonText=P("<span>",{class:"ui-selectmenu-text"}).appendTo(this.button),this._setText(this.buttonText,this.element.find("option:selected").text()),this._resizeButton(),this._on(this.button,this._buttonEvents),this.button.one("focusin",function(){t.menuItems||t._refreshMenu()}),this._hoverable(this.button),this._focusable(this.button)},_drawMenu:function(){var s=this;this.menu=P("<ul>",{"aria-hidden":"true","aria-labelledby":this.ids.button,id:this.ids.menu}),this.menuWrap=P("<div>",{class:"ui-selectmenu-menu ui-front"}).append(this.menu).appendTo(this._appendTo()),this.menuInstance=this.menu.menu({role:"listbox",select:function(t,e){t.preventDefault(),s._setSelection(),s._select(e.item.data("ui-selectmenu-item"),t)},focus:function(t,e){var i=e.item.data("ui-selectmenu-item");null!=s.focusIndex&&i.index!==s.focusIndex&&(s._trigger("focus",t,{item:i}),s.isOpen||s._select(i,t)),s.focusIndex=i.index,s.button.attr("aria-activedescendant",s.menuItems.eq(i.index).attr("id"))}}).menu("instance"),this.menu.addClass("ui-corner-bottom").removeClass("ui-corner-all"),this.menuInstance._off(this.menu,"mouseleave"),this.menuInstance._closeOnDocumentClick=function(){return!1},this.menuInstance._isDivider=function(){return!1}},refresh:function(){this._refreshMenu(),this._setText(this.buttonText,this._getSelectedItem().text()),this.options.width||this._resizeButton()},_refreshMenu:function(){this.menu.empty();var t,e=this.element.find("option");e.length&&(this._parseOptions(e),this._renderMenu(this.menu,this.items),this.menuInstance.refresh(),this.menuItems=this.menu.find("li").not(".ui-selectmenu-optgroup"),t=this._getSelectedItem(),this.menuInstance.focus(null,t),this._setAria(t.data("ui-selectmenu-item")),this._setOption("disabled",this.element.prop("disabled")))},open:function(t){this.options.disabled||(this.menuItems?(this.menu.find(".ui-state-focus").removeClass("ui-state-focus"),this.menuInstance.focus(null,this._getSelectedItem())):this._refreshMenu(),this.isOpen=!0,this._toggleAttr(),this._resizeMenu(),this._position(),this._on(this.document,this._documentClick),this._trigger("open",t))},_position:function(){this.menuWrap.position(P.extend({of:this.button},this.options.position))},close:function(t){this.isOpen&&(this.isOpen=!1,this._toggleAttr(),this.range=null,this._off(this.document),this._trigger("close",t))},widget:function(){return this.button},menuWidget:function(){return this.menu},_renderMenu:function(i,t){var s=this,n="";P.each(t,function(t,e){e.optgroup!==n&&(P("<li>",{class:"ui-selectmenu-optgroup ui-menu-divider"+(e.element.parent("optgroup").prop("disabled")?" ui-state-disabled":""),text:e.optgroup}).appendTo(i),n=e.optgroup),s._renderItemData(i,e)})},_renderItemData:function(t,e){return this._renderItem(t,e).data("ui-selectmenu-item",e)},_renderItem:function(t,e){var i=P("<li>");return e.disabled&&i.addClass("ui-state-disabled"),this._setText(i,e.label),i.appendTo(t)},_setText:function(t,e){e?t.text(e):t.html("&#160;")},_move:function(t,e){var i,s,n=".ui-menu-item";this.isOpen?i=this.menuItems.eq(this.focusIndex):(i=this.menuItems.eq(this.element[0].selectedIndex),n+=":not(.ui-state-disabled)"),(s="first"===t||"last"===t?i["first"===t?"prevAll":"nextAll"](n).eq(-1):i[t+"All"](n).eq(0)).length&&this.menuInstance.focus(e,s)},_getSelectedItem:function(){return this.menuItems.eq(this.element[0].selectedIndex)},_toggle:function(t){this[this.isOpen?"close":"open"](t)},_setSelection:function(){var t;this.range&&(window.getSelection?((t=window.getSelection()).removeAllRanges(),t.addRange(this.range)):this.range.select(),this.button.focus())},_documentClick:{mousedown:function(t){this.isOpen&&(P(t.target).closest(".ui-selectmenu-menu, #"+this.ids.button).length||this.close(t))}},_buttonEvents:{mousedown:function(){var t;window.getSelection?(t=window.getSelection()).rangeCount&&(this.range=t.getRangeAt(0)):this.range=document.selection.createRange()},click:function(t){this._setSelection(),this._toggle(t)},keydown:function(t){var e=!0;switch(t.keyCode){case P.ui.keyCode.TAB:case P.ui.keyCode.ESCAPE:this.close(t),e=!1;break;case P.ui.keyCode.ENTER:this.isOpen&&this._selectFocusedItem(t);break;case P.ui.keyCode.UP:t.altKey?this._toggle(t):this._move("prev",t);break;case P.ui.keyCode.DOWN:t.altKey?this._toggle(t):this._move("next",t);break;case P.ui.keyCode.SPACE:this.isOpen?this._selectFocusedItem(t):this._toggle(t);break;case P.ui.keyCode.LEFT:this._move("prev",t);break;case P.ui.keyCode.RIGHT:this._move("next",t);break;case P.ui.keyCode.HOME:case P.ui.keyCode.PAGE_UP:this._move("first",t);break;case P.ui.keyCode.END:case P.ui.keyCode.PAGE_DOWN:this._move("last",t);break;default:this.menu.trigger(t),e=!1}e&&t.preventDefault()}},_selectFocusedItem:function(t){var e=this.menuItems.eq(this.focusIndex);e.hasClass("ui-state-disabled")||this._select(e.data("ui-selectmenu-item"),t)},_select:function(t,e){var i=this.element[0].selectedIndex;this.element[0].selectedIndex=t.index,this._setText(this.buttonText,t.label),this._setAria(t),this._trigger("select",e,{item:t}),t.index!==i&&this._trigger("change",e,{item:t}),this.close(e)},_setAria:function(t){var e=this.menuItems.eq(t.index).attr("id");this.button.attr({"aria-labelledby":e,"aria-activedescendant":e}),this.menu.attr("aria-activedescendant",e)},_setOption:function(t,e){"icons"===t&&this.button.find("span.ui-icon").removeClass(this.options.icons.button).addClass(e.button),this._super(t,e),"appendTo"===t&&this.menuWrap.appendTo(this._appendTo()),"disabled"===t&&(this.menuInstance.option("disabled",e),this.button.toggleClass("ui-state-disabled",e).attr("aria-disabled",e),this.element.prop("disabled",e),e?(this.button.attr("tabindex",-1),this.close()):this.button.attr("tabindex",0)),"width"===t&&this._resizeButton()},_appendTo:function(){var t=this.options.appendTo;return t&&(t=t.jquery||t.nodeType?P(t):this.document.find(t).eq(0)),t&&t[0]||(t=this.element.closest(".ui-front")),t.length||(t=this.document[0].body),t},_toggleAttr:function(){this.button.toggleClass("ui-corner-top",this.isOpen).toggleClass("ui-corner-all",!this.isOpen).attr("aria-expanded",this.isOpen),this.menuWrap.toggleClass("ui-selectmenu-open",this.isOpen),this.menu.attr("aria-hidden",!this.isOpen)},_resizeButton:function(){var t=this.options.width;t||(t=this.element.show().outerWidth(),this.element.hide()),this.button.outerWidth(t)},_resizeMenu:function(){this.menu.outerWidth(Math.max(this.button.outerWidth(),this.menu.width("").outerWidth()+1))},_getCreateOptions:function(){return{disabled:this.element.prop("disabled")}},_parseOptions:function(t){var n=[];t.each(function(t,e){var i=P(e),s=i.parent("optgroup");n.push({element:i,index:t,value:i.attr("value"),label:i.text(),optgroup:s.attr("label")||"",disabled:s.prop("disabled")||i.prop("disabled")})}),this.items=n},_destroy:function(){this.menuWrap.remove(),this.button.remove(),this.element.show(),this.element.removeUniqueId(),this.label.attr("for",this.ids.element)}}),P.widget("ui.slider",P.ui.mouse,{version:"1.11.2",widgetEventPrefix:"slide",options:{animate:!1,distance:0,max:100,min:0,orientation:"horizontal",range:!1,step:1,value:0,values:null,change:null,slide:null,start:null,stop:null},numPages:5,_create:function(){this._keySliding=!1,this._mouseSliding=!1,this._animateOff=!0,this._handleIndex=null,this._detectOrientation(),this._mouseInit(),this._calculateNewMax(),this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget ui-widget-content ui-corner-all"),this._refresh(),this._setOption("disabled",this.options.disabled),this._animateOff=!1},_refresh:function(){this._createRange(),this._createHandles(),this._setupEvents(),this._refreshValue()},_createHandles:function(){var t,e,i=this.options,s=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),n=[];for(e=i.values&&i.values.length||1,s.length>e&&(s.slice(e).remove(),s=s.slice(0,e)),t=s.length;t<e;t++)n.push("<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>");this.handles=s.add(P(n.join("")).appendTo(this.element)),this.handle=this.handles.eq(0),this.handles.each(function(t){P(this).data("ui-slider-handle-index",t)})},_createRange:function(){var t=this.options,e="";t.range?(!0===t.range&&(t.values?t.values.length&&2!==t.values.length?t.values=[t.values[0],t.values[0]]:P.isArray(t.values)&&(t.values=t.values.slice(0)):t.values=[this._valueMin(),this._valueMin()]),this.range&&this.range.length?this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({left:"",bottom:""}):(this.range=P("<div></div>").appendTo(this.element),e="ui-slider-range ui-widget-header ui-corner-all"),this.range.addClass(e+("min"===t.range||"max"===t.range?" ui-slider-range-"+t.range:""))):(this.range&&this.range.remove(),this.range=null)},_setupEvents:function(){this._off(this.handles),this._on(this.handles,this._handleEvents),this._hoverable(this.handles),this._focusable(this.handles)},_destroy:function(){this.handles.remove(),this.range&&this.range.remove(),this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"),this._mouseDestroy()},_mouseCapture:function(t){var e,i,s,n,o,a,r,h=this,l=this.options;return!l.disabled&&(this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()},this.elementOffset=this.element.offset(),e={x:t.pageX,y:t.pageY},i=this._normValueFromMouse(e),s=this._valueMax()-this._valueMin()+1,this.handles.each(function(t){var e=Math.abs(i-h.values(t));(e<s||s===e&&(t===h._lastChangedValue||h.values(t)===l.min))&&(s=e,n=P(this),o=t)}),!1!==this._start(t,o)&&(this._mouseSliding=!0,this._handleIndex=o,n.addClass("ui-state-active").focus(),a=n.offset(),r=!P(t.target).parents().addBack().is(".ui-slider-handle"),this._clickOffset=r?{left:0,top:0}:{left:t.pageX-a.left-n.width()/2,top:t.pageY-a.top-n.height()/2-(parseInt(n.css("borderTopWidth"),10)||0)-(parseInt(n.css("borderBottomWidth"),10)||0)+(parseInt(n.css("marginTop"),10)||0)},this.handles.hasClass("ui-state-hover")||this._slide(t,o,i),this._animateOff=!0))},_mouseStart:function(){return!0},_mouseDrag:function(t){var e={x:t.pageX,y:t.pageY},i=this._normValueFromMouse(e);return this._slide(t,this._handleIndex,i),!1},_mouseStop:function(t){return this.handles.removeClass("ui-state-active"),this._mouseSliding=!1,this._stop(t,this._handleIndex),this._change(t,this._handleIndex),this._handleIndex=null,this._clickOffset=null,this._animateOff=!1},_detectOrientation:function(){this.orientation="vertical"===this.options.orientation?"vertical":"horizontal"},_normValueFromMouse:function(t){var e,i,s,n;return 1<(i=("horizontal"===this.orientation?(e=this.elementSize.width,t.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)):(e=this.elementSize.height,t.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)))/e)&&(i=1),i<0&&(i=0),"vertical"===this.orientation&&(i=1-i),s=this._valueMax()-this._valueMin(),n=this._valueMin()+i*s,this._trimAlignValue(n)},_start:function(t,e){var i={handle:this.handles[e],value:this.value()};return this.options.values&&this.options.values.length&&(i.value=this.values(e),i.values=this.values()),this._trigger("start",t,i)},_slide:function(t,e,i){var s,n,o;this.options.values&&this.options.values.length?(s=this.values(e?0:1),2===this.options.values.length&&!0===this.options.range&&(0===e&&s<i||1===e&&i<s)&&(i=s),i!==this.values(e)&&((n=this.values())[e]=i,o=this._trigger("slide",t,{handle:this.handles[e],value:i,values:n}),s=this.values(e?0:1),!1!==o&&this.values(e,i))):i!==this.value()&&!1!==(o=this._trigger("slide",t,{handle:this.handles[e],value:i}))&&this.value(i)},_stop:function(t,e){var i={handle:this.handles[e],value:this.value()};this.options.values&&this.options.values.length&&(i.value=this.values(e),i.values=this.values()),this._trigger("stop",t,i)},_change:function(t,e){if(!this._keySliding&&!this._mouseSliding){var i={handle:this.handles[e],value:this.value()};this.options.values&&this.options.values.length&&(i.value=this.values(e),i.values=this.values()),this._lastChangedValue=e,this._trigger("change",t,i)}},value:function(t){return arguments.length?(this.options.value=this._trimAlignValue(t),this._refreshValue(),void this._change(null,0)):this._value()},values:function(t,e){var i,s,n;if(1<arguments.length)return this.options.values[t]=this._trimAlignValue(e),this._refreshValue(),void this._change(null,t);if(!arguments.length)return this._values();if(!P.isArray(t))return this.options.values&&this.options.values.length?this._values(t):this.value();for(i=this.options.values,s=t,n=0;n<i.length;n+=1)i[n]=this._trimAlignValue(s[n]),this._change(null,n);this._refreshValue()},_setOption:function(t,e){var i,s=0;switch("range"===t&&!0===this.options.range&&("min"===e?(this.options.value=this._values(0),this.options.values=null):"max"===e&&(this.options.value=this._values(this.options.values.length-1),this.options.values=null)),P.isArray(this.options.values)&&(s=this.options.values.length),"disabled"===t&&this.element.toggleClass("ui-state-disabled",!!e),this._super(t,e),t){case"orientation":this._detectOrientation(),this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation),this._refreshValue(),this.handles.css("horizontal"===e?"bottom":"left","");break;case"value":this._animateOff=!0,this._refreshValue(),this._change(null,0),this._animateOff=!1;break;case"values":for(this._animateOff=!0,this._refreshValue(),i=0;i<s;i+=1)this._change(null,i);this._animateOff=!1;break;case"step":case"min":case"max":this._animateOff=!0,this._calculateNewMax(),this._refreshValue(),this._animateOff=!1;break;case"range":this._animateOff=!0,this._refresh(),this._animateOff=!1}},_value:function(){var t=this.options.value;return t=this._trimAlignValue(t)},_values:function(t){var e,i,s;if(arguments.length)return e=this.options.values[t],e=this._trimAlignValue(e);if(this.options.values&&this.options.values.length){for(i=this.options.values.slice(),s=0;s<i.length;s+=1)i[s]=this._trimAlignValue(i[s]);return i}return[]},_trimAlignValue:function(t){if(t<=this._valueMin())return this._valueMin();if(t>=this._valueMax())return this._valueMax();var e=0<this.options.step?this.options.step:1,i=(t-this._valueMin())%e,s=t-i;return 2*Math.abs(i)>=e&&(s+=0<i?e:-e),parseFloat(s.toFixed(5))},_calculateNewMax:function(){var t=(this.options.max-this._valueMin())%this.options.step;this.max=this.options.max-t},_valueMin:function(){return this.options.min},_valueMax:function(){return this.max},_refreshValue:function(){var e,i,t,s,n,o=this.options.range,a=this.options,r=this,h=!this._animateOff&&a.animate,l={};this.options.values&&this.options.values.length?this.handles.each(function(t){i=(r.values(t)-r._valueMin())/(r._valueMax()-r._valueMin())*100,l["horizontal"===r.orientation?"left":"bottom"]=i+"%",P(this).stop(1,1)[h?"animate":"css"](l,a.animate),!0===r.options.range&&("horizontal"===r.orientation?(0===t&&r.range.stop(1,1)[h?"animate":"css"]({left:i+"%"},a.animate),1===t&&r.range[h?"animate":"css"]({width:i-e+"%"},{queue:!1,duration:a.animate})):(0===t&&r.range.stop(1,1)[h?"animate":"css"]({bottom:i+"%"},a.animate),1===t&&r.range[h?"animate":"css"]({height:i-e+"%"},{queue:!1,duration:a.animate}))),e=i}):(t=this.value(),s=this._valueMin(),n=this._valueMax(),i=n!==s?(t-s)/(n-s)*100:0,l["horizontal"===this.orientation?"left":"bottom"]=i+"%",this.handle.stop(1,1)[h?"animate":"css"](l,a.animate),"min"===o&&"horizontal"===this.orientation&&this.range.stop(1,1)[h?"animate":"css"]({width:i+"%"},a.animate),"max"===o&&"horizontal"===this.orientation&&this.range[h?"animate":"css"]({width:100-i+"%"},{queue:!1,duration:a.animate}),"min"===o&&"vertical"===this.orientation&&this.range.stop(1,1)[h?"animate":"css"]({height:i+"%"},a.animate),"max"===o&&"vertical"===this.orientation&&this.range[h?"animate":"css"]({height:100-i+"%"},{queue:!1,duration:a.animate}))},_handleEvents:{keydown:function(t){var e,i,s,n=P(t.target).data("ui-slider-handle-index");switch(t.keyCode){case P.ui.keyCode.HOME:case P.ui.keyCode.END:case P.ui.keyCode.PAGE_UP:case P.ui.keyCode.PAGE_DOWN:case P.ui.keyCode.UP:case P.ui.keyCode.RIGHT:case P.ui.keyCode.DOWN:case P.ui.keyCode.LEFT:if(t.preventDefault(),!this._keySliding&&(this._keySliding=!0,P(t.target).addClass("ui-state-active"),!1===this._start(t,n)))return}switch(s=this.options.step,e=i=this.options.values&&this.options.values.length?this.values(n):this.value(),t.keyCode){case P.ui.keyCode.HOME:i=this._valueMin();break;case P.ui.keyCode.END:i=this._valueMax();break;case P.ui.keyCode.PAGE_UP:i=this._trimAlignValue(e+(this._valueMax()-this._valueMin())/this.numPages);break;case P.ui.keyCode.PAGE_DOWN:i=this._trimAlignValue(e-(this._valueMax()-this._valueMin())/this.numPages);break;case P.ui.keyCode.UP:case P.ui.keyCode.RIGHT:if(e===this._valueMax())return;i=this._trimAlignValue(e+s);break;case P.ui.keyCode.DOWN:case P.ui.keyCode.LEFT:if(e===this._valueMin())return;i=this._trimAlignValue(e-s)}this._slide(t,n,i)},keyup:function(t){var e=P(t.target).data("ui-slider-handle-index");this._keySliding&&(this._keySliding=!1,this._stop(t,e),this._change(t,e),P(t.target).removeClass("ui-state-active"))}}});function y(e){return function(){var t=this.element.val();e.apply(this,arguments),this._refresh(),t!==this.element.val()&&this._trigger("change")}}P.widget("ui.spinner",{version:"1.11.2",defaultElement:"<input>",widgetEventPrefix:"spin",options:{culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:!0,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max),this._setOption("min",this.options.min),this._setOption("step",this.options.step),""!==this.value()&&this._value(this.element.val(),!0),this._draw(),this._on(this._events),this._refresh(),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_getCreateOptions:function(){var s={},n=this.element;return P.each(["min","max","step"],function(t,e){var i=n.attr(e);void 0!==i&&i.length&&(s[e]=i)}),s},_events:{keydown:function(t){this._start(t)&&this._keydown(t)&&t.preventDefault()},keyup:"_stop",focus:function(){this.previous=this.element.val()},blur:function(t){this.cancelBlur?delete this.cancelBlur:(this._stop(),this._refresh(),this.previous!==this.element.val()&&this._trigger("change",t))},mousewheel:function(t,e){if(e){if(!this.spinning&&!this._start(t))return!1;this._spin((0<e?1:-1)*this.options.step,t),clearTimeout(this.mousewheelTimer),this.mousewheelTimer=this._delay(function(){this.spinning&&this._stop(t)},100),t.preventDefault()}},"mousedown .ui-spinner-button":function(t){var e;function i(){this.element[0]===this.document[0].activeElement||(this.element.focus(),this.previous=e,this._delay(function(){this.previous=e}))}e=this.element[0]===this.document[0].activeElement?this.previous:this.element.val(),t.preventDefault(),i.call(this),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur,i.call(this)}),!1!==this._start(t)&&this._repeat(null,P(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t)},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(t){if(P(t.currentTarget).hasClass("ui-state-active"))return!1!==this._start(t)&&void this._repeat(null,P(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t)},"mouseleave .ui-spinner-button":"_stop"},_draw:function(){var t=this.uiSpinner=this.element.addClass("ui-spinner-input").attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());this.element.attr("role","spinbutton"),this.buttons=t.find(".ui-spinner-button").attr("tabIndex",-1).button().removeClass("ui-corner-all"),this.buttons.height()>Math.ceil(.5*t.height())&&0<t.height()&&t.height(t.height()),this.options.disabled&&this.disable()},_keydown:function(t){var e=this.options,i=P.ui.keyCode;switch(t.keyCode){case i.UP:return this._repeat(null,1,t),!0;case i.DOWN:return this._repeat(null,-1,t),!0;case i.PAGE_UP:return this._repeat(null,e.page,t),!0;case i.PAGE_DOWN:return this._repeat(null,-e.page,t),!0}return!1},_uiSpinnerHtml:function(){return"<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"},_buttonHtml:function(){return"<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon "+this.options.icons.up+"'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon "+this.options.icons.down+"'>&#9660;</span></a>"},_start:function(t){return!(!this.spinning&&!1===this._trigger("start",t))&&(this.counter||(this.counter=1),this.spinning=!0)},_repeat:function(t,e,i){t=t||500,clearTimeout(this.timer),this.timer=this._delay(function(){this._repeat(40,e,i)},t),this._spin(e*this.options.step,i)},_spin:function(t,e){var i=this.value()||0;this.counter||(this.counter=1),i=this._adjustValue(i+t*this._increment(this.counter)),this.spinning&&!1===this._trigger("spin",e,{value:i})||(this._value(i),this.counter++)},_increment:function(t){var e=this.options.incremental;return e?P.isFunction(e)?e(t):Math.floor(t*t*t/5e4-t*t/500+17*t/200+1):1},_precision:function(){var t=this._precisionOf(this.options.step);return null!==this.options.min&&(t=Math.max(t,this._precisionOf(this.options.min))),t},_precisionOf:function(t){var e=t.toString(),i=e.indexOf(".");return-1===i?0:e.length-i-1},_adjustValue:function(t){var e,i,s=this.options;return i=t-(e=null!==s.min?s.min:0),t=e+(i=Math.round(i/s.step)*s.step),t=parseFloat(t.toFixed(this._precision())),null!==s.max&&t>s.max?s.max:null!==s.min&&t<s.min?s.min:t},_stop:function(t){this.spinning&&(clearTimeout(this.timer),clearTimeout(this.mousewheelTimer),this.counter=0,this.spinning=!1,this._trigger("stop",t))},_setOption:function(t,e){if("culture"===t||"numberFormat"===t){var i=this._parse(this.element.val());return this.options[t]=e,void this.element.val(this._format(i))}"max"!==t&&"min"!==t&&"step"!==t||"string"==typeof e&&(e=this._parse(e)),"icons"===t&&(this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(e.up),this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(e.down)),this._super(t,e),"disabled"===t&&(this.widget().toggleClass("ui-state-disabled",!!e),this.element.prop("disabled",!!e),this.buttons.button(e?"disable":"enable"))},_setOptions:y(function(t){this._super(t)}),_parse:function(t){return"string"==typeof t&&""!==t&&(t=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(t,10,this.options.culture):+t),""===t||isNaN(t)?null:t},_format:function(t){return""===t?"":window.Globalize&&this.options.numberFormat?Globalize.format(t,this.options.numberFormat,this.options.culture):t},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())})},isValid:function(){var t=this.value();return null!==t&&t===this._adjustValue(t)},_value:function(t,e){var i;""!==t&&null!==(i=this._parse(t))&&(e||(i=this._adjustValue(i)),t=this._format(i)),this.element.val(t),this._refresh()},_destroy:function(){this.element.removeClass("ui-spinner-input").prop("disabled",!1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.uiSpinner.replaceWith(this.element)},stepUp:y(function(t){this._stepUp(t)}),_stepUp:function(t){this._start()&&(this._spin((t||1)*this.options.step),this._stop())},stepDown:y(function(t){this._stepDown(t)}),_stepDown:function(t){this._start()&&(this._spin((t||1)*-this.options.step),this._stop())},pageUp:y(function(t){this._stepUp((t||1)*this.options.page)}),pageDown:y(function(t){this._stepDown((t||1)*this.options.page)}),value:function(t){if(!arguments.length)return this._parse(this.element.val());y(this._value).call(this,t)},widget:function(){return this.uiSpinner}}),P.widget("ui.tabs",{version:"1.11.2",delay:300,options:{active:null,collapsible:!1,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_isLocal:(w=/#.*$/,function(t){var e,i;e=(t=t.cloneNode(!1)).href.replace(w,""),i=location.href.replace(w,"");try{e=decodeURIComponent(e)}catch(t){}try{i=decodeURIComponent(i)}catch(t){}return 1<t.hash.length&&e===i}),_create:function(){var e=this,t=this.options;this.running=!1,this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible",t.collapsible),this._processTabs(),t.active=this._initialActive(),P.isArray(t.disabled)&&(t.disabled=P.unique(t.disabled.concat(P.map(this.tabs.filter(".ui-state-disabled"),function(t){return e.tabs.index(t)}))).sort()),!1!==this.options.active&&this.anchors.length?this.active=this._findActive(t.active):this.active=P(),this._refresh(),this.active.length&&this.load(t.active)},_initialActive:function(){var i=this.options.active,t=this.options.collapsible,s=location.hash.substring(1);return null===i&&(s&&this.tabs.each(function(t,e){if(P(e).attr("aria-controls")===s)return i=t,!1}),null===i&&(i=this.tabs.index(this.tabs.filter(".ui-tabs-active"))),null!==i&&-1!==i||(i=!!this.tabs.length&&0)),!1!==i&&-1===(i=this.tabs.index(this.tabs.eq(i)))&&(i=!t&&0),!t&&!1===i&&this.anchors.length&&(i=0),i},_getCreateEventData:function(){return{tab:this.active,panel:this.active.length?this._getPanelForTab(this.active):P()}},_tabKeydown:function(t){var e=P(this.document[0].activeElement).closest("li"),i=this.tabs.index(e),s=!0;if(!this._handlePageNav(t)){switch(t.keyCode){case P.ui.keyCode.RIGHT:case P.ui.keyCode.DOWN:i++;break;case P.ui.keyCode.UP:case P.ui.keyCode.LEFT:s=!1,i--;break;case P.ui.keyCode.END:i=this.anchors.length-1;break;case P.ui.keyCode.HOME:i=0;break;case P.ui.keyCode.SPACE:return t.preventDefault(),clearTimeout(this.activating),void this._activate(i);case P.ui.keyCode.ENTER:return t.preventDefault(),clearTimeout(this.activating),void this._activate(i!==this.options.active&&i);default:return}t.preventDefault(),clearTimeout(this.activating),i=this._focusNextTab(i,s),t.ctrlKey||(e.attr("aria-selected","false"),this.tabs.eq(i).attr("aria-selected","true"),this.activating=this._delay(function(){this.option("active",i)},this.delay))}},_panelKeydown:function(t){this._handlePageNav(t)||t.ctrlKey&&t.keyCode===P.ui.keyCode.UP&&(t.preventDefault(),this.active.focus())},_handlePageNav:function(t){return t.altKey&&t.keyCode===P.ui.keyCode.PAGE_UP?(this._activate(this._focusNextTab(this.options.active-1,!1)),!0):t.altKey&&t.keyCode===P.ui.keyCode.PAGE_DOWN?(this._activate(this._focusNextTab(this.options.active+1,!0)),!0):void 0},_findNextTab:function(t,e){var i=this.tabs.length-1;for(;-1!==P.inArray((i<t&&(t=0),t<0&&(t=i),t),this.options.disabled);)t=e?t+1:t-1;return t},_focusNextTab:function(t,e){return t=this._findNextTab(t,e),this.tabs.eq(t).focus(),t},_setOption:function(t,e){"active"!==t?"disabled"!==t?(this._super(t,e),"collapsible"===t&&(this.element.toggleClass("ui-tabs-collapsible",e),e||!1!==this.options.active||this._activate(0)),"event"===t&&this._setupEvents(e),"heightStyle"===t&&this._setupHeightStyle(e)):this._setupDisabled(e):this._activate(e)},_sanitizeSelector:function(t){return t?t.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):""},refresh:function(){var t=this.options,e=this.tablist.children(":has(a[href])");t.disabled=P.map(e.filter(".ui-state-disabled"),function(t){return e.index(t)}),this._processTabs(),!1!==t.active&&this.anchors.length?this.active.length&&!P.contains(this.tablist[0],this.active[0])?this.tabs.length===t.disabled.length?(t.active=!1,this.active=P()):this._activate(this._findNextTab(Math.max(0,t.active-1),!1)):t.active=this.tabs.index(this.active):(t.active=!1,this.active=P()),this._refresh()},_refresh:function(){this._setupDisabled(this.options.disabled),this._setupEvents(this.options.event),this._setupHeightStyle(this.options.heightStyle),this.tabs.not(this.active).attr({"aria-selected":"false","aria-expanded":"false",tabIndex:-1}),this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-hidden":"true"}),this.active.length?(this.active.addClass("ui-tabs-active ui-state-active").attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0}),this._getPanelForTab(this.active).show().attr({"aria-hidden":"false"})):this.tabs.eq(0).attr("tabIndex",0)},_processTabs:function(){var h=this,t=this.tabs,e=this.anchors,i=this.panels;this.tablist=this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role","tablist").delegate("> li","mousedown"+this.eventNamespace,function(t){P(this).is(".ui-state-disabled")&&t.preventDefault()}).delegate(".ui-tabs-anchor","focus"+this.eventNamespace,function(){P(this).closest("li").is(".ui-state-disabled")&&this.blur()}),this.tabs=this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({role:"tab",tabIndex:-1}),this.anchors=this.tabs.map(function(){return P("a",this)[0]}).addClass("ui-tabs-anchor").attr({role:"presentation",tabIndex:-1}),this.panels=P(),this.anchors.each(function(t,e){var i,s,n,o=P(e).uniqueId().attr("id"),a=P(e).closest("li"),r=a.attr("aria-controls");h._isLocal(e)?(n=(i=e.hash).substring(1),s=h.element.find(h._sanitizeSelector(i))):(i="#"+(n=a.attr("aria-controls")||P({}).uniqueId()[0].id),(s=h.element.find(i)).length||(s=h._createPanel(n)).insertAfter(h.panels[t-1]||h.tablist),s.attr("aria-live","polite")),s.length&&(h.panels=h.panels.add(s)),r&&a.data("ui-tabs-aria-controls",r),a.attr({"aria-controls":n,"aria-labelledby":o}),s.attr("aria-labelledby",o)}),this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role","tabpanel"),t&&(this._off(t.not(this.tabs)),this._off(e.not(this.anchors)),this._off(i.not(this.panels)))},_getList:function(){return this.tablist||this.element.find("ol,ul").eq(0)},_createPanel:function(t){return P("<div>").attr("id",t).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",!0)},_setupDisabled:function(t){P.isArray(t)&&(t.length?t.length===this.anchors.length&&(t=!0):t=!1);for(var e,i=0;e=this.tabs[i];i++)!0===t||-1!==P.inArray(i,t)?P(e).addClass("ui-state-disabled").attr("aria-disabled","true"):P(e).removeClass("ui-state-disabled").removeAttr("aria-disabled");this.options.disabled=t},_setupEvents:function(t){var i={};t&&P.each(t.split(" "),function(t,e){i[e]="_eventHandler"}),this._off(this.anchors.add(this.tabs).add(this.panels)),this._on(!0,this.anchors,{click:function(t){t.preventDefault()}}),this._on(this.anchors,i),this._on(this.tabs,{keydown:"_tabKeydown"}),this._on(this.panels,{keydown:"_panelKeydown"}),this._focusable(this.tabs),this._hoverable(this.tabs)},_setupHeightStyle:function(t){var i,e=this.element.parent();"fill"===t?(i=e.height(),i-=this.element.outerHeight()-this.element.height(),this.element.siblings(":visible").each(function(){var t=P(this),e=t.css("position");"absolute"!==e&&"fixed"!==e&&(i-=t.outerHeight(!0))}),this.element.children().not(this.panels).each(function(){i-=P(this).outerHeight(!0)}),this.panels.each(function(){P(this).height(Math.max(0,i-P(this).innerHeight()+P(this).height()))}).css("overflow","auto")):"auto"===t&&(i=0,this.panels.each(function(){i=Math.max(i,P(this).height("").height())}).height(i))},_eventHandler:function(t){var e=this.options,i=this.active,s=P(t.currentTarget).closest("li"),n=s[0]===i[0],o=n&&e.collapsible,a=o?P():this._getPanelForTab(s),r=i.length?this._getPanelForTab(i):P(),h={oldTab:i,oldPanel:r,newTab:o?P():s,newPanel:a};t.preventDefault(),s.hasClass("ui-state-disabled")||s.hasClass("ui-tabs-loading")||this.running||n&&!e.collapsible||!1===this._trigger("beforeActivate",t,h)||(e.active=!o&&this.tabs.index(s),this.active=n?P():s,this.xhr&&this.xhr.abort(),r.length||a.length||P.error("jQuery UI Tabs: Mismatching fragment identifier."),a.length&&this.load(this.tabs.index(s),t),this._toggle(t,h))},_toggle:function(t,e){var i=this,s=e.newPanel,n=e.oldPanel;function o(){i.running=!1,i._trigger("activate",t,e)}function a(){e.newTab.closest("li").addClass("ui-tabs-active ui-state-active"),s.length&&i.options.show?i._show(s,i.options.show,o):(s.show(),o())}this.running=!0,n.length&&this.options.hide?this._hide(n,this.options.hide,function(){e.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),a()}):(e.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),n.hide(),a()),n.attr("aria-hidden","true"),e.oldTab.attr({"aria-selected":"false","aria-expanded":"false"}),s.length&&n.length?e.oldTab.attr("tabIndex",-1):s.length&&this.tabs.filter(function(){return 0===P(this).attr("tabIndex")}).attr("tabIndex",-1),s.attr("aria-hidden","false"),e.newTab.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0})},_activate:function(t){var e,i=this._findActive(t);i[0]!==this.active[0]&&(i.length||(i=this.active),e=i.find(".ui-tabs-anchor")[0],this._eventHandler({target:e,currentTarget:e,preventDefault:P.noop}))},_findActive:function(t){return!1===t?P():this.tabs.eq(t)},_getIndex:function(t){return"string"==typeof t&&(t=this.anchors.index(this.anchors.filter("[href$='"+t+"']"))),t},_destroy:function(){this.xhr&&this.xhr.abort(),this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"),this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"),this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(),this.tablist.unbind(this.eventNamespace),this.tabs.add(this.panels).each(function(){P.data(this,"ui-tabs-destroy")?P(this).remove():P(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")}),this.tabs.each(function(){var t=P(this),e=t.data("ui-tabs-aria-controls");e?t.attr("aria-controls",e).removeData("ui-tabs-aria-controls"):t.removeAttr("aria-controls")}),this.panels.show(),"content"!==this.options.heightStyle&&this.panels.css("height","")},enable:function(i){var t=this.options.disabled;!1!==t&&(t=void 0!==i&&(i=this._getIndex(i),P.isArray(t)?P.map(t,function(t){return t!==i?t:null}):P.map(this.tabs,function(t,e){return e!==i?e:null})),this._setupDisabled(t))},disable:function(t){var e=this.options.disabled;if(!0!==e){if(void 0===t)e=!0;else{if(t=this._getIndex(t),-1!==P.inArray(t,e))return;e=P.isArray(e)?P.merge([t],e).sort():[t]}this._setupDisabled(e)}},load:function(t,e){t=this._getIndex(t);var i=this,s=this.tabs.eq(t),n=s.find(".ui-tabs-anchor"),o=this._getPanelForTab(s),a={tab:s,panel:o};this._isLocal(n[0])||(this.xhr=P.ajax(this._ajaxSettings(n,e,a)),this.xhr&&"canceled"!==this.xhr.statusText&&(s.addClass("ui-tabs-loading"),o.attr("aria-busy","true"),this.xhr.success(function(t){setTimeout(function(){o.html(t),i._trigger("load",e,a)},1)}).complete(function(t,e){setTimeout(function(){"abort"===e&&i.panels.stop(!1,!0),s.removeClass("ui-tabs-loading"),o.removeAttr("aria-busy"),t===i.xhr&&delete i.xhr},1)})))},_ajaxSettings:function(t,i,s){var n=this;return{url:t.attr("href"),beforeSend:function(t,e){return n._trigger("beforeLoad",i,P.extend({jqXHR:t,ajaxSettings:e},s))}}},_getPanelForTab:function(t){var e=P(t).attr("aria-controls");return this.element.find(this._sanitizeSelector("#"+e))}}),P.widget("ui.tooltip",{version:"1.11.2",options:{content:function(){var t=P(this).attr("title")||"";return P("<a>").text(t).html()},hide:!0,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flip"},show:!0,tooltipClass:null,track:!1,close:null,open:null},_addDescribedBy:function(t,e){var i=(t.attr("aria-describedby")||"").split(/\s+/);i.push(e),t.data("ui-tooltip-id",e).attr("aria-describedby",P.trim(i.join(" ")))},_removeDescribedBy:function(t){var e=t.data("ui-tooltip-id"),i=(t.attr("aria-describedby")||"").split(/\s+/),s=P.inArray(e,i);-1!==s&&i.splice(s,1),t.removeData("ui-tooltip-id"),(i=P.trim(i.join(" ")))?t.attr("aria-describedby",i):t.removeAttr("aria-describedby")},_create:function(){this._on({mouseover:"open",focusin:"open"}),this.tooltips={},this.parents={},this.options.disabled&&this._disable(),this.liveRegion=P("<div>").attr({role:"log","aria-live":"assertive","aria-relevant":"additions"}).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body)},_setOption:function(t,e){var i=this;if("disabled"===t)return this[e?"_disable":"_enable"](),void(this.options[t]=e);this._super(t,e),"content"===t&&P.each(this.tooltips,function(t,e){i._updateContent(e.element)})},_disable:function(){var s=this;P.each(this.tooltips,function(t,e){var i=P.Event("blur");i.target=i.currentTarget=e.element[0],s.close(i,!0)}),this.element.find(this.options.items).addBack().each(function(){var t=P(this);t.is("[title]")&&t.data("ui-tooltip-title",t.attr("title")).removeAttr("title")})},_enable:function(){this.element.find(this.options.items).addBack().each(function(){var t=P(this);t.data("ui-tooltip-title")&&t.attr("title",t.data("ui-tooltip-title"))})},open:function(t){var i=this,e=P(t?t.target:this.element).closest(this.options.items);e.length&&!e.data("ui-tooltip-id")&&(e.attr("title")&&e.data("ui-tooltip-title",e.attr("title")),e.data("ui-tooltip-open",!0),t&&"mouseover"===t.type&&e.parents().each(function(){var t,e=P(this);e.data("ui-tooltip-open")&&((t=P.Event("blur")).target=t.currentTarget=this,i.close(t,!0)),e.attr("title")&&(e.uniqueId(),i.parents[this.id]={element:this,title:e.attr("title")},e.attr("title",""))}),this._updateContent(e,t))},_updateContent:function(e,i){var t,s=this.options.content,n=this,o=i?i.type:null;if("string"==typeof s)return this._open(i,e,s);(t=s.call(e[0],function(t){e.data("ui-tooltip-open")&&n._delay(function(){i&&(i.type=o),this._open(i,e,t)})}))&&this._open(i,e,t)},_open:function(t,i,e){var s,n,o,a,r,h=P.extend({},this.options.position);function l(t){h.of=t,n.is(":hidden")||n.position(h)}e&&((s=this._find(i))?s.tooltip.find(".ui-tooltip-content").html(e):(i.is("[title]")&&(t&&"mouseover"===t.type?i.attr("title",""):i.removeAttr("title")),s=this._tooltip(i),n=s.tooltip,this._addDescribedBy(i,n.attr("id")),n.find(".ui-tooltip-content").html(e),this.liveRegion.children().hide(),e.clone?(r=e.clone()).removeAttr("id").find("[id]").removeAttr("id"):r=e,P("<div>").html(r).appendTo(this.liveRegion),this.options.track&&t&&/^mouse/.test(t.type)?(this._on(this.document,{mousemove:l}),l(t)):n.position(P.extend({of:i},this.options.position)),n.hide(),this._show(n,this.options.show),this.options.show&&this.options.show.delay&&(a=this.delayedShow=setInterval(function(){n.is(":visible")&&(l(h.of),clearInterval(a))},P.fx.interval)),this._trigger("open",t,{tooltip:n}),o={keyup:function(t){if(t.keyCode===P.ui.keyCode.ESCAPE){var e=P.Event(t);e.currentTarget=i[0],this.close(e,!0)}}},i[0]!==this.element[0]&&(o.remove=function(){this._removeTooltip(n)}),t&&"mouseover"!==t.type||(o.mouseleave="close"),t&&"focusin"!==t.type||(o.focusout="close"),this._on(!0,i,o)))},close:function(t){var e,i=this,s=P(t?t.currentTarget:this.element),n=this._find(s);n&&(e=n.tooltip,n.closing||(clearInterval(this.delayedShow),s.data("ui-tooltip-title")&&!s.attr("title")&&s.attr("title",s.data("ui-tooltip-title")),this._removeDescribedBy(s),n.hiding=!0,e.stop(!0),this._hide(e,this.options.hide,function(){i._removeTooltip(P(this))}),s.removeData("ui-tooltip-open"),this._off(s,"mouseleave focusout keyup"),s[0]!==this.element[0]&&this._off(s,"remove"),this._off(this.document,"mousemove"),t&&"mouseleave"===t.type&&P.each(this.parents,function(t,e){P(e.element).attr("title",e.title),delete i.parents[t]}),n.closing=!0,this._trigger("close",t,{tooltip:e}),n.hiding||(n.closing=!1)))},_tooltip:function(t){var e=P("<div>").attr("role","tooltip").addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content "+(this.options.tooltipClass||"")),i=e.uniqueId().attr("id");return P("<div>").addClass("ui-tooltip-content").appendTo(e),e.appendTo(this.document[0].body),this.tooltips[i]={element:t,tooltip:e}},_find:function(t){var e=t.data("ui-tooltip-id");return e?this.tooltips[e]:null},_removeTooltip:function(t){t.remove(),delete this.tooltips[t.attr("id")]},_destroy:function(){var n=this;P.each(this.tooltips,function(t,e){var i=P.Event("blur"),s=e.element;i.target=i.currentTarget=s[0],n.close(i,!0),P("#"+t).remove(),s.data("ui-tooltip-title")&&(s.attr("title")||s.attr("title",s.data("ui-tooltip-title")),s.removeData("ui-tooltip-title"))}),this.liveRegion.remove()}});var w,x,k="ui-effects-",C=P;P.effects={effect:{}},function(c,u){var l,d=/^([\-+])=\s*(\d+\.?\d*)/,t=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(t){return[t[1],t[2],t[3],t[4]]}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(t){return[2.55*t[1],2.55*t[2],2.55*t[3],t[4]]}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(t){return[parseInt(t[1],16),parseInt(t[2],16),parseInt(t[3],16)]}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(t){return[parseInt(t[1]+t[1],16),parseInt(t[2]+t[2],16),parseInt(t[3]+t[3],16)]}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(t){return[t[1],t[2]/100,t[3]/100,t[4]]}}],p=c.Color=function(t,e,i,s){return new c.Color.fn.parse(t,e,i,s)},f={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},m={byte:{floor:!0,max:255},percent:{max:1},degrees:{mod:360,floor:!0}},a=p.support={},e=c("<p>")[0],g=c.each;function v(t,e,i){var s=m[e.type]||{};return null==t?i||!e.def?null:e.def:(t=s.floor?~~t:parseFloat(t),isNaN(t)?e.def:s.mod?(t+s.mod)%s.mod:t<0?0:s.max<t?s.max:t)}function r(a){var r=p(),h=r._rgba=[];return a=a.toLowerCase(),g(t,function(t,e){var i,s=e.re.exec(a),n=s&&e.parse(s),o=e.space||"rgba";if(n)return i=r[o](n),r[f[o].cache]=i[f[o].cache],h=r._rgba=i._rgba,!1}),h.length?("0,0,0,0"===h.join()&&c.extend(h,l.transparent),r):l[a]}function h(t,e,i){return 6*(i=(i+1)%1)<1?t+(e-t)*i*6:2*i<1?e:3*i<2?t+(e-t)*(2/3-i)*6:t}e.style.cssText="background-color:rgba(1,1,1,.5)",a.rgba=-1<e.style.backgroundColor.indexOf("rgba"),g(f,function(t,e){e.cache="_"+t,e.props.alpha={idx:3,type:"percent",def:1}}),p.fn=c.extend(p.prototype,{parse:function(n,t,e,i){if(n===u)return this._rgba=[null,null,null,null],this;(n.jquery||n.nodeType)&&(n=c(n).css(t),t=u);var o=this,s=c.type(n),a=this._rgba=[];return t!==u&&(n=[n,t,e,i],s="array"),"string"===s?this.parse(r(n)||l._default):"array"===s?(g(f.rgba.props,function(t,e){a[e.idx]=v(n[e.idx],e)}),this):"object"===s?(g(f,n instanceof p?function(t,e){n[e.cache]&&(o[e.cache]=n[e.cache].slice())}:function(t,i){var s=i.cache;g(i.props,function(t,e){if(!o[s]&&i.to){if("alpha"===t||null==n[t])return;o[s]=i.to(o._rgba)}o[s][e.idx]=v(n[t],e,!0)}),o[s]&&c.inArray(null,o[s].slice(0,3))<0&&(o[s][3]=1,i.from&&(o._rgba=i.from(o[s])))}),this):void 0},is:function(t){var n=p(t),o=!0,a=this;return g(f,function(t,e){var i,s=n[e.cache];return s&&(i=a[e.cache]||e.to&&e.to(a._rgba)||[],g(e.props,function(t,e){if(null!=s[e.idx])return o=s[e.idx]===i[e.idx]})),o}),o},_space:function(){var i=[],s=this;return g(f,function(t,e){s[e.cache]&&i.push(t)}),i.pop()},transition:function(t,a){var r=p(t),e=r._space(),i=f[e],s=0===this.alpha()?p("transparent"):this,h=s[i.cache]||i.to(s._rgba),l=h.slice();return r=r[i.cache],g(i.props,function(t,e){var i=e.idx,s=h[i],n=r[i],o=m[e.type]||{};null!==n&&(l[i]=null===s?n:(o.mod&&(n-s>o.mod/2?s+=o.mod:s-n>o.mod/2&&(s-=o.mod)),v((n-s)*a+s,e)))}),this[e](l)},blend:function(t){if(1===this._rgba[3])return this;var e=this._rgba.slice(),i=e.pop(),s=p(t)._rgba;return p(c.map(e,function(t,e){return(1-i)*s[e]+i*t}))},toRgbaString:function(){var t="rgba(",e=c.map(this._rgba,function(t,e){return null==t?2<e?1:0:t});return 1===e[3]&&(e.pop(),t="rgb("),t+e.join()+")"},toHslaString:function(){var t="hsla(",e=c.map(this.hsla(),function(t,e){return null==t&&(t=2<e?1:0),e&&e<3&&(t=Math.round(100*t)+"%"),t});return 1===e[3]&&(e.pop(),t="hsl("),t+e.join()+")"},toHexString:function(t){var e=this._rgba.slice(),i=e.pop();return t&&e.push(~~(255*i)),"#"+c.map(e,function(t){return 1===(t=(t||0).toString(16)).length?"0"+t:t}).join("")},toString:function(){return 0===this._rgba[3]?"transparent":this.toRgbaString()}}),p.fn.parse.prototype=p.fn,f.hsla.to=function(t){if(null==t[0]||null==t[1]||null==t[2])return[null,null,null,t[3]];var e,i,s=t[0]/255,n=t[1]/255,o=t[2]/255,a=t[3],r=Math.max(s,n,o),h=Math.min(s,n,o),l=r-h,c=r+h,u=.5*c;return e=h===r?0:s===r?60*(n-o)/l+360:n===r?60*(o-s)/l+120:60*(s-n)/l+240,i=0===l?0:u<=.5?l/c:l/(2-c),[Math.round(e)%360,i,u,null==a?1:a]},f.hsla.from=function(t){if(null==t[0]||null==t[1]||null==t[2])return[null,null,null,t[3]];var e=t[0]/360,i=t[1],s=t[2],n=t[3],o=s<=.5?s*(1+i):s+i-s*i,a=2*s-o;return[Math.round(255*h(a,o,e+1/3)),Math.round(255*h(a,o,e)),Math.round(255*h(a,o,e-1/3)),n]},g(f,function(h,t){var i=t.props,a=t.cache,r=t.to,l=t.from;p.fn[h]=function(t){if(r&&!this[a]&&(this[a]=r(this._rgba)),t===u)return this[a].slice();var e,s=c.type(t),n="array"===s||"object"===s?t:arguments,o=this[a].slice();return g(i,function(t,e){var i=n["object"===s?t:e.idx];null==i&&(i=o[e.idx]),o[e.idx]=v(i,e)}),l?((e=p(l(o)))[a]=o,e):p(o)},g(i,function(a,r){p.fn[a]||(p.fn[a]=function(t){var e,i=c.type(t),s="alpha"===a?this._hsla?"hsla":"rgba":h,n=this[s](),o=n[r.idx];return"undefined"===i?o:("function"===i&&(t=t.call(this,o),i=c.type(t)),null==t&&r.empty?this:("string"===i&&(e=d.exec(t))&&(t=o+parseFloat(e[2])*("+"===e[1]?1:-1)),n[r.idx]=t,this[s](n)))})})}),p.hook=function(t){var e=t.split(" ");g(e,function(t,o){c.cssHooks[o]={set:function(t,e){var i,s,n="";if("transparent"!==e&&("string"!==c.type(e)||(i=r(e)))){if(e=p(i||e),!a.rgba&&1!==e._rgba[3]){for(s="backgroundColor"===o?t.parentNode:t;(""===n||"transparent"===n)&&s&&s.style;)try{n=c.css(s,"backgroundColor"),s=s.parentNode}catch(t){}e=e.blend(n&&"transparent"!==n?n:"_default")}e=e.toRgbaString()}try{t.style[o]=e}catch(t){}}},c.fx.step[o]=function(t){t.colorInit||(t.start=p(t.elem,o),t.end=p(t.end),t.colorInit=!0),c.cssHooks[o].set(t.elem,t.start.transition(t.end,t.pos))}})},p.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor"),c.cssHooks.borderColor={expand:function(i){var s={};return g(["Top","Right","Bottom","Left"],function(t,e){s["border"+e+"Color"]=i}),s}},l=c.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"}}(C),function(){var o,n,a,r=["add","remove","toggle"],h={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};function l(t){var e,i,s=t.ownerDocument.defaultView?t.ownerDocument.defaultView.getComputedStyle(t,null):t.currentStyle,n={};if(s&&s.length&&s[0]&&s[s[0]])for(i=s.length;i--;)"string"==typeof s[e=s[i]]&&(n[P.camelCase(e)]=s[e]);else for(e in s)"string"==typeof s[e]&&(n[e]=s[e]);return n}P.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(t,e){P.fx.step[e]=function(t){("none"!==t.end&&!t.setAttr||1===t.pos&&!t.setAttr)&&(C.style(t.elem,e,t.end),t.setAttr=!0)}}),P.fn.addBack||(P.fn.addBack=function(t){return this.add(null==t?this.prevObject:this.prevObject.filter(t))}),P.effects.animateClass=function(n,t,e,i){var o=P.speed(t,e,i);return this.queue(function(){var t,i=P(this),e=i.attr("class")||"",s=o.children?i.find("*").addBack():i;s=s.map(function(){return{el:P(this),start:l(this)}}),(t=function(){P.each(r,function(t,e){n[e]&&i[e+"Class"](n[e])})})(),s=s.map(function(){return this.end=l(this.el[0]),this.diff=function(t,e){var i,s,n={};for(i in e)s=e[i],t[i]!==s&&(h[i]||!P.fx.step[i]&&isNaN(parseFloat(s))||(n[i]=s));return n}(this.start,this.end),this}),i.attr("class",e),s=s.map(function(){var t=this,e=P.Deferred(),i=P.extend({},o,{queue:!1,complete:function(){e.resolve(t)}});return this.el.animate(this.diff,i),e.promise()}),P.when.apply(P,s.get()).done(function(){t(),P.each(arguments,function(){var e=this.el;P.each(this.diff,function(t){e.css(t,"")})}),o.complete.call(i[0])})})},P.fn.extend({addClass:(a=P.fn.addClass,function(t,e,i,s){return e?P.effects.animateClass.call(this,{add:t},e,i,s):a.apply(this,arguments)}),removeClass:(n=P.fn.removeClass,function(t,e,i,s){return 1<arguments.length?P.effects.animateClass.call(this,{remove:t},e,i,s):n.apply(this,arguments)}),toggleClass:(o=P.fn.toggleClass,function(t,e,i,s,n){return"boolean"==typeof e||void 0===e?i?P.effects.animateClass.call(this,e?{add:t}:{remove:t},i,s,n):o.apply(this,arguments):P.effects.animateClass.call(this,{toggle:t},e,i,s)}),switchClass:function(t,e,i,s,n){return P.effects.animateClass.call(this,{add:e,remove:t},i,s,n)}})}(),function(){function s(t,e,i,s){return P.isPlainObject(t)&&(t=(e=t).effect),t={effect:t},null==e&&(e={}),P.isFunction(e)&&(s=e,i=null,e={}),("number"==typeof e||P.fx.speeds[e])&&(s=i,i=e,e={}),P.isFunction(i)&&(s=i,i=null),e&&P.extend(t,e),i=i||e.duration,t.duration=P.fx.off?0:"number"==typeof i?i:i in P.fx.speeds?P.fx.speeds[i]:P.fx.speeds._default,t.complete=s||e.complete,t}function i(t){return!(t&&"number"!=typeof t&&!P.fx.speeds[t])||("string"==typeof t&&!P.effects.effect[t]||(!!P.isFunction(t)||"object"==typeof t&&!t.effect))}var n,o,a;P.extend(P.effects,{version:"1.11.2",save:function(t,e){for(var i=0;i<e.length;i++)null!==e[i]&&t.data(k+e[i],t[0].style[e[i]])},restore:function(t,e){var i,s;for(s=0;s<e.length;s++)null!==e[s]&&(void 0===(i=t.data(k+e[s]))&&(i=""),t.css(e[s],i))},setMode:function(t,e){return"toggle"===e&&(e=t.is(":hidden")?"show":"hide"),e},getBaseline:function(t,e){var i,s;switch(t[0]){case"top":i=0;break;case"middle":i=.5;break;case"bottom":i=1;break;default:i=t[0]/e.height}switch(t[1]){case"left":s=0;break;case"center":s=.5;break;case"right":s=1;break;default:s=t[1]/e.width}return{x:s,y:i}},createWrapper:function(i){if(i.parent().is(".ui-effects-wrapper"))return i.parent();var s={width:i.outerWidth(!0),height:i.outerHeight(!0),float:i.css("float")},t=P("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),e={width:i.width(),height:i.height()},n=document.activeElement;try{n.id}catch(t){n=document.body}return i.wrap(t),(i[0]===n||P.contains(i[0],n))&&P(n).focus(),t=i.parent(),"static"===i.css("position")?(t.css({position:"relative"}),i.css({position:"relative"})):(P.extend(s,{position:i.css("position"),zIndex:i.css("z-index")}),P.each(["top","left","bottom","right"],function(t,e){s[e]=i.css(e),isNaN(parseInt(s[e],10))&&(s[e]="auto")}),i.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})),i.css(e),t.css(s).show()},removeWrapper:function(t){var e=document.activeElement;return t.parent().is(".ui-effects-wrapper")&&(t.parent().replaceWith(t),(t[0]===e||P.contains(t[0],e))&&P(e).focus()),t},setTransition:function(s,t,n,o){return o=o||{},P.each(t,function(t,e){var i=s.cssUnit(e);0<i[0]&&(o[e]=i[0]*n+i[1])}),o}}),P.fn.extend({effect:function(){var o=s.apply(this,arguments),t=o.mode,e=o.queue,a=P.effects.effect[o.effect];if(P.fx.off||!a)return t?this[t](o.duration,o.complete):this.each(function(){o.complete&&o.complete.call(this)});function i(t){var e=P(this),i=o.complete,s=o.mode;function n(){P.isFunction(i)&&i.call(e[0]),P.isFunction(t)&&t()}(e.is(":hidden")?"hide"===s:"show"===s)?(e[s](),n()):a.call(e[0],o,n)}return!1===e?this.each(i):this.queue(e||"fx",i)},show:(a=P.fn.show,function(t){if(i(t))return a.apply(this,arguments);var e=s.apply(this,arguments);return e.mode="show",this.effect.call(this,e)}),hide:(o=P.fn.hide,function(t){if(i(t))return o.apply(this,arguments);var e=s.apply(this,arguments);return e.mode="hide",this.effect.call(this,e)}),toggle:(n=P.fn.toggle,function(t){if(i(t)||"boolean"==typeof t)return n.apply(this,arguments);var e=s.apply(this,arguments);return e.mode="toggle",this.effect.call(this,e)}),cssUnit:function(t){var i=this.css(t),s=[];return P.each(["em","px","%","pt"],function(t,e){0<i.indexOf(e)&&(s=[parseFloat(i),e])}),s}})}(),x={},P.each(["Quad","Cubic","Quart","Quint","Expo"],function(e,t){x[t]=function(t){return Math.pow(t,e+2)}}),P.extend(x,{Sine:function(t){return 1-Math.cos(t*Math.PI/2)},Circ:function(t){return 1-Math.sqrt(1-t*t)},Elastic:function(t){return 0===t||1===t?t:-Math.pow(2,8*(t-1))*Math.sin((80*(t-1)-7.5)*Math.PI/15)},Back:function(t){return t*t*(3*t-2)},Bounce:function(t){for(var e,i=4;t<((e=Math.pow(2,--i))-1)/11;);return 1/Math.pow(4,3-i)-7.5625*Math.pow((3*e-2)/22-t,2)}}),P.each(x,function(t,e){P.easing["easeIn"+t]=e,P.easing["easeOut"+t]=function(t){return 1-e(1-t)},P.easing["easeInOut"+t]=function(t){return t<.5?e(2*t)/2:1-e(-2*t+2)/2}});P.effects,P.effects.effect.blind=function(t,e){var i,s,n,o=P(this),a=["position","top","bottom","left","right","height","width"],r=P.effects.setMode(o,t.mode||"hide"),h=t.direction||"up",l=/up|down|vertical/.test(h),c=l?"height":"width",u=l?"top":"left",d=/up|left|vertical|horizontal/.test(h),p={},f="show"===r;o.parent().is(".ui-effects-wrapper")?P.effects.save(o.parent(),a):P.effects.save(o,a),o.show(),s=(i=P.effects.createWrapper(o).css({overflow:"hidden"}))[c](),n=parseFloat(i.css(u))||0,p[c]=f?s:0,d||(o.css(l?"bottom":"right",0).css(l?"top":"left","auto").css({position:"absolute"}),p[u]=f?n:s+n),f&&(i.css(c,0),d||i.css(u,n+s)),i.animate(p,{duration:t.duration,easing:t.easing,queue:!1,complete:function(){"hide"===r&&o.hide(),P.effects.restore(o,a),P.effects.removeWrapper(o),e()}})},P.effects.effect.bounce=function(t,e){var i,s,n,o=P(this),a=["position","top","bottom","left","right","height","width"],r=P.effects.setMode(o,t.mode||"effect"),h="hide"===r,l="show"===r,c=t.direction||"up",u=t.distance,d=t.times||5,p=2*d+(l||h?1:0),f=t.duration/p,m=t.easing,g="up"===c||"down"===c?"top":"left",v="up"===c||"left"===c,_=o.queue(),b=_.length;for((l||h)&&a.push("opacity"),P.effects.save(o,a),o.show(),P.effects.createWrapper(o),u||(u=o["top"===g?"outerHeight":"outerWidth"]()/3),l&&((n={opacity:1})[g]=0,o.css("opacity",0).css(g,v?2*-u:2*u).animate(n,f,m)),h&&(u/=Math.pow(2,d-1)),i=(n={})[g]=0;i<d;i++)(s={})[g]=(v?"-=":"+=")+u,o.animate(s,f,m).animate(n,f,m),u=h?2*u:u/2;h&&((s={opacity:0})[g]=(v?"-=":"+=")+u,o.animate(s,f,m)),o.queue(function(){h&&o.hide(),P.effects.restore(o,a),P.effects.removeWrapper(o),e()}),1<b&&_.splice.apply(_,[1,0].concat(_.splice(b,p+1))),o.dequeue()},P.effects.effect.clip=function(t,e){var i,s,n,o=P(this),a=["position","top","bottom","left","right","height","width"],r="show"===P.effects.setMode(o,t.mode||"hide"),h="vertical"===(t.direction||"vertical"),l=h?"height":"width",c=h?"top":"left",u={};P.effects.save(o,a),o.show(),i=P.effects.createWrapper(o).css({overflow:"hidden"}),n=(s="IMG"===o[0].tagName?i:o)[l](),r&&(s.css(l,0),s.css(c,n/2)),u[l]=r?n:0,u[c]=r?0:n/2,s.animate(u,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){r||o.hide(),P.effects.restore(o,a),P.effects.removeWrapper(o),e()}})},P.effects.effect.drop=function(t,e){var i,s=P(this),n=["position","top","bottom","left","right","opacity","height","width"],o=P.effects.setMode(s,t.mode||"hide"),a="show"===o,r=t.direction||"left",h="up"===r||"down"===r?"top":"left",l="up"===r||"left"===r?"pos":"neg",c={opacity:a?1:0};P.effects.save(s,n),s.show(),P.effects.createWrapper(s),i=t.distance||s["top"===h?"outerHeight":"outerWidth"](!0)/2,a&&s.css("opacity",0).css(h,"pos"===l?-i:i),c[h]=(a?"pos"===l?"+=":"-=":"pos"===l?"-=":"+=")+i,s.animate(c,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===o&&s.hide(),P.effects.restore(s,n),P.effects.removeWrapper(s),e()}})},P.effects.effect.explode=function(t,e){var i,s,n,o,a,r,h=t.pieces?Math.round(Math.sqrt(t.pieces)):3,l=h,c=P(this),u="show"===P.effects.setMode(c,t.mode||"hide"),d=c.show().css("visibility","hidden").offset(),p=Math.ceil(c.outerWidth()/l),f=Math.ceil(c.outerHeight()/h),m=[];function g(){m.push(this),m.length===h*l&&function(){c.css({visibility:"visible"}),P(m).remove(),u||c.hide();e()}()}for(i=0;i<h;i++)for(o=d.top+i*f,r=i-(h-1)/2,s=0;s<l;s++)n=d.left+s*p,a=s-(l-1)/2,c.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-s*p,top:-i*f}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:p,height:f,left:n+(u?a*p:0),top:o+(u?r*f:0),opacity:u?0:1}).animate({left:n+(u?0:a*p),top:o+(u?0:r*f),opacity:u?1:0},t.duration||500,t.easing,g)},P.effects.effect.fade=function(t,e){var i=P(this),s=P.effects.setMode(i,t.mode||"toggle");i.animate({opacity:s},{queue:!1,duration:t.duration,easing:t.easing,complete:e})},P.effects.effect.fold=function(t,e){var i,s,n=P(this),o=["position","top","bottom","left","right","height","width"],a=P.effects.setMode(n,t.mode||"hide"),r="show"===a,h="hide"===a,l=t.size||15,c=/([0-9]+)%/.exec(l),u=!!t.horizFirst,d=r!==u,p=d?["width","height"]:["height","width"],f=t.duration/2,m={},g={};P.effects.save(n,o),n.show(),i=P.effects.createWrapper(n).css({overflow:"hidden"}),s=d?[i.width(),i.height()]:[i.height(),i.width()],c&&(l=parseInt(c[1],10)/100*s[h?0:1]),r&&i.css(u?{height:0,width:l}:{height:l,width:0}),m[p[0]]=r?s[0]:l,g[p[1]]=r?s[1]:0,i.animate(m,f,t.easing).animate(g,f,t.easing,function(){h&&n.hide(),P.effects.restore(n,o),P.effects.removeWrapper(n),e()})},P.effects.effect.highlight=function(t,e){var i=P(this),s=["backgroundImage","backgroundColor","opacity"],n=P.effects.setMode(i,t.mode||"show"),o={backgroundColor:i.css("backgroundColor")};"hide"===n&&(o.opacity=0),P.effects.save(i,s),i.show().css({backgroundImage:"none",backgroundColor:t.color||"#ffff99"}).animate(o,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===n&&i.hide(),P.effects.restore(i,s),e()}})},P.effects.effect.size=function(o,t){var e,i,a,r=P(this),s=["position","top","bottom","left","right","width","height","overflow","opacity"],h=["width","height","overflow"],n=["fontSize"],l=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],c=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],u=P.effects.setMode(r,o.mode||"effect"),d=o.restore||"effect"!==u,p=o.scale||"both",f=o.origin||["middle","center"],m=r.css("position"),g=d?s:["position","top","bottom","left","right","overflow","opacity"],v={height:0,width:0,outerHeight:0,outerWidth:0};"show"===u&&r.show(),e={height:r.height(),width:r.width(),outerHeight:r.outerHeight(),outerWidth:r.outerWidth()},"toggle"===o.mode&&"show"===u?(r.from=o.to||v,r.to=o.from||e):(r.from=o.from||("show"===u?v:e),r.to=o.to||("hide"===u?v:e)),a={from:{y:r.from.height/e.height,x:r.from.width/e.width},to:{y:r.to.height/e.height,x:r.to.width/e.width}},"box"!==p&&"both"!==p||(a.from.y!==a.to.y&&(g=g.concat(l),r.from=P.effects.setTransition(r,l,a.from.y,r.from),r.to=P.effects.setTransition(r,l,a.to.y,r.to)),a.from.x!==a.to.x&&(g=g.concat(c),r.from=P.effects.setTransition(r,c,a.from.x,r.from),r.to=P.effects.setTransition(r,c,a.to.x,r.to))),"content"!==p&&"both"!==p||a.from.y!==a.to.y&&(g=g.concat(n).concat(h),r.from=P.effects.setTransition(r,n,a.from.y,r.from),r.to=P.effects.setTransition(r,n,a.to.y,r.to)),P.effects.save(r,g),r.show(),P.effects.createWrapper(r),r.css("overflow","hidden").css(r.from),f&&(i=P.effects.getBaseline(f,e),r.from.top=(e.outerHeight-r.outerHeight())*i.y,r.from.left=(e.outerWidth-r.outerWidth())*i.x,r.to.top=(e.outerHeight-r.to.outerHeight)*i.y,r.to.left=(e.outerWidth-r.to.outerWidth)*i.x),r.css(r.from),"content"!==p&&"both"!==p||(l=l.concat(["marginTop","marginBottom"]).concat(n),c=c.concat(["marginLeft","marginRight"]),h=s.concat(l).concat(c),r.find("*[width]").each(function(){var t=P(this),e=t.height(),i=t.width(),s=t.outerHeight(),n=t.outerWidth();d&&P.effects.save(t,h),t.from={height:e*a.from.y,width:i*a.from.x,outerHeight:s*a.from.y,outerWidth:n*a.from.x},t.to={height:e*a.to.y,width:i*a.to.x,outerHeight:e*a.to.y,outerWidth:i*a.to.x},a.from.y!==a.to.y&&(t.from=P.effects.setTransition(t,l,a.from.y,t.from),t.to=P.effects.setTransition(t,l,a.to.y,t.to)),a.from.x!==a.to.x&&(t.from=P.effects.setTransition(t,c,a.from.x,t.from),t.to=P.effects.setTransition(t,c,a.to.x,t.to)),t.css(t.from),t.animate(t.to,o.duration,o.easing,function(){d&&P.effects.restore(t,h)})})),r.animate(r.to,{queue:!1,duration:o.duration,easing:o.easing,complete:function(){0===r.to.opacity&&r.css("opacity",r.from.opacity),"hide"===u&&r.hide(),P.effects.restore(r,g),d||("static"===m?r.css({position:"relative",top:r.to.top,left:r.to.left}):P.each(["top","left"],function(n,t){r.css(t,function(t,e){var i=parseInt(e,10),s=n?r.to.left:r.to.top;return"auto"===e?s+"px":i+s+"px"})})),P.effects.removeWrapper(r),t()}})},P.effects.effect.scale=function(t,e){var i=P(this),s=P.extend(!0,{},t),n=P.effects.setMode(i,t.mode||"effect"),o=parseInt(t.percent,10)||(0===parseInt(t.percent,10)?0:"hide"===n?0:100),a=t.direction||"both",r=t.origin,h={height:i.height(),width:i.width(),outerHeight:i.outerHeight(),outerWidth:i.outerWidth()},l="horizontal"!==a?o/100:1,c="vertical"!==a?o/100:1;s.effect="size",s.queue=!1,s.complete=e,"effect"!==n&&(s.origin=r||["middle","center"],s.restore=!0),s.from=t.from||("show"===n?{height:0,width:0,outerHeight:0,outerWidth:0}:h),s.to={height:h.height*l,width:h.width*c,outerHeight:h.outerHeight*l,outerWidth:h.outerWidth*c},s.fade&&("show"===n&&(s.from.opacity=0,s.to.opacity=1),"hide"===n&&(s.from.opacity=1,s.to.opacity=0)),i.effect(s)},P.effects.effect.puff=function(t,e){var i=P(this),s=P.effects.setMode(i,t.mode||"hide"),n="hide"===s,o=parseInt(t.percent,10)||150,a=o/100,r={height:i.height(),width:i.width(),outerHeight:i.outerHeight(),outerWidth:i.outerWidth()};P.extend(t,{effect:"scale",queue:!1,fade:!0,mode:s,complete:e,percent:n?o:100,from:n?r:{height:r.height*a,width:r.width*a,outerHeight:r.outerHeight*a,outerWidth:r.outerWidth*a}}),i.effect(t)},P.effects.effect.pulsate=function(t,e){var i,s=P(this),n=P.effects.setMode(s,t.mode||"show"),o="show"===n,a="hide"===n,r=o||"hide"===n,h=2*(t.times||5)+(r?1:0),l=t.duration/h,c=0,u=s.queue(),d=u.length;for(!o&&s.is(":visible")||(s.css("opacity",0).show(),c=1),i=1;i<h;i++)s.animate({opacity:c},l,t.easing),c=1-c;s.animate({opacity:c},l,t.easing),s.queue(function(){a&&s.hide(),e()}),1<d&&u.splice.apply(u,[1,0].concat(u.splice(d,h+1))),s.dequeue()},P.effects.effect.shake=function(t,e){var i,s=P(this),n=["position","top","bottom","left","right","height","width"],o=P.effects.setMode(s,t.mode||"effect"),a=t.direction||"left",r=t.distance||20,h=t.times||3,l=2*h+1,c=Math.round(t.duration/l),u="up"===a||"down"===a?"top":"left",d="up"===a||"left"===a,p={},f={},m={},g=s.queue(),v=g.length;for(P.effects.save(s,n),s.show(),P.effects.createWrapper(s),p[u]=(d?"-=":"+=")+r,f[u]=(d?"+=":"-=")+2*r,m[u]=(d?"-=":"+=")+2*r,s.animate(p,c,t.easing),i=1;i<h;i++)s.animate(f,c,t.easing).animate(m,c,t.easing);s.animate(f,c,t.easing).animate(p,c/2,t.easing).queue(function(){"hide"===o&&s.hide(),P.effects.restore(s,n),P.effects.removeWrapper(s),e()}),1<v&&g.splice.apply(g,[1,0].concat(g.splice(v,l+1))),s.dequeue()},P.effects.effect.slide=function(t,e){var i,s=P(this),n=["position","top","bottom","left","right","width","height"],o=P.effects.setMode(s,t.mode||"show"),a="show"===o,r=t.direction||"left",h="up"===r||"down"===r?"top":"left",l="up"===r||"left"===r,c={};P.effects.save(s,n),s.show(),i=t.distance||s["top"===h?"outerHeight":"outerWidth"](!0),P.effects.createWrapper(s).css({overflow:"hidden"}),a&&s.css(h,l?isNaN(i)?"-"+i:-i:i),c[h]=(a?l?"+=":"-=":l?"-=":"+=")+i,s.animate(c,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){"hide"===o&&s.hide(),P.effects.restore(s,n),P.effects.removeWrapper(s),e()}})},P.effects.effect.transfer=function(t,e){var i=P(this),s=P(t.to),n="fixed"===s.css("position"),o=P("body"),a=n?o.scrollTop():0,r=n?o.scrollLeft():0,h=s.offset(),l={top:h.top-a,left:h.left-r,height:s.innerHeight(),width:s.innerWidth()},c=i.offset(),u=P("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(t.className).css({top:c.top-a,left:c.left-r,height:i.innerHeight(),width:i.innerWidth(),position:n?"fixed":"absolute"}).animate(l,t.duration,t.easing,function(){u.remove(),e()})}});


/*!
 * jQuery Migrate - v3.0.0 - 2016-06-09
 * Copyright jQuery Foundation and other contributors
 */
(function( jQuery, window ) {
    "use strict";
    
    
    jQuery.migrateVersion = "3.0.0";
    
    
    ( function() {
    
        // Support: IE9 only
        // IE9 only creates console object when dev tools are first opened
        // Also, avoid Function#bind here to simplify PhantomJS usage
        var log = window.console && window.console.log &&
                function() { window.console.log.apply( window.console, arguments ); },
            rbadVersions = /^[12]\./;
    
        if ( !log ) {
            return;
        }
    
        // Need jQuery 3.0.0+ and no older Migrate loaded
        if ( !jQuery || rbadVersions.test( jQuery.fn.jquery ) ) {
            log( "JQMIGRATE: jQuery 3.0.0+ REQUIRED" );
        }
        if ( jQuery.migrateWarnings ) {
            log( "JQMIGRATE: Migrate plugin loaded multiple times" );
        }
    
        // Show a message on the console so devs know we're active
        log( "JQMIGRATE: Migrate is installed" +
            ( jQuery.migrateMute ? "" : " with logging active" ) +
            ", version " + jQuery.migrateVersion );
    
    } )();
    
    var warnedAbout = {};
    
    // List of warnings already given; public read only
    jQuery.migrateWarnings = [];
    
    // Set to false to disable traces that appear with warnings
    if ( jQuery.migrateTrace === undefined ) {
        jQuery.migrateTrace = true;
    }
    
    // Forget any warnings we've already given; public
    jQuery.migrateReset = function() {
        warnedAbout = {};
        jQuery.migrateWarnings.length = 0;
    };
    
    function migrateWarn( msg ) {
        var console = window.console;
        if ( !warnedAbout[ msg ] ) {
            warnedAbout[ msg ] = true;
            jQuery.migrateWarnings.push( msg );
            if ( console && console.warn && !jQuery.migrateMute ) {
                console.warn( "JQMIGRATE: " + msg );
                if ( jQuery.migrateTrace && console.trace ) {
                    console.trace();
                }
            }
        }
    }
    
    function migrateWarnProp( obj, prop, value, msg ) {
        Object.defineProperty( obj, prop, {
            configurable: true,
            enumerable: true,
            get: function() {
                migrateWarn( msg );
                return value;
            }
        } );
    }
    
    if ( document.compatMode === "BackCompat" ) {
    
        // JQuery has never supported or tested Quirks Mode
        migrateWarn( "jQuery is not compatible with Quirks Mode" );
    }
    
    
    var oldInit = jQuery.fn.init,
        oldIsNumeric = jQuery.isNumeric,
        oldFind = jQuery.find,
        rattrHashTest = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,
        rattrHashGlob = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g;
    
    jQuery.fn.init = function( arg1 ) {
        var args = Array.prototype.slice.call( arguments );
    
        if ( typeof arg1 === "string" && arg1 === "#" ) {
    
            // JQuery( "#" ) is a bogus ID selector, but it returned an empty set before jQuery 3.0
            migrateWarn( "jQuery( '#' ) is not a valid selector" );
            args[ 0 ] = [];
        }
    
        return oldInit.apply( this, args );
    };
    jQuery.fn.init.prototype = jQuery.fn;
    
    jQuery.find = function( selector ) {
        var args = Array.prototype.slice.call( arguments );
    
        // Support: PhantomJS 1.x
        // String#match fails to match when used with a //g RegExp, only on some strings
        if ( typeof selector === "string" && rattrHashTest.test( selector ) ) {
    
            // The nonstandard and undocumented unquoted-hash was removed in jQuery 1.12.0
            // First see if qS thinks it's a valid selector, if so avoid a false positive
            try {
                document.querySelector( selector );
            } catch ( err1 ) {
    
                // Didn't *look* valid to qSA, warn and try quoting what we think is the value
                selector = selector.replace( rattrHashGlob, function( _, attr, op, value ) {
                    return "[" + attr + op + "\"" + value + "\"]";
                } );
    
                // If the regexp *may* have created an invalid selector, don't update it
                // Note that there may be false alarms if selector uses jQuery extensions
                try {
                    document.querySelector( selector );
                    migrateWarn( "Attribute selector with '#' must be quoted: " + args[ 0 ] );
                    args[ 0 ] = selector;
                } catch ( err2 ) {
                    migrateWarn( "Attribute selector with '#' was not fixed: " + args[ 0 ] );
                }
            }
        }
    
        return oldFind.apply( this, args );
    };
    
    // Copy properties attached to original jQuery.find method (e.g. .attr, .isXML)
    var findProp;
    for ( findProp in oldFind ) {
        if ( Object.prototype.hasOwnProperty.call( oldFind, findProp ) ) {
            jQuery.find[ findProp ] = oldFind[ findProp ];
        }
    }
    
    // The number of elements contained in the matched element set
    jQuery.fn.size = function() {
        migrateWarn( "jQuery.fn.size() is deprecated; use the .length property" );
        return this.length;
    };
    
    jQuery.parseJSON = function() {
        migrateWarn( "jQuery.parseJSON is deprecated; use JSON.parse" );
        return JSON.parse.apply( null, arguments );
    };
    
    jQuery.isNumeric = function( val ) {
    
        // The jQuery 2.2.3 implementation of isNumeric
        function isNumeric2( obj ) {
            var realStringObj = obj && obj.toString();
            return !jQuery.isArray( obj ) && ( realStringObj - parseFloat( realStringObj ) + 1 ) >= 0;
        }
    
        var newValue = oldIsNumeric( val ),
            oldValue = isNumeric2( val );
    
        if ( newValue !== oldValue ) {
            migrateWarn( "jQuery.isNumeric() should not be called on constructed objects" );
        }
    
        return oldValue;
    };
    
    migrateWarnProp( jQuery, "unique", jQuery.uniqueSort,
        "jQuery.unique is deprecated, use jQuery.uniqueSort" );
    
    // Now jQuery.expr.pseudos is the standard incantation
    migrateWarnProp( jQuery.expr, "filters", jQuery.expr.pseudos,
        "jQuery.expr.filters is now jQuery.expr.pseudos" );
    migrateWarnProp( jQuery.expr, ":", jQuery.expr.pseudos,
        "jQuery.expr[\":\"] is now jQuery.expr.pseudos" );
    
    
    var oldAjax = jQuery.ajax;
    
    jQuery.ajax = function( ) {
        var jQXHR = oldAjax.apply( this, arguments );
    
        // Be sure we got a jQXHR (e.g., not sync)
        if ( jQXHR.promise ) {
            migrateWarnProp( jQXHR, "success", jQXHR.done,
                "jQXHR.success is deprecated and removed" );
            migrateWarnProp( jQXHR, "error", jQXHR.fail,
                "jQXHR.error is deprecated and removed" );
            migrateWarnProp( jQXHR, "complete", jQXHR.always,
                "jQXHR.complete is deprecated and removed" );
        }
    
        return jQXHR;
    };
    
    
    var oldRemoveAttr = jQuery.fn.removeAttr,
        oldToggleClass = jQuery.fn.toggleClass,
        rmatchNonSpace = /\S+/g;
    
    jQuery.fn.removeAttr = function( name ) {
        var self = this;
    
        jQuery.each( name.match( rmatchNonSpace ), function( i, attr ) {
            if ( jQuery.expr.match.bool.test( attr ) ) {
                migrateWarn( "jQuery.fn.removeAttr no longer sets boolean properties: " + attr );
                self.prop( attr, false );
            }
        } );
    
        return oldRemoveAttr.apply( this, arguments );
    };
    
    jQuery.fn.toggleClass = function( state ) {
    
        // Only deprecating no-args or single boolean arg
        if ( state !== undefined && typeof state !== "boolean" ) {
            return oldToggleClass.apply( this, arguments );
        }
    
        migrateWarn( "jQuery.fn.toggleClass( boolean ) is deprecated" );
    
        // Toggle entire class name of each element
        return this.each( function() {
            var className = this.getAttribute && this.getAttribute( "class" ) || "";
    
            if ( className ) {
                jQuery.data( this, "__className__", className );
            }
    
            // If the element has a class name or if we're passed `false`,
            // then remove the whole classname (if there was one, the above saved it).
            // Otherwise bring back whatever was previously saved (if anything),
            // falling back to the empty string if nothing was stored.
            if ( this.setAttribute ) {
                this.setAttribute( "class",
                    className || state === false ?
                    "" :
                    jQuery.data( this, "__className__" ) || ""
                );
            }
        } );
    };
    
    
    var internalSwapCall = false;
    
    // If this version of jQuery has .swap(), don't false-alarm on internal uses
    if ( jQuery.swap ) {
        jQuery.each( [ "height", "width", "reliableMarginRight" ], function( _, name ) {
            var oldHook = jQuery.cssHooks[ name ] && jQuery.cssHooks[ name ].get;
    
            if ( oldHook ) {
                jQuery.cssHooks[ name ].get = function() {
                    var ret;
    
                    internalSwapCall = true;
                    ret = oldHook.apply( this, arguments );
                    internalSwapCall = false;
                    return ret;
                };
            }
        } );
    }
    
    jQuery.swap = function( elem, options, callback, args ) {
        var ret, name,
            old = {};
    
        if ( !internalSwapCall ) {
            migrateWarn( "jQuery.swap() is undocumented and deprecated" );
        }
    
        // Remember the old values, and insert the new ones
        for ( name in options ) {
            old[ name ] = elem.style[ name ];
            elem.style[ name ] = options[ name ];
        }
    
        ret = callback.apply( elem, args || [] );
    
        // Revert the old values
        for ( name in options ) {
            elem.style[ name ] = old[ name ];
        }
    
        return ret;
    };
    
    var oldData = jQuery.data;
    
    jQuery.data = function( elem, name, value ) {
        var curData;
    
        // If the name is transformed, look for the un-transformed name in the data object
        if ( name && name !== jQuery.camelCase( name ) ) {
            curData = jQuery.hasData( elem ) && oldData.call( this, elem );
            if ( curData && name in curData ) {
                migrateWarn( "jQuery.data() always sets/gets camelCased names: " + name );
                if ( arguments.length > 2 ) {
                    curData[ name ] = value;
                }
                return curData[ name ];
            }
        }
    
        return oldData.apply( this, arguments );
    };
    
    var oldTweenRun = jQuery.Tween.prototype.run;
    
    jQuery.Tween.prototype.run = function( percent ) {
        if ( jQuery.easing[ this.easing ].length > 1 ) {
            migrateWarn(
                "easing function " +
                "\"jQuery.easing." + this.easing.toString() +
                "\" should use only first argument"
            );
    
            jQuery.easing[ this.easing ] = jQuery.easing[ this.easing ].bind(
                jQuery.easing,
                percent, this.options.duration * percent, 0, 1, this.options.duration
            );
        }
    
        oldTweenRun.apply( this, arguments );
    };
    
    var oldLoad = jQuery.fn.load,
        originalFix = jQuery.event.fix;
    
    jQuery.event.props = [];
    jQuery.event.fixHooks = {};
    
    jQuery.event.fix = function( originalEvent ) {
        var event,
            type = originalEvent.type,
            fixHook = this.fixHooks[ type ],
            props = jQuery.event.props;
    
        if ( props.length ) {
            migrateWarn( "jQuery.event.props are deprecated and removed: " + props.join() );
            while ( props.length ) {
                jQuery.event.addProp( props.pop() );
            }
        }
    
        if ( fixHook && !fixHook._migrated_ ) {
            fixHook._migrated_ = true;
            migrateWarn( "jQuery.event.fixHooks are deprecated and removed: " + type );
            if ( ( props = fixHook.props ) && props.length ) {
                while ( props.length ) {
                   jQuery.event.addProp( props.pop() );
                }
            }
        }
    
        event = originalFix.call( this, originalEvent );
    
        return fixHook && fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
    };
    
    jQuery.each( [ "load", "unload", "error" ], function( _, name ) {
    
        jQuery.fn[ name ] = function() {
            var args = Array.prototype.slice.call( arguments, 0 );
    
            // If this is an ajax load() the first arg should be the string URL;
            // technically this could also be the "Anything" arg of the event .load()
            // which just goes to show why this dumb signature has been deprecated!
            // jQuery custom builds that exclude the Ajax module justifiably die here.
            if ( name === "load" && typeof args[ 0 ] === "string" ) {
                return oldLoad.apply( this, args );
            }
    
            migrateWarn( "jQuery.fn." + name + "() is deprecated" );
    
            args.splice( 0, 0, name );
            if ( arguments.length ) {
                return this.on.apply( this, args );
            }
    
            // Use .triggerHandler here because:
            // - load and unload events don't need to bubble, only applied to window or image
            // - error event should not bubble to window, although it does pre-1.7
            // See http://bugs.jquery.com/ticket/11820
            this.triggerHandler.apply( this, args );
            return this;
        };
    
    } );
    
    // Trigger "ready" event only once, on document ready
    jQuery( function() {
        jQuery( document ).triggerHandler( "ready" );
    } );
    
    jQuery.event.special.ready = {
        setup: function() {
            if ( this === document ) {
                migrateWarn( "'ready' event is deprecated" );
            }
        }
    };
    
    jQuery.fn.extend( {
    
        bind: function( types, data, fn ) {
            migrateWarn( "jQuery.fn.bind() is deprecated" );
            return this.on( types, null, data, fn );
        },
        unbind: function( types, fn ) {
            migrateWarn( "jQuery.fn.unbind() is deprecated" );
            return this.off( types, null, fn );
        },
        delegate: function( selector, types, data, fn ) {
            migrateWarn( "jQuery.fn.delegate() is deprecated" );
            return this.on( types, selector, data, fn );
        },
        undelegate: function( selector, types, fn ) {
            migrateWarn( "jQuery.fn.undelegate() is deprecated" );
            return arguments.length === 1 ?
                this.off( selector, "**" ) :
                this.off( types, selector || "**", fn );
        }
    } );
    
    
    var oldOffset = jQuery.fn.offset;
    
    jQuery.fn.offset = function() {
        var docElem,
            elem = this[ 0 ],
            origin = { top: 0, left: 0 };
    
        if ( !elem || !elem.nodeType ) {
            migrateWarn( "jQuery.fn.offset() requires a valid DOM element" );
            return origin;
        }
    
        docElem = ( elem.ownerDocument || document ).documentElement;
        if ( !jQuery.contains( docElem, elem ) ) {
            migrateWarn( "jQuery.fn.offset() requires an element connected to a document" );
            return origin;
        }
    
        return oldOffset.apply( this, arguments );
    };
    
    
    var oldParam = jQuery.param;
    
    jQuery.param = function( data, traditional ) {
        var ajaxTraditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
    
        if ( traditional === undefined && ajaxTraditional ) {
    
            migrateWarn( "jQuery.param() no longer uses jQuery.ajaxSettings.traditional" );
            traditional = ajaxTraditional;
        }
    
        return oldParam.call( this, data, traditional );
    };
    
    var oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack;
    
    jQuery.fn.andSelf = function() {
        migrateWarn( "jQuery.fn.andSelf() replaced by jQuery.fn.addBack()" );
        return oldSelf.apply( this, arguments );
    };
    
    
    var oldDeferred = jQuery.Deferred,
        tuples = [
    
            // Action, add listener, callbacks, .then handlers, final state
            [ "resolve", "done", jQuery.Callbacks( "once memory" ),
                jQuery.Callbacks( "once memory" ), "resolved" ],
            [ "reject", "fail", jQuery.Callbacks( "once memory" ),
                jQuery.Callbacks( "once memory" ), "rejected" ],
            [ "notify", "progress", jQuery.Callbacks( "memory" ),
                jQuery.Callbacks( "memory" ) ]
        ];
    
    jQuery.Deferred = function( func ) {
        var deferred = oldDeferred(),
            promise = deferred.promise();
    
        deferred.pipe = promise.pipe = function( /* fnDone, fnFail, fnProgress */ ) {
            var fns = arguments;
    
            migrateWarn( "deferred.pipe() is deprecated" );
    
            return jQuery.Deferred( function( newDefer ) {
                jQuery.each( tuples, function( i, tuple ) {
                    var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];
    
                    // Deferred.done(function() { bind to newDefer or newDefer.resolve })
                    // deferred.fail(function() { bind to newDefer or newDefer.reject })
                    // deferred.progress(function() { bind to newDefer or newDefer.notify })
                    deferred[ tuple[ 1 ] ]( function() {
                        var returned = fn && fn.apply( this, arguments );
                        if ( returned && jQuery.isFunction( returned.promise ) ) {
                            returned.promise()
                                .done( newDefer.resolve )
                                .fail( newDefer.reject )
                                .progress( newDefer.notify );
                        } else {
                            newDefer[ tuple[ 0 ] + "With" ](
                                this === promise ? newDefer.promise() : this,
                                fn ? [ returned ] : arguments
                            );
                        }
                    } );
                } );
                fns = null;
            } ).promise();
    
        };
    
        if ( func ) {
            func.call( deferred, deferred );
        }
    
        return deferred;
    };
    
    
    
    })( jQuery, window );


    
//WEBUI = {};
//WEBUI.CONTEXT = {};
//WEBUI.CONTEXT.RootPath = "";

var BasePath = "";

window.mobile = false;
if (matchMedia) {
    var mq = window.matchMedia("(min-width: 500px)");
    mq.addListener(WidthChange);
    WidthChange(mq);
}

// media query change
function WidthChange(mq) {
    if (mq.matches) {
        window.mobile = false;
        // window width is at least 500px
    } else {
        window.mobile = true;
        // window width is less than textBo
    }

}

WEBUI.UICONTROLS.ready(function () {

    $.fn.actionButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            $(page.selectedObject).attr("type", "button");
            $(page.selectedObject).addClass("button");

            //Expose the click event
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler();
                            }); //Pass function with empty argumnets.Inside that pass the necessary arguments in scope.
                        } else
                            page.clickHandler();
                    }
                });
            };

            //Available public interface.
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };




        });

    }



    //Default button action implementation
    //Note the default button should be inside the panel that has attribute as default-button
    //$("body").keydown(function (e) {
    //    if (e.keyCode == 13) {

    //        var defButton = $(e.target).closest("[default-button]").attr("default-button");
    //        setTimeout(function () {
    //            $(e.target).closest("[default-button]").find("[controlid=" + defButton + "]").click();
    //        }, 50);

    //        e.stopPropagation();
    //        e.preventDefault();
    //    }
    //});

    //Generic html Control.Returns a plain jquery object 
    //$.fn.htmlControl = function () {
    //    return $.pageController.getControl(this, function (page) {
    //        page.interface = $(page.selectedObject);
    //        page.attachCommonroperties();
    //    });
    //}


    $.fn.multiButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            //  $(page.selectedObject).attr("type", "button");
            var htmlTemplate = '<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">New Invoice  ';
            htmlTemplate = htmlTemplate + '<span class="caret"></span></button>';
            htmlTemplate = htmlTemplate + '<ul class="dropdown-menu">';
            $($(page.selectedObject).attr("multi-buttons").split(",")).each(function (i, item) {
                htmlTemplate = htmlTemplate + '<li><a href="#">' + item + '</a></li>';
            });

            //htmlTemplate= htmlTemplate + '<li><a href="#">CSS</a></li>';
            //htmlTemplate= htmlTemplate + '<li><a href="#">JavaScript</a></li>';
            htmlTemplate = htmlTemplate + '</ul>';
            $(page.selectedObject).attr("class", "dropdown");
            $(page.selectedObject).html(htmlTemplate);

            //Page Load
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).find("li").click(function () {
                    if (page.clickHandler != null) {
                        //Unsaved changes check
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler($(this).text());
                            });
                        }
                        else
                            page.clickHandler($(this).text());
                    }
                });
            };

            //Available public interface.
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };
            page.attachCommonroperties();

        });

    }
    $.fn.checkBox = function () {
        return $.pageController.getControl(this, function (chkbox) {
            $(chkbox.selectedObject).addClass("chkBox");
            //$(chkbox.selectedObject).parent().children('.chkBox').wrap('<label class="chk-container"></label>');
            //$('<span> ' + $(chkbox.selectedObject).parent().children('.chkBox').attr("chkValue") + '</span>').insertAfter($(chkbox.selectedObject).parent().children('.chkBox'));

            chkbox.attachCommonroperties();
        })
    }
    $.fn.radioBox = function () {
        return $.pageController.getControl(this, function (radiobox) {
            $(radiobox.selectedObject).addClass("radioBox");

            radiobox.attachCommonroperties();
        })
    }
    $.fn.modalPopUp = function () {
        return $.pageController.getControl(this, function (popup) {

            $(popup.selectedObject).addClass("modal");

            $(popup.selectedObject).parent().children('.modal').wrapInner('<div class="modal-body"></div>');
            $(popup.selectedObject).parent().children('.modal').wrapInner('<div class="modal-content"></div>');

            $('<div class="modal-header"><span class="close">&times;</span><h4 class="modal-title" ></h4></div>').insertBefore($(popup.selectedObject).find('.modal-body'));
            $('<div class="modal-footer"></div>').insertAfter($(popup.selectedObject).find('.modal-body'));

            popup.interface = {                  
                title: function (title) {                    
                    $(popup.selectedObject).children().find('.modal-title').html(title);
                },
                open: function (title) {
                    $(popup.selectedObject).children().find('.modal-title').html(title);
                    $(popup.selectedObject).css("display", "block");
                    $(popup.selectedObject).find('.close').click(function () {
                        $(popup.selectedObject).css("display", "none");
                    })
                },
                width: function (width) {                    //Width of the Popup
                    $(popup.selectedObject).children().find('.modal-content').css("width", width);
                },
                height: function (height) {                   //Height of the Popup

                    $(popup.selectedObject).children().find('.modal-body').css("height", height);
                },
                close: function () {
                    $(popup.selectedObject).children().find('.modal').css("display", "none");
                }
            }


            popup.attachCommonroperties();
        })
    }
    //Action Buttons
    //Primary Save or write operation
    $.fn.primaryWriteButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            $(page.selectedObject).attr("type", "button");
            $(page.selectedObject).addClass("primaryWriteButton");

            //Page Load
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        //Unsaved changes check
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler();
                            });
                        }
                        else
                            page.clickHandler();
                    }
                });
            };

            //Available public interface.
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };
            page.attachCommonroperties();

        });

    }
    //Primary Read,Search or preview information
    $.fn.primaryReadButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            $(page.selectedObject).attr("type", "button");
            $(page.selectedObject).addClass("primaryReadButton");

            //Page Load
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        //Unsaved changes check
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler();
                            });
                        }
                        else
                            page.clickHandler();
                    }
                });
            };

            //Available public interface.
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };
            page.attachCommonroperties();
        });
    }
    //All actions other than primary
    $.fn.secondaryButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            $(page.selectedObject).attr("type", "button");
            $(page.selectedObject).addClass("secondaryButton");

            //Page Load
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        //Unsaved changes check
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler();
                            });
                        }
                        else
                            page.clickHandler();
                    }
                });
            };

            //Available public interface.
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };
            page.attachCommonroperties();
        });
    }

    //Image Button
    $.fn.imageButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            $(page.selectedObject).attr("type", "image");
            $(page.selectedObject).addClass("");

            //Expose the click event
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler();
                            });    //Pass function with empty argumnets.Inside that pass the necessary arguments in scope.
                        }
                        else
                            page.clickHandler();
                    }
                });
            };

            //Available public interface for the Button
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };
            page.attachCommonroperties();
        });
    }

    //Buttons used to Switching view
    $.fn.tabButton = function () {
        return $.pageController.getControl(this, function (page) {
            //Set the default properties for button
            $(page.selectedObject).attr("type", "button");
            $(page.selectedObject).addClass("tabButton");

            //Page Load
            page.events.page_load = function () {
                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        //Unsaved changes check
                        if (typeof $(page.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(page.selectedObject).attr("requiredUnSavedCheck"), function () {
                                page.clickHandler();
                            });
                        }
                        else
                            page.clickHandler();
                    }
                });
            };

            //Available public interface.
            page.clickHandler = null;
            page.interface.click = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();
                } else {
                    page.clickHandler = handler;
                }
            };
            page.attachCommonroperties();
        });
    }

    //Label 
    $.fn.label = function () {
        return $.pageController.getControl(this, function (page) {
            $(page.selectedObject).addClass("class", "label");
            page.interface = $(page.selectedObject);
            page.attachCommonroperties();
            page.interface.value = function (val) {
                if (typeof val != "undefined")
                    $(page.selectedObject).html(val);
                return $(page.selectedObject).html();
            };
        });
    }
    //Label Value 
    $.fn.labelValue = function () {
        return $.pageController.getControl(this, function (page) {
            $(page.selectedObject).addClass("label-value");
            page.interface = $(page.selectedObject);
            page.attachCommonroperties();

            page.interface.value = function (val) {
                if (typeof val != "undefined")
                    $(page.selectedObject).html(val);
                return $(page.selectedObject).html();
            };
        });
    }

    //Form Controls
    //TextBox
    $.fn.textBox = function () {
        return $.pageController.getControl(this, function (page) {

            //Event handlers
            page.changeHandler = null;
            page.focusHandler = null;
            page.blurHandler = null;
            page.keyUpHandler = null;


            //Capture and Bind Events
            page.events.page_load = function () {
                //Change Event
                $(page.selectedObject).change(function () {
                    if (page.changeHandler != null) {
                        page.changeHandler();
                    }
                });

                //Focus Event
                $(page.selectedObject).focus(function () {
                    if (page.focusHandler != null) {
                        page.focusHandler();
                    }
                });

                //Blur Event
                $(page.selectedObject).blur(function () {
                    if (page.blurHandler != null) {
                        page.blurHandler();
                    }
                });


                //keyup Event
                $(page.selectedObject).keyup(function () {
                    if (page.keyUpHandler != null) {
                        page.keyUpHandler();
                    }
                });






            }

            //Available public Events.            
            page.interface.change = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).change();
                } else {
                    page.changeHandler = handler;
                }
            };
            page.interface.focus = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).focus();
                } else {
                    page.focusHandler = handler;
                }
            };
            page.interface.blur = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).blur();
                } else {
                    page.blurHandler = handler;
                }
            };

            page.interface.keyup = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).keyup();
                } else {
                    page.keyUpHandler = handler;
                }
            };




            //Available Properties
            //Commmon properties like show,hide,css,addClass,removeClass
            page.attachCommonroperties();

            //Get and Set textbox value.
            page.interface.value = function (data) {
                if (typeof data == "undefined")
                    return $(page.selectedObject).val();
                else
                    return $(page.selectedObject).val(data);
            }

            page.interface.val = function (data) {
                if (typeof data == "undefined")
                    return $(page.selectedObject).val();
                else
                    return $(page.selectedObject).val(data);
            }

            //Width of the textbox
            page.interface.width = function (value) {
                $(page.selectedObject).css("width", value);
            }

            //Make textbox readonly
            // ReSharper disable once UnusedParameter
            page.interface.readOnly = function (value) {
                //TODO
            }

            //Get and set the value. This method is depreciated.Use value instead
            //page.interface.val = function (data) {
            //    if (typeof data == "undefined")
            //        return $(page.selectedObject).val();
            //    else
            //        return $(page.selectedObject).val(data);
            //}

        });
    }
    //Hidden Control
    $.fn.hidden = function () {
        return $.pageController.getControl(this, function (page) {
            page.interface.value = function (data) {
                if (typeof data == "undefined")
                    return $(page.selectedObject).val();
                else
                    return $(page.selectedObject).val(data);
            }
        });
    }

    //DropDown List
    //Implement List Interface : selectedValue,selectedData,getValue,dataBind,selectionChange
    $.fn.dropDownList = function () {
        return $.pageController.getControl(this, function (page) {

            //Event Handler
            page.selectionChangeHandler = null;
            page.clickHandler = null;

            //Capture and Bind Events
            page.events.page_load = function () {
                //Change Event
                $(page.selectedObject).change(function () {
                    if (page.selectionChangeHandler != null) {
                        page.selectionChangeHandler();
                    }
                });

                //To support default button trigger when selection is triggered in drop.
                $(page.selectedObject).keydown(function (event) {
                    if (event.keyCode === $.ui.keyCode.ENTER) {
                        event.preventDefault();
                        event.stopPropagation();
                    }

                });

                //Click Event
                $(page.selectedObject).click(function () {
                    if (page.clickHandler != null) {
                        page.clickHandler();
                    }
                });

            }

            //Available public events
            //Fires dropdown selection change event or assign a event handler for selection change event.
            page.interface.selectionChange = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).change();     //Trigger Event
                } else {
                    page.selectionChangeHandler = handler;     //Assign Event handler
                }
            };

            //Fires dropdown click event or assign a event handler for click
            page.interface.triggerClick = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).click();     //Trigger Event
                } else {
                    page.clickHandler = handler;     //Assign Event handler
                }
            };



            //Available public Methods & Properties
            //Commmon properties like show,hide,css,addClass,removeClass
            page.attachCommonroperties();
            //Bind an object array to dropdown
            page.interface.dataBind = function (data, valueField, textField, addText) {
                page.valueField = valueField;
                page.textField = textField;
                //Clear dropdown
                $(page.selectedObject).get(0).options.length = 0;

                //Add Empty row If any
                if (typeof addText != "undefined")
                    $(page.selectedObject).get(0).options[0] = new Option(addText, "-1");

                //Bind data to dropdown.TODO Bind with array for better perfoemance
                $.each(data, function (i, dataitem) {
                    var opt = $("<option></option>");
                    opt.val(dataitem[valueField]);
                    opt.html(dataitem[textField]);
                    opt.attr("data", JSON.stringify(dataitem));
                    $(page.selectedObject).append(opt);
                });
                //change event is firing after bind only when count i > 
                if(data.length==1)
                    $(page.selectedObject).trigger('change');
            

            };
            //Get selected data
            page.interface.selectedData = function (value) {
                //Set the selection if value is passed.
                if (typeof (value) != "undefined") {
                    page.interface.selectedValue(value[page.valueField]);
                }
                //Get the selected data from the attribute data.
                if (typeof $(page.selectedObject).find(":selected").attr("data") != "undefined")
                    return JSON.parse($(page.selectedObject).find(":selected").attr("data"));
                else
                    return null;
            };
            //Get or set selected value.
            page.interface.selectedValue = function (value) {
                if (typeof (value) == "undefined")
                    return $(page.selectedObject).val();
                else
                    return $(page.selectedObject).val(value).trigger('change');
            };
            //Set the selected value. Append the value and text if it is not present in grid. Useful when displaying deleted informations.
            page.interface.selectedValueAppend = function (value, text) {
                //Append maximum one option.So remove previouly appended items.
                $(page.selectedObject).find("option[added]").remove();
                //Append the value and text if not available in select
                if ($(page.selectedObject).find("option[value='" + value + "']").length <= 0) {
                    var o = new Option(text, value);
                    $(o).attr("added", "true");
                    $(page.selectedObject).append(o);
                }
                //set the selected value                
                return $(page.selectedObject).val(value);
            };
            //Get value based on option Text
            page.interface.getValue = function (text) {
                if (text != undefined) {
                    return $(page.selectedObject).find("option").filter(function () {
                        return $(this).html() == text;
                    }).val();
                }
                else {

                    return page.selectedObject.find(":selected").text();
                }

            };



        });

    }

   

$.fn.checkBoxList = function () {
    return $.pageController.getControl(this, function (page) {

        //Event Handler
        page.selectionChangeHandler = null;
        page.clickHandler = null;
        page.allData={};
        page.selectedData={};
        page.valueField = null;
        page.textField =null;

        //Design the control for initial load
        $(page.selectedObject).html("<button>"+$(page.selectedObject).attr("place-holder")+"</button><span></span> <div class='dropdown-simple-content'></div>");         
        $(page.selectedObject).addClass("dropdown-simple");


        //Capture and Bind Events
        page.events.page_load = function () {              
           

        }

        //Available public events
        //Fires dropdown selection change event or assign a event handler for selection change event.
        page.interface.selectionChange = function (handler) {
            page.selectionChangeHandler = handler;     //Assign Event handler                
        };



        //Available public Methods & Properties
        //Commmon properties like show,hide,css,addClass,removeClass
        page.attachCommonroperties();

        //Bind an object array to dropdown
        page.interface.dataBind = function (data, valueField, textField, addText) {
            page.valueField = valueField;
            page.textField = textField;
            
            var html="";
            $(data).each(function(i,item){
                //Add item to model and ui
                page.allData[item[page.valueField]]=item;
                html=html+"<a data-value='" +item[page.valueField] + "' href='#'><img src='' style='height:15px;width:15px;''> " + item[page.textField]+ "</a>";
            });
            $(page.selectedObject).find(".dropdown-simple-content").html(html);              

            //Find selection event
            $(page.selectedObject).find(".dropdown-simple-content a").click(function () {
                //Add selection to model and ui
                if(!page.selectedData[$(this).attr("data-value")]){
                    page.selectedData[$(this).attr("data-value")]=page.allData[$(this).attr("data-value")] ;
                   
                    $(this).find("img").attr("src","/img/tick.jpg");
                }
                else{
                    delete page.selectedData[$(this).attr("data-value")];
                    $(this).find("img").attr("src","");
                }

                if(Object.keys(page.selectedData).length==0)
                    $(page.selectedObject).find("button").html("Select");
                else if(Object.keys(page.selectedData).length==1)
                    $(page.selectedObject).find("button").html(page.seletedDataArray()[0][page.textField]);
                else
                    $(page.selectedObject).find("button").html(Object.keys(page.selectedData).length + " selected");


                if (page.selectionChangeHandler != null) {
                   setTimeout(function(){
                    page.selectionChangeHandler(page.seletedDataArray());
                   },50);
                    
                }
            });

        };
        page.seletedDataArray = function () {
            var selArray = [];
            for (var prop in page.selectedData)
                selArray.push(page.selectedData[prop]);
            return selArray;
        }
        page.seletedValueArray = function (data) {
            var selArray = [];
            for (var prop in data)
                selArray.push(prop);
            return selArray;
        }

        //Get selected data
        page.interface.selectedData = function (data) {
            if (typeof (data) != "undefined") {
                page.interface.selectedValues(page.seletedValueArray());
            }                  
            return page.seletedDataArray();
        };

        //Get or set selected value.
        page.interface.selectedValues = function (value) {
            if (typeof (value) != "undefined" ){
                if (JSON.stringify(page.seletedValueArray(page.selectedData)) != JSON.stringify(value)) {

                    //Add neew selection to model and ui

                    $(page.selectedObject).find(".dropdown-simple-content a").each(function(i,item){
                        
                        $(value.split(",")).each(function (i, valItem) {
                            if($(item).attr("data-value")==valItem)                       
                                $(item).click();                         
                        });

                    });
                   /* page.selectedData = {};
                    $(value.split(",")).each(function (i, valItem) {
                        page.selectedData[valItem] = page.allData[valItem];
                    });

                    if (Object.keys(page.selectedData).length == 1)
                        $(page.selectedObject).find("button").html(page.selectedData[page.textField]);
                    else
                        $(page.selectedObject).find("button").html(Object.keys(page.selectedData).length + " selected");

                    if (page.selectionChangeHandler != null) {
                        setTimeout(function () {
                            page.selectionChangeHandler(page.seletedDataArray());
                        }, 50);
                    }*/
                }
               
            }
            return  page.seletedValueArray(page.selectedData) ;
        };

       



    });

}




$.fn.dropDownList2= function () {
    return $.pageController.getControl(this, function (page) {

        //Event Handler
        page.selectionChangeHandler = null;
        page.clickHandler = null;
        page.allData={};
        page.selectedData=null;
        page.valueField = null;
        page.textField =null;
        page.openListCount=0;

        //Design the control for initial load
        $(page.selectedObject).html("<div class='dropdown-simple-data-open'></div><div class='dropdown-simple-data-closed'><button>"+$(page.selectedObject).attr("place-holder")+"</button><span></span><span style='background: url(/img/cancel.png) 0% 0% / contain no-repeat; position: absolute; right: 5px; height: 11px; margin-top: 15px; width: 29px; display: none;'></span>  <div class='dropdown-simple-content'><div class='dropdown-simple-search'><div class='search-textbox'><input placeholder='Search By Text' type='text' /></div><div class='search-range'><input type='text' placeholder='Min' /> <input type='text' placeholder='Max' /></div></div><div class='dropdown-simple-data'></div></div></div>");         
        $(page.selectedObject).addClass("dropdown-simple");


        
    
        page.interface.clear=function(){
            $(page.selectedObject).find(".dropdown-simple-data-closed button").html($(page.selectedObject).attr("place-holder"));
            page.selectedData=null;
            $(page.selectedObject).find(".dropdown-simple-data-closed input[type=text]").html("");
        }
        //Capture and Bind Events
        page.events.page_load = function () {      
            if( $(page.selectedObject).attr("wu-open-list-count"))
                page.openListCount=parseInt(   $(page.selectedObject).attr("wu-open-list-count")) + 1;
            if($(page.selectedObject).attr("wu-mode")=="range")
            {
                $(page.selectedObject).find(".search-textbox").hide();
                $(page.selectedObject).find(".dropdown-simple-search .search-range input").keyup(function(){
                    if( $(page.selectedObject).find(".dropdown-simple-search .search-range input[placeholder=Max]").val()!="" &&  $(page.selectedObject).find(".dropdown-simple-search .search-range input[placeholder=Min]").val()!="")
                        $(page.selectedObject).find(".dropdown-simple-search .search-textbox input").val( $(page.selectedObject).find(".dropdown-simple-search .search-range input[placeholder=Min]").val() + ":" +  $(page.selectedObject).find(".dropdown-simple-search .search-range input[placeholder=Max]").val());               
                    else
                        $(page.selectedObject).find(".dropdown-simple-search .search-textbox input").val("");
                    $(page.selectedObject).find(".dropdown-simple-search .search-textbox input").keyup();
                }); 
            }    
            else{
                $(page.selectedObject).find(".search-range").hide();
                if($(page.selectedObject).attr("wu-search")=="false")
                    $(page.selectedObject).find(".search-textbox").hide();
            }  
                

            function delay(callback, ms) {
                var timer = 0;
                return function() {
                  var context = this, args = arguments;
                  clearTimeout(timer);
                  timer = setTimeout(function () {
                    callback.apply(context, args);
                  }, ms || 0);
                };
              }                           
             
              $(page.selectedObject).find(".dropdown-simple-data-closed button").hover(
                function() {
                  $( this ).next().next().show();
                }, function() {
                    $( this ).next().next().hide();
                }
              );
              $(page.selectedObject).find(".dropdown-simple-data-closed button").next().next().hover(
                function() {
                  $( this ).show();
                }, function() {
                    $( this ).hide();
                }
              );

              $(page.selectedObject).find(".dropdown-simple-data-closed button").next().next().click(function(){
                  page.interface.clear();
              });

            page.setValue=function(){
                var txt= $(page.selectedObject).find(".dropdown-simple-search .search-textbox input").val();
                    delete  page.allData[ page.default[page.valueField]];
                    $(page.selectedObject).find(".dropdown-simple-data a:first-child").attr("data-value",txt);
                    $(page.selectedObject).find(".dropdown-simple-data a:first-child").html(txt);
                    page.default[page.valueField]=txt;
                    page.default[page.textField]=txt;
                    page.allData[ page.default[page.valueField]]= page.default;

                    if(txt!="" ){
                        $(page.selectedObject).find(".dropdown-simple-data a").each(function(i,item){
                            if( $(item).html().includes(txt))
                                $(item).show();
                            else if( ($(page.selectedObject).attr("wu-mode")!="range"))
                                $(item).hide();
                            
                        });
                    }
            }
            $(page.selectedObject).find(".dropdown-simple-search .search-textbox input").keyup(              
                delay(function (e) {
                    page.setValue();
                  }, 500)
            );

        }

        //Available public events
        //Fires dropdown selection change event or assign a event handler for selection change event.
        page.interface.selectionChange = function (handler) {
            page.selectionChangeHandler = handler;     //Assign Event handler                
        };



        //Available public Methods & Properties
        //Commmon properties like show,hide,css,addClass,removeClass
        page.attachCommonroperties();

        
        //Bind an object array to dropdown
        page.interface.dataBind = function (data, valueField, textField, addText) {
            page.valueField = valueField;
            page.textField = textField;
            
            var html="";
            var openhtml="";
            page.default={};
            page.default[page.valueField]="";
            page.default[page.textField]="";
            data.splice(0, 0, page.default);

            var count=0;
            $(data).each(function(i,item){           
                //Add item to model and ui
                page.allData[item[page.valueField]]=item;
                var hide=item[page.valueField]==""?"style='display:none'":"";
                html=html+"<a "+hide+" data-value='" +item[page.valueField] + "' href='#'>" + item[page.textField]+ "</a>";
                if(count<page.openListCount)
                    openhtml=openhtml+"<a "+hide+" data-value='" +item[page.valueField] + "' href='#'>" + item[page.textField]+ "</a>";
                count++;
            });
            $(page.selectedObject).find(".dropdown-simple-data").html(html);   
            $(page.selectedObject).find(".dropdown-simple-data-open").html(openhtml);             

            $(page.selectedObject).find(".dropdown-simple-data-open a").click(function () {
                $(page.selectedObject).find(".dropdown-simple-data a[data-value="+$(this).attr("data-value")+"]").click()
            });
            //Find selection event
            $(page.selectedObject).find(".dropdown-simple-data a").click(function () {
                //Add selection to model and ui
                page.selectedData=page.allData[$(this).attr("data-value")] ;
                $(page.selectedObject).find("button").html(page.selectedData[page.textField]);

                if (page.selectionChangeHandler != null) {
                    page.selectionChangeHandler(page.selectedData);
                }
            });

        };

        //Get selected data
        page.interface.selectedData = function (value) {
            if (typeof (value) != "undefined") 
                page.interface.selectedValue(value[page.valueField]);
            return page.selectedData;
        };

        //Get or set selected value.
        page.interface.selectedValue = function (value) {
            if (typeof (value) != "undefined" ){
               if(!page.allData[value]){
                    $(page.selectedObject).find(".dropdown-simple-search .search-textbox input").val(value);
                    page.setValue();
                   // $(page.selectedObject).find(".dropdown-simple-data-open a:first-child").html(value);
                    //$(page.selectedObject).find(".dropdown-simple-data-open a:first-child").click();
               }
                if(value!=null && ( page.selectedData==null || page.selectedData[page.valueField]!=value)){
                    //Add selection to model and ui
                    page.selectedData = page.allData[value];
                    $(page.selectedObject).find("button").html(page.selectedData[page.textField]);

                    if (page.selectionChangeHandler != null) {
                        page.selectionChangeHandler(page.selectedData);
                    }
                }
               
            }
            return page.selectedData==null?null:page.selectedData[page.valueField];
        };

        
        //Set the selected value. Append the value and text if it is not present in grid. Useful when displaying deleted informations.
        page.interface.selectedDataAppend = function (item) {
            if(!page.allData[item.valueField])
                page.allData[item.valueField]=item;

            page.interface.selectedData(item);
        };


        //Get value based on option Text
        page.interface.getValue = function (text) {

        };



    });

}



    //Radio List  : Select one value  from list of radio options.TODO : Yet to implement and test
    $.fn.radioList = function () {
        return $.pageController.getControl(this, function (page) {
            //Future : RepeateLayout,CSS,SelectedIndex...

            //Event Handler
            page.selectionChangeHandler = null;

            //Capture and Bind Events
            page.events.page_load = function () {
                //Change Event
                $(page.selectedObject).change(function () {
                    if (page.selectionChangeHandler != null) {
                        page.selectionChangeHandler();
                    }
                });
            }

            //Available public events
            page.interface.selectionChange = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).change();
                } else {
                    page.selectionChangeHandler = handler;
                }
            };

            //Available public Methods & Properties
            //Commmon properties like show,hide,css,addClass,removeClass
            page.attachCommonroperties();
            //Bind an object array to dropdown
            page.interface.dataBind = function (data, valueField, textField) {
                page.valueField = valueField;
                page.textField = textField;
                //TODO

            };
            //Get selected data
            // ReSharper disable once UnusedParameter
            page.interface.selectedData = function (value) {
                //TODO
            };
            //Get or set selected value.
            // ReSharper disable once UnusedParameter
            page.interface.selectedValue = function (value) {
                //TODO
            };
            //Get value based on option Text
            // ReSharper disable once UnusedParameter
            page.interface.getValue = function (text) {
                //TODO
            };

        });

    }

   


    //Auto Complete List : 
    $.fn.simpleList = function () {
        return $.pageController.getControl(this, function (page) {
            //page scope objects
            page.dataSource = null; // Datasource the client has provided while binding.
            page.data = null;
            page.lastTerm = null;
            page.minLength = 0;
            page.selectedItem = null;
            page.valueField = "value";
            page.textField = "label";
            page.allowCustomText = false;

            page.textChangeHandler = null;
            page.selectHandler = null;
            page.noRecordFoundHandler = null;

            //Available public events
            //Event raised when text is changed in textbox.
            page.interface.textChange = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).find("input").change();
                } else {
                    page.changeHandler = handler;
                }
            };

            //Event raised when any item is selected from autocomplete list.
            page.interface.select = function (handler) {
                if (typeof handler == "undefined") {
                    if (page.selectHandler)     //Trigger Event 
                        page.selectHandler(page.selectedItem);

                } else {
                    page.selectHandler = handler;     //Assign Event handler
                }
            };

            //Event raised when autocomplete could not find a matching record.
            page.interface.noRecordFound = function (handler) {
                if (typeof handler == "undefined") {
                    if (page.noRecordFoundHandler)     //Trigger Event 
                        page.noRecordFoundHandler(page.selectedObject);

                } else {
                    page.noRecordFoundHandler = handler;     //Assign Event handler
                }
            };

            page.interface.setTemplate = function (data) {
                page.minLength = data.minLength;
            }
            //Available properties and method
            //Set the datasource to be used for autocomplete
            page.interface.dataBind = function (dataSource, valueField, textField) {

                if (typeof valueField != "undefined") {
                    page.valueField = valueField;
                    page.textField = textField;
                }

                page.dataSource = dataSource;
            };
            //Get the selected value.
            page.interface.selectedItems = function (data) {
                if(typeof(data)!== "undefined"){
                    page.selVal=data;
                }
                // var obj = [];
                // $($(page.selectedObject).find("input").autocomplete("widget").find("input:checked")).each(function (i, item) {
                //     obj.push({
                //         value: $(item).attr("data-value"),
                //         label: $(item).attr("data-label"),
                //         object: $(item).attr("data-object")
                //     })
                // });

                return page.selVal;

                //return obj;
            };

            page.interface.selectedValues = function () {
               

                var obj = [];
                $(page.selVal).each(function (i, item) {
                    obj.push(item.value);
                });
                return obj;
            }




            page.interface.selectedText = function (text) {
                if (text != null) {
                    if (page.dataSource.getData != null) {

                        page.dataSource.getData(text, function (data) {

                            if (data.length > 0) {
                                $(page.selectedObject).find("input").val(text);
                                page.selectedItem = { value: data[0][page.valueField], text: data[0][page.textField] };
                                return $(page.selectedObject).find("input").val();
                            }


                        });
                    } else {
                        page.dataSource.getDataCustom(text, function (data) {
                            if (data.length > 0) {
                                $(page.selectedObject).find("input").val(text);
                                page.selectedItem = { value: data[0][page.valueField], text: data[0][page.textField] };
                                return $(page.selectedObject).find("input").val();
                            }


                        });
                    }

                }
                else {
                    return $(page.selectedObject).find("input").val();
                }

            }

            //Set text on textbox. TODO : Check this func
            page.interface.customText = function (val) {
                page.selectedObject.find("input").val(val);
                page.selectedObject.find("input").change();

            };

            page.interface.allowCustomText = function (val) {
                page.allowCustomText = val;

            };

            //Clear autocomplete selection
            page.interface.clear = function () {
                page.selectedObject.find("input").val("");
                page.selectedItem = null;

            }

            page.interface.itemTemplate = function (item) {
                return "<a>" + item.label + "</a>";
            }
            //ToDo : remove
            page.interface.clearLastTerm = function () { page.lastTerm = null; }

            page.mode = "dropdown";

            //Bind autocomplete and evetns.
            page.events.page_load = function () {

                if ($(page.selectedObject).attr("mode"))
                    page.mode = $(page.selectedObject).attr("mode");

                if (page.mode == "dropdown") {
                    $(page.selectedObject).find("input").prop('readonly', true);
                    $(page.selectedObject).find("input").css('cursor', "pointer");


                }
                $(page.selectedObject).find("input").click(function () {
                    $(page.selectedObject).find("button").click();
                });

                //Track text change event
                $(page.selectedObject).find("input").change(function () {
                    if (page.textChangeHandler != null) {
                        page.textChangeHandler(page.selectedObject.find("input").val());
                    }
                });

                $(page.selectedObject).find("button").click(function () {
                    $(page.selectedObject).find("input").data("canClose", false);
                    if (page.mode == "dropdown" || page.mode == "checkbox")
                        $(page.selectedObject).find("input").autocomplete("search", "");
                    else
                        $(page.selectedObject).find("input").autocomplete("search");
                });




                // Fix : don't navigate away from the field on tab when selecting an item
                $(page.selectedObject).find("input").bind("keydown", function (event) {
                    if ($(page.selectedObject).find("input").val() === "") page.selectedItem = null;
                    if (event.keyCode === $.ui.keyCode.ENTER && $(page.selectedObject).find("input").autocomplete("instance").menu.active) {
                        event.preventDefault();
                        event.stopPropagation();
                    } else if (event.keyCode === $.ui.keyCode.ENTER) {
                        //   $(page.selectedObject).blur();
                        //     $(page.selectedObject).focus();
                    }
                    if (event.keyCode === $.ui.keyCode.TAB && $(page.selectedObject).find("input").autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
                    autoFocus: true,
                    minLength: page.minLength,    //Configure Min length to start search..
                    //Fired when user enters text.
                    source: function (request, response) {
                        var term = this.element.val().split(/,\s*/).pop();
                        if (page.mode == "dropdown" || page.mode == "checkbox")
                            term = "";
                        //Try to get from cache page.data.
                        //Go to server if cache is empty,If search term is not part of last search term, If search has wild card or last last result has more than 500 records (only first 500 is always rea.). 
                        if (!page.data || !$.util.contains(term, page.lastTerm) || (term !== page.lastTerm && page.data.length > 499)) {
                            //Query from server. Noe :  page.dataSource will be defined by user.

                            if ($(page.selectedObject).find("input").next("[spinner]").length == 0)
                                $(page.selectedObject).find("input").after('<div spinner="true" style="position: absolute; display: inline-block; float: left;"><i style="top: 0px; position: relative; display: inline-block; font-size: small;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');

                            if (page.dataSource.getData != null) {

                                page.dataSource.getData(term, function (data) {

                                    page.data = data;
                                    page.lastTerm = term;
                                    $(page.selectedObject).find("input").next("[spinner]").remove();
                                    $(page.selectedObject).find("input").autocomplete("search");
                                });
                            } else {

                                page.dataSource.getDataCustom(term, function (data) {
                                    var customTextObject = { label: term, value: 'xxxx' };
                                    data.unshift(customTextObject);
                                    page.data = data;
                                    page.lastTerm = term;
                                    $(page.selectedObject).find("input").next("[spinner]").remove();
                                    $(page.selectedObject).find("input").autocomplete("search");


                                });
                            }

                        }
                        //If last term has the current term as part. for ex : last term = cat  and term = cate.Then filter from last search result.
                        if ($.util.contains(term, page.lastTerm)) {
                            var ldata = $.ui.autocomplete.filter(page.data, term);
                            if (page.dataSource.getData != null) {
                                if (ldata.length == 0) {

                                    //Raise event if no record is filtered.
                                    if (page.selectHandler)
                                        page.selectHandler(page.selectedItem);
                                    if (page.noRecordFoundHandler)
                                        page.noRecordFoundHandler(page.selectedObject);
                                }
                                response(ldata);
                            }
                            else {
                                if (term != page.lastTerm) {
                                    var customText = { label: term, value: 'xxxx' };
                                    ldata.unshift(customText);
                                }

                                if (ldata.length == 0) {

                                    //Raise event if no record is filtered.
                                    if (page.selectHandler)
                                        page.selectHandler(page.selectedItem);
                                    if (page.noRecordFoundHandler)
                                        page.noRecordFoundHandler(page.selectedObject);
                                }
                                response(ldata);
                            }
                        }
                        else {
                            //rerurn the queried data
                            response(page.data);
                        }

                    },

                    focus: function () {
                        return false;
                    },
                    //Selecting a list value should display text field in textbox
                    select: function (event, ui) {
                        if (page.mode == "dropdown") {
                            this.value = ui.item[page.textField];
                            $(page.selectedObject).find("input").data("canClose", true);

                        } else {
                            var cnt = $($(page.selectedObject).find("input").autocomplete("widget").find("input:checked")).length;
                            if (cnt > 0)
                                this.value = cnt + " selected";
                            else
                                this.value = "";

                        }

                        return false;
                    },
                    //Track change events
                    change: function (event, ui) { },
                    create: function (event, ui) {

                        $(this).autocomplete("widget").on("click", "a", function () {
                            var chkVaue = $(this).find("input").prop("checked");
                            //For dropdown clear previous selection
                            if (page.mode == "dropdown")
                                $(event.currentTarget).prop("checked", false);

                            $(this).find("input").prop("checked", !chkVaue).change();

                            page.selVal[$(this).find("input").attr("data-value")] = !chkVaue;

                            $(this).find("img").css("display", page.selVal[$(this).find("input").attr("data-value")] ? "" : "none");
                        });

                        $(this).autocomplete("widget").mouseleave(function () {
                            $(page.selectedObject).find("input").data("canClose", true);
                            $(page.selectedObject).find("input").delay(2000).autocomplete("close");
                        });
                    },
                    open: function () { //Fix to move the list on top of dialog box                    
                        var dialog = $(this).closest(".ui-dialog");
                        if (dialog.length > 0) {
                            $(".ui-autocomplete.ui-front").zIndex(dialog.zIndex() + 1);
                        }
                        $(this).autocomplete("widget").css({
                            "padding": ".1em", "min-height": "0", "max-Height": "150px"
                        });
                    },
                    appendTo: "body", //Fix to move the list on top of dialog box
                    close: function () {
                        //Fix to move the list on top of dialog box
                        $(this).autocomplete("option", "appendTo", "body");

                        //Dont Close the dropdown until
                        if (!jQuery(this).data("canClose")) {
                            jQuery(this).autocomplete('search', '');
                        }

                        return false;

                    }

                }).autocomplete("instance")._renderItem = function (ul, item) {
                    //Custom list layout
                    var temp = "<a style='float:left;width:100%;'><img  src='" + (page.selVal[item.value] ? "/img/tick.jpg" : "") + "' style='height:15px;width:15px;' /><input style='display:none' type='checkbox' data-value='" + item.value + "' " + (page.selVal[item.value] ? "checked" : "") + " />" + item.label + "</a>";
                    return $("<li>")
                        //Check It Jebas Change This
                        .append(temp)
                        .appendTo(ul);
                    //.append("<a>" + item.label + "</a>")
                    //.appendTo(ul);


                }
            };
            page.selVal = {};
            page.attachCommonroperties();
        });


    }



    //Auto Complete List : 
    $.fn.autoCompleteList = function () {
        return $.pageController.getControl(this, function (page) {
            //page scope objects
            page.dataSource = null; // Datasource the client has provided while binding.
            page.data = null;
            page.lastTerm = null;

            page.selectedItem = null;
            page.valueField = "value";
            page.textField = "label";
            page.allowCustomText = false;

            page.textChangeHandler = null;
            page.selectHandler = null;
            page.noRecordFoundHandler = null;
            //Available public events
            //Event raised when text is changed in textbox.
            page.interface.textChange = function (handler) {
                if (typeof handler == "undefined") {
                    $(page.selectedObject).change();
                } else {
                    page.changeHandler = handler;
                }
            };
            //Event raised when any item is selected from autocomplete list.
            page.interface.select = function (handler) {
                if (typeof handler == "undefined") {
                    if (page.selectHandler)     //Trigger Event 
                        page.selectHandler(page.selectedItem);

                } else {
                    page.selectHandler = handler;     //Assign Event handler
                }
            };
            //Event raised when autocomplete could not find a matching record.
            page.interface.noRecordFound = function (handler) {
                if (typeof handler == "undefined") {
                    if (page.noRecordFoundHandler)     //Trigger Event 
                        page.noRecordFoundHandler(page.selectedObject);

                } else {
                    page.noRecordFoundHandler = handler;     //Assign Event handler
                }
            };
page.minLength=0;
             page.interface.setTemplate = function (data){
                 page.minLength=data.minLength;
             }
            //Available properties and method
            //Set the datasource to be used for autocomplete
            page.interface.dataBind = function (dataSource, valueField, textField) {

                if (typeof valueField != "undefined") {
                    page.valueField = valueField;
                    page.textField = textField;
                }

                page.dataSource = dataSource;
            };
            //Get the selected value.
            page.interface.selectedValue = function () {
                if (typeof page.selectedItem != "undefined") {
                    if (page.selectedItem != null)
                        return page.selectedItem.value;
                }
                return null;
            };

            page.interface.selectedText = function (text) {
                if (text != null) {
                    if (page.dataSource.getData != null) {

                        page.dataSource.getData(text, function (data) {

                            if (data.length > 0) {
                                $(page.selectedObject).val(text);
                                page.selectedItem = { value: data[0][page.valueField], text: data[0][page.textField] };
                                return $(page.selectedObject).val();
                            }


                        });
                    } else {
                        page.dataSource.getDataCustom(text, function (data) {
                            if (data.length > 0) {
                                $(page.selectedObject).val(text);
                                page.selectedItem = { value: data[0][page.valueField], text: data[0][page.textField] };
                                return $(page.selectedObject).val();
                            }


                        });
                    }

                }
                else {
                    return $(page.selectedObject).val();
                }

            }

            //Set text on textbox. TODO : Check this func
            page.interface.customText = function (val) {
                page.selectedObject.val(val);
                page.selectedObject.change();

            };

            page.interface.allowCustomText = function (val) {
                page.allowCustomText = val;

            };

            //Clear autocomplete selection
            page.interface.clear = function () {
                page.selectedObject.val("");
                page.selectedItem = null;

            }

            page.interface.itemTemplate = function (item) {
                return "<a>" + item.label + "</a>";
            }
            //ToDo : remove
            page.interface.clearLastTerm = function () { page.lastTerm = null; }


            //Bind autocomplete and evetns.
            page.events.page_load = function () {

                //Track text change event
                $(page.selectedObject).change(function () {
                    if (page.textChangeHandler != null) {
                        page.textChangeHandler(page.selectedObject.val());
                    }
                });

                $(page.selectedObject).click(function (){
                    $(page.selectedObject).autocomplete().open();
                });

                // Fix : don't navigate away from the field on tab when selecting an item
                $(page.selectedObject).bind("keydown", function (event) {
                    if ($(page.selectedObject).val() === "") page.selectedItem = null;
                    if (event.keyCode === $.ui.keyCode.ENTER && $(page.selectedObject).autocomplete("instance").menu.active) {
                        event.preventDefault();
                        event.stopPropagation();
                    } else if (event.keyCode === $.ui.keyCode.ENTER) {
                        //   $(page.selectedObject).blur();
                        //     $(page.selectedObject).focus();
                    }
                    if (event.keyCode === $.ui.keyCode.TAB && $(page.selectedObject).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                }).autocomplete({
                    autoFocus: true,
                    minLength: page.minLength,    //Configure Min length to start search..
                    //Fired when user enters text.
                    source: function (request, response) {
                        var term = this.element.val().split(/,\s*/).pop();
                        //Try to get from cache page.data.
                        //Go to server if cache is empty,If search term is not part of last search term, If search has wild card or last last result has more than 500 records (only first 500 is always rea.). 
                        if (!page.data || !$.util.contains(term, page.lastTerm) || (term !== page.lastTerm && page.data.length > 499)) {
                            //Query from server. Noe :  page.dataSource will be defined by user.

                            if ($(page.selectedObject).next("[spinner]").length == 0)
                                $(page.selectedObject).after('<div spinner="true" style="position: absolute; display: inline-block; float: left;"><i style="top: 0px; position: relative; display: inline-block; font-size: small;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');

                            if (page.dataSource.getData != null) {

                                page.dataSource.getData(term, function (data) {

                                    page.data = data;
                                    page.lastTerm = term;
                                    $(page.selectedObject).next("[spinner]").remove();
                                    $(page.selectedObject).autocomplete("search");
                                });
                            } else {

                                page.dataSource.getDataCustom(term, function (data) {
                                    var customTextObject = { label: term, value: 'xxxx' };
                                    data.unshift(customTextObject);
                                    page.data = data;
                                    page.lastTerm = term;
                                    $(page.selectedObject).next("[spinner]").remove();
                                    $(page.selectedObject).autocomplete("search");


                                });
                            }

                        }
                        //If last term has the current term as part. for ex : last term = cat  and term = cate.Then filter from last search result.
                        if ($.util.contains(term, page.lastTerm)) {
                            var ldata = $.ui.autocomplete.filter(page.data, term);
                            if (page.dataSource.getData != null) {
                                if (ldata.length == 0) {

                                    //Raise event if no record is filtered.
                                    if (page.selectHandler)
                                        page.selectHandler(page.selectedItem);
                                    if (page.noRecordFoundHandler)
                                        page.noRecordFoundHandler(page.selectedObject);
                                }
                                response(ldata);
                            }
                            else {
                                if (term != page.lastTerm) {
                                    var customText = { label: term, value: 'xxxx' };
                                    ldata.unshift(customText);
                                }

                                if (ldata.length == 0) {

                                    //Raise event if no record is filtered.
                                    if (page.selectHandler)
                                        page.selectHandler(page.selectedItem);
                                    if (page.noRecordFoundHandler)
                                        page.noRecordFoundHandler(page.selectedObject);
                                }
                                response(ldata);
                            }
                        }
                        else {
                            //rerurn the queried data
                            response(page.data);
                        }

                    },
                    focus: function () {
                        return false;
                    },
                    //Selecting a list value should display text field in textbox
                    select: function (event, ui) {
                        this.value = ui.item[page.textField];
                        page.selectedItem = ui.item; //Set the current selected Item in page scope

                        setTimeout(function () {
                            //Raise Event : Select
                            if (page.selectHandler)
                                page.selectHandler(page.selectedItem);
                        }, 100);

                       return false;


                        
                    },
                    //Track change events
                    change: function (event, ui) {
                        if (page.allowCustomText == false && ui.item == null) //Disallow entering custom text.Text entered in texbox should match with value in list
                        {
                            this.value = "";
                            page.selectedItem = null;
                        }
                        //Raise Event : Select
                        //if (page.selectHandler)
                        //    page.selectHandler(page.selectedItem);
                        $(page.selectedObject).next("[spinner]").remove();

                    },

                    open: function () { //Fix to move the list on top of dialog box                    
                        var dialog = $(this).closest(".ui-dialog");


                        if (dialog.length > 0) {
                            $(".ui-autocomplete.ui-front").zIndex(dialog.zIndex() + 1);
                        }
                        $(this).autocomplete("widget").css({
                            "padding": ".1em", "min-height": "0", "max-Height": "150px"
                        });
                    },
                    appendTo: "body", //Fix to move the list on top of dialog box
                    close: function () {
                        //Fix to move the list on top of dialog box

                        $(this).autocomplete("option", "appendTo", "body");

                       
                     


                    }

                }).autocomplete("instance")._renderItem = function (ul, item) {
                    //Custom list layout


                    return $("<li>")
                    //Check It Jebas Change This
                    .append(page.interface.itemTemplate(item))
                        .appendTo(ul);
                    //.append("<a>" + item.label + "</a>")
                    //.appendTo(ul);


                }
            };

            page.attachCommonroperties();
        });


    }

    //Date Selector
    $.fn.dateSelector = function () {

        return $.pageController.getControl(this, function (page) {

            if (page.selectedObject[0].controlId == "dtSelector") {
                page.mode = "YearMonth";
                page.dateFormat = "M yy";
            } else {
                page.mode = null;
                page.dateFormat = "dd-mm-yy";
            }


            page.events.page_load = function () {

                $(page.selectedObject).attr("placeholder", page.dateFormat);
                //DatePicker control.Default properties
                $.datepicker.setDefaults({
                    dateFormat: page.mode == "YearMonth" ? "M yy" : "dd-mm-yy",
                    //defaultDate: new Date(),
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: page.mode ? false : true,
                    showWeek: page.mode ? false : true,
                    firstDay: 1,
                    constrainInput: true,


                    beforeShow: function () {
                        if (page.mode == "YearMonth") {
                            if ((datestr = $(this).val()).length > 0) {
                                year = datestr.substring(datestr.length - 4, datestr.length);
                                month = jQuery.inArray(datestr.substring(0, 3),
                                         $(this).datepicker('option', 'monthNamesShort'));
                                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                                $(this).datepicker('setDate', new Date(year, month, 1));
                            }
                        }
                        else {


                            if ($(this).attr("IEFocusFix") === "true") {
                                return false;
                            }
                            else {
                                $(this).attr("placeholder", "dd-mm-yy"); //TODO : Need to fix in future.Will work only if datepicker is opned.
                                $(this).blur(function () {
                                    try {
                                        if (!page.mode) {

                                            $.datepicker.parseDate(WEBUI.CONSTANTS.DateFormat, $(this).val());
                                        }

                                    } catch (e) {
                                        $(this).val("");
                                        // alert(WEBUI.MESSAGES.InvalidDateFormat);
                                    };
                                });
                            }
                        }
                    },
                    onClose: function (dateText, inst) {

                        if (page.mode) {


                            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();

                            $(this).datepicker('setDate', new Date(year, month, 1));
                            $(this).datepicker({
                                dateFormat: "M yy"
                            });

                            var self = $(this);
                        }
                    },
                    onSelect: function () {

                        var self = $(this);
                        $(this).attr("IEFocusFix", "true");
                        setTimeout(function () {
                            $(self).parent().attr("tabindex", -1).focus();
                            self.attr("IEFocusFix", "false");
                        }, 500);


                    }
                }
                );

                if (page.mode == "YearMonth") {
                    $(page.selectedObject).datepicker({
                        dateFormat: "M yy"
                    });

                }
                else {
                    $(page.selectedObject).datepicker({
                        dateFormat: "dd-mm-yy"
                    });
                }
            }



            page.interface.setDate = function (date) {
                return page.selectedObject.datepicker('setDate', new Date(date))
            }
            page.interface.getDate = function () {

                return $(page.selectedObject).datepicker().val();   //{ dateFormat: 'yy-mm-dd' }
            }

            page.interface.disable = function (val) {
                if (!val)
                    $(self).removeAttr('disabled');
                else
                    $(self).prop('disabled', 'disabled');
            }

            page.attachCommonroperties();
        });


    }

    $.fn.dateTimePicker = function () {
        return $.pageController.getControl(this, function (page) {
            page.dateFormat = "dd-mm-yyyy hh:mm";
            page.events.page_load = function () {
                $(page.selectedObject).attr("placeholder", page.dateFormat);
                //DatePicker control.Default properties
                //  $('#datetimepicker1').datetimepicker();
                $(page.selectedObject).datetimepicker({
                    format: 'DD/MM/YYYY hh:mm'
                });
            }
            page.interface.getDate = function () {
                return $(page.selectedObject).datetimepicker().val(); //{ dateFormat: 'yy-mm-dd' }
            }
            page.interface.setDate = function (date) {
                $(page.selectedObject).datetimepicker(date);
            }
        });
    }

    //Panel and Popups
    $.fn.simplePanel = function () {
        $(this).prepend("<HR>");
        $(this).append("<HR>");
        return $(this);
    }

    //Tab control
    $.fn.tab = function () {
        return $.pageController.getControl(this, function (tab) {
            //Click Event for the Tab
            $(tab.selectedObject).on("click", "li a", function () {

                tab.selectedTabObject = $(this);
                if ($(tab.selectedTabObject).parent().parent().children("li").children("a").filter('.tabSelected').attr("target") !==
                    $(tab.selectedTabObject).attr("target")) {


                    //Function to HAndle to TAb Selection
                    function SelectAction() {
                        if (tab.interface.beforeSelectionChanged) {

                            tab.interface.beforeSelectionChanged($(this).attr("target"));
                        }

                        $(tab.selectedTabObject).parent().parent().children("li").children("a").each(function (i, a) {
                            if ($(a).attr("target").substring(0, 1) == "#") {
                                $($(a).attr("target")).hide();
                            }
                            else {
                                var parentControlName = $(tab.selectedObject).attr("control").split(".")[0];
                                var parentControl = null;
                                if (parentControlName == "page")
                                    parentControl = $("body");
                                else
                                    parentControl = $(tab.selectedObject).closest("[control$='." + parentControlName + "']");

                                parentControl.find("[controlid=" + $(a).attr("target") + "]").hide();
                            }


                            $(a).removeClass("tabSelected");
                            $(a).parent("li").removeClass("tabSelected");
                        });

                        $(tab.selectedTabObject).addClass("tab tabSelected");
                        $(tab.selectedTabObject).parent("li").addClass("tab tabSelected");
                        // $($(tab.selectedTabObject).attr("target")).show();

                        if ($(tab.selectedTabObject).attr("target").substring(0, 1) == "#") {
                            $($(tab.selectedTabObject).attr("target")).show();
                        }
                        else {
                            var parentControlName = $(tab.selectedObject).attr("control").split(".")[0];
                            var parentControl = null;
                            if (parentControlName == "page")
                                parentControl = $("body");
                            else
                                parentControl = $(tab.selectedObject).closest("[control$='." + parentControlName + "']");

                            parentControl.find("[controlid=" + $(tab.selectedTabObject).attr("target") + "]").show();
                        }




                        if (tab.interface.selectionChanged) {
                            tab.interface.selectionChanged($(tab.selectedTabObject).attr("target"));
                        }

                    }

                    //Checking If there are any Unsaved Changes in one Tab before Moving to the Other Tab
                    if (typeof $(tab.selectedObject).attr("requiredUnSavedCheck") != "undefined") {

                        $.unSavedChangesCheck($(tab.selectedObject).attr("requiredUnSavedCheck"),
                            function () {
                                SelectAction();
                            }); //Pass function with empty argumnets.Inside that pass the necessary arguments in scope.
                    } else
                        SelectAction();
                }

            });

            //Page Load event for Tab
            tab.events.page_load = function () {
                if (tab.selectedObject.children("li").children("a").length > 0)
                    tab.selectedObject.children("li").children("a")[0].click();

                //tab.selectedObject.children("li").children("a")[0].click();         //On the Page Load, Bydefault Making the First Tab to be Selected
            }

            tab.interface.selectionChanged = null;
            tab.interface.beforeSelectionChanged = null;
            //An Global Method to change the Target Tab based on the ID of the Tab
            tab.interface.selectedTabTarget = function (target) {
                if (typeof target == "undefined")
                    return tab.selectedObject.find(".tabSelected a").attr("target");
                else {
                    tab.selectedObject.find("[target=" + target + "]").click();
                }
            };
            tab.interface.selectedObject = tab.selectedObject;
            tab.interface.show = function () {
                $(tab.selectedObject).show();
            };
            tab.interface.hide = function () {
                $(tab.selectedObject).hide();
            };
        });

    }

    //Poppup control
    $.fn.popup = function () {
        return $.pageController.getControl(this, function (page) {
            
            $(page.selectedObject).addClass("popup");
            page.events.page_load = function () {

            }
            page.interface = {
                dialogClose: null,                          //Initializing dialogClose(to close thepopup)
                dialogBeforeClose: null,                     //Initilizaing a handler for Event dialogBeforeClose
                title: function (title) {                    //Title of the POPUP
                    $(page.selectedObject).parent().find('.ui-dialog-title').html(title);
                },
                width: function (width) {                    //Width of the Popup
                    $(page.selectedObject).dialog("option", "width", width);
                },
                height: function (height) {                   //Height of the Popup
                    $(page.selectedObject).dialog("option", "maxHeight", height);
                    $(page.selectedObject).dialog("option", "height", height);
                },
                open: function () {                           //An event to OPen the Popup
                    $(page.selectedObject).dialog({
                        autoOpen: false,                      //autoOpen is False
                        modal: true,                          //modal:true means By default this will be on the Top
                        position: {                           //positioning the Popup in the Page
                            my: "center",
                            at: "center",
                            of: $("body"),
                            within: $("body")
                        },

                        draggable: true,                                        //Draggable Event of Popup
                        resizable: false, close: function () {                  //Resizable and Close Events
                            if (page.interface.dialogClose != null)
                                page.interface.dialogClose();
                        },
                        beforeClose: function () {
                            if (page.interface.dialogBeforeClose != null)
                                return page.interface.dialogBeforeClose();
                            return true;
                        },


                        title: "Title"                                           //Default Title of the Popup, if the title is not defined in the code

                    });
                    $(page.selectedObject).dialog("open");

                }, close: function () {                                        //To close the Popup
                    $(page.selectedObject).dialog("close");

                }, beforeClose: function () {
                    $(page.selectedObject).dialog("beforeClose");

                }

            }
            page.attachCommonroperties();                                      //Attaching Common Properties to popup Like SHOW, HIDE etc..
        });

    }

    //popup window controls
    $.fn.windowPopup = function (defaults) {
        //windowsPopup is used to load an entire page or url in a popup.
        //Properties supported: height,width,url;
        var self = this;   //local reference

        var width = 1000, height = 600;
        if (defaults.width)
            width = defaults.width;  //override default width
        if (defaults.height)
            height = defaults.height; //override default height

        var left = Number((screen.width / 2) - (width / 2)), top = Number((screen.height / 2) - (height / 2)); //calculate left and top

        self.popUpObj = window.open(defaults.url, "ModalPopUp", "toolbar=no,scrollbars=yes,location=no,statusbar=no,menubar=no,resizable=1,left=" + left + ",top=" + top + ",width=" + width + ",height=" + height + ",modal=yes");

        //Hide background of main page
        if (!$("#pnlBackground").length)
            $(body).append("<div id='divBackground' style='position: fixed; z-index: 999; height: 100%; width: 100%; top: 0; left: 0; background-color: Black; filter: alpha(opacity=60); opacity: 0.6; -moz-opacity: 0.8; display: none'>Resume</div>");
        $("#pnlBackground").show();

        function monitor(closeMethod) {
            if (self.popUpObj.closed === false) {
                setTimeout(function () {
                    monitor(closeMethod);
                }, 2000);
            } else {
                if (closeMethod)
                    closeMethod();
                $("#pnlBackground").hide();
            }
        }

        monitor(defaults.close);
        self.popUpObj.focus();

    };

    $.fn.messagePanel = function () {
        var self = $(this);
        return {
            flash: function (msg, title, icon) {
                $(self).html(msg);
                $(self).show().delay(5000).fadeOut();
            },
            show: function (msg, title, icon) {
                $(self).html("<button style='border: none;cursor: pointer;background-color: transparent;float:right'  onclick='$(this).parent().hide();' >X</button>" + msg);
                $(self).show();
            },
            hide: function () {
                $(self).hide();
            },

        };


    };
    //Progress bar and Main Menu   
    $.fn.progressBar = function (status, msg) {
        //Progress bar that can appended before any panel   
        if (!$("#pnlProgress").length)
            $("body").append('<div id="pnlProgress"><span>Loading...</span>       <a title="Cancel the request." style="display:none">X</a>            <img src="/' + appConfig.root + '/css/images/ajaxloader.gif">    </div>');

        var prog;
        if (status === "show") {
            $("[control$=primaryWriteButton]").attr("disabled", "disabled");

            prog = $("#pnlProgress");

            prog.find("span").hide();
            prog.find("a").hide();
            //prog.find("span").text("Loading...");
            //if (msg)
            //    prog.find("span").text(msg);
            prog.remove();
            if ($(this).is("body"))
                $(prog).insertBefore($(this));
            else
                $(prog).insertBefore($(this));
            prog.show();
            var width = prog[0].clientWidth;
            prog.find("span").css("left", (width / 2) - 50);
            prog.find("a").css("left", (width / 2) + 20);
            prog.find("img").css("left", (width / 2) - 250);

        } else if (status === "hide") {
            $("[control$=primaryWriteButton]").removeAttr("disabled");
            prog = $("#pnlProgress");
            prog.remove();
            $(prog).insertBefore($("body"));
            prog.hide();
        }
    };

    $.fn.progressBarnew = function (status, msg, obj) {
        var prog;
        if (status === "show") {             //To Show the Progress Bar



            if (obj == null) {
                $.blockUI({ message: ' <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Just a moment... ' });         //The Whole Style of the Progress Bar can be defined in the blockUI Funstion
            } else {
                $('body').css('cursor', 'wait');
                if (msg != null)
                    $(obj).block({ message: ' <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>' + msg });
                else
                    $(obj).block({ message: ' <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>' });
            }

        } else if (status === "hide") {                       //To hide the ProgressBar

            if (obj == null) {
                $.unblockUI();
            } else {
                $(obj).unblock();
                $('body').css('cursor', 'auto');
            }

        }

        return {
            show: function () { $.blockUI({ message: ' <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Just a moment... ' }); },            //To Show the ProgressBar
            hide: function () { $.unblockUI(); }                                                               //To Hide The Progress Bar

        };
    };

    //Mainmenu
    $.fn.mainMenu = function (menuData) {

        var htmlContent = [];

        function loadMenu(menu, root) {
            htmlContent.push("<ul>");
            $(menu.ChildItems).each(function (i, menuChild) {
                if (menuChild.NavigateUrl !== "")
                    htmlContent.push("<li><a href='" + menuChild.NavigateUrl + "'>" + menuChild.Text);
                else
                    htmlContent.push("<li><a href='#'>" + menuChild.Text);

                if (menuChild.ChildItems.length > 0 && !root)
                    htmlContent.push("<img src='../images/rightArrow.gif' style='width:10px;height:10px' />");
                htmlContent.push("</a>");
                loadMenu(menuChild);
                htmlContent.push("</li>");
            });
            htmlContent.push("</ul>");
        }

        loadMenu(menuData, "root");
        $(this).append(htmlContent.join(" "));
        $(this).attr("class", "menu");

    };

    //Html Advanced controls  
    $.fn.grid = function () {
        /*
        Data Source : Array , ObjectDataSource and Remote DataSource.Refer grid.objectDataSource for interface methods to be implemented
        Paging : LoadMore, Standard
        Selection : None,Single,Multiple
        Layout Customisation : height,width,cssGrid,cssGridHeader,cssGridData,cssGridRow ,cssGridColumn
        Sorting : Yes. No custom sorting

        */
        return $.pageController.getControl(this, function (grid) {

            //#region "Reload,Filter,Sort,Paging"

            //Reload grid with updated sortExpression, filterExpression, pageNo and pageSize
            grid.interface.reloadGrid = function () {

                grid.clearPreviousDataSource();   //When doing rebind. I need to apply the modified feature in between like page size etc
                grid.getData(function (data) {
                    //Clear header
                    grid.selectedObject.find(".grid_header").find(".grid_header_select input[type=checkbox]").prop("checked", false);

                    grid.bindData(data);
                    grid.totalRecords();
                });

            }


            //Filter method to filter data  based on filterExpression. 
            grid.interface.filter = function (filterExpression) {
                if (typeof filterExpression != "undefined")
                    grid.filterExpression = filterExpression;
                grid.currentPage = 1;      //reset page no during filter
                grid.interface.reloadGrid();

            }


            //Sort method to sort data  based on sortExpression
            grid.interface.sort = function (sortExpression) {
                if (typeof sortExpression != "undefined")
                    grid.sortExpression = sortExpression;

                grid.interface.reloadGrid();
            }


            //Method to get and set current Page
            grid.interface.currentPage = function (pageNo) {

                if (typeof pageNo != "undefined") {
                    grid.currentPage = parseInt(pageNo);
                    grid.interface.reloadGrid();
                }

                return grid.currentPage;
            };

            //Method to get total Page count.
            grid.interface.totalPages = function () {
                return grid.totalPage();
            };

            grid.recordCount = 0;

            grid.getData = function (callback) {
                if (grid.paging) {
                    grid.dataSource.getData(grid.currentPage * grid.pageSize - grid.pageSize, grid.currentPage * grid.pageSize, grid.sortExpression, grid.filterExpression, function (data, recordCount) {
                        grid.recordCount = recordCount;
                        callback(data);
                    });
                } else
                    grid.dataSource.getData(-1, -1, grid.sortExpression, grid.filterExpression, function (data, recordCount) {
                        callback(data);
                        grid.recordCount = recordCount;
                    });

            }

            grid.totalPage = function () {
                return Math.floor(grid.recordCount % grid.pageSize == 0 ? grid.recordCount / grid.pageSize : grid.recordCount / grid.pageSize + 1);
            };

            grid.totalRecords = function () {

                if (grid.paging) {

                    $(grid.selectedObject).find(".total_record").html("Records : " + grid.pager.start() + " - " + grid.pager.end() + " of " + grid.recordCount);
                    var rowTemplate = "";
                    for (var i = 1; i <= grid.totalPage() ; i++)
                        rowTemplate = rowTemplate + "<option>" + i + "</option>";
                    $(grid.selectedObject).find(".grid_page_standard > select[page-action=goto]").html(rowTemplate);
                    $(grid.selectedObject).find(".grid_page_standard > select[page-action]").val(grid.currentPage);
                } else
                    $(grid.selectedObject).find(".total_record").html("Total Records : " + grid.recordCount);

            }

            grid.like = function (object, dataField, value) {
                if (typeof object[dataField] == "undefined")
                    return false;
                else if (object[dataField] == null)
                    return false;
                else
                    return $.util.like(object[dataField].toString().toUpperCase(), value.toString().toUpperCase());

            }

            grid.equal = function (object, dataField, value) {
                if (typeof object[dataField] == "undefined")
                    return false;
                else if (object[dataField] == null)
                    return false;
                else
                    return object[dataField].toString().toUpperCase() == value.toString().toUpperCase();

            }

            //#endregion


            //#region "Inbuilt filter Implementation"

            /*Inbuilt filter : will add a filter row below header. It can be controlled with the below 3 api methods             
              In Memory : Support LIKE (TextBox) and EQUALS (DropDown)
              External Data : Support only  LIKE(TextBox)
            */

            //show the inbuilt filter bar.            
            grid.interface.showFilter = function () {
                $(grid.selectedObject).find(".grid_filter").show();

                //InMemory: If Dropdown is used, then load data to dropdown when show is clicked.
                if (grid.dataLocation == "InMemory") {
                    var data = grid.interface.allData();
                    $($(grid.selectedObject).find(".grid_filter").find("select")).each(function (i, ctrl) {
                        var select = $(this);
                        select.html("");

                        var blankFound = false;
                        $($.util.uniqueBy($.util.sortBy(data, $(ctrl).attr("sortDataField")), function (x) { return x[$(ctrl).attr("dataField")]; })).each(function (j, dataItem) {
                            if (dataItem != null)
                                select.append("<option value='" + dataItem + "'>" + dataItem + "</option>");
                            else
                                blankFound = true;
                        });

                        if (blankFound)
                            select.prepend("<option value='[blank]'>[blank]</option>");
                        select.prepend("<option value='' selected='selected'></option>");
                    });
                }
            }

            //Hide the inbuilt filter bar,reset the filterbar data and clear filter expression.
            grid.interface.clearFilter = function () {
                //Clear all textbox and dropdown value
                $(grid.selectedObject).find(".grid_filter  [filterDataField]").val("");

                //Hide Filter
                $(grid.selectedObject).find(".grid_filter").hide();

                //Apply Filter
                if (grid.dataSource != null && grid.recordCount > 0) {
                    grid.interface.applyFilter();
                }
            }

            //Apply the filter based on filter bar or manually apply the filter expression that is passed.
            grid.interface.applyFilter = function () {

                //Build the filter expression
                var filterExpression = '';
                $($(grid.selectedObject).find(".grid_filter  [filterDataField]")).each(function (i, ctrl) {
                    if ($(ctrl).val() != '' && $(ctrl).val() != null) {
                        if ($(ctrl).prop("tagName") == "Select")
                            filterExpression = filterExpression + (filterExpression == '' ? "" : " && ") + " grid.equal(object,'" + $(ctrl).attr("filterDataField") + "','" + ($(ctrl).val() == "[blank]" ? "" : $(ctrl).val()) + "')";
                        else
                            filterExpression = filterExpression + (filterExpression == '' ? "" : " && ") + " grid.like(object,'" + $(ctrl).attr("filterDataField") + "','" + ($(ctrl).val() == "[blank]" ? "" : $(ctrl).val()) + "')";
                    }
                });


                grid.interface.filter(filterExpression);
            }

            //#endregion


            //#region "Inbuilt Sort Implementation"            
            grid.sortImplementation = {

                sortColumnClicked: null,
                sortDataFieldList: {},

                getDefaultSortTemplate: function (column) {
                    var columnTemplate = "<div sortDataField='" + (typeof (column.sortDataField) != "undefined" ? column.sortDataField : column.dataField) + "' style='cursor:pointer;width:100%'>";
                    columnTemplate = columnTemplate + "<img  currentSortState='None' src='" + WEBUI.CONTEXT.RootPath + "/css/images/topArrow.gif'  style=';display:none;vertical-align: bottom;'/>"; //arrow indicator reflecting currentSortState.Intitally currentSortState is NONE                                                    
                    if (column.headerTemplate)
                        columnTemplate = columnTemplate + column.headerTemplate;
                    else
                        columnTemplate = columnTemplate + "<span>" + column.name + "</span>";
                    columnTemplate = columnTemplate + "</div>";
                    return columnTemplate;
                },

                sortColumnClickHandler: function () {
                    //Hide all other icons.   
                    var img = $(this.sortColumnClicked).find("img[currentSortState]");

                    if (this.selectionMode == "Single") {
                        $(grid.selectedObject).find("." + grid.cssGridHeader + " img[currentSortState]").hide();
                        var currentState = img.attr("currentSortState");
                        $(grid.selectedObject).find("." + grid.cssGridHeader + " img[currentSortState]").attr("currentSortState", "None");
                        img.attr("currentSortState", currentState);
                    }

                    //Find the next sort state
                    img.attr("currentSortState", img.attr("currentSortState") == "None" ? "Asc" : img.attr("currentSortState") == "Asc" ? "Desc" : "None");

                    if (this.selectionMode == "Single")
                        this.sortDataFieldList = {};


                    if (img.attr("currentSortState") == "None") {
                        img.hide();
                        if (this.sortDataFieldList[$(this.sortColumnClicked).attr("sortDataField")])
                            delete this.sortDataFieldList[$(this.sortColumnClicked).attr("sortDataField")];
                    } else if (img.attr("currentSortState") == "Asc") {
                        img.attr("src", "" + WEBUI.CONTEXT.RootPath + "/css/images/topArrow.gif");
                        img.show();
                        this.sortDataFieldList[$(this.sortColumnClicked).attr("sortDataField")] = "";

                    }
                    else if (img.attr("currentSortState") == "Desc") {
                        img.attr("src", "" + WEBUI.CONTEXT.RootPath + "/css/images/downArrow.gif");
                        img.show();
                        this.sortDataFieldList[$(this.sortColumnClicked).attr("sortDataField")] = "-";
                    }

                    grid.sortExpression = [];
                    for (var prop in this.sortDataFieldList) {
                        grid.sortExpression.push(this.sortDataFieldList[prop] + prop);
                    }
                    grid.interface.reloadGrid();
                },

                selectionMode: "Single",

                bindEvents: function () {
                    var self = this;
                    //Monitor to trigger sortColumnClickHandler.
                    $(grid.selectedObject).find("." + grid.cssGridHeader + " [sortDataField]").click(function (e) {
                        self.selectionMode = e.ctrlKey ? "Multiple" : "Single";
                        self.sortColumnClicked = $(this);
                        if (typeof $(grid.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                            $.unSavedChangesCheck($(grid.selectedObject).attr("requiredUnSavedCheck"), function () {
                                self.sortColumnClickHandler();
                            });
                        }
                        else
                            self.sortColumnClickHandler();
                    });
                }
            }


            grid.sortColumnClickHandler = function () {
                grid.selectedObject.progressBar('show');
                setTimeout(function () {
                    $(grid.selectedObject).find("." + grid.cssGridHeader + " div[dataField]").children("span[sort]").hide();

                    var img = $(grid.sortColumnClicked).children("span[sort]");
                    img.attr("sort", img.attr("sort") == "" ? "-" : "");
                    img.attr("sort") == "" ? img.html("&#8593;") : img.html("&#8595;");
                    img.show();

                    grid.sortDataField = img.attr("sort") + "" + $(grid.sortColumnClicked).attr("sortDataField");
                    grid.clearPreviousDataSource();
                    grid.getData(function (data) {
                        grid.bindData(data);
                        grid.selectedObject.progressBar('hide');
                    });

                }, 200);
            }

            //#endregion


            //#region "Inbuilt Paging Implementation : LoadMore and Standard"

            //Return the pager object associated with grid
            grid.interface.pager = function () {
                return grid.pager;
            }

            //Load More Class : Load more keeps appending next page one by one.
            grid.LoadMorePager = function () {
                //LoadMore method to load next page
                this.loadMore = function () {
                    if (grid.paging == true) {
                        if (grid.currentPage < grid.totalPage()) {
                            grid.interface.currentPage(grid.currentPage + 1);
                        }
                    }
                }

                //return start row
                this.start = function () {
                    return 1;
                }

                //return end row
                this.end = function () {
                    var end = grid.currentPage * grid.pageSize;
                    if (end > grid.recordCount)
                        end = grid.recordCount;
                    return end;
                }
            }

            //Standard Pager Class : Pagingg with Next,Previous and GotoPage functionality
            grid.StandardPager = function () {

                //Go to Next Page
                this.nextPage = function () {
                    if (grid.currentPage < grid.totalPage())
                        this.goToPage(grid.currentPage + 1);
                }

                //Go to Previous Page
                this.previousPage = function () {
                    if (grid.currentPage > 1)
                        this.goToPage(grid.currentPage - 1);
                }

                //Go to specified page.
                this.goToPage = function (pageNo) {
                    grid.interface.currentPage(pageNo);
                }

                //Return start row in current view
                this.start = function () {
                    return ((grid.currentPage - 1) * grid.pageSize) + 1;
                }

                //Return end row in current view
                this.end = function () {
                    var end = grid.currentPage * grid.pageSize;
                    if (end > grid.recordCount)
                        end = grid.recordCount;
                    return end;
                }
            }

            //#endregion


            //#region  "Data Source Implementation"

            //Set the data source.Also support user defined datasource
            grid.interface.dataSource = function (dataSource) {
                if (dataSource) {
                    if (Object.prototype.toString.call(dataSource) === '[object Array]') {
                        grid.dataSource = new grid.ObjectDataSource(dataSource); //In Memory Array Data Source
                        grid.dataLocation = "InMemory";
                    } else {
                        grid.dataLocation = "External";
                        grid.dataSource = dataSource; // Remote Data Source
                    }

                    //Clear old data and update the new settings./
                    grid.clearPreviousDataSource();
                    grid.updateSettings();
                }

                return grid.dataSource;
            }




            /*
                DataSource : Data is the underlying core of grid.DataSource is implemented as an interface so that user can customise it when needed.OOTB implementation provided with grid is grid.ObjectDataSource.
                Using OOTB grid.ObjectDataSource :  The entire data should reside in memory as array of objects.ObjectDataSource implementation is added OOTB with paging,filtering and sorting.If array is passed to grid.interface.datasource, automatically ObjectDataSource will be used.
                CustomDataSource :  The data can be external or inmemory. 
                             Use CustomDataSource.
                                 1. for huge volume of records where keeping entire data in memory is not efficient.
                                 2. Customising filtering,paging or sorting impementation. 
                            Steps
                             1. Implement the below DataSource Interface
                                  IDataSource{
                                        this.getCount = function () {}
                                        this.getData = function (start, end, sortExpression, filterExpression){}
                                        this.create = function (item)
                                        this.update = function (item, updatedItem)
                                        this.delete = function (item) 
                                  } 
                             2. set the dataSource before calling databind using
                                          $$("gridId").dataDource(functiotion(){
                                              .... IMPLEMENTed datasource interface
                                          });
                                          $$("gridId").dataBind();
                             Note : user should handle filtering,paging and sorting by themselves.
            */
            grid.ObjectDataSource = function (objectArray) {
                var originalData = objectArray;   //This will preserve the original data.Useful when filtering is applied which changes the data variable.
                var data = objectArray;
                //return the total count.
                this.getCount1 = function (filterExpression, callback) {
                    callback(data.length);
                }
                /*  
                    Get the data to be bind to grid.
                    Paging : start and end decides the current page to be displayed. 
                    Sorting : user can specify multi sort using sort expression. ex -pcaName,projName,-status. - denotes descending.
                    Filtering : filterExpression is a javascript statement which should evaluate to true or false. Only records matching filterExpression will be returned. For ex pcaName == "Chairs" .
                                Two methods grid.like and grid.equals can be used in expression which evaluates the most common search.Ex.grid.like(object,pcaName,'cha')
                */
                this.getData = function (start, end, sortExpression, filterExpression, callback) {
                    var tempData = originalData;
                    if (filterExpression) {
                        //Eval will read the object.So needed
                        // ReSharper disable once UnusedParameter
                        tempData = $.grep(data, function (object) {
                            return eval(filterExpression);
                        });
                    }
                    data = tempData;
                    //Create a new object. Ignore null value.
                    if (sortExpression)         //TODO : If filtered already.. no need to slice again to avoid inplace sorting.
                        callback($.util.sortBy(data.slice(0), sortExpression).slice(start != -1 ? start : 0, end != -1 ? end : tempData.length), tempData.length);
                    else
                        callback(data.slice(start != -1 ? start : 0, end != -1 ? end : tempData.length), tempData.length);
                }

                //Create a new object
                this.create = function (item) {
                    data.push(item);
                }

                //Update existing object
                this.update = function (item, updatedItem) {
                    for (var prop in updatedItem) {
                        if (!updatedItem.hasOwnProperty(prop)) continue;
                        item[prop] = updatedItem[prop];
                    }
                }

                //Delete an object
                this.delete = function (item) {
                    data.pop(item);
                }
            }

            //#endregion


            //#region "Read Data and Row"

            //Get selected Row Id
            grid.interface.selectedRowIds = function () {
                var dataList = new Array();
                $(grid.selectedObject).find(".grid_data").find(".grid_select input[type=checkbox]:checked").each(function () {
                    dataList.push($(this).closest(".grid_row").attr("row_id"));
                });
                return dataList;
            }

            //Get selected data rows
            grid.interface.selectedRows = function () {
                var dataList = new Array();
                $(grid.selectedObject).find(".grid_data").find(".grid_select input[type=checkbox]:checked").each(function () {
                    dataList.push($(this).closest(".grid_row"));
                });
                return dataList;
            }

            //Get Rows matching data filter
            grid.interface.getRow = function (filter) {
                var dataList = new Array();
                $(grid.selectedObject).find(".grid_data").find(".grid_row").each(function (i1, row) {
                    row = $(row);
                    var rowId = row.attr("row_id");
                    var data = grid.currentData[rowId];
                    var found = true;
                    for (var prop in filter) {
                        if (typeof data[prop] === "undefined")
                            found = false;
                        if (filter[prop] != data[prop])
                            found = false;

                    }
                    if (found)
                        dataList.push(row);
                });
                return dataList;
                // return dataList;
            }

            //Get all rows
            grid.interface.getAllRows = function () {
                return $(grid.selectedObject).find(".grid_data").find(".grid_row");
            }



            //Get the selected rows data
            grid.interface.selectedData = function () {
                var dataList = new Array();
                $(grid.selectedObject).find(".grid_data").find(".grid_select input[type=checkbox]:checked").each(function () {
                    dataList.push(grid.currentData[$(this).closest(".grid_row").attr("row_id")]);
                });
                return dataList;
            }

            //Get data for the given row
            grid.interface.getRowData = function (row) {
                return grid.currentData[$(row).attr("row_id")];
            }

            //Get data for the given row id
            grid.interface.getRowDataByID = function (rowId) {
                return grid.currentData[rowId];
            }

            //Get all data.
            grid.interface.allData = function () {
                var dataList = new Array();
                for (var prop in grid.currentData) {
                    dataList.push(grid.currentData[prop]);
                }
                return dataList;
            }

            //#endregion


            //#region "Select,Create,Update and Delete Row"

            //Select a Row  
            grid.interface.selectRow = function (rowId) {
                //Change the html content
                if (grid.rowClickSelection == true)
                    grid.getRowContainer().children(".grid_row[row_id='" + rowId + "']").click();
                else {
                    var chk = grid.getRowContainer().children(".grid_row[row_id='" + rowId + "']").find(".grid_select_chk");
                    if (chk.is(":checked") == false)
                        chk.trigger("click");
                }

            }

            //Add a single row
            grid.interface.createRow = function (item, first) {
                grid.dataSource.create(item);
                var rowIds = grid.bindData([item], first);
                grid.totalRecords(++grid.dataCount);
                return rowIds;
            }

            //Update Row
            grid.interface.updateRow = function (rowId, item) {
                //Change the html content
                var row = grid.getRowContainer().children(".grid_row[row_id='" + rowId + "']");
                var selected = row.find(".grid_select input[type=checkbox]").prop("checked");
                var rowTemplate = grid.getRowTemplate();
                row.html(rowTemplate.html());

                //Change the originalData and underlying DataSource.data                    
                grid.dataSource.update(grid.originalData[rowId], item);


                //Rebind from dataSource and reset current and original data
                grid.bindRow(row, grid.originalData[rowId]);
                row.find(".grid_select input[type=checkbox]").prop("checked", selected);
            }

            //Delete a single row
            grid.interface.deleteRow = function (rowId) {
                //Delete from datasource
                grid.dataSource.delete(grid.originalData[rowId]);
                //Delete from Orinal
                delete grid.originalData[rowId];
                //DElete from current
                delete grid.currentData[rowId];
                //Delete from grid
                var row = grid.getRowContainer().children(".grid_row[row_id='" + rowId + "']");
                row.remove();
                grid.totalRecords(--grid.dataCount);
            }
            //#endregion


            //#region "Grid Setting and Binding"

            //#region "Properties to change Style."
            grid.interface.height = function (height) {
                if (height != null) {
                    grid.selectedObject.children(".grid_data").css("height", height);
                    grid.height = height;
                }
                return grid.selectedObject.children(".grid_data").css("height");
            }
            grid.interface.width = function (width) {
                if (width != null) {
                    grid.selectedObject.css("width", width);
                    grid.width = width;
                }
                return grid.selectedObject.css("width");
            }
            grid.interface.display = function (display) {
                if (display != null) {

                    grid.selectedObject.css("display", display);
                }
                return grid.selectedObject.css("display");
            }
            grid.interface.cssGrid = function (cssGrid) {
                if (cssGrid != null) {
                    grid.selectedObject.find("." + grid.cssGrid).attr("class", cssGrid);
                    grid.cssGrid = cssGrid;
                }
                return grid.css_grid;
            }
            grid.interface.rowHide = function (rowId) {

                var row = grid.getRowContainer().children(".grid_row[row_id='" + rowId + "']");
                row.hide();
                grid.totalRecords(--grid.dataCount);
            }
            grid.updateSettings = function () {
                grid.columns = grid.updatedColumns;
                grid.selection = grid.updatedSelection;
                grid.selectionType = grid.updatedSelectionType;
                grid.sorting = grid.updatedSorting;
                grid.paging = grid.updatedPaging;
                grid.pagingType = grid.updatedPagingType;
                grid.pageSize = grid.updatedPageSize;
                grid.rowClickSelection = grid.updatedRowClickSelection;

                //Update new Values
                if (grid.paging) {

                    if (grid.pagingType == "LoadMore")
                        grid.pager = new grid.LoadMorePager();
                    else if (grid.pagingType == "Standard")
                        grid.pager = new grid.StandardPager();
                }
            }

            //#endregion


            //#region "Properties to change feature"

            //Enable or Disable sorting.
            grid.interface.sorting = function (sorting) {
                if (sorting != null) {
                    grid.updatedSorting = sorting;
                }
                return grid.updatedSorting;
            };

            //Enable or Disable Paging and set the paging Type
            grid.interface.paging = function (paging) {
                if (paging != null) {
                    grid.tempPaging = paging;
                    if (paging == true) {
                        grid.updatedPaging = true;
                        grid.updatedPagingType = "Standard";
                    }
                    else if (paging == false) {
                        grid.updatedPaging = false;
                        grid.updatedPagingType = "None";
                    }
                    else if (paging == "None") {
                        grid.updatedPaging = false;
                        grid.updatedPagingType = "None";
                    }
                    else if (paging == "Standard") {
                        grid.updatedPaging = true;
                        grid.updatedPagingType = "Standard";
                    }
                    else if (paging == "LoadMore") {
                        grid.updatedPaging = true;
                        grid.updatedPagingType = "LoadMore";
                    }

                }
                return grid.tempPaging;
            };

            //Method to get or Set current page size.
            grid.interface.pageSize = function (pageSize) {
                if (pageSize != null) {
                    grid.updatedPageSize = pageSize;
                }
                return grid.updatedPageSize;
            };


            grid.interface.rowClickSelection = function (rowClickSelection) {
                if (rowClickSelection != null) {
                    grid.updatedRowClickSelection = rowClickSelection;
                }
                return grid.updatedRowClickSelection;
            };


            //Enable or Disable Selection and set the selection Type
            grid.interface.selection = function (selection) {
                if (selection != null) {
                    grid.tempSelection = selection;
                    if (selection == true) {
                        grid.updatedSelection = true;
                        grid.updatedSelectionType = "Single";
                    }
                    else if (selection == false) {
                        grid.updatedSelection = false;
                        grid.updatedSelectionType = "None";
                    }
                    else if (selection == "None") {
                        grid.updatedSelection = false;
                        grid.updatedSelectionType = "None";
                    }
                    else if (selection == "Single") {
                        grid.updatedSelection = true;
                        grid.updatedSelectionType = "Single";
                    }
                    else if (selection == "Multiple") {
                        grid.updatedSelection = true;
                        grid.updatedSelectionType = "Multiple";
                    }

                }
                return grid.tempSelection;
            };

            //Update or read columns
            grid.interface.columns = function (columns) {
                if (columns != null) {
                    grid.updatedColumns = columns;
                }
                return grid.updatedColumns;
            };


            //#endregion


            //#region "Template and Binding"

            //bind tehe datasource to grid
            grid.interface.dataBind = function (dataSource) {
                //Argument data is depreceated and should be used only for backward compatability.Use dataSource method to set data.                    
                if (dataSource)
                    grid.interface.dataSource(dataSource);

                //Create Header,rowTemplate and placeholder for data
                grid.bindHeader();

                //Bind the data
                grid.currentPage = 1;
                grid.getData(function (data) {
                    grid.bindData(data);
                    grid.totalRecords(data.length);
                });



            }

            //Clear previous data
            grid.clearPreviousDataSource = function () {
                //Remove all the old rows bind
                grid.getRowTemplate().next().children("div").remove();

                grid.rowCounter = 0;
                //grid.currentPage = 1; //Reset the page no if data source is changed
                grid.originalData = new Object();
                grid.currentData = new Object();

            }

            //Set the template for the grid
            grid.interface.setTemplate = function (template) {
                if (typeof template !== "undefined" && template != null) {
                    //Layout Changes.
                    if (typeof template.width !== "undefined")
                        grid.interface.width(template.width);

                    if (typeof template.height !== "undefined")
                        grid.interface.height(template.height);

                    //Feature Changes.
                    if (typeof template.selection !== "undefined")
                        grid.interface.selection(template.selection);

                    if (typeof template.sorting !== "undefined")
                        grid.interface.sorting(template.sorting);

                    if (typeof template.paging !== "undefined")
                        grid.interface.paging(template.paging);

                    if (typeof template.pageSize !== "undefined")
                        grid.interface.pageSize(template.pageSize);

                    if (typeof template.columns !== "undefined")
                        grid.interface.columns(template.columns);

                    if (typeof template.rowClickSelection !== "undefined")
                        grid.interface.rowClickSelection(template.rowClickSelection);

                }
            };

            //#endregion



            //#endregion


            //#region "Class variable to hold data"

            //DataSource
            grid.dataSource = null;


            //Sorting
            grid.sorting = true;
            grid.updatedSorting = true;     //temporary holding before databind
            grid.sortExpression = [];


            //Paging
            grid.paging = false;      //Decided whether paging should be enabled or not
            grid.updatedPaging = false;

            grid.pagingType = "Standard";  //LoadMore,Standard. 
            grid.updatedPagingType = "Standard"; //LoadMore,Standard,Custom

            grid.pageSize = 20;
            grid.updatedPageSize = 20;

            grid.currentPage = 1;
            grid.pager = null;       //Holds the paging Implemention depending on pagingType.


            //Selection
            grid.selection = true;
            grid.updatedSelection = true;
            grid.selectionType = "None";
            grid.updatedSelectionType = "None";
            grid.rowClickSelection = false;
            grid.updatedRowClickSelection = false;

            //Filtering
            grid.filterExpression = null;

            //Template Columns
            grid.columns = null;
            grid.updatedColumns = null;



            //Layout             
            grid.height = null;
            grid.width = null;
            grid.cssGrid = "grid";
            grid.cssGridHeader = "grid_header";
            grid.cssGridData = "grid_data";
            grid.cssGridRow = "grid_row";
            grid.cssGridColumn = "grid_column"; // Add first row and first column in future if needed.


            //Data
            grid.rowCounter = 0;
            grid.originalData = new Object();
            grid.currentData = new Object();

            //#endregion


            //#region "Grid Interface - Events"

            grid.interface.rowBound = null;
            grid.interface.rowCommand = null;
            grid.interface.selectionChanged = null;

            //#endregion


            //#region "local event handling"
            grid.selectedObject.on("focus", ".grid_row", function () {
                if (grid.selection && grid.selectionType == "Single" || grid.selection && grid.selectionType == "Multiple")
                    $(this).addClass("grid_focus");
            });

            grid.selectedObject.on("blur", ".grid_row", function () {
                $(this).removeClass("grid_focus");
            });


            grid.selectedObject.on("keydown", ".grid_row", function (event) {
                if (event.target.tagName != "SELECT" && event.target.tagName != "INPUT" && event.target.tagName != "TEXTAREA") {
                    if (event.keyCode === $.ui.keyCode.ENTER) {
                        $(this).click();
                    } else if (event.keyCode === $.ui.keyCode.UP) {
                        if ($(this).prev().hasClass("grid_row")) {
                            $(this).prev().click();
                            $(this).prev().focus();
                            event.preventDefault();
                            event.stopPropagation();
                        }


                    } else if (event.keyCode === $.ui.keyCode.DOWN) {
                        if ($(this).next().hasClass("grid_row")) {
                            $(this).next().click();
                            $(this).next().focus();
                            event.preventDefault();
                            event.stopPropagation();
                        }

                    }
                }

            });

            grid.selectedObject.on("click", ".grid_row", function (e) {


                var row;
                if (grid.selection && grid.selectionType == "Single") {
                    if (e.target.tagName != "INPUT" && e.target.tagName != "SELECT" && e.target.tagName != "TEXTAREA")
                        $(this).focus();

                    row = $(e.target).closest(".grid_row");


                    row.parent().children("div").css("background-color", "");
                    row.css("background-color", "#DEECC3");  //rgb(219, 238, 196)

                    row.parent().find(".grid_select input[type=checkbox]").prop("checked", false);
                    row.find(".grid_select input[type=checkbox]").prop("checked", true);

                    if (grid.interface.selectionChanged)
                        grid.interface.selectionChanged(row, grid.currentData[row.attr("row_id")]);
                }
                else if (grid.selection && grid.selectionType == "Multiple") {
                    if (grid.rowClickSelection == true && !(e.target.type == "checkbox" && $(e.target).attr("class") == "grid_select_chk")) {
                        row = $(e.target).closest(".grid_row");
                        row.css("cursor", "pointer");
                        if (e.shiftKey) {
                            if (grid.interface.selectedRows().length > 0) {

                                var selRow = grid.interface.selectedRows()[0];
                                row.parent().children("div").css("background-color", "");
                                row.parent().find(".grid_select input[type=checkbox]").prop("checked", false);
                                $(selRow).css("background-color", "#DEECC3");
                                $(selRow).find(".grid_select input[type=checkbox]").prop("checked", true);

                                var startRow = row.parent().children("div").index(selRow);
                                var endRow = row.parent().children("div").index(row);
                                while (startRow != endRow) {

                                    row.css("background-color", "#DEECC3");
                                    row.find(".grid_select input[type=checkbox]").prop("checked", true);

                                    if (endRow > startRow) {
                                        startRow++;
                                        selRow = $(selRow).next();
                                        $(selRow).css("background-color", "#DEECC3");
                                        $(selRow).find(".grid_select input[type=checkbox]").prop("checked", true);
                                    } else {
                                        startRow--;
                                        selRow = $(selRow).prev();
                                        $(selRow).css("background-color", "#DEECC3");
                                        $(selRow).find(".grid_select input[type=checkbox]").prop("checked", true);
                                    }
                                }
                            }

                            row.css("background-color", "#DEECC3");
                            row.find(".grid_select input[type=checkbox]").prop("checked", true);


                            if (window.getSelection) {
                                if (window.getSelection().empty) {  // Chrome
                                    window.getSelection().empty();
                                } else if (window.getSelection().removeAllRanges) {  // Firefox
                                    window.getSelection().removeAllRanges();
                                }
                            } else if (document.selection) {  // IE?
                                document.selection.empty();
                            }


                        }
                        else if (e.ctrlKey) {
                            if (row.find(".grid_select input[type=checkbox]").prop("checked") == true) {
                                row.css("background-color", "");
                                row.find(".grid_select input[type=checkbox]").prop("checked", false);
                            } else {
                                row.css("background-color", "#DEECC3");
                                row.find(".grid_select input[type=checkbox]").prop("checked", true);
                            }
                        }
                        else {
                            if (e.target.tagName != "INPUT" && e.target.tagName != "SELECT" && e.target.tagName != "TEXTAREA")
                                $(this).focus();

                            row.parent().children("div").css("background-color", "");
                            row.css("background-color", "#DEECC3");  //rgb(219, 238, 196)

                            row.parent().find(".grid_select input[type=checkbox]").prop("checked", false);
                            row.find(".grid_select input[type=checkbox]").prop("checked", true);
                        }

                    }
                    else if (e.target.type == "checkbox" && $(e.target).attr("class") == "grid_select_chk") {
                        row = $(e.target).closest(".grid_row");

                        if (e.shiftKey) {
                            if (grid.interface.selectedRows().length > 0) {
                                var selRow;
                                if (row.parent().children("div").index(row) < row.parent().children("div").index(grid.interface.selectedRows()[grid.interface.selectedRows().length - 1]))
                                    selRow = grid.interface.selectedRows()[grid.interface.selectedRows().length - 1];
                                else
                                    selRow = grid.interface.selectedRows()[0];
                                row.parent().children("div").css("background-color", "");
                                row.parent().find(".grid_select input[type=checkbox]").prop("checked", false);
                                $(selRow).css("background-color", "#DEECC3");
                                $(selRow).find(".grid_select input[type=checkbox]").prop("checked", true).change();

                                var startRow = row.parent().children("div").index(selRow);
                                var endRow = row.parent().children("div").index(row);
                                while (startRow != endRow) {

                                    row.css("background-color", "#DEECC3");
                                    row.find(".grid_select input[type=checkbox]").prop("checked", true).change();

                                    if (endRow > startRow) {
                                        startRow++;
                                        selRow = $(selRow).next();
                                        $(selRow).css("background-color", "#DEECC3");
                                        $(selRow).find(".grid_select input[type=checkbox]").prop("checked", true).change();
                                    } else {
                                        startRow--;
                                        selRow = $(selRow).prev();
                                        $(selRow).css("background-color", "#DEECC3");
                                        $(selRow).find(".grid_select input[type=checkbox]").prop("checked", true).change();
                                    }
                                }
                            }
                            //added for grid header checkbox issue
                            if ($(grid.selectedObject).find(".grid_row:visible").length == grid.interface.selectedRows().length)
                                row.parent().parent().find(".grid_header").find(".grid_header_select input[type=checkbox]").prop("checked", true);
                            else
                                row.parent().parent().find(".grid_header").first().find(".grid_header_select input[type=checkbox]").prop("checked", false);
                            row.css("background-color", "#DEECC3");
                            row.find(".grid_select input[type=checkbox]").prop("checked", true);


                            if (window.getSelection) {
                                if (window.getSelection().empty) {  // Chrome
                                    window.getSelection().empty();
                                } else if (window.getSelection().removeAllRanges) {  // Firefox
                                    window.getSelection().removeAllRanges();
                                }
                            } else if (document.selection) {  // IE?
                                document.selection.empty();
                            }
                        }
                        else {
                            if ($(this).find(".grid_select input[type=checkbox]").prop("checked")) {
                                row.css("background-color", "#DEECC3");
                            }
                            else {
                                row.css("background-color", "white");
                            }
                            if ($(grid.selectedObject).find(".grid_row:visible").length == grid.interface.selectedRows().length)
                                row.parent().parent().find(".grid_header").first().find(".grid_header_select input[type=checkbox]").prop("checked", true);
                            else
                                row.parent().parent().find(".grid_header").first().find(".grid_header_select input[type=checkbox]").prop("checked", false);
                        }
                    }

                    if (grid.interface.selectionChanged) {
                        var dataList = new Array();
                        var rowList = new Array();
                        $(grid.selectedObject).find(".grid_data").find(".grid_select input[type=checkbox]:checked").each(function () {
                            rowList.push($(this).closest(".grid_row"));
                            dataList.push(grid.currentData[$(this).closest(".grid_row").attr("row_id")]);
                        });
                        grid.interface.selectionChanged(rowList, dataList);
                    }

                }

                var actionElement = $(e.target);
                if (actionElement.attr("action")) {
                    if (grid.interface.rowCommand)
                        grid.interface.rowCommand(actionElement.attr("action"), actionElement, $(actionElement).closest(".grid_row").attr("row_id"), $(actionElement).closest(".grid_row"), grid.currentData[$(actionElement).closest(".grid_row").attr("row_id")]);
                }
            });

            $(grid.selectedObject).on("keydown", "input[filterDataField]", function (event) {
                if (event.which == 13) {
                    grid.interface.applyFilter();
                }

            });

            $(grid.selectedObject).on("change", "select[filterDataField]", function () {
                grid.interface.applyFilter();
            });


            //#endregion 


            //#region "Building UI"

            grid.bindHeader = function () {



                //Clear the entire grid
                $(grid.selectedObject).html("");
                $(grid.selectedObject).attr("class", grid.cssGrid);

                //Building header
                var header = [];
                header.push("<div class='" + grid.cssGridHeader + "'>");

                //When Selection mode is miltiple. Add checkbox to perform selectAll and deSelectAll.
                if (grid.selection && grid.selectionType == "Multiple") {
                    header.push("<div style='width:20px' class='grid_header_select' >");//;display:none
                    header.push("<input type='checkbox'  >");
                    header.push("</div>");
                }

                //Add Column Headers
                $(grid.columns).each(function (iIndex, item) {
                    /* Sorting Implementation
                        1. Default Sort
                                1.By default, sorting is true for all columns. if user dont need sorting for that column, user can set to false.
                                2.When header column is clicked (note header column should have attribute sortDataField),
                                        Based on currentSortState attribute, calculate the next sort state .. State transition  allowed -  NONE->ASC->DESC->NONE. Intitally set to none.
                                        Sort expression is calculated based on new currentSortState and passed to getData method and the resulting data is rebind to grid.                                                       
                                3. Multi Column Sort possibility
                                        Press ctrl and click the span[sortDataField]. New sort expression for current column is appended to previous sort expression and then passed to getData.
                        
                        2. Default sort with header template 
                                same as above.
                        
                        3. Custom Sort 
                                    1. set sorting=false first
                                    2. Define your own header template and bind click event.
                                    3. When your header is clicked, call grid.interface.sort(sortExpression)
                     */

                    //Decide whether to use span or use user defined template.
                    var columnTemplate;
                    if (typeof item.sorting == "undefined" || item.sorting == true) {
                        columnTemplate = grid.sortImplementation.getDefaultSortTemplate(item);
                    } else {
                        if (item.headerTemplate)
                            columnTemplate = item.headerTemplate;
                        else
                            columnTemplate = "<span>" + item.name + "</span>";
                    }


                    //add the column to the header row.                     
                    header.push("<div dataField='" + item.dataField + "' style='" + (item.visible === false ? "display:none;" : "") + "width: " + item.width + "'>" + columnTemplate + "</div>");
                });
                header.push("</div>");

                $(grid.selectedObject).append(header.join(""));





                if (grid.sorting == true) {
                    grid.sortImplementation.bindEvents();
                }




                //Filter Code starts herer
                header = [];
                header.push("<div class='grid_filter' style='display:none'>");
                //When Selection mode is miltiple. placeholder to occupy some space
                if (grid.selection && grid.selectionType == "Multiple") {
                    header.push("<div style='width:20px'></div>");
                }

                //Add Column Headers
                $(grid.columns).each(function (iIndex, item) {
                    //Prepare the column level style to be set.
                    var columnStyle = "";
                    if (item.visible === false)
                        columnStyle = "display:none;";
                    columnStyle = columnStyle + "width: " + item.width;

                    var filterTemplate;
                    if (item.filterTemplate)       //Enables customisation by providing custom template for filtering
                        filterTemplate = item.filterTemplate;
                    else {
                        //Select Type filter is allowed only for InMemory data. For remote data use filter template and custom code,.
                        if (item.filterType == "Select" && grid.dataLocation == "InMemory")   //Binding data to select will be delayed until show filter is called.
                            filterTemplate = "<select filterDataField='" + (typeof (item.filterDataField) != "undefined" ? item.filterDataField : item.dataField) + "' sortDataField='" + (typeof (item.sortDataField) != "undefined" ? item.sortDataField : item.dataField) + "' dataField='" + item.dataField + "' filter='true' style='width:calc( 100% - 10px );font-size:10px;font-family:verdana;margin-top:2px;margin-bottom:0px'></select>";
                        else
                            filterTemplate = "<input filterDataField='" + (typeof (item.filterDataField) != "undefined" ? item.filterDataField : item.dataField) + "' dataField='" + item.dataField + "' filter='true' type=text style='font-size:10px;font-family:verdana;width:calc( 100% - 10px );margin-top:2px;margin-bottom:4px' />";
                    }

                    //add the column to the header row.                    
                    header.push("<div  style='" + columnStyle + "'>" + filterTemplate + "</div>");
                });
                header.push("</div>");



                $(grid.selectedObject).append(header.join(""));

                $(grid.selectedObject).find(".grid_header_select > input[type=checkbox]").change(function () {
                    $(grid.selectedObject).find(".grid_data .grid_select input[type=checkbox]").prop("checked", $(this).prop("checked")).change();
                    if ($(grid.selectedObject).find(".grid_data .grid_select input[type=checkbox]").prop("checked")) {
                        $(grid.selectedObject).find(".grid_data .grid_row").css("background-color", "gold");
                    }
                    else {
                        $(grid.selectedObject).find(".grid_data .grid_row").css("background-color", "rgba(255, 255, 255, 1");
                    }
                });



                //Build the row template. Row template is used to build individual row when binding
                var rowTemplate = "<div style='display:none'>";
                rowTemplate = rowTemplate + "<div class='grid_select'  style='" + ((grid.selection && grid.selectionType == "Multiple") ? "width:20px" : "display:none") + "'><input class='grid_select_chk' type='checkbox' ></div>";
                $(grid.columns).each(function (iIndex, item) {
                    var columnStyle = "width: " + item.width;

                    if (item.minimumWidth === false)
                        columnStyle = columnStyle + ";min-width:" + item.minimumWidth;
                    if (item.visible === false && (typeof item.visible !== "undefined"))
                        columnStyle = columnStyle + ";display:none";

                    //Check whether default template or user defined template
                    var itemTemplate = "<div>{" + item.dataField + "}</div>";
                    if (item.itemTemplate)
                        itemTemplate = "<div>" + item.itemTemplate + "</div>";

                    //Check whether editable and to add a placeholder for edit
                    var editTemplate = "";
                    if (item.editable) {
                        editTemplate = "<div class='grid_edit' style='display:none' column_index='" + iIndex + "'>";
                        editTemplate = editTemplate + "</div>";
                    }

                    //add the column to the row.
                    rowTemplate = rowTemplate + "<div style='" + columnStyle + "' dataField='" + item.dataField + "' >" + itemTemplate + editTemplate + "</div>";

                });
                rowTemplate = rowTemplate + "</div><div style='height:" + grid.height + "' class='" + grid.cssGridData + "'></div>";
                rowTemplate = rowTemplate + "<div class='grid_header'>";
                if (grid.paging == true && grid.pagingType == "LoadMore")
                    rowTemplate = rowTemplate + "<span style='float: left; margin-right: 20px; margin-top: 5px;' class='grid_page_load_more'>Load More</span>";
                else if (grid.paging == true && grid.pagingType == "Standard") {
                    rowTemplate = rowTemplate + "<div  class='grid_page_standard'><span page-action='prev' style='margin-right: 10px;'>Prev</span><select page-action='goto' style='margin-right: 10px; padding: 0px;'>";
                    rowTemplate = rowTemplate + "</select><span page-action='next'>Next</span></div>";

                }
                rowTemplate = rowTemplate + " <span style='float: right; margin-right: 20px; margin-top: 5px;' class='total_record'></span></div>";

                $(grid.selectedObject).append(rowTemplate);
                if (grid.sorting) {
                    $(grid.selectedObject).find("." + grid.cssGridHeader + " div[dataField]").click(function (e) {
                        //Hide all other icons.
                        if ($(this).attr("sorting") == "true") {

                            if (!(e.target.tagName == "OPTION" || e.target.tagName == "SELECT" || e.target.tagName == "INPUT")) {

                                grid.selectionMode = e.ctrlKey ? "Multiple" : "Single";
                                grid.sortColumnClicked = $(this);
                                if (typeof $(grid.selectedObject).attr("requiredUnSavedCheck") != "undefined") {
                                    unSavedChangesCheck($(grid.selectedObject).attr("requiredUnSavedCheck"), function () {
                                        grid.sortColumnClickHandler();
                                    });
                                }
                                else {

                                    grid.sortColumnClickHandler();

                                }
                            }
                        }
                    });
                }


                if (grid.paging == true && grid.pagingType == "LoadMore")
                    $(grid.selectedObject).find(".grid_page_load_more").click(function () {
                        grid.pager.loadMore();
                    });
                else if (grid.paging == true && grid.pagingType == "Standard") {
                    $(grid.selectedObject).find(".grid_page_standard > [page-action]").click(function () {
                        if ($(this).attr("page-action") == "prev") {
                            grid.pager.previousPage();
                            //$(grid.selectedObject).find(".grid_page_standard > select[page-action]").val(grid.currentPage);
                        } else if ($(this).attr("page-action") == "next") {
                            grid.pager.nextPage();
                            // $(grid.selectedObject).find(".grid_page_standard > select[page-action]").val(grid.currentPage);
                        }

                    });
                    $(grid.selectedObject).find(".grid_page_standard > [page-action]").change(function () {
                        if ($(this).attr("page-action") == "goto") {
                            grid.pager.goToPage($(this).val());

                        }
                    });
                }


            }
            grid.addRow = function (first) {
                //Create a dummy row
                var row = grid.getRowTemplate().clone();
                //Add the dummy row to grid.
                if (typeof first == "undefined")
                    grid.getRowContainer().append(row);
                else
                    grid.getRowContainer().prepend(row);

                //Set the row_id                     
                row.attr("row_id", ++grid.rowCounter);

                //Set the css
                row.css("display", "");
                row.attr("class", grid.cssGridRow);
                row.attr("tabindex", "1");
                row.addClass(grid.rowCounter % 2 == 1 ? "odd-row" : "even-row");
                return row;

            }
            grid.bindRow = function (row, item) {
                //Store the data                         
                grid.originalData[row.attr("row_id")] = item;
                grid.currentData[row.attr("row_id")] = $.util.clone(item); //Clone it.

                //Raise row bound event. So that user can override data and style before binding.
                if (grid.rowClickSelection) {
                    row.css("cursor", "pointer");
                }


                if (grid.interface.beforeRowBound)
                    grid.interface.beforeRowBound(row, grid.currentData[row.attr("row_id")]);

                //Bind value in each column.
                row.children("div").each(function () {
                    var dataFields = $(this).attr("dataField");
                    if (dataFields) {
                        //Bind the data and reload in div
                        $(this).html(grid.bindTemplate($(this).html(), dataFields, grid.currentData[row.attr("row_id")]));
                        // if ($(this).find("div[mode=view]").length > 0)
                        //     $(this).attr("title", $(this).find("div[mode=view]").text().trim());
                        // else
                        //    $(this).attr("title", $(this).text().trim());

                    }
                });

                if (row.attr("row-click") != "true") {
                    row.attr("row-click", "true");
                    row.click(function (evt) {
                        var actionElement = $(evt.target);
                        if (actionElement.attr("action")) {
                            var e = {
                                action: actionElement.attr("action"),
                                actionElement: actionElement,
                                currentRowId: $(actionElement).closest(".grid_row").attr("row_id"),
                                currentRow: $(actionElement).closest(".grid_row"),
                                currentData: grid.currentData[$(actionElement).closest(".grid_row").attr("row_id")],
                                originalData: grid.currentData[$(actionElement).closest(".grid_row").attr("row_id")],
                                save: function () {
                                    grid.interface.updateRow(this.currentRowId, this.currentData);
                                },
                                cancel: function () {
                                    grid.interface.updateRow(this.currentRowId, this.originalData);
                                }
                            };
                            if (grid.interface.rowCommand)
                                grid.interface.rowCommand(e);
                        }
                    });
                }




                if (grid.interface.rowBound)
                    grid.interface.rowBound(row, grid.currentData[row.attr("row_id")]);
            }
            grid.bindData = function (data, first) {
                var rowIds = [];
                $(data).each(function (iIndex, item) {
                    //Add a new row to grid.
                    var row = grid.addRow(first);
                    rowIds.push($(row).attr("row_id"));
                    //Bind data to row.
                    grid.bindRow(row, item);

                });
                return rowIds;
            }
            grid.getRowTemplate = function () {
                //Find the row template in grid.It is placed next to header section and hidden.                    
                return $(grid.selectedObject).find(".grid_data").prev();
            }
            grid.bindTemplate = function (tempContent, dataFields, item) {

                $(dataFields.split(",")).each(function (i, dataField) {
                    tempContent = $.util.replaceAll(tempContent, "{" + dataField + "}", item[dataField] == null ? "" : item[dataField]);
                });
                return tempContent;
            }
            grid.getRowContainer = function () {
                return $(grid.selectedObject).find(".grid_data");
            }

            //#endregion


            //#region Edit the grid data
            grid.interface.edit = function (val, rowId) {
                var editableColumns;
                if (!rowId)
                    editableColumns = $(grid.selectedObject).find(".grid_data").find(".grid_edit"); //Edit All row
                else
                    editableColumns = $(grid.selectedObject).find(".grid_data").find(".grid_row[row_id='" + rowId + "']").find(".grid_edit"); //Edit SPecific row.In future pass multiple rowId and select multiple row to editable column

                editableColumns.each(function (iIndex, item) {
                    if (val) {
                        var row = $(item).closest(".grid_row");
                        var column = grid.columns[$(this).attr("column_index")]; //Find the column to be updated                                
                        var data = grid.currentData[row.attr("row_id")];
                        $(item).html(grid.bindTemplate(column.editTemplate ? column.editTemplate : "<input type='text' dataField='" + column.dataField + "' value='{" + column.dataField + "}' />", column.dataField, data));
                    } else {
                        $(item).html("");
                    }
                    if (!val) {
                        $(item).show();
                        $(item).prev().hide();

                    } else {
                        $(item).hide();
                        $(item).prev().show();

                    }
                    $(item).toggle();
                    $(item).prev().toggle();

                });
                editableColumns.find("input[dataField]").bind('keyup keydown keypress onDOMAttrModified propertychange change', function () {
                    var rowId = $(this).closest(".grid_row").attr("row_id");
                    grid.currentData[rowId][$(this).attr("dataField")] = $(this).val();

                });
            }
            //#endregion


            //#region "For automation"
            grid.interface.AutoSetValue = function (filter, selector, value) {

                var row = $(grid.interface.getRow(filter)[0]);
                row.find(selector).val(value).trigger('change');

            }

            //Get value of the cell based on filter values
            grid.interface.AutoGetValue = function (filter, dataField) {
                var row = $(grid.interface.getRow(filter)[0]);
                return $(row.find("[datafield=" + dataField + "]")).first().text().trim()
            }

            //RowAction({ pcaName: "dfvds" }, "edit");
            //SetValue({ pcaName: "" }, "input[dataField=pcaName]", "Hellow")
            //SelectRow({ pcaName: "dfvds" });
            grid.interface.AutoSelectRow = function (filter) {

                var rowId = $(grid.interface.getRow(filter)[0]).attr("row_id");
                grid.interface.selectRow(rowId);
            }

            grid.interface.AutoRowAction = function (filter, actionName) {

                var row = $(grid.interface.getRow(filter)[0]);
                row.find("[action=" + actionName + "]").click();

            }
            //#endregion

            grid.attachCommonroperties();


        });


    };

    //File List
    $.fn.fileList = function () {
        return $.pageController.getControl(this, function (page, $$) {
            page.previewDialog = null;
            page.maxFiles = null;
            page.maxSize = null; // this will be in bytes

            page.interface.maxFiles = function (maxFiles) {
                if (maxFiles != null) {
                    page.maxFiles = maxFiles;
                }
            }
            page.interface.maxSize = function (maxSize) {
                if (maxSize != null) {
                    page.maxSize = maxSize;
                }
            }


            page.events.page_load = function () {
                $(page.selectedObject).addClass("file-list");
                var html = [];
                html.push("<div class='preview' style='display: none;' ><img src='' /></div>");
                html.push("<div class='file-list-data' style='display: none'></div>");
                html.push("<div class='file-list-action'>");
                //html.push('<input type="button" value="Click here to Upload" style="cursor:pointer;font-family: verdana; font-size: 10px; margin-top: 3px;"> <span> Or Drag and Drop here</span>');
                html.push('<input type="button" value="Click here to Upload" style="display: none">');
                //html.push('<input type="file"  accept="image/x-png, image/gif, image/jpeg"  class="inputfile" multiple style="display: none;">');
                html.push('<input type="file"  class="inputfile" multiple style="display: none;">');
                html.push("</div>");
                $(page.selectedObject).append(html.join(" "));
                //Trigger to open file dialog 
                $(page.selectedObject).on("click", ".file-list-action input[type=button]", function (e) {
                    $(page.selectedObject).find(".file-list-action input[type=file]").click();
                });




                $(page.selectedObject).find(".file-list-action input[type=file]").on("change", function () {
                    //var self = this;
                    //var approvedFiles = ValidateFileExtension(this.files);
                    //FileUpload(approvedFiles,
                    //           function (data) {
                    //               $(data)
                    //                   .each(function (i, imageItem) {

                    //                       page.allData[page.counter] = imageItem;
                    //                       bindRow(page.counter, imageItem);
                    //                       page.counter = page.counter + 1;
                    //                   });
                    //               if (page.interface.dataChange)
                    //                   page.interface.dataChange(page.interface.allData());

                    //               $(self).val("");
                    //           });



                });



                $(page.selectedObject).on('dragover', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                });

                function ValidateFileExtension(files) {
                    var approvedFiles = [];
                    //var _validFileExtensions = ["jpg", "jpeg", "gif", "png", "bmp"];

                    //$.each(files, function (i, n) {
                    //    if ($.inArray(this.type.split('/').pop().toLowerCase(), _validFileExtensions) == -1) {
                    //        alert("Only file formats jpg, jpeg, gif and png are allowed.");
                    //    } else {
                    //        approvedFiles.push(this);

                    //    }

                    //});
                    approvedFiles = files;
                    return approvedFiles;
                }

                $(page.selectedObject).on("drop", function (e) {

                    var droppedFiles = e.originalEvent.dataTransfer.files;

                    var approvedFiles = ValidateFileExtension(droppedFiles);


                    //FileUpload(approvedFiles, function (data) {
                    //    $(data).each(function (i, imageItem) {
                    //        page.allData[page.counter] = imageItem;
                    //        bindRow(page.counter, imageItem);
                    //        page.counter = page.counter + 1;
                    //    });
                    //    if (page.interface.dataChange)
                    //        page.interface.dataChange(page.interface.allData());

                    //});
                    e.preventDefault();
                    e.stopPropagation();
                });


            }



            function FileUpload(files, callback) {
                //var formData = new winFormData($(this)[0]);
                var allowUpload = true;
                var showSizeWarning = false;
                if (page.maxFiles != null) {
                    if (files.length + page.interface.allData().length > page.maxFiles) {
                        allowUpload = false;
                    }
                }

                if (allowUpload) {
                    if (files.length > 0) {
                        var datas = new window.FormData();
                        if (page.maxSize != null) {
                            for (var i = 0; i < files.length; i++) {
                                if (files[i].size < page.maxSize)
                                    datas.append(files[i].name, files[i]);
                                else
                                    showSizeWarning = true;
                            }
                        } else {
                            showSizeWarning = false;
                            for (var i = 0; i < files.length; i++) {
                                datas.append(files[i].name, files[i]);
                            }
                        }


                        page.container.progressBar('show');
                        $.ajax({
                            url: '../WebServices/FileUploadHandler.ashx/ProcessRequest',
                            type: 'POST',
                            data: datas,
                            cache: false,
                            crossDomain: true,
                            processData: false, // Don't process the files
                            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                            success: function (data, textStatus, jqXHR) {
                                var mapData = [];
                                $(data)
                                    .each(function (i, item) {
                                        mapData.push({
                                            fileNo: item.FileNo,
                                            fileName: item.FileName,
                                            filePath: item.FilePath.substring(2)
                                        });
                                    });
                                $(page.selectedObject).find(".file-list-data").css("height", "125px");
                                callback(mapData);

                                page.container.progressBar('hide');
                                if (showSizeWarning) {
                                    var tenL = 1000000;
                                    alert("Files greater than " + page.maxSize / tenL + " MB are not uploaded.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                page.container.progressBar('hide');
                            }
                        });
                    }
                } else {
                    alert('Max. ' + page.maxFiles + ' files can be stored here.');
                }
            }
            function bindRow(rowId, imageItem) {
                var html = [];
                html.push("<div style='position:relative' row-id='" + rowId + "' fileNo='" + imageItem.fileNo + "'>");
                html.push("<img title='Click to Preview'  style=' cursor: pointer;' src='" + imageItem.filePath + "' />");
                // html.push("<span>" + imageItem.fileName + "</span> ");
                html.push('<input type="button" title="Delete the file." style="position: absolute; cursor: pointer; color: black; background-color: white; font-family: courier new; width: 15px; margin-left: 0px; left: 85px; border: 1px solid black; padding-left: 2px; top: 0px;" action="delete" value="x" />');
                html.push("</div>");

                $(page.selectedObject).find(".file-list-data").append(html.join(" "));
                $(page.selectedObject).find(".file-list-data img").on("dragstart", function (event) {
                    event.preventDefault();

                    return false;
                });
            }

            $(page.selectedObject).on("click", ".file-list-data input[action]", function (e) {
                delete page.allData[$(e.currentTarget).closest("div").attr("row-id")];
                $(e.currentTarget).closest("div").remove();
                if (page.interface.dataChange)
                    page.interface.dataChange(page.interface.allData());
            });

            $(page.selectedObject).on("click", ".file-list-data img", function (e) {
                if (page.previewDialog == null) {

                    var div = $(page.selectedObject).find(".preview");
                    div.dialog();
                    div.dialog("open");
                    div.parent().find('.ui-dialog-title').html("Image Viewer");
                    div.dialog("option", "width", "900");
                    div.dialog("option", "maxHeight", "400");
                    div.dialog("option", "height", "400");
                    page.previewDialog = div;
                }
                page.previewDialog.find("img").attr("src", $(e.target).attr("src"));
                page.previewDialog.find("img").attr("width", '600');
                page.previewDialog.find("img").attr("height", '300');
                page.previewDialog.dialog("open");


            });

            page.interface.dataChange = null;
            page.counter = 1;
            page.interface.dataBind = function (imageList) {
                page.allData = {};
                $(page.selectedObject).find(".file-list-data").html("");
                $(imageList).each(function (i, imageItem) {

                    page.allData[page.counter] = imageItem;
                    bindRow(page.counter, imageItem);
                    page.counter = page.counter + 1;

                });

            }
            page.interface.allData = function () {
                var dataList = [];
                for (var prop in page.allData) {
                    dataList.push(page.allData[prop]);
                }
                return dataList;
            }


        });

    }

    //Repeater control
    $.fn.repeater = function () {
        var selectedObject = $(this);

        var bindTemplate = function (tempContent, dataFields, item) {

            $(dataFields.split(",")).each(function (i, dataField) {
                tempContent = tempContent.replaceAll("{" + dataField + "}", item[dataField]);
            });
            return tempContent;
        }
        var self = this;
        return {
            selectedObject: selectedObject,
            setTemplate: function (template) {
                selectedObject.template = template;
            },

            dataBind: function (data) {
                $(self).find(".grid_repeater_data").html("");
                $(data).each(function (i, item) {
                    var row = bindTemplate($(self).find(".grid_repeater_template").html(), "item_no,item_name,qty,price", item);
                    $(self).find(".grid_repeater_data").append("<div class='grid_repeater_row'>" + row + "</div>");

                });



            }




        };

    }

    //Tree controls
    $.fn.tree = function (defaults) {
        return $.pageController.getControl(this, function (page, $$) {

            page.events.page_load = function () {
                $(page.selectedObject).addClass("tree");
                page.currentSize = 0;
                page.pageSize = 200;
                page.interface.action = null;
                page.data = null;
                page.template = null;

                //Events to handle template level actions.For example edit,save etc which are placed in headers will be handled here.
                //User should set the action attribute of link tag to TemplateEdit,TemplateCancel,TemplateSave inorder to automatically handle ui change.
                $(page.selectedObject).on("click", " .tree_header a", function (e) {
                    var actionElement = $(e.target);
                    if (actionElement.attr("action")) {

                    }
                });

                $(page.selectedObject).keydown(function (e) {
                    if (e.keyCode == 38) {
                        //  $(page.selectedObject).
                    } else {

                    }
                });

                //Event to handle row level actions and expand sub tree
                $(page.selectedObject).on("click", " .tree_data", function (e) {
                    //Row level actions will be triggered for link tag with action attribute set. User can handle this event
                    var action = $(e.target).attr("action");
                    if (action) {

                        //Handle inbuilt actions like expand and collapse.
                        if ($(e.target).attr("action") == "expand") {

                            //Show the sub content and change the icon.
                            $(this).parent().children("ul").css("display", "block");
                            $(this).closest("li").children("a:first").children("span:first").children("img").attr("src", BasePath + "ui/ui/images/minus.gif");
                            $(this).closest("li").children("a:first").children("span:first").children("img").attr("action", "collapse");

                            if (page.interface.expand)
                                page.interface.expand({
                                    objectName: $(this).attr("object-name"),
                                    objectKey: $(this).attr("object-key"),
                                    objectValue: $(this).attr("object-value"),
                                    objectContext: $(this).attr("object-context"),
                                    objectData: JSON.parse($(this).attr("object-data")),
                                    currentNode: $(this).parent()
                                });

                        } else if ($(e.target).attr("action") == "collapse") {
                            $(this).parent().children("ul").css("display", "none");
                            $(this).closest("li").children("a:first").children("span:first").children("img").attr("src", BasePath + "ui/ui/images/plus.gif");
                            $(this).closest("li").children("a:first").children("span:first").children("img").attr("action", "expand");


                        } else {
                            //Method Syntax : action(actionName,selectedData,selectedObjectType,selectedCheckbox,parentData,target)
                            if (page.interface.action != null)
                                page.interface.action({
                                    action: $(e.target).attr("action"),
                                    actionElement: e.target,
                                    objectName: $(this).attr("object-name"),
                                    objectKey: $(this).attr("object-key"),
                                    objectValue: $(this).attr("object-value"),
                                    objectContext: $(this).attr("object-context"),
                                    objectData: JSON.parse($(this).attr("object-data")),
                                    currentNode: $(this).parent(),
                                    parentObject: $(this).parent().parent().parent().parent().parent().children("a").attr("object"),
                                });
                        }
                    } else {
                        $(page.selectedObject).find(".tree_data_chk:checked").closest("a").css("background-color", "white");
                        $(page.selectedObject).find(".tree_data_chk:checked").prop("checked", false);

                        if ($(this).find('.tree_data_chk').prop("checked") == true)
                            $(this).find('.tree_data_chk').closest("a").css("background-color", "white");
                        else
                            $(this).find('.tree_data_chk').closest("a").css("background-color", "rgb(213, 254, 168);");
                        $(this).find('.tree_data_chk').prop("checked", !$(this).find('.tree_data_chk').prop("checked"));

                        if (page.interface.selectionChange != null) {
                            page.interface.selectionChange({
                                action: "Select",
                                actionElement: e.target,
                                objectName: $(this).attr("object-name"),
                                objectKey: $(this).attr("object-key"),
                                objectValue: $(this).attr("object-value"),
                                objectContext: $(this).attr("object-context"),
                                objectData: JSON.parse($(this).attr("object-data")),
                                currentNode: $(this).parent(),

                                parentObject: $(this).parent().parent().parent().children("a").attr("object"),
                                parentNode: $(this).parent().parent().parent()
                            });
                        }
                    }

                });

                //Implement sorting for only root level headers,
                $(page.selectedObject).on("click", ".tree_root > li > .tree_header span[dataField]", function () {
                    //Comparison logic
                    function dynamicSort(property, order) {
                        var sortOrder = -1;
                        if (order === "ASC") {
                            sortOrder = 1;
                        }
                        return function (a, b) {
                            var result = (a[property].toLowerCase() < b[property].toLowerCase()) ? -1 : (a[property].toLowerCase() > b[property].toLowerCase()) ? 1 : 0;
                            return result * sortOrder;
                        };
                    }

                    //toggle sort direction on each click
                    var sort;
                    if ($(this).children("img").length === 0 || $(this).children("img").attr("sort") === "DESC")
                        sort = "ASC";
                    else
                        sort = "DESC";

                    //Sort the records
                    page.data.sort(dynamicSort($(this).attr("dataField"), sort));


                    //Load the sorted data
                    $(page.selectedObject).tree().addNodes(page.template, page.data, page.currentSize);

                    //Toggle the plus\minus icon
                    var header = $(page.selectedObject).find(".tree_header span[dataField='" + $(this).attr("dataField") + "']");
                    if (sort === "ASC")
                        header.append("<img sort='ASC'  src='" + WEBUI.CONTEXT.RootPath + "/css/images/topArrow.gif'  style='vertical-align: bottom;'/>");
                    else
                        header.append("<img sort='DESC' src='" + WEBUI.CONTEXT.RootPath + "/css/images/downArrow.gif' style='vertical-align: bottom;'/>");
                });

                //Logic to uncheck the last checkbox selection that is not made on current grid
                //$(page.selectedObject).on("change", ".tree_data_chk", function () {
                //    var par = $(this).parent().parent().parent().parent();
                //    $(page.selectedObject).find(".tree_data_chk:checked").each(function () {
                //        if (!($(this).parent().parent().parent().parent()[0] === par[0]))
                //            $(this).prop("checked", false);
                //    });
                //    $(page.selectedObject).find(".tree_header_chk:checked").prop("checked", false);
                //});

                //Implement Select All and DeSelect All methods.
                $(page.selectedObject).on("change", ".tree_header_chk", function () {
                    $(this).parent().parent().parent().nextAll().children("a").find(".tree_data_chk").prop("checked", $(this).prop("checked"));
                    var par = $(this).parent().parent().parent().parent();
                    $(page.selectedObject + "  .tree_data_chk:checked").each(function () {
                        if (!($(this).parent().parent().parent().parent()[0] === par[0]))
                            $(this).prop("checked", false);
                    });
                });



                //TODO : Simple implementation.Extend in future
                page.executeCondition = function (condition, item) {
                    var data = condition.replace("IF({", "").replace("}", "").replace(")", "").split(",");
                    //Dont change to === automatic conversion is needed here.
                    if (item[data[0]] == true)
                        return data[1].replace("'", "").replace("'", "");
                    else
                        return data[2].replace("'", "").replace("'", "");
                }


                //TODO : Move to common.js.
                page.entityMap = {
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#39;",
                    "/": "&#x2F;"
                };

                //Escape html and js specidal characters.
                page.escapeHtml = function (string) {
                    return String(string).replace(/[&<>"'\/]/g, function (s) {
                        return page.entityMap[s];
                    });
                }


            }


            page.interface = {
                //Private utility method to build the header part.Set the datafield so that it will be used in sorting. 
                //If Edit mode is set to true then add the edit template actions like edit,save and cancel.
                getHeader: function (template) {
                    var htmlContent = [];

                    htmlContent.push("<li STYLE='DISPLAY:NONE;'><div class='tree_header' object='" + template.object + "'>");
                    //Default show minus and checkbox should selectAll and DeselectAll
                    htmlContent.push("<span><img class='tree_header_img' src='../images/minus.gif'><input type='checkbox' action='selectAll' class='tree_header_chk' /></span>");
                    $(template.columns).each(function (i, item) {
                        if (!item.visible || item.visible === true)
                            htmlContent.push("<span action='sort' dataField='" + item.dataField + "' style='width:" + item.width + "' title='" + item.title + "' >" + item.title + "</span>");
                    });
                    htmlContent.push(" </div> </li>");
                    return htmlContent;
                },
                //Build each row along with data,searchInput and keys
                //It is mandatory to have a key to identify unique records.Will be used during update and delete.
                getData: function (data, template, leaf) {

                    var htmlContent = [];
                    var odd = true;
                    $(data).each(function (i1, item1) {
                        //Alternate colors
                        var style;
                        // if (odd === true)
                        style = "background-color:white;";
                        // else
                        //  style = "background-color:whitesmoke;";

                        if (template.styleCondition)
                            style = style + page.executeCondition(template.styleCondition, item1);



                        htmlContent.push("<li style='cursor:pointer;" + style + "'>");
                        odd = !odd;

                        var leafDataField = false;
                        if (typeof template.leafDataField != "undefined")
                            leafDataField = item1[template.leafDataField];
                        //Store all important data in attributes
                        htmlContent.push("<a class='tree_data' object-title='" + item1[template.objectTitle] + "' object-data='" + page.escapeHtml(JSON.stringify(item1)) + "'  object-name='" + template.object + "' object-value='" + item1[template.objectKey] + "' object-key='" + template.objectKey + "'  object-context-index='" + page.contextIndex + "' >");
                        if (leaf === true)
                            htmlContent.push("<span ><img action='expand' style='display: none'   class='tree_data_img' src='" + BasePath + "ui/ui/images/plus.gif'><input type='checkbox' class='tree_data_chk' style='display:none' /> </span>");
                        else if (leafDataField == true)
                            htmlContent.push("<span ><img action='expand' style='visibility: hidden'   class='tree_data_img' src='" + BasePath + "ui/ui/images/plus.gif'><input type='checkbox' class='tree_data_chk' style='display:none' /> </span>");
                        else
                            htmlContent.push("<span ><img action='expand' class='tree_data_img' src='" + BasePath + "ui/ui/images/plus.gif'><input type='checkbox' class='tree_data_chk' style='display:none' /> </span>");

                        //Bind all columns
                        $(template.columns).each(function (i2, item2) {
                            if (!item2.visible || item2.visible === true) {
                                //If template is specified for a column,then bind the data in template and generate html
                                if (item2.template) {
                                    var tempContent = item2.template;
                                    $(item2.dataField.split(",")).each(function (i3, item3) {
                                        tempContent = tempContent.replace("{" + item3 + "}", item1[item3]);
                                    });
                                    htmlContent.push(tempContent);
                                } else { //If no template is specified then add span tag
                                    var data = item1[item2.dataField];
                                    //Logic to trim out for specified max size-
                                    try {
                                        if (item2.maxSize)
                                            data = data.substring(0, item2.maxSize);
                                    } catch (e) {
                                    }
                                    //For edit mode , bind the input tag that should be used to get data value..
                                    if (item2.editable && item2.editable === true)
                                        htmlContent.push("<span title='" + data + "' style='width:" + item2.width + "' ><data>" + data + "</data><input type='text' style='display:none' value='" + data + "'  dataField='" + item2.dataField + "'/></span>");
                                    else
                                        htmlContent.push("<span title='" + data + "' style='width:" + item2.width + "' >" + data + "</span>");

                                }


                            }
                        });
                        htmlContent.push("</a></li>");

                    });
                    return htmlContent;

                },
                //Bind the data at root level.
                addNodes: function (template, data, context, oldCount) {
                    //WEBUI.UICONTROLS  all important information.
                    page.data = data;
                    page.template = template;
                    page.currentSize = 0;
                    page.context = new Array();
                    page.contextIndex = 0;
                    page.context[page.contextIndex] = context;


                    var currentSize = page.currentSize;
                    var pageSize = page.pageSize;

                    var htmlContent = [];
                    htmlContent.push("<ul class='tree_root'>");
                    htmlContent = htmlContent.concat(this.getHeader(template));      //Load Header

                    //To override the default no of records that should be shown.Used with sorting.
                    if (oldCount && (oldCount - pageSize) > 0)
                        currentSize = oldCount - pageSize;

                    //No record found
                    if (data.length === 0) {
                        // htmlContent.push("<li><span style='width:90%;text-align:center;'>No Records Found</span></li>");
                    } else
                        htmlContent = htmlContent.concat(page.interface
                            .getData(page.data.slice(0, currentSize + pageSize), template, false));

                    //Update the current no of records visible.
                    if (currentSize + pageSize < data.length)
                        currentSize = currentSize + pageSize;
                    else
                        currentSize = data.length;
                    page.currentSize = currentSize;

                    htmlContent.push("</ul>");


                    //Load more button implementation
                    page.selectedObject.html(htmlContent.join(" "));
                    page.selectedObject.next().remove();
                    page.selectedObject.after("<div style='display: inline-block; margin-top: 10px; width: 100%;'><span style='float: left; margin-top: 0px;'></span><a id='loadMorebtn' style='float: right; display: inline-block;cursor:pointer;'>Load More...</a></div>");
                    page.selectedObject.next().children("a").click(function () {
                        page.interface.loadMoreNodes();
                    });

                    //Footer information.
                    page.pageNo = page.selectedObject.next().children("span");
                    //if (page.data.length >= 500)
                    //    page.pageNo.text(currentSize + " of " + page.data.length + "     (only the first 500 record will display, please refine your search criteria to get more specific record)");
                    //else
                    //    page.pageNo.text(currentSize + " of " + page.data.length);
                    //End of page. Hide Load More.
                    if (currentSize === data.length) {
                        page.selectedObject.next().children("a").hide();
                    }

                    //Bind dragging support.
                    //  this.bindDragging();
                },
                //Bind additional data with diferent search criterial to current result.Note the data object should have both the old and new data together.
                addMoreNodes: function (template, data) {
                    if (WEBUI.UICONTROLS[uniqueId]["data"] && WEBUI.UICONTROLS[uniqueId]["data"].length > 0) {
                        WEBUI.UICONTROLS[uniqueId]["data"] = data;
                        WEBUI.UICONTROLS[uniqueId]["template"] = template;

                        WEBUI.UICONTROLS[uniqueId]["searchHistoryIndex"] = WEBUI.UICONTROLS[uniqueId]["searchHistoryIndex"] + 1;
                        WEBUI.UICONTROLS[uniqueId]["searchHistory"][WEBUI.UICONTROLS[uniqueId]["searchHistoryIndex"]] = template.criteria;

                        this.loadMoreNodes();
                        //   this.bindDragging();
                    } else
                        this.addNodes(template, data);
                },
                //Load Next page of records
                loadMoreNodes: function () {
                    var currentSize = page.currentSize;
                    var pageSize = page.pageSize;
                    var template = page.template;

                    if (currentSize === page.data.length) {
                        $("#loadMorebtn").hide();
                    }

                    var data = page.data.slice(currentSize, currentSize + pageSize);
                    $(page.selectedObject).find(".tree_root").append(page.interface.getData(data, template, false).join(" "));


                    if (currentSize + pageSize < page.data.length)
                        page.currentSize = currentSize + pageSize;
                    else
                        page.currentSize = page.data.length;

                    if (page.data.length >= 500)
                        page.pageNo.text(page.currentSize + " of " + page.data.length + "     (only the first 500 record will display, please refine your search criteria to get more specific record)");
                    else
                        page.pageNo.text(page.currentSize + " of " + page.data.length);
                    //    this.bindDragging();

                },
                //Load sub tree.
                addChildNodes: function (parentNode, template, data, leaf) {

                    var htmlContent = [];
                    htmlContent.push("<ul class='tree_child'>");

                    htmlContent = htmlContent.concat(page.interface.getHeader(template));

                    if (data.length === 0) {
                        //       htmlContent.push("<li><span style='width:90%;text-align:center;'>No Records Found</span></li>");
                    } else {

                        htmlContent = htmlContent.concat(page.interface.getData(data, template, leaf));
                    }
                    htmlContent.push("</ul>");
                    $(parentNode).append(htmlContent.join(" "));
                    //  this.bindDragging();

                },
                //Implementatio of drag and drop feature.
                bindDragging: function () {

                    $(page.selectedObject).find(".tree_data").draggable({
                        revert: true,
                        appendTo: "body",
                        drag: function () {
                            if (!$(this).find("input[type=checkbox]").prop("checked"))
                                return false;
                            return true;
                        },
                        helper: function () {
                            if (!$(this).find("input[type=checkbox]").prop("checked"))
                                return "";
                            var key = "";
                            var object = "";
                            $($(page.selectedObject).tree().getSelectedData()).each(function (iIndex, item) {
                                key = key + item.keyValue + ",";
                                object = item.object;

                            });
                            return "<span style='position:absolute;margin:10px;border:solid 1px black;font-weight:bold;background-color:whitesmoke'>" + object + ":" + key + "</span>";
                        }
                    });

                    $(page.selectedObject).find(".tree_data").droppable({
                        hoverClass: "ui-dropping",
                        accept: $(page.selectedObject).find(".tree_data"),
                        drop: function () {
                            var chk = $(this).find("input[type=checkbox]");

                            if (WEBUI.UICONTROLS[uniqueId].drop)
                                WEBUI.UICONTROLS[uniqueId].drop($(page.selectedObject).tree().getNodeData(chk), $(page.selectedObject).tree().getSelectedData(), chk);


                        }
                    });
                },
                //Get selected data from tree.
                getSelectedData: function () {
                    var dataList = new Array();
                    $(selectedObject).find(".tree_data_chk:checked").each(function (iIndex) {

                        dataList[iIndex] = JSON.parse($(this).parent().parent().attr("data"));
                        dataList[iIndex]["key"] = $(this).parent().parent().attr("object-key");
                        dataList[iIndex]["keyValue"] = $(this).parent().parent().attr("object-value");
                        dataList[iIndex]["object"] = $(this).parent().parent().attr("object");
                        dataList[iIndex]["source"] = $(this).parent().parent().attr("object");
                        dataList[iIndex]["destination"] = $(this).parent().parent().parent().parent().parent().children("a").attr("object");

                    });
                    return dataList;
                },
                selectNode: function (keyValue) {
                    $(page.selectedObject).find("[object-value=" + keyValue + "]").click();
                },
                selectNodeByText: function (textValue) {
                    $(page.selectedObject).find("[object-title='" + textValue + "']").click();
                },
                expandNodeByText: function (textValue) {
                    $(page.selectedObject).find("[object-title='" + textValue + "']").find(".tree_data_img").click();
                },
                deSelectNode: function (chk) {
                    $(chk).closest("[object-value]").css("background-color", "white");
                    $(chk).prop("checked", false);

                },




                //get Selected Checkboxes
                getSelectedNodes: function () {
                    return $(page.selectedObject).find(".tree_data_chk:checked");
                },
                //Get parent checkbox
                getParentNode: function () {
                    return $(selectedObject).find(".tree_data_chk:checked").parent().parent().parent().parent().parent().children("a").find(".tree_data_chk");
                },
                //Get the data for the the input checkbox.
                getNodeData: function (chk) {
                    var dataList = new Array();
                    var iIndex = 0;
                    dataList[iIndex] = JSON.parse($(chk).parent().parent().attr("object-data"));
                    dataList[iIndex]["key"] = $(chk).parent().parent().attr("object-key");
                    dataList[iIndex]["keyValue"] = $(this).parent().parent().attr("object-value");
                    dataList[iIndex]["object"] = $(chk).parent().parent().attr("object-name");
                    dataList[iIndex]["source"] = $(chk).parent().parent().attr("object-name");
                    dataList[iIndex]["destination"] = $(chk).parent().parent().parent().parent().parent().children("a").attr("object-name");
                    return dataList;
                },
                //To reload the row with updated data.The checkbox input parameter decides the record to be updated.
                reLoadNode: function (chk, template, data) {
                    var liContent = this.getData(data, template, false);

                    var selectedNode = $(chk).parent().parent().parent();
                    $(liContent.join(" ")).insertBefore(selectedNode);
                    selectedNode.remove();

                },
                //Re-expand the tree.
                reLoadChildNodes: function (chk) {
                    var expColIcon = $(chk).parent().parent().find(".tree_data_img");

                    $(chk).parent().parent().nextAll().remove();

                    expColIcon.attr("action", "expand");
                    expColIcon.click();
                    //$(chk).parent().parent().each(function () {
                    //    if (".tree_data_img")
                    //    if ($(this).next().is(":visible")) {
                    //        $(this).nextAll().remove();
                    //        $(this).click();
                    //    } else
                    //        $(this).nextAll().remove();
                    //});
                },
                //Delete a node
                deleteNode: function (chk) {
                    var selectedNode = $(chk).parent().parent().parent();
                    selectedNode.remove();

                }
            };

        });




        //Build the object with useful properties and return to users.


    };

    $.fn.fileUploader = function (defaults) {
        return $.pageController.getControl(this, function (page, $$) {

            $(page.selectedObject).html('<input type="file" id="fileElem" multiple accept="image/*" style="display:none"  onchange="">            <a class="col-lg-3 col-lg-offset-5 col-xs-12 post_ad" href="#" id="fileSelect">Upload Image</a>             <div class="col-lg-3 col-lg-offset-5 col-xs-12" id="fileList"> <img/>                     </div>');//<p>No Files selected!</p>   

            page.interface.fileAdded=null;

            function handleFiles(files) {
                for (let i = 0; i < files.length; i++) {
                    const file = files[i];

                    new FileUpload(files[i], files[i].file);

                    // if (!file.type.startsWith('image/')) { continue }

                    // var preview= $(page.selectedObject).find("#fileList");

                    // const img = document.createElement("img");
                    // img.classList.add("obj");
                    // img.file = file;
                    // preview.appendChild(img); // Assuming that "preview" is the div output where the content will be displayed.

                    // const reader = new FileReader();
                    // reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
                    // reader.readAsDataURL(file);
                }
            }

            // function sendFiles() {
            //     const imgs = document.querySelectorAll(".obj");

            //     for (let i = 0; i < imgs.length; i++) {
            //         new FileUpload(imgs[i], imgs[i].file);
            //     }
            // }
            function FileUpload(file ) {
                const reader = new FileReader();
                reader.onload = function (evt) {
                    //xhr.send(evt.target.result);

                    $.server.webMethodPOST("propon", "propon/files/",[ {
                        file_no:"##GUID##",
                        file_name:file.name,
                        file_path:"",
                        file_data:evt.target.result,"org_id":"##org_id##",
                    }], function (data) {
                      
                        //$(page.selectedObject).find("img").attr("src",data[0].file_data);
                        if(page.interface.fileAdded){
                            page.interface.fileAdded(data);
                           
                        }
                       
                    });
                    
                };
                reader.readAsDataURL(file);

                // this.ctrl = createThrobber(img);
                // const xhr = new XMLHttpRequest();
                // this.xhr = xhr;

                // const self = this;
                // this.xhr.upload.addEventListener("progress", function (e) {
                //     if (e.lengthComputable) {
                //         const percentage = Math.round((e.loaded * 100) / e.total);
                //         self.ctrl.update(percentage);
                //     }
                // }, false);

                // xhr.upload.addEventListener("load", function (e) {
                //     self.ctrl.update(100);
                //     const canvas = self.ctrl.ctx.canvas;
                //     canvas.parentNode.removeChild(canvas);
                // }, false);
                // xhr.open("POST", "http://demos.hacks.mozilla.org/paul/demos/resources/webservices/devnull.php");
                // xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
                // reader.onload = function (evt) {
                //     xhr.send(evt.target.result);
                // };
                // reader.readAsBinaryString(file);
            }

            page.events.page_load = function () {
                $(page.selectedObject).find("#fileSelect").click(function (e) {

                    $(page.selectedObject).find("#fileElem").click();

                    e.preventDefault();

                });
                $(page.selectedObject).find("#fileElem").change(function (e) {

                    handleFiles(this.files);

                });
            }

        });
    }


});





var parseJSON = function (str) {
    return JSON.parse(str);
}


//var PrintService = {};

//PrintService.Print = function (printBox) {
//    var array = JSON.stringify(printBox);


//    $.getJSON('http://localhost:8888/PrintService/printreceipt' + '?receiptData=' + encodeURIComponent(array) + '&sam=1&callback=?', function (data) {
//        alert('Receipt printed successfully!');
//    }, function (error) {
//        alert('Error in printing this receipt!. Configure your printer properly!');

//    });
//    return "success";
//}

//PrintService.PrintReceipt = function (printBox) {

//    var printBoxString = JSON.stringify(printBox);
//    //var printBox={ 
//    //    PrinterName: "Microsoft Print to PDF",
//    //    Width:300,
//    //    Height:200,
//    //    Lines:[]

//    //}


//    $.ajax({
//        type: "POST",

//        url: "http://localhost:8888/PrintService/printtest",  //CONTEXT.ReceiptPrinterURL  try now   "http://localhost:8087/printtest", //
//        async: true,

//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        // crossDomain: true,
//        data: JSON.stringify(printBox),
//        //headers: {
//        //    "auth-token": getCookie("auth_token")

//        //},
//        success: function (items) {
//            alert("Bill Printed Successfully...");
//        },
//        error: function (err) {
//            alert("Sorry Error Will Occur Please Check Your Printer Configuration");
//        }
//    });

//    //var printBoxString = JSON.stringify(printBox);
//    ////var printBoxString = JSON.stringify({ root: printBox });


//    //$.getJSON(CONTEXT.ReceiptPrinterURL.replace("{printBox}", encodeURIComponent(printBoxString)), function (data) {
//    //    alert('Receipt printed successfully!');
//    //}, function (error) {
//    //    alert('Error in printing this receipt!. Configure your printer properly!');

//    //});
//    //return "success";

//    //var array = JSON.stringify(receipt);
//    ////var url = 'http://example.com/?data=' + encodeURIComponent(array);

//    //$.getJSON('http://localhost:8888/PrintService/printreceipt' + '?receiptData=' + encodeURIComponent(array) + '&sam=1&callback=?', function (data) {
//    //    alert('Receipt printed successfully!');
//    //}, function (error) {
//    //    alert('Error in printing this receipt!. Configure your printer properly!');

//    //});
//    //return "success";
//}


//PrintService.PrintReceiptPDF = function (printBox) {

//    var printBoxString = JSON.stringify(printBox);


//    $.getJSON(CONTEXT.ReceiptPrinterURL.replace("{printBox}", encodeURIComponent(printBoxString)), function (data) {
//        alert('Receipt printed successfully!');
//    }, function (error) {
//        alert('Error in printing this receipt!. Configure your printer properly!');

//    });
//    return "success";

//    //var array = JSON.stringify(receipt);
//    ////var url = 'http://example.com/?data=' + encodeURIComponent(array);

//    //$.getJSON('http://localhost:8888/PrintService/printreceipt' + '?receiptData=' + encodeURIComponent(array) + '&sam=1&callback=?', function (data) {
//    //    alert('Receipt printed successfully!');
//    //}, function (error) {
//    //    alert('Error in printing this receipt!. Configure your printer properly!');

//    //});
//    //return "success";
//}


//PrintService.PrintFile = function (printData) {

//    //printData = { "root": { "BillType": "INVOICE", "CustomerName": "", "Phone": "", "CustAddress": "", "CustCityStreetZipCode": "", "DLNo": "", "isSalesExe": "false", "GST": "", "TIN": "", "Area": "", "SalesExecutiveName": "", "VehicleNo": "", "BillNo": "145", "Abdeen": "Abdeen:", "AbdeenMobile": "9443463089", "Off": "Off:", "OffMobile": "9944410350", "Home": "PH:", "HomeMobile": "04639-245478", "BillDate": "31-08-2017", "NoofItems": "1", "Quantity": "1.00", "BSubTotal": "60.00", "DiscountAmount": "0.00", "BCGST": "3.60", "BSGST": "3.60", "TaxAmount": "7.20", "BillAmount": "67.00", "ApplicaName": "Shop On 3.0", "ApplsName": "SHOP ON 3.0", "CompanyAddress": "VEERAPANDIYAPATNA", "CompanyCityStreetPincode": "", "CompanyPhoneNoEtc": "9944410350", "CompanyDLNo": "TNT/161/20B-TNT/149/21B", "CompanyTINNo": "33586450443", "CompanyGST": "33CCKPS9949CIZ4", "SSSS": "DUPLICATE", "Original": "ORIGINAL", "RoundAmount": "-0.20", "BillItem": [{ "BillItemNo": 1, "ProductName": "Bag", "Pack": "", "Batch": "", "Exp": "", "Qty": "1.00", "Per": "No", "Qty_unit": "1.00", "FreeQty": "0.00", "Rate": "60.00", "PDis": "0.00", "MRP": "60.00", "CGST": "3.60@6.00", "SGST": "3.60@6.00", "GValue": "67.20" }], "BillName": "SALES BILL" } };

//    CONTEXT.BarcodePrinterURL11 = "http://localhost:8888/PrintService/printfile?printData={printData}&appName=ShopOnDev&templateURI=sales-bill-print/main-sales-bill-short-new1.jrxml&exportType=PDF"

//    var printBoxString = JSON.stringify({ root: printData });


//    $.getJSON(CONTEXT.BarcodePrinterURL11.replace("{printData}", encodeURIComponent(printBoxString)), function (data) {
//        alert('Jasper printed successfully!');
//    }, function (error) {
//        alert('Error in printing this receipt!. Configure your printer properly!');

//    });
//    return "success";

//    //// Get the JsonP data

//    //var array = JSON.stringify(barCode);
//    ////var url = 'http://example.com/?data=' + encodeURIComponent(array);


//    //$.getJSON('http://localhost:8888/PrintService/printbarcode' + '?barcodeData=' + encodeURIComponent(array) + '&sam=1&callback=?', function (data) {
//    //    alert('Barcode printed successfully for this item!');
//    //}, function (error) {
//    //    alert('Error in printing this item barcode !. Configure your printer properly!');

//    //});
//    //return "success";
//}



//PrintService.PrintBarcode = function (printBox) {

//    var printBoxString = JSON.stringify(printBox);


//    $.getJSON(CONTEXT.BarcodePrinterURL.replace("{printBox}", encodeURIComponent(printBoxString)), function (data) {
//        alert('Barcode printed successfully!');
//    }, function (error) {
//        alert('Error in printing this receipt!. Configure your printer properly!');

//    });
//    return "success";

//    //// Get the JsonP data

//    //var array = JSON.stringify(barCode);
//    ////var url = 'http://example.com/?data=' + encodeURIComponent(array);


//    //$.getJSON('http://localhost:8888/PrintService/printbarcode' + '?barcodeData=' + encodeURIComponent(array) + '&sam=1&callback=?', function (data) {
//    //    alert('Barcode printed successfully for this item!');
//    //}, function (error) {
//    //    alert('Error in printing this item barcode !. Configure your printer properly!');

//    //});
//    //return "success";
//}