cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-android-permissions.Permissions",
    "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
    "pluginId": "cordova-plugin-android-permissions",
    "clobbers": [
      "cordova.plugins.permissions"
    ]
  },
  {
    "id": "cordova-plugin-call-number.CallNumber",
    "file": "plugins/cordova-plugin-call-number/www/CallNumber.js",
    "pluginId": "cordova-plugin-call-number",
    "clobbers": [
      "call"
    ]
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-firebase.FirebasePlugin",
    "file": "plugins/cordova-plugin-firebase/www/firebase.js",
    "pluginId": "cordova-plugin-firebase",
    "clobbers": [
      "FirebasePlugin"
    ]
  },
  {
    "id": "cordova-plugin-geolocation.geolocation",
    "file": "plugins/cordova-plugin-geolocation/www/android/geolocation.js",
    "pluginId": "cordova-plugin-geolocation",
    "clobbers": [
      "navigator.geolocation"
    ]
  },
  {
    "id": "cordova-plugin-geolocation.PositionError",
    "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
    "pluginId": "cordova-plugin-geolocation",
    "runs": true
  },
  {
    "id": "cordova-plugin-spinner.SpinnerPlugin",
    "file": "plugins/cordova-plugin-spinner/www/spinner-plugin.js",
    "pluginId": "cordova-plugin-spinner",
    "clobbers": [
      "SpinnerPlugin"
    ]
  },
  {
    "id": "cordova-plugin-spinner-dialog.SpinnerDialog",
    "file": "plugins/cordova-plugin-spinner-dialog/www/spinner.js",
    "pluginId": "cordova-plugin-spinner-dialog",
    "merges": [
      "window.plugins.spinnerDialog"
    ]
  },
  {
    "id": "cordova-plugin-x-toast.Toast",
    "file": "plugins/cordova-plugin-x-toast/www/Toast.js",
    "pluginId": "cordova-plugin-x-toast",
    "clobbers": [
      "window.plugins.toast"
    ]
  },
  {
    "id": "kr.co.joycorp.cordova.exitapp.exitApp",
    "file": "plugins/kr.co.joycorp.cordova.exitapp/www/ExitApp.js",
    "pluginId": "kr.co.joycorp.cordova.exitapp",
    "merges": [
      "navigator.app"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-android-permissions": "1.0.0",
  "cordova-plugin-call-number": "1.0.1",
  "cordova-plugin-device": "2.0.2",
  "cordova-plugin-firebase": "2.0.5",
  "cordova-plugin-geolocation": "4.0.1",
  "cordova-plugin-spinner": "1.1.0",
  "cordova-plugin-spinner-dialog": "1.3.1",
  "cordova-plugin-whitelist": "1.3.3",
  "cordova-plugin-x-toast": "2.7.2",
  "cordova-plugin-browsersync": "0.1.7",
  "kr.co.joycorp.cordova.exitapp": "1.0.0"
};
// BOTTOM OF METADATA
});