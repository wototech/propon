

window.shopOnContext = {
    user: {},
    isAndroid: false,
    companyProduct: //{}
    {
        app_flag: "1",
        comp_id: "5420",
        comp_name: "Sruthi12",
        comp_prod_id: "100228",
        comp_prod_name: "eShopOn Restuarant",
        comp_prod_uri: "http://eshopon.wototech.com",
        instance_id: "37",
        prod_id: "35",
        prod_img: "Image/textiles.png",
        prod_name: "E-SHOPON",
        prod_uri: "",
        SMS_API_URL:"http://104.251.212.122:8080/woto-utility-rest/rest/sendSMS/monster-text-message"
    },
    isLoggedIn: false,
    shoppingList: [],
    deliveryAddress: {
        full_name: "",
        email: "",
        addrLine1: "Plotno 29, nehru street",
        addrLine2: "Nadambakkam",
        addrLine3: "East",
        city: "Chennai",
        pincode: "600574",
        state: "TN"
    }
};
var redTheme = {
    primaryColor: "rebeccapurple",
    secondaryColor: "#834db9",
    fontFamily:"abel-regular"
};
window.theme = redTheme;
window.settings = {
    initialSearch: "None",
    //All, None,
    mainSlider: [{
        imagePath: "image1.png"
    }, {
        imagePath: "image2.png"
    }],
    showCallNow: true,
    COUNTRY_CURRENCY_SYMBOL: "&#x20B9"
}


$.fn.propertySearch = function () {
    return $.pageController.getControl(this, function (control, $$, e) {

        control.interface.search=function(data){
            control.view.searchLocation(data.location);
            control.view.searchActionType(data.actionType);
            control.view.searchTextBox(data.freeText);
            control.view.searchPrice(data.price);
            control.view.searchProdTypes(data.prodType);
            
            setTimeout(function(){
                control.events.btnSearch_click();
            },300);
        }
        control.interface.selectProduct = function (row,rowItem) {

            //loading property common details
            var filter = "item_no='" + rowItem.item_no + "'"
            control.data.items.allData(encodeURIComponent(filter), "upd_date",function (data) {

                //control.interface.subscribe("product-detail");
                var item = data[0];
                $$("lblItemName").html(item.item_name);
                $$("lblItemNo").html("( " + item.item_no + ")");
                $$("lblItemLocation").html(item.location);
                $$("lblPrice").html(" at " + window.settings.COUNTRY_CURRENCY_SYMBOL + " " + item.price);
                $$("lblProject").html(item.project);

                if (item.contact_flag == "3" || item.contact_flag == "5") {
                    $$("btnContactUs").removeClass("hide");
                    if (item.owner_type == "Owner") {
                        $$("btnContactUs").text("Contact Owner");
                    } else if (item.owner_type == "Agent") {
                        $$("btnContactUs").text("Contact Agent");
                    } else if (item.owner_type == "Builder") {
                        $$("btnContactUs").text("Contact Builder");
                    }
                    if (item.contact_flag == "3") {
                        $$("btnMessageUs").addClass("hide");
                    } else {
                        $$("btnMessageUs").removeClass("hide");
                    }
    
                } else if (item.contact_flag == "1") {
                    $$("btnContactUs").text("View Contact");
                    $$("btnContactUs").removeClass("hide");
                    $$("btnMessageUs").addClass("hide");
                } else if (item.contact_flag == "2") {
                    $$("btnContactUs").addClass("hide");
                    $$("btnMessageUs").removeClass("hide");
                } else if (item.contact_flag == "4") {
                    $$("btnContactUs").removeClass("hide");
                    $$("btnMessageUs").removeClass("hide");
                    $$("btnContactUs").text("View Contact");
                }                


                })
                //loading images
                data=rowItem.attributes;
                // control.data.attributes.withFilter(encodeURIComponent(filter), function (data) {
                    if (data !="" && data !=undefined) {
                        var img = "";
                        
                        for ( var property in data ) {
                            if(property.startsWith("image")){
                                if(img=="")
                                    img= "'"+data[property]+"'";
                                else
                                    img= img+","+"'"+data[property]+"'";
                            } 
                          }
                          if(img!=""){
                            var filter = " file_no in (" + img + ")";
                            control.data.file.allData(filter, function (data) {
                                if(data.length>0){
                                    control.view.loadPropImages(data);
                                    var file = data[0];
                                    if (file.file_data == undefined || file.file_data == "" || file.file_data == null)
                                        rowItem.image1 = "img\\alter-img.png";
                                    else
                                    rowItem.image1 = file.file_data;
                                } else {
                                    rowItem.image1 = "img\\alter-img.png";
                                }
                            $$("lblimg").attr("src", rowItem.image1);
                            })
                        }else{
                            rowItem.image1 = "img\\alter-img.png";
                            $(row).find("#idPropDefImg").attr("src", rowItem.image1);
                        }                        
                    } else {
                        rowItem.image1 = "img\\alter-img.png";
                        $$("lblimg").attr("src", rowItem.image1);
                    }

                //loading attributes
                var filter = " item_no='"+ rowItem.item_no+"'";
                control.data.attributes.withFilter(encodeURIComponent(filter),"attributes", function (data) {    
                    control.view.itemAttrList(data);
                    window.itemList = rowItem;
                })
                
        }
        control.interface.showDetail = function () {
            $$("pnlSearchResult").addClass("hide");
            $$("pnlItemDetail").removeClass("hide");
            $$("lnksortandfilter").addClass("hide");
        }
        control.interface.showFilter = function () {
            $$("pnlFilter").removeClass("mobile-hide");
            $(".search-panel ").addClass("hide");
            $$("pnlImagePanel").addClass("hide");
            $$("lnksortandfilter").addClass("hide");
            $$("lnkFilterApplyCancel").removeClass("hide");
        }
        control.interface.showSort= function(){
            $$("pnlFilter").addClass("mobile-hide");
            $(".search-panel").addClass("hide");
            $$("pnlImagePanel").removeClass("hide");
            $$("lnksortandfilter").addClass("hide");
            $$("lnkFilterApplyCancel").addClass("hide");   
            $$("lnkSortApplyCancel").removeClass("hide");         
            $$("pnlSort").removeClass("mobile-hide");   
            $$("pnlSort").removeClass("hide");   
            $$("grdProductList").addClass("hide");
            $(".product-header").addClass("hide");
        }
        control.interface.showSearch = function () {
            $$("pnlImagePanel").removeClass("hide");
            $(".search-panel").removeClass("hide");
            $$("pnlFilter").addClass("mobile-hide");
            $$("lnksortandfilter").removeClass("hide");
            $$("lnkFilterApplyCancel").addClass("hide");
            $$("pnlSort").addClass("mobile-hide");   
            $$("pnlSort").removeClass("hide");     
            $$("grdProductList").removeClass("hide");
            $(".product-header").removeClass("hide");      
            $$("lnkSortApplyCancel").addClass("hide");      
        }        
        control.interface.hideDetail = function(){
            $$("pnlSearchResult").removeClass("hide");
            $$("pnlItemDetail").addClass("hide");       
        }
        control.interface.main = function (data) {
            control.events.init();
            if (typeof (data) !== "undefined" && data!=null) {
                control.interface.search(data);
            }
            $$("lnksortandfilter").removeClass("hide");
            $$("lnksortandfilter").addClass("desktop-hide");

               
            $$("pnlSearchResult").removeClass("hide");
            $$("pnlItemDetail").addClass("hide");


        }
        control.attrType =function (attrKey){
            if(attrKey=="expected_price")
                return "range";
            else
                return "list";
        }


        control.view = {
            locationDataSurce: function (dataSource) {
                $$("aclLocation").dataBind(dataSource, "attr_value", "attr_text");
            },
            productTypeDataSource: function (dataSource) {
             
                $$("aclPropertyType").dataBind(dataSource, "ptype_no", "ptype_name");
            },
            priceDataSource: function (dataSource) {
             
                $$("aclPrice").dataBind(dataSource, "value", "label");
            },
            actionDataSource: function (dataSource) {
            
                $$("aclActionType").dataBind(dataSource, "value", "label");
            },
            searchLocation: function (data) {
                if(typeof(data)!=="undefined" && data!="")
                   $$("aclLocation").selectedValue(data);              
                return $$("aclLocation").selectedValue();          
            },
            searchActionType: function (data) {
                if(typeof(data)!=="undefined" && data!="")
                    $$("aclActionType").selectedValue(data);      
                return  $$("aclActionType").selectedValue(); 
            },
            searchProdTypes: function (data) { 
                
                if(typeof(data)!=="undefined" && data!="")
                    $$("aclPropertyType").selectedValues(data);      
                return  $$("aclPropertyType").selectedValues().join(",");

            },
            searchTextBox: function (data) {
                if(typeof(data)!=="undefined" && data!="")
                    $$("txtSearchBox").val(data);
                else
                    return $$("txtSearchBox").val();
            },
            searchPrice: function (data) {
                if(typeof(data)!=="undefined" && data!="")
                $$("aclPrice").selectedValue(data);      
            return  $$("aclPrice").selectedValue(); 

            
            },
            itemFilterAttributes: function (data) {
                $$("grdFilters").width("100%");
                $$("grdFilters").height("auto");
                $$("grdFilters").setTemplate({
                    selection: "None",
                    columns: [


                        { 'name': "", 'width': "95%", 'dataField': "attr_key,attr_value,attr_text", itemTemplate: "<div> <input attr_key='{attr_key}' attr_value='{attr_value}' style='vertical-align:middle' type='checkbox' /> {attr_text}</div>" }

                    ]
                });
                var lastKey = "";
                $$("grdFilters").rowBound = function (row, item) {
                    var attrType= control.attrType(item.attr_key);
                    
                    var header="<div class='filter-header'>" + item.attr_key + "</div><br>";
                    if(attrType=="range"){
                        $(row).hide();
                        
                        if (lastKey != item.attr_key) {
                            $(row).show();
                            header="<div class='filter-header'><div>" + item.attr_key + "</div><input style='float:left' type='text'><input style='float:left' type='text'><button style='float:left' >apply<button></div><br>";
                        }
                            
                    }

                    if (lastKey != item.attr_key) {
                        lastKey = item.attr_key;
                        $(row).prepend(header);
                    }

                    $(row).find("input[type=checkbox]").click(function(){
                        control.events.searchAndFilter(2);
                    });

                }

             
                $$("grdFilters").dataBind(data);

                $($$("grdFilters").selectedObject).find("input[type=checkbox]").each(function (i, chk) {
                        if (control.lastFilterSelection[$(chk).attr("attr_key")]){
                            for(var i=0;i<control.lastFilterSelection[$(chk).attr("attr_key")].length;i++){
                                if(control.lastFilterSelection[$(chk).attr("attr_key")][i]==  ("'" +  $(chk).attr("attr_value") + "'") )
                                    $(chk).prop("checked",true);
                            }
                        }                     
                });


                $$("grdFilters").selectedObject.find(".grid_row").css("width", "100%");
                $$("grdFilters").selectedObject.find(".grid_header").addClass("hide");
                $$("grdFilters").selectedObject.css("border", "none");
                $$("grdFilters").selectedObject.find(".grid_data").css("overflow", "auto");
            },
            itemList: function (data) {
                $$("grdProductList").width("100%");
                $$("grdProductList").height("auto");
                $$("grdProductList").setTemplate({
                    paging: true, pagingType: "LoadMore", pageSize: 100, selection: "None",
                    columns: [
                        {
                            'name': "", 'width': "100%", 'dataField': "item_no,item_name,price,location,project,addr1,addr2,addr3",
                            itemTemplate: '<div class="product-list"><div id="idProduct"><div class="product-img col-lg-4 col-xs-12" style="padding: 0px;"><img id="idPropDefImg" alt="Product image" width="200" height="200" /></div><div class="product-desc" style="margin-top: 20px;padding-left: 20px;"><span class="item-text" >{item_name}</span></div><div class="item-desc" style="margin-bottom: 3px;padding-left: 20px;" ><span class="project"> Project : {project} </span><br><span class="location"> Location : {location} </span><br><span class="address"> {addr1} </span><span class="address"> {addr2} </span><span class="address"> {addr3} </span><br><span class="selling_price"> Price : ' + window.settings.COUNTRY_CURRENCY_SYMBOL + ' {price}  </span></div></div> </div></div>'//<div class="product-button"><button class="btn-grid" id="idBuyNow">Buy Now</button></div>
                        }

                    ]
                });
                $$("grdProductList").beforeRowBound = function (row, item) {
                    var filter = "item_no='" + item.item_no + "' and attr_key ='image1'";
                    
                    data=item.attributes;
                    // control.data.attributes.withFilter(encodeURIComponent(filter), function (data) {
                        if (data !="" && data !=undefined) {
                            var img = "";
                            
                            for ( var property in data ) {
                                if(property.startsWith("image")){
                                    img= data[property];
                                } 
                              }
                              if(img!=""){
                                var filter = " file_no='" + img + "'";
                                control.data.file.allData(filter, function (data) {
                                    var img = data[0];
                                    if (img.file_data == undefined || img.file_data == "" || img.file_data == null)
                                        item.image1 = "img\\alter-img.png";
                                    else
                                        item.image1 = img.file_data;

                                        $(row).find("#idPropDefImg").attr("src", item.image1);
                                })
                              }else{
                                item.image1 = "img\\alter-img.png";
                                $(row).find("#idPropDefImg").attr("src", item.image1);
                              }


                        } else {
                            item.image1 = "img\\alter-img.png";
                            $(row).find("#idPropDefImg").attr("src", item.image1);
                        }

                    //})
                }
                $$("grdProductList").rowBound = function (row, item) {
                    
                    $(row).find("[id=idProduct]").click(function () {
                        control.interface.selectProduct(row, item);
                        control.interface.subscribe("product-detail");
                    });
                }
                $$("grdProductList").dataBind(data);
                control.interface.hideDetail();
                $$("grdProductList").selectedObject.find(".grid_header").addClass("hide");
                $$("grdProductList").selectedObject.find(".grid_data").addClass("row");
                $$("grdProductList").selectedObject.find(".grid_row").addClass("col-lg-12 col-xs-12");
                $$("grdProductList").selectedObject.find(".grid_row").css("background", "transparent");
                $$("grdProductList").selectedObject.find(".grid_row").css("margin-top", "10px");
                $$("grdProductList").selectedObject.find(".grid_row").css("margin-bottom", "10px");
                $$("grdProductList").selectedObject.find(".grid_data").css("background", "transparent");
                $$("grdProductList").selectedObject.css("border", "none");
                $$("grdProductList").selectedObject.find(".grid_data").css("overflow", "auto");
            },
            loadPropImages: function (data) {

                // var filter = "item_no='" + item.item_no + "' and attr_key like'%image%'"
                
                // control.data.attributes.withFilter(encodeURIComponent(filter),"",  function (data) {
                    
                //     control.itemImgs = "";
                //     $(data).each(function (i, item) {
                //         if (i == 0)
                //             control.itemImgs = control.itemImgs + "'" + item.attr_value + "'";
                //         else
                //             control.itemImgs = control.itemImgs + ",'" + item.attr_value + "'";
                //     })

                    // if (item != "") {
                    //     var filter = "";
                    //     filter = filter + " file_no in(" + item + ")"
                    //     control.data.file.allData(filter, function (data) {
                        if(data.length>0){
                            var html = "";
                            html = html + '<div class="slideshow-container">';
                            $(data).each(function (i, item) {
                                if (i == 0)
                                    html = html + '<div class="mySlides slide-fade" style="display:block">';
                                else
                                    html = html + '<div class="mySlides slide-fade" style="display:none">';

                                    html = html + '<span class="numbertext"><span class="card-number" >' + (i + 1) + "/" + data.length + '</span> Photos</span>';
                                    html = html + '<img src="' + encodeURI(item.file_data) + '" style="width:100%;height:400px;">';

                                
                                html = html + '</div>';
                            })
                            

                            html = html + '<a class="prev" onclick="plusSlides(-1)">&#10094;</a>';
                            html = html + '<a class="next" onclick="plusSlides(1)">&#10095;</a>';
                            html = html + '</div>';
                            html = html + '<br>';

                            html = html + '<div style="text-align:left">';
                            $(data).each(function (i, item) {
                                if (i == 0)
                                    html = html + '<div class="dot active" style="background-position: center;background-size: contain;background-image:url(' + encodeURI(item.file_data) + ')" onclick="currentSlide(' + (i + 1) + ')"></div>';
                                else
                                    html = html + '<div class="dot" style="background-position: center;background-size: contain;background-image:url(' + encodeURI(item.file_data) + ')" onclick="currentSlide(' + (i + 1) + ')"></div>';
                            })
                            html = html + '</div>';

                            $$("pnlPropertyImageSlider").html(html);
                            $$("pnlPropertyImageSlider").removeClass("hide");
                        }
                        
                    else {
                        $$("pnlPropertyImageSlider").addClass("hide");
                    }

            },
            itemAttrList: function (data) {
                $$("grdProductAttributes").width("100%");
                $$("grdProductAttributes").height("auto");
                $$("grdProductAttributes").setTemplate({
                    selection: "None",
                    columns: [
                        {
                            'name': "", 'width': "100%", 'dataField': "attr_no,attr_name,attr_key,attr_value,attr_text",
                            itemTemplate: ' <div style="margin-top:5px">  <span class="col-xs-12 col-lg-4 attr-name" style="color: black;font-weight:bold;"> {attr_name} </span><span class="col-xs-12 col-lg-6 attr-text" style="color: black;white-space: pre-line;font-size:15px">   {attr_text}</span><br /> </div>'
                        }

                    ]
                })
                    

                $$("grdProductAttributes").beforeRowBound = function(row,item){
                    // for (var prop in control.lastFilterSelection) {

                    // }
                }
                //reLoadImg = function (index) {
                //    control.imgData.splice(index, 1);
                //    control.view.loadImages(control.imgData);
                //}
                    
                    $$("grdProductAttributes").dataBind(data);
                    $$("grdProductAttributes").selectedObject.find(".grid_header").addClass("hide");
                    $$("grdProductAttributes").selectedObject.find(".grid_data").addClass("row");
                    $$("grdProductAttributes").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-6");
                    $$("grdProductAttributes").selectedObject.css("border", "none");
                    $$("grdProductAttributes").selectedObject.find(".grid_data").css("overflow", "auto");

            },
            sortExpression:function(data){
                if(typeof(data)!=="undefined"){
                    $$("ddlSort").selectedValue("");
                }
                return $$("ddlSort").selectedValue();
            }
        }

        control.events = {
            init:function(){
                $$("ddlSort").selectionChange(function () {
                    control.events .searchAndFilter(2);
                });
            },
            lnkSortApply_click:function(){
                control.interface.subscribe("propertySearch",null);
                control.events.searchAndFilter(1);
            },
            lnkSortCancel_click :function(){
                control.interface.subscribe("propertySearch",null);
            },
            lnkSort_click : function () {
                control.interface.showSort();
            },
            lnkFilterCancel_click: function(){
                control.interface.subscribe("propertySearch",null);
            },
            lnkFilterApply_click : function () {
                control.interface.subscribe("propertySearch",null);
            },
            btnMessageUs_click: function(){
                control.controls.mdlMessaging.open();
                control.controls.mdlMessaging.title("Messaging");
                control.controls.mdlMessaging.height("300");
                control.controls.mdlMessaging.width("30%");
            },
            btnStartMsg_click: function () {
                if ($$("txtTypeMessage").value().trim() == "") {
                    msgPanel("please type something to send");
                    $$("txtTypeMessage").focus();
                } else {
                    var data = [];
                    data.push({
                        msg_id: "##GUID##",
                        item_no: window.itemList.item_no,
                        message: $$("txtTypeMessage").value(),
                        state_no: 10,
                        org_id: window.shopOnContext.companyProduct.comp_prod_id,
                        user_id: window.shopOnContext.user.user_id,
                        msg_from: window.shopOnContext.user.user_id,
                        msg_to: window.itemList.user_id,
                        date_time: dbCurrentDateTime(),
                    });
                    $.server.webMethodPOST("propon", "propon/item_messages/", data, function (data) {
                        console.log("message sent");
                        $$("txtTypeMessage").value("");
                        control.controls.mdlMessaging.close();
                    })
                }
            },
            btnContactUs_click: function () {

                control.controls.mdlViewContacts.open();
                control.controls.mdlViewContacts.title($$("btnContactUs").text());
                
                
                if ($$("btnContactUs").text()=="View Contact") {
                    control.controls.mdlViewContacts.width("40%");
                    control.controls.mdlViewContacts.height("300");
                    $$("pnlViewContacts").removeClass("hide");
                    $$("pnlContactMe").addClass("hide");
                    $$("lblContactPerson").html("<i class='glyphicon glyphicon-user'></i><span>Contact Person : </span><span>" + window.itemList.name + "</span>");
                    $$("lblContactNo").html("<i class='glyphicon glyphicon-phone'></i><span>Mobile :</span><span>" + window.itemList.mobile + "</span>");
                    $$("lblEmail").html("<i class='glyphicon glyphicon-envelope'></i><span>Email :</span><span>" + window.itemList.email + "</span>");
                    $$("lblAddress").html("<i class='glyphicon glyphicon-map-marker'></i><span>Address :</span><span>" + window.itemList.name + "</span>");
                    var viewData = [];
                    viewData.push({
                        view_id: "##GUID##",
                        item_no: window.itemList.item_no,
                        org_id: window.shopOnContext.companyProduct.comp_prod_id,
                        state_no: 10,
                        user_id: (window.shopOnContext.isLoggedIn) ? window.shopOnContext.user.user_id : undefined,
                        email: (window.shopOnContext.isLoggedIn) ? window.shopOnContext.user.email : undefined,
                        mobile: (window.shopOnContext.isLoggedIn) ? window.shopOnContext.user.phone_no : undefined,
                        
                    });
                    $.server.webMethodPOST("propon", "propon/item_viewers/", viewData, function (data) {
                        console.log("viewers details saved successfully");
                    })
                } else {
                    control.controls.mdlViewContacts.width("30%");
                    control.controls.mdlViewContacts.height("300");
                    $$("pnlViewContacts").addClass("hide");
                    $$("pnlContactMe").removeClass("hide");
                    $$("btnContactMe").text($$("btnContactUs").text());
                }
            },
            btnContactMe_click: function () {
                if ($$("txtYourName").value().trim() == "") {
                    msgPanel("Please enter your name");
                    $$("txtYourName").focus();
                } else if ($$("txtEmail").value().trim() == "") {
                    msgPanel("Please enter your email id");
                    $$("txtEmail").focus();
                } else if ($$("txtMobile").value().trim() == "") {
                    msgPanel("Please enter your mobile number");
                    $$("txtMobile").focus();
                } else if (!isInt($$("txtMobile").value()) || $$("txtMobile").value().length < 10) {
                    $$("txtMobile").focus();
                    msgPanel("Enter valid mobile number...!");
                }
                else {
                    var viewData = [];
                    viewData.push({
                        view_id: "##GUID##",
                        item_no: window.itemList.item_no,
                        org_id: window.shopOnContext.companyProduct.comp_prod_id,
                        state_no: 10,
                        name:$$("txtYourName").value(),
                        user_id: (window.shopOnContext.isLoggedIn) ? window.shopOnContext.user.user_id : undefined,
                        email: $$("txtEmail").value(),
                        mobile: $$("txtMobile").value(),
                    });
                    $.server.webMethodPOST("propon", "propon/item_viewers/", viewData, function (data) {
                        msgPanel("Thank you. We will Reach you shortly...!");
                        $$("txtYourName").value("");
                        $$("txtEmail").value("");
                        $$("txtMobile").value("");
                        control.controls.mdlViewContacts.close();
                    })
                }
            },
            searchAndFilter: function (mode) {
                //var filter = "location eq '" + control.view.searchLocation() + "'&action_type eq '" + control.view.searchActionType() + "'&ptype_no in (" + control.view.searchProdTypes() + ")&price between " + control.view.searchMinPrice() + " and " + control.view.searchMaxPrice();
                var filter = "state='30'";
                if (control.view.searchLocation() != null && control.view.searchLocation() != undefined && control.view.searchLocation() != "") {
                    filter = filter + " and upper(location) like upper('" + control.view.searchLocation() + "')"
                }
                if (control.view.searchActionType() != null && control.view.searchActionType() != undefined && control.view.searchActionType() != "") {
                    filter = filter + " and action_type like '" + control.view.searchActionType() + "'"
                }
                if (control.view.searchProdTypes() != null && control.view.searchProdTypes() != undefined && control.view.searchProdTypes() != "") {
                    filter = filter + " and ptype_no in (" + control.view.searchProdTypes() + ")"
                }
                if (control.view.searchPrice() != null && control.view.searchPrice() != undefined && control.view.searchPrice() != "") {
                    filter = filter + " and price between " + control.view.searchPrice().split(":")[0] + " and " + control.view.searchPrice().split(":")[1];
                }
                if (control.view.searchTextBox() != null && control.view.searchTextBox() != undefined && control.view.searchTextBox() != "") {
                    filter = filter + " and item_name like '%" + control.view.searchTextBox() + "%'";
                }
                var sort = $$("ddlSort").selectedValue() ;
                var attrFilter = "";
                var commonAttrs = "'location'";


                //Mode=1  only search   Mode=2  search and filter
                control.lastFilterSelection = {};
                if (mode == 2) {

                    var sort = control.view.sortExpression();
                    $($$("grdFilters").selectedObject).find("input[type=checkbox]:checked").each(function (i, chk) {
                        if (!control.lastFilterSelection[$(chk).attr("attr_key")])
                        control.lastFilterSelection[$(chk).attr("attr_key")] = [];
                        control.lastFilterSelection[$(chk).attr("attr_key")].push("'" + $(chk).attr("attr_value") + "'");
                    });

                    for (var prop in control.lastFilterSelection) {
                        attrFilter = attrFilter + (attrFilter == "" ? "" : ";") + prop + ":" + control.lastFilterSelection[prop].join(",");
                    }

                }else{
                    control.view.sortExpression("");
                }

                //Search and Load grid
                //control.data.items.allData(filter, sort, function (data) {
                control.data.items.search(filter, attrFilter,sort,function(data){
                    control.view.itemList(data);
                });

                //Load the possible filter attributes
                control.data.items.filterAttributes(filter, attrFilter, commonAttrs,"item_attributes", function (data) {
                    control.view.itemFilterAttributes(data);
                });
            },
            btnSearch_click: function () {
              control.events.searchAndFilter(1);

            },
            lnkFilter_click: function () {
              control.interface.subscribe("filter");

            }
            , btnBacktoSearch_click: function () {
                $$("pnlSearchResult").removeClass("hide");
                $$("pnlItemDetail").addClass("hide");
            }
            ,page_load:function(){
                control.data.attributes.distinctValues("location", "", function (data) {
                    control.view.locationDataSurce(data);
                });
                control.data.productTypes.allData(function (data) {
                    control.view.productTypeDataSource(data);
                });    
                control.view.priceDataSource([
                            { value: "0:100000", label: '1L' },
                            { value: "0:500000", label: '5L' },
                            { value: "0:1000000", label: '10L' },
                            { value: "0:2000000", label: '20L' },
                            { value: "0:5000000", label: '50L' },
                            { value: "0:1000000", label: '1C' }
                        ]);
                control.view.actionDataSource([
                            { value: "BUY", label: 'BUY' },
                            { value: "RENT", label: 'RENT' }
                ]);
    
            }
        }



        control.data = {
            items: {
                search: function (filter, attrFilter, sort, fn) {
                    $.server.webMethodGET("propon", "propon/search/items?filter=" + filter + "&attr_filter=" + attrFilter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                            item.price=parseInt(item.price);
                            item.popularity=parseInt(item.popularity);
                        });
                        if(sort=="")
                            fn( data);
                        else if(sort=="popularity desc")
                            fn( $.util.sortBy(data,["-popularity"]));
                        else if(sort=="price asc")
                            fn( $.util.sortBy(data,["price"]));
                        else if(sort=="price desc")
                            fn( $.util.sortBy(data,["-price"]));
                       else
                       fn( data);
                    });
                },
                searchs: function (filter, attrFilter, sort,start,end, fn) {
                    $.server.webMethodGET("propon", "propon/search/items?start="+start+"&end="+end+"&filter=" + filter + "&attr_filter=" + attrFilter + "&sort=" + sort, function (data) {
                  //$.server.webMethodGET("propon", "propon/search/items?filter=" + filter + "&attr_filter=" + attrFilter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.item_name;
                            item.value = item.item_no;
                        });
                        fn(data);                        
                    })
                },                
                 allData: function (filter,sort, fn) {
                     $.server.webMethodGET("propon", "propon/items?filter=" + filter, function (data) {
                         $(data).each(function (i, item) {
                             item.label = item.attr_text;
                             item.value = item.attr_value;
                         });
                         fn(data);
                     });
                 },
                filterAttributes: function (filter, attrFilter, attrKeys,ref, fn) {
                    $.server.webMethodGET("propon", "propon/search/items/filter-attributes?attr_keys=" + attrKeys + "&filter=" + filter + "&attr_filter=" + attrFilter+"&ref="+ref, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            file: {
                allData: function (filter, fn) {
                    $.server.webMethodGET("propon", "propon/files?filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.file_name;
                            item.value = item.file_no;
                        });
                        fn(data);
                    });
                }
            },
            attributes: {
                distinctValues: function (attrKey, text, fn) {
                    $.server.webMethodGET("propon", "propon/item-attributes?select=distinct attr_value,attr_text&filter=item_attributes_t.attr_key eq '" + attrKey + "' and item_attributes_t.attr_value like '%" + text + "%' ", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                withFilter: function (filter, ref, fn) {
                    if (ref != "") {
                        $.server.webMethodGET("propon", "propon/item-attributes?filter=" + filter + "&ref=" + ref, function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    } else {
                        $.server.webMethodGET("propon", "propon/item-attributes?filter=" + filter, function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    }

                },
            },
            productTypes: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/product_types", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.ptype_name;
                            item.value = item.ptype_no;
                        });
                        fn(data);
                    });
                }
            },
            categories: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/categories?sort=seq_no", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.cat_name;
                            item.value = item.cat_no;
                        });
                        fn(data);
                    });
                }
            }
        }

    });
}

$.fn.schoolPage = function () {
    return $.pageController.getPage(this, function (page, $$, e) {
        var root = document.documentElement;
        root.style.setProperty('--color-primary', window.theme.primaryColor);
        root.style.setProperty('--color-secondary', window.theme.secondaryColor);
        root.style.setProperty('--font-family', window.theme.fontFamily);
        page.context = {
            currControl: [],
            currControlData: []
        }

        page.events.page_load = function () {

           // navigate("splash", null);
            navigate("home", null);

         //  navigate("splash", null);
            // navigate("propertySearch", null);


            //$$("ucHeader").postFreeAd = function () {
            //    $$("ucPostFreeAd").postFreeAd();
            //    $$("ucPostFreeAd").selectedObject.show();
            //}

          
            $$("ucHome").subscribe = function (eventName, data,item) {
                if (eventName == "postfreead") {
                    navigate("postfreead");
                }
                if (eventName == "propertySearch") {
                    navigate("propertySearch", data);
                }
                if (eventName == "home") {
                    navigate("home", null);
                }
                if (eventName == "grdProductList") {
                    //navigate("grdProductList", data, item);
                    $$("ucPropertySearch").selectProduct(data, item);
                    navigate("product-detail");
                }
            }
            $$("ucPropertySearch").subscribe = function (eventName, data) {
                if (eventName == "product-detail") {
                    navigate("product-detail");
                    showOnly("ucPropertySearch,ucHeader");
                    $$("ucPropertySearch").main(data);
                    $$("ucPropertySearch").showDetail();
                    setTimeout(function () {
                        $$("ucHeader").main();
                    }, 200)                    
                }
                if(eventName=="filter"){
                    $$("ucPropertySearch").showFilter();
                }
                if(eventName=="propertySearch"){
                    navigate("propertySearch");
                }
            }
            $$("ucHeader").subscribe = function (eventName, data) {
             
                if (eventName == "login") {
                    navigate("login");
                }
                if (eventName == "myads") {
                    navigate("myads");
                }
                if (eventName == "postfreead") {
                    navigate("postfreead");
                }
                if (eventName == "myaccount") {
                    navigate("myaccount");
                }
                if (eventName == "messaging") {
                    navigate("messaging");
                }
                if (eventName == "propertySearch") {
                    navigate("propertySearch", data);
                }
                if (eventName == "home") {
                    navigate("home", null);
                    //setTimeout(function () {
                    //    $$("ucHeader").main();
                    //}, 200)
                }
            }
            $$("ucPostFreeAd").subscribe = function (eventName) {
                if (eventName == "myads") {
                    navigate("myads");
                }

            }
            $$("ucOrders").editPostFreeAd = function (eventName,item) {
                if (eventName == "editAd") {
                    navigate("editAd", item);
                }
            }

            $$("ucOrders").subscribe = function (eventName, item) {
                if (eventName == "postfreead") {
                    navigate("postfreead");
                }
            }

            $$("ucHeader").showLogin = function (eventName) {
                navigate(eventName)
            }
            $$("ucLogin").subscribe = function (eventName, data) {
                if (eventName == "loginSuccess") {
                    //navigate("dashboard", data);
                    navigate("home", null);
                    setTimeout(function () {
                        $$("ucHeader").main();
                    }, 200)
                    //navigate("splash", null);
                }
            }

            $$("ucCompProdSelection").subscribe = function (eventName, data) {
                if (eventName == "compProdSelected") {
                    navigate("propertySearch", data);
                    setTimeout(function () {
                        $$("ucHeader").main();
                    },200)
                    
                }
            }
        }

        page.events.btnGoBack_click = function () {
            page.context.currControl.pop();
            page.context.currControlData.pop();
            navigate(page.context.currControl[page.context.currControl.length - 1], page.context.currControlData[page.context.currControlData.length - 1]);
        }


        function showOnly(controlName) {
            var controls = ["ucSplash", "ucHeader", "ucLogin", "ucHome", "ucCompProdSelection", "ucPropertySearch", "ucPostFreeAd", "ucOrders", "ucAccount", "ucMessaging"];
            for (var i = 0; i < controls.length; i++) {
                $$(controls[i]).selectedObject.hide();
            }
            for (var i = 0; i < controlName.split(",").length; i++) {
                $$(controlName.split(",")[i]).selectedObject.show();
            }

        }
        function navigate(controlName, data,item) {

            if (controlName != page.context.currControl[page.context.currControl.length - 1]) {
                page.context.currControl.push(controlName);
                page.context.currControlData.push(data);
            }


            if (controlName == "splash") {
                showOnly("ucSplash");
                $$("ucSplash").selectedObject.fadeOut(1500);
                navigate("ucCompProdSelection", null);
            }

            if (controlName == "ucCompProdSelection") {
                showOnly("ucCompProdSelection");
                $$("ucCompProdSelection").main();
                $$("ucHeader").main();
            }

            if (controlName == "login") {
                showOnly("ucLogin,ucHeader");
                $$("ucLogin").main();
            }
            //if (controlName == "postfreead") {
            //    showOnly("ucLogin");
            //    $$("ucLogin").main();
            //}
            if (controlName == "home") {
                showOnly("ucHome,ucHeader");
                $$("ucHome").main();
                $$("ucHeader").main();


            }
            // if (controlName == "grdProductList") {
            //     $$("ucPropertySearch").selectProduct(data, item);
            // }
            if (controlName == "myads") {
                showOnly("ucHeader,ucOrders");
                $$("ucOrders").show();
            }
            if (controlName == "postfreead") {
                showOnly("ucHeader,ucPostFreeAd");
                $$("ucPostFreeAd").main();
            }
            if (controlName == "myaccount") {
                showOnly("ucHeader,ucAccount");
                $$("ucAccount").show();
            }
            if (controlName == "messaging") {
                showOnly("ucHeader,ucMessaging");
                $$("ucMessaging").show();
            }
            if (controlName == "editAd") {
                showOnly("ucHeader,ucPostFreeAd");
                $$("ucPostFreeAd").main(data);
            }
            if (controlName == "propertySearch") {
                showOnly("ucPropertySearch,ucHeader");
                $$("ucPropertySearch").main(data);
                $$("ucPropertySearch").showSearch();
                setTimeout(function () {
                    $$("ucHeader").main();
                }, 200)
            }
            if (controlName == "product-detail") {
                showOnly("ucPropertySearch,ucHeader");
                $$("ucPropertySearch").main(data);
                $$("ucPropertySearch").showDetail();
                setTimeout(function () {
                    $$("ucHeader").main();
                }, 200)
            }
            if (controlName == "dataManager") {
                showOnly("ucdataManager");
                $$("ucdataManager").main(data);
            }

            //if (controlName == "postFreeAd") {
            //    showOnly("ucPostFreeAd");
            //    $$("ucPostFreeAd").main(data);
            //}

        }

    });
}

$.fn.home = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        control.interface.subscribe=null;
        control.interface.main = function () {

            $$("pnlSearchPanelTab").addClass("mobile-hide");
            
            $$("aclLocation").selectionChange(function(){
                $$("aclLocationReAd").selectedValue($$("aclLocation").selectedValue());
            })
            control.data.attributes.distinctValues("location", "", function (data) {
                control.view.locationDataSurce(data);
            });
            control.data.productTypes.allData(function (data) {
                control.view.productTypeDataSource(data);
            });    
            control.view.priceDataSource([
                        { value: "0:100000", label: '1L' },
                        { value: "0:500000", label: '5L' },
                        { value: "0:1000000", label: '10L' },
                        { value: "0:2000000", label: '20L' },
                        { value: "0:5000000", label: '50L' },
                        { value: "0:1000000", label: '1C' }
                    ]);
            control.view.actionDataSource([
                        { value: "BUY", label: 'BUY' },
                        { value: "RENT", label: 'RENT' }
            ]);
            
            control.data.attributes.distinctValues("location", "", function (data) {
                control.view.popularLocation(data);
            },"0","8");
            

            control.data.items.searchs("", "", "upd_date desc","0","12",function(data){
                control.view.recentAds(data);
            });
            
            var featData=[];
            featData=[
                {
                    feat_icon:'<img class="svg" src="/img/features/bigger-results.svg"></img>',
                    feat_name:"Bigger Results",
                    feat_desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida."
                },
                {
                    feat_icon:'<img class="svg" src="/img/features/better-support.svg"></img>',
                    feat_name:"Better Suppport",
                    feat_desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida.",
                },
                {
                    feat_icon:'<img class="svg" src="/img/features/complete-analysis.svg"></img>',
                    feat_name:"Complete Analyis",
                    feat_desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida.",
                },
                {
                    feat_icon:'<img class="svg" src="/img/features/simple-process.svg"></img>',
                    feat_name:"Simple Process",
                    feat_desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida.",
                }]
            control.view.features(featData);

        }

        control.view = {
            popularLocation: function (dataSource) {
                //Design the grid
                $$("grdPopularLocation").width("100%");
                $$("grdPopularLocation").height("auto");
                $$("grdPopularLocation").setTemplate({
                    selection: "Single",
                    columns: [{
                        'name': "comp_prod_name",
                        'width': "100%",
                        'dataField': "attr_text,attr_value,attr_image",
                        itemTemplate: ' <div class="comp_prod_row location-box" id="btnSearchLoc" style="background-position: center;background-color: rgba(0, 0, 0, 0.04);background-image:url({attr_image})" ><div class="overlay"><a><span class="location-name" style=""> {attr_text}</span></a></div></div>'
                    }]
                });
                var lastItemLoc=""
                $$("grdPopularLocation").beforeRowBound = function (row, item) {
                    item.attr_text=item.attr_text.toUpperCase();
                    if(item.attr_text=="CHENNAI")
                        item.attr_image="/img/location/chennai.jpg"
                    else if(item.attr_text=="TIRUCHI")
                        item.attr_image="/img/location/trichy.jpg"
                    else if(item.attr_text=="KANNIYAKUMARI")
                        item.attr_image="/img/location/kaniyakumari.jpg"
                    else if(item.attr_text=="TIRUNELVELI")
                        item.attr_image="/img/location/tirunelveli.jpg"
                    else if(item.attr_text=="MADURAI")
                        item.attr_image="/img/location/madurai.jpg"
                    else if(item.attr_text=="COIMBATORE")
                        item.attr_image="/img/location/coimbatore.jpg"                                                                                                
                    else if(item.attr_text=="GOA")
                        item.attr_image="/img/location/goa.jpg"                        
                    else if(item.attr_text=="TIRUCHENDUR")
                        item.attr_image="/img/location/tiruchendur.jpg"
                    else if(item.attr_text=="KAYALPATNAM")
                        item.attr_image="/img/location/kayalpatnam.jpg"                                                

                }
                $$("grdPopularLocation").rowBound = function (row, item) {
                    $(row).find("#btnSearchLoc").click(function(){
                        if (control.interface.subscribe)
                            control.interface.subscribe("propertySearch", {
                                location: item.attr_value,
                                actionType: "",
                                prodType: "",
                                price: ""
                            });
                    })
                }
                $$("grdPopularLocation").dataBind(dataSource);
                $$("grdPopularLocation").selectedObject.find(".grid_header").addClass("hide");
                $$("grdPopularLocation").selectedObject.find(".grid_data").addClass("row");
                $$("grdPopularLocation").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-4");
                $$("grdPopularLocation").selectedObject.css("border", "none");
                $$("grdPopularLocation").selectedObject.find(".grid_data").css("overflow", "auto");
            },
            features  : function (dataSource) {
                 //Design the grid
                 $$("grdMyFeatures").width("100%");
                 $$("grdMyFeatures").height("auto");
                 $$("grdMyFeatures").setTemplate({
                     selection: "Single",
                     columns: [{
                         'name': "comp_prod_name",
                         'width': "100%",
                         'dataField': "feat_name,feat_icon,feat_desc",
                         //itemTemplate: ' <div class="comp_prod_row" id="btnSearchFeatAd"><a><span  style="float:left">  {feat_name}</span> <span style="float:right">  {feat_name}</span></a></div>'
                         itemTemplate:
                         `
                            <div class="features-box">
                                <div class="icon">
                                    <i class="flaticon-pin">{feat_icon}</i>
                                </div>
                                <div class="detail">
                                    <h5>{feat_name}</h5>
                                    <p>{feat_desc}</p>
                                </div>
                            </div>
                         `
                     }]
                 });
                 $$("grdMyFeatures").rowBound = function (row, item) {
                     $(row).find("#btnSearchFeatAd").click(function () {
                         if (control.interface.subscribe)
                             control.interface.subscribe("propertySearch", {
                                 location: "",
                                 actionType: "",
                                 prodType: "",
                                 price: "",
                                 feat_name: item.feat_name
                             });
                     })
                 }
                 $$("grdMyFeatures").dataBind(dataSource);
                 $$("grdMyFeatures").selectedObject.find(".grid_header").addClass("hide");
                 $$("grdMyFeatures").selectedObject.find(".grid_data").addClass("row");
                 $$("grdMyFeatures").selectedObject.find(".grid_row").addClass("col-lg-6 col-md-6 col-sm-12 col-xs-12");
                 $$("grdMyFeatures").selectedObject.css("border", "none");
                 $$("grdMyFeatures").selectedObject.find(".odd-row").css("background","transparent !important");
                 $$("grdMyFeatures").selectedObject.find(".even-row").css("background","transparent !important");
                 $$("grdMyFeatures").selectedObject.find(".grid_data").css("overflow", "auto");
            },
            recentAds: function (dataSource) {
                //Design the grid
                $$("grdRecentAds").width("100%");
                $$("grdRecentAds").height("auto");
                $$("grdRecentAds").setTemplate({
                    selection: "Single", paging: true, pageSize: 20,
                    columns: [{
                        //'name': "comp_prod_name",
                        //'width': "100%",
                        //'dataField': "item_no,item_name,location",
                        //itemTemplate: ' <div class="comp_prod_row" id="btnSearchRecentAd"><a><span  style="float:left">  {item_name}</span> <span style="float:right">  {location}</span></a></div>'

                        // 'name': "", 'width': "100%", 'dataField': "item_no,item_name,price,location,project,addr1,addr2,addr3,image1",
                        // itemTemplate: '<div class="product-list"><div class="fav"><button class="btn-fav" id="idAddtoFav"><i id="idFavHeart" class="glyphicon glyphicon-heart" style="float: right;"></i></button></div><div id="idProduct"><div class="product-img"><img alt="Property image" id="idPropDefImg" src="{image1}" width="200" height="200" /></div><div class=product-desc><span class="item-text" >{item_name}</span></div><div class="item-desc" style="margin-bottom: 3px;" ><span class="project"> Project : {project} </span><br><span class="location"> Location : {location} </span><br><span class="address"> {addr1} </span><span class="address"> {addr2} </span><span class="address"> {addr3} </span><br><span class="selling_price"> Price : ' + window.settings.COUNTRY_CURRENCY_SYMBOL + ' {price}  </span></div></div> </div></div>'//<div class="product-button"><button class="btn-grid" id="idBuyNow">Buy Now</button></div>
                        'name': "", 'width': "100%", 'dataField': "item_no,item_name,price,location,project,addr1,addr2,addr3,image1",
                        itemTemplate: 
                        `<div class="property-box" id="idProduct">
                            <div class="property-thumbnail">
                                <a  class="property-img">
                                    <div class="tag">Recend Ad</div>
                                    <div class="price-box"><span>Price : ` + window.settings.COUNTRY_CURRENCY_SYMBOL + ` {price}</span></div>
                                    <img class="d-block w-100" id="idPropDefImg" src={image1} alt="properties">
                                </a>
                            </div>
                            <div class="detail">
                                <h1 class="title">
                                    <a href="properties-grid-rightside.html">{item_name}</a>
                                </h1>
                                <div class="location">
                                    <a >
                                        <i class="flaticon-pin"></i>{location}
                                    </a>
                                </div>
                            </div>                            
                        </div>`
                    }]

                });
                //$$("grdRecentAds").rowBound = function (row, item) {
                //    $(row).find("#btnSearchRecentAd").click(function () {
                //        if (control.interface.subscribe)
                //            control.interface.subscribe("propertySearch", {
                //                location: "",
                //                actionType: "",
                //                prodType: "",
                //                price: "",
                //                item_no: item.item_no
                //            });
                //    })
                //}
                $$("grdRecentAds").beforeRowBound = function (row, item) {
                    var filter = "item_no='" + item.item_no + "' and attr_key like '%image%'";
                    data=item.attributes;
                    // control.data.attributes.withFilter(encodeURIComponent(filter), function (data) {
                        if (data !="" && data !=undefined) {
                            var img = "";
                            
                            for ( var property in data ) {
                                if(property.startsWith("image")){
                                    img= data[property];
                                } 
                              }
                              if(img!=""){
                                var filter = " file_no='" + img + "'";
                                control.data.file.allData(filter, function (data) {
                                if (data.length > 0) {
                                    var file = data[0];
                                    if (file.file_data == undefined || file.file_data == "" || file.file_data == null)
                                        item.image1 = "img\\alter-img.png";
                                    else
                                        item.image1 = file.file_data;
                                } else {
                                    item.image1 = "img\\alter-img.png";
                                }
                                $(row).find("#idPropDefImg").attr("src", item.image1);
                            })
                            }else{
                                item.image1 = "img\\alter-img.png";
                                $(row).find("#idPropDefImg").attr("src", item.image1);
                            }                            
                        } else {
                            item.image1 = "img\\alter-img.png";
                            $(row).find("#idPropDefImg").attr("src", item.image1);
                        }

                    // })
                }
                $$("grdRecentAds").rowBound = function (row, item) {
                    $(row).find('#idProduct').click(function () {
                        control.interface.subscribe("grdProductList", row, item);
                    })
                    
                    //$$("grdProductList").rowBound(row, item);
                    
                }
                $$("grdRecentAds").dataBind(dataSource);
                $$("grdRecentAds").selectedObject.find(".grid_header").addClass("hide");
                $$("grdRecentAds").selectedObject.find(".grid_data").addClass("row");
                $$("grdRecentAds").selectedObject.find(".grid_row").addClass("col-lg-3 col-md-6 col-xs-12");
                $$("grdRecentAds").selectedObject.find(".grid_row").css("background", "whitesmoke");
                $$("grdRecentAds").selectedObject.find(".grid_row").css("margin-top", "10px");
                $$("grdRecentAds").selectedObject.find(".grid_row").css("margin-bottom", "10px");
                $$("grdRecentAds").selectedObject.find(".grid_data").css("background", "whitesmoke");
                $$("grdRecentAds").selectedObject.css("border", "none");
                $$("grdRecentAds").selectedObject.find(".grid_data").css("overflow", "auto");
                //$$("grdRecentAds").selectedObject.find(".grid_header").addClass("hide");
                //$$("grdRecentAds").selectedObject.find(".grid_data").addClass("row");
                //$$("grdRecentAds").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-3");
                //$$("grdRecentAds").selectedObject.css("border", "none");
                //$$("grdRecentAds").selectedObject.find(".grid_data").css("overflow", "auto");
            },

            locationDataSurce: function (dataSource) {
                $$("aclLocation").dataBind(dataSource, "attr_value", "attr_text");
                $$("aclLocationReAd").dataBind(dataSource, "attr_value", "attr_text");
            },
            productTypeDataSource: function (dataSource) {
             
                $$("aclPropertyType").dataBind(dataSource, "ptype_no", "ptype_name");
            },
            priceDataSource: function (dataSource) {
             
                $$("aclPrice").dataBind(dataSource,"value","label");
            },
            actionDataSource: function (dataSource) {
            
                $$("aclActionType").dataBind(dataSource,"value","label");
            },
            searchLocation: function (data) {
                if(typeof(data)!=="undefined")
                   $$("aclLocation").selectedValue(data);              
                return $$("aclLocation").selectedValue();          
            },
            searchActionType: function (data) {
                if(typeof(data)!=="undefined")
                    $$("aclActionType").selectedValue(data);      
                return  $$("aclActionType").selectedValue(); 
            },
            searchProdTypes: function (data) { 
                
                if(typeof(data)!=="undefined")
                    $$("aclPropertyType").selectedValues(data);      
                return  $$("aclPropertyType").selectedValues().join(",");

            },
            searchTextBox: function (data) {
                if (typeof (data) !== "undefined")
                    $$("txtSearchBox").val(data);
                else
                    return $$("txtSearchBox").val();
            },
            searchPrice: function (data) {
                if(typeof(data)!=="undefined")
                $$("aclPrice").selectedValue(data);      
            return  $$("aclPrice").selectedValue(); 

            
            }

            
        }
        
        control.events = {
            btnSearch_click: function () {
                if( control.interface.subscribe)
                    control.interface.subscribe("propertySearch",{
                       location: control.view.searchLocation(),
                       actionType:control.view.searchActionType(),

                       freeText:control.view.searchTextBox(),
                       prodType:control.view.searchProdTypes(),
                       price:control.view.searchPrice()
                    });
            },
            btnBuy_click : function () {
            control.interface.subscribe("propertySearch", {
                location: "",
                actionType: "BUY",
                prodType: "",
                price: ""
            });
        },
        btnSell_click : function () {
            if (window.shopOnContext.isLoggedIn) {
                control.interface.subscribe("postfreead");
                window.isEdit = false;
            }
            else {
                control.interface.subscribe("login");
            }
        },
        btnFreeAd_click : function () {
            if (window.shopOnContext.isLoggedIn) {
                control.interface.subscribe("postfreead");
                window.isEdit = false;
            }
            else {
                control.interface.subscribe("login");
            }
        },
        btnRent_click : function () {
            control.interface.subscribe("propertySearch", {
                location: "",
                actionType: "RENT",
                prodType: "",
                price: ""
            });

        },
        page_load : function () {
            $$("aclLocationReAd").selectionChange(function(){
                var filter = " upper(location) like upper('" + $$("aclLocationReAd").selectedValue() + "')"
                control.data.items.searchs(filter, "", "upd_date desc","0","12",function(data){
                    control.view.recentAds(data);
                });                
            })
        }
        }


        control.data = {
            items: {
             
                search: function (filter, attrFilter, sort, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter + "&attr_filter=" + attrFilter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                searchs: function (filter, attrFilter, sort,start,end, fn) {
                    $.server.webMethodGET("propon", "propon/search/items?start="+start+"&end="+end+"&filter=" + filter + "&attr_filter=" + attrFilter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);                        
                    })
                },
                withFilter: function (filter, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                filterAttributes: function (filter, attrFilter, attrKeys, fn) {
                    $.server.webMethodGET("propon", "propon/search/items/filter-attributes?attr_keys=" + attrKeys + "&filter=" + filter + "&attr_filter=" + attrFilter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            attributes: {
                distinctValues: function (attrKey, text, fn,start,end) {
                    if(start!=""&& start!=undefined && end!="" && end!=undefined){
                        $.server.webMethodGET("propon", "propon/item-attributes?select=distinct attr_value,attr_text&start="+start+"&end="+end+"&filter=item_attributes_t.attr_key eq '" + attrKey + "' and item_attributes_t.attr_value like '%" + text + "%' ", function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    }else{
                        $.server.webMethodGET("propon", "propon/item-attributes?select=distinct attr_value,attr_text&filter=item_attributes_t.attr_key eq '" + attrKey + "' and item_attributes_t.attr_value like '%" + text + "%' ", function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    }
                },
                withFilter: function (filter, fn) {
                    $.server.webMethodGET("propon", "propon/item-attributes?filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
          
            productTypes: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/product_types", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.ptype_name;
                            item.value = item.ptype_no;
                        });
                        fn(data);
                    });
                }
            },
            categories: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/categories?sort=seq_no", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.cat_name;
                            item.value = item.cat_no;
                        });
                        fn(data);
                    });
                }
            },
            file: {
            allData: function (filter,fn) {
                $.server.webMethodGET("propon", "propon/files?filter="+filter, function (data) {
                    $(data).each(function (i, item) {
                        item.label = item.file_name;
                        item.value = item.file_no;
                    });
                    fn(data);
                });
            }
        },
        }
    });
}
$.fn.postFreeAd = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        window.isEdit = false;
        control.interface.main = function (item) {
            control.imgData = [];
            $$("fuTest").fileAdded = function (data) {
                control.imgData.push({
                    file_no: data[0].file_no,
                    file_data: data[0].file_data,
                })
                control.view.loadImages(control.imgData);
            }

            // control.view.loadWhoIam = function(callback){
            //     callback(([
            //     { value: "Owner", label: 'Owner' },
            //     { value: "Builder", label: 'Builder' },
            //     { value: "Agent", label: 'Agent' }
            // ]));
            // }
            
            // control.view.loadWhoIam(function(dataSource) {
            //     $$("ddlfPropWho").dataBind(dataSource,"value","label");
            // })


            
            control.data.mainProductTypes.allData(function (data) {
                $$("ddlMainPropType").dataBind(data, "mpt_no", "mpt_name","Select");
            });

            control.data.productTypes.allData(function (data) {
                $$("ddlPropType").dataBind(data, "ptype_no", "ptype_name", "Select");
                control.interface.loadAsPerType("prop", "", "");
                if (item != undefined) {
                    control.view.loadBuilder(function(){
                        control.view.loadProject(function(){
                            control.interface.preLoadData(item);
                            window.isEdit = true;
                            window.item_no = item.item_no;
                        })
                    })

                } else {
                    control.view.loadBuilder(function(){
                        control.view.loadProject(function(){
                            control.interface.clearAllData();
                            window.isEdit = false;
                            window.item_no = undefined;
                        })
                    })
                }
            });

        }
        previewImage = function (element,i) {
            document.getElementById("idUpImage").setAttribute("src", element.getAttribute("src"));
            $(control.imgData).each(function (j, item) {
                $("#idImageDiv" + j).removeClass("active-img-div");
            })
            $("#idImageDiv" +i).addClass("active-img-div");
        }
        reLoadImg = function (index) {
            control.imgData.splice(index, 1);
            control.view.loadImages(control.imgData);
        }
        control.view.loadImages = function (data) {
            var html = "";
            if (data.length > 0) {
            html = html + '<div class="col-lg-12">';
            html = html + '<div class="col-lg-12" style="background: whitesmoke;border: solid 1px #d8d2d2;" >';
            html = html + '<img class="prd-img-upload-big" id="idUpImage" src="' + encodeURI(data[data.length - 1].file_data) + '"/>';
            html = html + '</div>';
            
                $(data).each(function (i, item) {
                    html = html + '<div class="col-lg-3 prd-img-upload-div" id="idImageDiv' + i + '" >';
                    html = html + '<div class="product-list">';
                    html = html + '<span style="display:none">' + item.file_no + ' </span>';
                    html = html + '<span style="float:right"><i class="fa fa-remove" onClick="reLoadImg(' + i + ');" ></i> </span>';
                    html = html + '<img class="prd-img-upload" id="idImage' + i + '" src="' + encodeURI(item.file_data) + '" onClick="previewImage(this,'+i+');" />';
                    html = html + '</div>';
                    html = html + '</div>';

                })
                html = html + '</div>';
            }
            $$("grdFilesUploaded").html(html);

            ///Load immg tag
        }
        control.interface.preLoadData = function (item) {
            if (item.owner_type != undefined && item.owner_type != "")
                $$("ddlPropWho").selectedObject.val(item.owner_type);

            if (item.name != undefined && item.name != "")
                $$("txtYourName").value(item.name);

            if (item.mobile != undefined && item.mobile != "")
                $$("txtYourMobile").value(item.mobile);

            if (item.email != undefined && item.email != "")
                $$("txtYourEmail").value(item.email);

            if (item.action_type != undefined && item.action_type != "") {
                if (item.action_type == "Buy") {
                    $$("ddlPropWhatisFor").selectedObject.val("Sell");
                } else if (item.action_type == "Rent") {
                    $$("ddlPropWhatisFor").selectedObject.val("Rent");
                } else {
                    $$("ddlPropWhatisFor").selectedObject.val("Select");
                }
            }
            if (item.project_code != undefined && item.project_code != "") {
                $$("ddlProjectName").selectedObject.val(item.project_code);
                $$("pnlPropertyProjName").removeClass("hide");
            }
            if (item.builder_code != undefined && item.builder_code != "") {
                $$("ddlBuilderName").selectedObject.val(item.builder_code);
                $$("pnlBuilderName").removeClass("hide");
            }            
            if (item.item_name != undefined && item.item_name != "")
                $$("txtYourPropName").value(item.item_name);

            if (item.location != undefined && item.location != "")
                $$("txtYourPropLoc").value(item.location);

            if (item.project != undefined && item.project != "")
                $$("txtProjName").value(item.project);

            if (item.ptype_no != undefined && item.ptype_no != "") {
                $$("ddlPropType").selectedValue(item.ptype_no);
                $$("ddlMainPropType").selectedValue($$("ddlPropType").selectedData(0).mpt_no);
                setTimeout(function () {
                    $$("ddlPropType").selectedValue(item.ptype_no);
                }, 1500)
            }
            if (item.price_plan_id != undefined && item.price_plan_id != "") {
                $$("ddlPricePlan").selectedObject.val(item.price_plan_id);
            }
            $$("pnlPropPricePlan").removeClass("hide");
            if(window.isEdit){
                $$("ddlPricePlan").disable(true);
            }
            var filter = "item_no='" + item.item_no + "'"
            control.data.attributes.withFilter(encodeURIComponent(filter), function (data) {
                control.interface.preLoadAttrData(data);
                control.view.preLoadImages(data);
            })
            if (item.contact_flag == "1") {
                $("#chkContactbyOwn").prop('checked', true);
            } else if (item.contact_flag == "2") {
                $("#chkContactView").prop('checked', true);
            } else if (item.contact_flag == "3") {
                $("#chkContactOnlybyme").prop('checked', true);
            } else if (item.contact_flag == "4") {
                $("#chkContactbyOwnMsg").prop('checked', true);
            } else if (item.contact_flag == "5") {
                $("#chkContactOnlybymeMsg").prop('checked', true);
            }
            
        }
        control.view.preLoadImages = function (attrs) {
            control.imgData = [];
            $(attrs).each(function (i, item) {
                if (item.attr_key.startsWith("image")) {
                    var filter = " file_no='" + item.attr_value + "'";
                    control.data.file.allData(filter, function (data) {
                        if (data.length > 0) {
                            control.imgData.push({
                                item_attr_no:item.item_attr_no,
                                file_no: data[0].file_no,
                                file_data: data[0].file_data,
                            })
                        }
                        control.view.loadImages(control.imgData);
                    })

                }
            })
            
        }
        control.interface.preLoadAttrData = function (attrs) {
            $(attrs).each(function (i, item) {
                $$("pnlPropertyDetails").removeClass("hide");
                if (item.attr_key == "bed_room") {
                    $$("ddlPropTypeBedRooms").selectedObject.val(item.attr_value);
                    $$("pnlBedRooms").removeClass("hide");
                    $$("lblPropBedRoomsId").value(item.item_attr_no);
                }
                else if (item.attr_key == "location") {
                    $$("txtYourPropLoc").value(item.attr_value);
                    $$("pnlPropertyLoc").removeClass("hide");
                    $$("lblPropLocId").value(item.item_attr_no);
                }
                else if (item.attr_key == "avail_from") {
                    $$("dsPropAvailFrom").setDate(item.attr_value);
                    $$("pnlPropAvailFrom").removeClass("hide");
                    $$("lblAvailFromId").value(item.item_attr_no);
                }
                else if (item.attr_key == "action_type") {
                    $$("ddlPropWhatisFor").selectedObject.val(item.attr_value);
                    $$("pnlPropWhatisFor").removeClass("hide");
                    $$("lblPropActionId").value(item.item_attr_no);
                }
                else if (item.attr_key == "area") {
                    $$("txtAreaSize").value(item.attr_value);
                    $$("pnlPropArea").removeClass("hide");
                    $$("lblPropAreaId").value(item.item_attr_no);
                }
                else if (item.attr_key == "area_unit") {
                    $$("ddlPropTypeAreaSizeUnit").selectedObject.val(item.attr_value);
                    $$("pnlPropArea").removeClass("hide");
                    $$("lblAreaUnitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "available_for") {
                    $$("ddlPropAvailFor").selectedObject.val(item.attr_value);
                    $$("pnlPropAvailFor").removeClass("hide");
                    $$("lblAvailForId").value(item.item_attr_no);
                }
                else if (item.attr_key == "bath_rooms") {
                    $$("ddlPropTypeBathRooms").selectedObject.val(item.attr_value);
                    $$("pnlBathRooms").removeClass("hide");
                    $$("lblBathroomsId").value(item.item_attr_no);
                }
                else if (item.attr_key == "construction_status") {
                    $$("ddlPropContStatus").selectedObject.val(item.attr_value);
                    $$("pnlPropConstruStatus").removeClass("hide");
                    $$("lblConstStatusId").value(item.item_attr_no);
                }
                else if (item.attr_key == "corner_property") {
                    $$("ddlPropCorner").selectedObject.val(item.attr_value);
                    $$("pnlPropCornerProp").removeClass("hide");
                    $$("lblCornerPropId").value(item.item_attr_no);
                }
                else if (item.attr_key == "expected_price") {
                    $$("txtExpectedPrice").value(item.attr_value);
                    $$("pnlPropExpectedPrice").removeClass("hide");
                    $$("lblExpectedPriceId").value(item.item_attr_no);
                }
                else if (item.attr_key == "expected_rent") {
                    $$("txtExpectedRent").value(item.attr_value);
                    $$("pnlPropExpectedRent").removeClass("hide");
                    $$("lblExpectedRentId").value(item.item_attr_no);
                }
                else if (item.attr_key == "floors_allowed") {
                    $$("txtTotalFloorsAllowed").value(item.attr_value);
                    $$("pnlPropFloorsAllowed").removeClass("hide");
                    $$("lblFloorsAllowId").value(item.item_attr_no);
                }
                else if (item.attr_key == "floor_number") {
                    $$("txtFloorNumber").value(item.attr_value);
                    $$("pnlPropFloorNumber").removeClass("hide");
                    $$("lblFloorNoId").value(item.item_attr_no);
                }
                else if (item.attr_key == "furnished_status") {
                    $$("ddlPropFurnStatus").selectedObject.val(item.attr_value);
                    $$("pnlPropFurnStatus").removeClass("hide");
                    $$("lblFurnStatusId").value(item.item_attr_no);
                }
                else if (item.attr_key == "occupancy") {
                    $$("ddlPropOccupancy").selectedObject.val(item.attr_value);
                    $$("pnlPropOccupancy").removeClass("hide");
                    $$("lblOccupancyId").value(item.item_attr_no);
                }
                else if (item.attr_key == "open_side") {
                    $$("ddlPropOpenSides").selectedObject.val(item.attr_value);
                    $$("pnlPropOpenSides").removeClass("hide");
                    $$("lbOpenSidesId").value(item.item_attr_no);
                }
                else if (item.attr_key == "total_floors") {
                    $$("txtTotalFloors").value(item.attr_value);
                    $$("pnlPropTotalFloors").removeClass("hide");
                    $$("lblTotFloorsId").value(item.item_attr_no);
                }
                else if (item.attr_key == "transaction_type") {
                    $$("ddlPropTransType").selectedObject.val(item.attr_value);
                    $$("pnlPropTransType").removeClass("hide");
                    $$("lblTransTypeId").value(item.item_attr_no);
                }
                else if (item.attr_key == "no_of_rooms_pg") {
                    $$("ddlPropNoofRoomsPg").selectedObject.val(item.attr_value);
                    $$("pnlNoofRoomsPg").removeClass("hide");
                    $$("lblNoofRoomsPgId").value(item.item_attr_no);
                }
                else if (item.attr_key == "nearby_businesses") {
                    $$("txtNearbyBusi").value(item.attr_value);
                    $$("pnlPropNearbyBusi").removeClass("hide");
                    $$("lblNearbyBusiId").value(item.item_attr_no);
                }
                else if (item.attr_key == "monthly_rent") {
                    $$("txtMonthlyRent").value(item.attr_value);
                    $$("pnlPropMonthlyRent").removeClass("hide");
                    $$("lblMonthlyRentId").value(item.item_attr_no);
                }
                else if (item.attr_key == "lock_in_periods") {
                    $$("txtLockinPeriods").value(item.attr_value);
                    $$("pnlPropLockinPeriods").removeClass("hide");
                    $$("lblLockinPeriodsId").value(item.item_attr_no);
                }
                else if (item.attr_key == "land_zone") {
                    $$("ddlPropLandZone").selectedObject.val(item.attr_value);
                    $$("pnlPropLandZone").removeClass("hide");
                    $$("lblLandZoneId").value(item.item_attr_no);
                }
                else if (item.attr_key == "is_main_road_facing") {
                    $$("ddlPropIsMainRoadFac").selectedObject.val(item.attr_value);
                    $$("pnlPropIsMainRoadFac").removeClass("hide");
                    $$("lblIsMainRoadFacId").value(item.item_attr_no);
                }
                else if (item.attr_key == "ideal_for_businesses") {
                    $$("txtIdealBusi").value(item.attr_value);
                    $$("pnlPropIdealBusi").removeClass("hide");
                    $$("lblIdealBusiId").value(item.item_attr_no);
                }
                else if (item.attr_key == "getting_response_from_brokers") {
                    if (item.attr_value == "1") {
                        $("#chkGettingBrokResp").prop('checked', true);
                    } else {
                        $("#chkGettingBrokResp").prop('checked', false);
                    }
                    $$("pnlGettingBrokResp").removeClass("hide");
                    $$("lblchkGettingBrokRespId").value(item.item_attr_no);
                }
                else if (item.attr_key == "furnishing_details") {
                    var attrs = [];
                    attrs = item.attr_value.split(",");
                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Cupboards"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkCupboards").prop('checked', true);
                    } else {
                        $("#chkGettingBrokResp").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Washing Machine"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkWashingMc").prop('checked', true);
                    } else {
                        $("#chkWashingMc").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Study Table"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkStudyTable").prop('checked', true);
                    } else {
                        $("#chkStudyTable").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "AC"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkAC").prop('checked', true);
                    } else {
                        $("#chkAC").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Geyser"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkGeyser").prop('checked', true);
                    } else {
                        $("#chkGeyser").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Wifi"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkWifi").prop('checked', true);
                    } else {
                        $("#chkWifi").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Fridge"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkFridge").prop('checked', true);
                    } else {
                        $("#chkFridge").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "Cooler"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkCooler").prop('checked', true);
                    } else {
                        $("#chkCooler").prop('checked', false);
                    }

                    var isAttrExsist = attrs.filter(function (find) {
                        return find == "TV"
                    })
                    if (isAttrExsist.length > 0) {
                        $("#chkTV").prop('checked', true);
                    } else {
                        $("#chkTV").prop('checked', false);
                    }

                    $$("pnlFurnishingDetails").removeClass("hide");
                    $$("lblchkCupboardsId").value(item.item_attr_no);
                }

                else if (item.attr_key == "electricity_water_charges_included") {
                    if (item.attr_value == "1") {
                        $("#chkElectricityWater").prop('checked', true);
                    } else {
                        $("#chkElectricityWater").prop('checked', false);
                    }
                    $$("pnlPropElectricityWater").removeClass("hide");
                    $$("lblElectricityWaterId").value(item.item_attr_no);
                }
                else if (item.attr_key == "security_amount") {
                    $$("txtSecurityAmt").value(item.attr_value);
                    $$("pnlPropSecurityAmt").removeClass("hide");
                    $$("lblSecurityAmtId").value(item.item_attr_no);
                }
                else if (item.attr_key == "laundry_amount") {
                    $$("txtLaundry").value(item.attr_value);
                    $$("pnlPropLaundry").removeClass("hide");
                    $$("lblLaundryId").value(item.item_attr_no);
                }
                else if (item.attr_key == "other_charges") {
                    $$("txtOtherChar").value(item.attr_value);
                    $$("pnlPropOtherChar").removeClass("hide");
                    $$("lblOtherCharId").value(item.item_attr_no);
                }
                else if (item.attr_key == "electricity_amount") {
                    $$("txtElectricityAmt").value(item.attr_value);
                    $$("pnlPropElectricityAmt").removeClass("hide");
                    $$("lblElectricityAmtId").value(item.item_attr_no);
                }
                else if (item.attr_key == "currently_rent_out") {
                    $$("ddlPropRentout").selectedObject.val(item.attr_value);
                    $$("pnlPropRentout").removeClass("hide");
                    $$("lblRentoutId").value(item.item_attr_no);
                }
                else if (item.attr_key == "currently_leased_out") {
                    $$("ddlPropLeasedout").selectedObject.val(item.attr_value);
                    $$("pnlPropLeasedout").removeClass("hide");
                    $$("lblLeasedoutId").value(item.item_attr_no);
                }
                else if (item.attr_key == "corner_showroom") {
                    $$("ddlPropCornerShow").selectedObject.val(item.attr_value);
                    $$("pnlPropCornerShow").removeClass("hide");
                    $$("lblCornerShowId").value(item.item_attr_no);
                }
                else if (item.attr_key == "corner_shop") {
                    $$("ddlPropCornerProp").selectedObject.val(item.attr_value);
                    $$("pnlPropCornerShop").removeClass("hide");
                    $$("lblCornerShopId").value(item.item_attr_no);
                }
                else if (item.attr_key == "corner_plot") {
                    if (item.attr_value == "1") {
                        $("#chkCornerPlot").prop('checked', true);
                    } else {
                        $("#chkCornerPlot").prop('checked', false);
                    }
                    $$("pnlPropCornerPlot").removeClass("hide");
                    $$("lblCornerPlotId").value(item.item_attr_no);
                }
                else if (item.attr_key == "common_area") {
                    $$("ddlPropCommonArea").selectedObject.val(item.attr_value);
                    $$("pnlPropCommonArea").removeClass("hide");
                    $$("lblCommonAreaId").value(item.item_attr_no);
                }
                else if (item.attr_key == "plot_area") {
                    $$("txtPlotArea").value(item.attr_value);
                    $$("pnlPropPlotArea").removeClass("hide");
                    $$("lblPlotAreaId").value(item.item_attr_no);

                } else if (item.attr_key == "plot_area_unit") {
                    $$("ddlPropPlotArea").selectedObject.val(item.attr_value);
                    $$("pnlPropPlotArea").removeClass("hide");
                    $$("lblPlotAreaUnitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "covered_area") {
                    $$("txtCoveredArea").value(item.attr_value);
                    $$("pnlPropCoveredArea").removeClass("hide");
                    $$("lblCoveredAreaId").value(item.item_attr_no);

                } else if (item.attr_key == "covered_area_unit") {
                    $$("ddlPropCoveredArea").selectedObject.val(item.attr_value);
                    $$("pnlPropCoveredArea").removeClass("hide");
                    $$("lblCoveredAreaUnitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "carpet_area") {
                    $$("txtCarpetArea").value(item.attr_value);
                    $$("pnlPropCarpet").removeClass("hide");
                    $$("lblCarpetAreaId").value(item.item_attr_no);

                }
                else if (item.attr_key == "carpet_area_unit") {
                    $$("ddlPropCarpetArea").selectedObject.val(item.attr_value);
                    $$("pnlPropCarpet").removeClass("hide");
                    $$("lblCarpetAreaUnitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "cafeteria") {
                    $$("ddlPropCafeteria").selectedObject.val(item.attr_value);
                    $$("pnlCafeteria").removeClass("hide");
                    $$("lblCafeteriaId").value(item.item_attr_no);
                }
                else if (item.attr_key == "cabin_meeting_rooms") {
                    $$("ddlPropCabins").selectedObject.val(item.attr_value);
                    $$("pnlCabins").removeClass("hide");
                    $$("lblCabinsId").value(item.item_attr_no);
                }
                else if (item.attr_key == "super_built_up_area") {
                    $$("txtSuperBuiltUpArea").value(item.attr_value);
                    $$("pnlPropSuperBuiltUpArea").removeClass("hide");
                    $$("lblSuperBuiltUpAreaId").value(item.item_attr_no);

                }
                else if (item.attr_key == "super_built_up_area_unit") {
                    $$("ddlPropSuperBuiltUpArea").selectedObject.val(item.attr_value);
                    $$("pnlPropSuperBuiltUpArea").removeClass("hide");
                    $$("lblSuperBuiltUpAreaUnitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "built_up_area") {
                    $$("txtBuiltUpArea").value(item.attr_value);
                    $$("pnlPropBuiltUpArea").removeClass("hide");
                    $$("lblBuiltUpAreaId").value(item.item_attr_no);

                }
                else if (item.attr_key == "built_up_area_unit") {
                    $$("ddlPropBuiltUpArea").selectedObject.val(item.attr_value);
                    $$("pnlPropBuiltUpArea").removeClass("hide");
                    $$("lblBuiltUpAreaUnitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "maintenance_charge") {
                    $$("txtMainCharge").value(item.attr_value);
                    $$("pnlMainCharge").removeClass("hide");
                    $$("lblMainChargeId").value(item.item_attr_no);

                }
                else if (item.attr_key == "maintenance_charge_limit") {
                    $$("ddlMainCharge").selectedObject.val(item.attr_value);
                    $$("pnlMainCharge").removeClass("hide");
                    $$("lblMainChargeLimitId").value(item.item_attr_no);
                }
                else if (item.attr_key == "booking_amount") {
                    $$("txtBookingAmt").value(item.attr_value);
                    $$("pnlPropBookingAmt").removeClass("hide");
                    $$("lblBookingAmtId").value(item.item_attr_no);

                }
                else if (item.attr_key == "balconies") {
                    $$("ddlPropBalconies").selectedObject.val(item.attr_value);
                    $$("pnlBalconies").removeClass("hide");
                    $$("lblBalconiesId").value(item.item_attr_no);
                }
                else if (item.attr_key == "attached_bath_room") {
                    $$("ddlPropAttachedBath").selectedObject.val(item.attr_value);
                    $$("pnlPropAttachedBath").removeClass("hide");
                    $$("lblAttachedBathroomId").value(item.item_attr_no);
                }
                else if (item.attr_key == "attached_balcony") {
                    $$("ddlPropAttachedBalco").selectedObject.val(item.attr_value);
                    $$("pnlPropAttachedBalco").removeClass("hide");
                    $$("lblAttachedBalcoId").value(item.item_attr_no);
                }
                else if (item.attr_key == "assured_returns") {
                    $$("ddlPropAssuredReturn").selectedObject.val(item.attr_value);
                    $$("pnlPropAssuredReturn").removeClass("hide");
                    $$("lblAssuredReturId").value(item.item_attr_no);
                }
                else if (item.attr_key == "any_constructions_done") {
                    $$("ddlPropAnyConsDone").selectedObject.val(item.attr_value);
                    $$("pnlPropAnyContrDone").removeClass("hide");
                    $$("lblAnyConstDoneId").value(item.item_attr_no);
                }
                else if (item.attr_key == "age_of_pg") {
                    $$("ddlPropAgePG").selectedObject.val(item.attr_value);
                    $$("pnlPropAgeofPG").removeClass("hide");
                    $$("lblAgePGId").value(item.item_attr_no);
                }
                else if (item.attr_key == "age_of_construction") {
                    $$("ddlPropAgeCont").selectedObject.val(item.attr_value);
                    $$("pnlPropAgeofConstru").removeClass("hide");
                    $$("lblAgeConstId").value(item.item_attr_no);
                }
                else if (item.attr_key == "road_width") {
                    $$("txtRoadWidth").value(item.attr_value);
                    $$("pnlPropRoadWidth").removeClass("hide");
                    $$("lblRoadWidthId").value(item.item_attr_no);
                }
                else if (item.attr_key == "width_of_entrance") {
                    $$("ddlPropWidthOfentrance").selectedObject.val(item.attr_value);
                    $$("pnlWidthOfentrance").removeClass("hide");
                    $$("lblWidthOfentranceId").value(item.item_attr_no);
                }
                else if (item.attr_key == "wash_rooms") {
                    $$("ddlPropWashRooms").selectedObject.val(item.attr_value);
                    $$("pnlWashRooms").removeClass("hide");
                    $$("lblPropWashRoomsId").value(item.item_attr_no);
                }
                else if (item.attr_key == "stamp_duty_registration_charges_excluded") {
                    if (item.attr_key == "1") {
                        $("#chkStampDuty").prop("checked", true);
                    } else {
                        $("#chkStampDuty").prop("checked", false);
                    }
                    $$("pnlStampDuty").removeClass("hide");
                    $$("lblchkStampDutyId").value(item.item_attr_no);
                }
                else if (item.attr_key == "price_per_sq_ft") {
                    $$("txtPricePerSqFt").value(item.attr_value);
                    $$("pnlPropPricePerSqFt").removeClass("hide");
                    $$("lblPricePerSqFtDId").value(item.item_attr_no);
                }
                else if (item.attr_key == "price_per_seats_monthly") {
                    $$("txtPricePerSeatM").value(item.attr_value);
                    $$("pnlPropPricePerSeatD").removeClass("hide");
                    $$("lblPricePerSeatMId").value(item.item_attr_no);
                }
                else if (item.attr_key == "price_per_seats_daily") {
                    $$("txtPricePerSeatD").value(item.attr_value);
                    $$("pnlPropPricePerSeatD").removeClass("hide");
                    $$("lblPricePerSeatDId").value(item.item_attr_no);
                }
                else if (item.attr_key == "price_includes") {
                    var price_includes = item.attr_value.split(",");
                    $(price_includes).each(function (i, item) {
                        if (item == "1") {
                            $("#chkPLC").prop("checked", true);
                        } else if (item == "2") {
                            $("#chkCarParking").prop("checked", true);
                        } else if (item == "3") {
                            $("#chkClubMem").prop("checked", true);
                        } 
                    })

                    $$("pnlPropPriceIncludes").removeClass("hide");
                    $$("lblPriceIncludesId").value(item.item_attr_no);
                }
                else if (item.attr_key == "plot_gated_colony") {
                    $$("ddlPropGatedColony").selectedObject.val(item.attr_value);
                    $$("pnlPropGatedColony").removeClass("hide");
                    $$("lblGatedColonyId").value(item.item_attr_no);
                }
                else if (item.attr_key == "personal_washroom") {
                    $$("ddlPropPerWashRoom").selectedObject.val(item.attr_value);
                    $$("pnlPropPerWashRoom").removeClass("hide");
                    $$("lblPerWashRoomId").value(item.item_attr_no);
                }
                else if (item.attr_key == "personal_kitchen") {
                    $$("ddlPropPerKitchen").selectedObject.val(item.attr_value);
                    $$("pnlPropPerKitchen").removeClass("hide");
                    $$("lblPerKitchenId").value(item.item_attr_no);
                }
                else if (item.attr_key == "personal_bathroom") {
                    $$("ddlPropPerBathroom").selectedObject.val(item.attr_value);
                    $$("pnlPropPerBathroom").removeClass("hide");
                    $$("lblPerBathroomId").value(item.item_attr_no);
                }
                else if (item.attr_key == "opening_hours") {
                    $$("txtOpeningHours").value(item.attr_value);
                    $$("pnlPropOpeningHours").removeClass("hide");
                    $$("lblOpeningHoursId").value(item.item_attr_no);
                }
                else if (item.attr_key == "no_of_seats") {
                    $$("txtNoOfSeats").value(item.attr_value);
                    $$("pnlPropNoOfSeats").removeClass("hide");
                    $$("lblNoOfSeatsId").value(item.item_attr_no);
                }


            })
            control.view.loadImages(control.imgData);
        }
        control.interface.show =function(){
            $(control.selectedObject).removeClass("hide");
        }
        control.interface.hide = function () {
            $(control.selectedObject).addClass("hide");
        }

       

       
        control.itemImages = [];

        control.data = {
            items: {
                search: function (filter, attrFilter, sort, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter + "&attr_filter=" + attrFilter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                withFilter: function (filter, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                filterAttributes: function (filter, attrFilter, attrKeys, fn) {
                    $.server.webMethodGET("propon", "propon/search/items/filter-attributes?attr_keys=" + attrKeys + "&filter=" + filter + "&attr_filter=" + attrFilter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },  
            builders: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/builder", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.builder_name;
                            item.value = item.builder_code;
                        });
                        fn(data);
                    });
                },
                search: function(filter,sort,fn){
                    $.server.webMethodGET("propon", "propon/builder?filter=" + filter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.builder_name;
                            item.value = item.builder_code;
                        });
                        fn(data);
                    });                    
                }
            },
            projects: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/project", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.project_name;
                            item.value = item.project_code;
                        });
                        fn(data);
                    });
                },
                search: function(filter,sort,fn){
                    $.server.webMethodGET("propon", "propon/project?filter=" + filter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.project_name;
                            item.value = item.project_code;
                        });
                        fn(data);
                    });                    
                }
            },
            payments: {
                search: function (filter, sort, fn) {
                    $.server.webMethodGET("propon", "propon/item_payments?filter=" + filter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },            
            attributes: {
                distinctValues: function (attrKey, text, fn) {
                    $.server.webMethodGET("propon", "propon/item-attributes?select=distinct attr_value,attr_text&filter=item_attributes_t.attr_key eq '" + attrKey + "' and item_attributes_t.attr_value like '%" + text + "%' ", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                withFilter: function (filter, fn) {
                    $.server.webMethodGET("propon", "propon/item-attributes?filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            productTypes: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/product_types", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.ptype_name;
                            item.value = item.ptype_no;
                        });
                        fn(data);
                    });
                },
                withFilter: function (data,fn) {
                    $.server.webMethodGET("propon", "propon/product_types?filter=" + data.filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.ptype_name;
                            item.value = item.ptype_no;
                        });
                        fn(data);
                    });
                }
            },
            file: {
                allData: function (filter,fn) {
                    $.server.webMethodGET("propon", "propon/files?filter="+filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.file_name;
                            item.value = item.file_no;
                        });
                        fn(data);
                    });
                }
            },
            mainProductTypes: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/main_product_types", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.mpt_name;
                            item.value = item.mpt_no;
                        });
                        fn(data);
                    });
                },
                withFilter: function (data,fn) {
                    $.server.webMethodGET("propon", "propon/main_product_types?filter=" + data.filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.mpt_name;
                            item.value = item.mpt_no;
                        });
                        fn(data);
                    });
                }
            },
            categories: {
                allData: function (fn) {
                    $.server.webMethodGET("propon", "propon/categories?sort=seq_no", function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.cat_name;
                            item.value = item.cat_no;
                        });
                        fn(data);
                    });
                }
            }
        }
        control.interface.postFreeAd = function () {
            $$("mdlPostFreeAd").open("Post Free Ad");
            $$("mdlPostFreeAd").height("300");
            $$("mdlPostFreeAd").width("60%");
            control.interface.loadAsPerType("prop", "", "");
            control.interface.main();
        }
        control.interface.showSelectPerson = function () {
            $$("pnlPersonType").removeClass("hide");
            $$("pnlIfOwner").addClass("hide");
            $$("pnlIfOwnerPropertyDetails").addClass("hide");
        }
        control.interface.showIfOwner = function () {
            $$("pnlPersonType").addClass("hide");
            $$("pnlIfOwner").removeClass("hide");
            $$("pnlPropWhatisFor").removeClass("hide");
            $$("pnlIfOwnerPropertyDetails").addClass("hide");
        }
        control.interface.showIfOwnerPropDetails = function () {
            $$("pnlPersonType").addClass("hide");
            $$("pnlIfOwner").addClass("hide");
            $$("pnlIfOwnerPropertyDetails").removeClass("hide");
        }
        control.events.page_load = function () {
            window.postData = {};
            $$("ddlPropWhatisFor").selectionChange(function () {
                if ($$("ddlPropWhatisFor").selectedValue() != "Select") {
                    var data ={ 
                        filter: "action_type eq '" + ($$("ddlPropWhatisFor").selectedValue() == "Sell" ? "Buy" : "Rent") + "'"
                    };
                    control.data.mainProductTypes.withFilter(data,function (data) {
                        $$("ddlMainPropType").dataBind(data, "mpt_no", "mpt_name", "Select");
                        control.interface.loadAsPerType("prop", "one", "");
                    });
                } else {
                    control.interface.loadAsPerType("prop", "", "");
                }
            })
            $$("ddlPropWho").selectionChange(function () {
                if($$("ddlPropWho").selectedValue()!="Select" && ($$("ddlPropWho").selectedValue()=="Builder" || $$("ddlPropWho").selectedValue()=="Agent")){
                    control.view.loadBuilder();
                    control.view.loadProject();
                    $$("pnlBuilderName").removeClass("hide");
                    $$("pnlPropertyProjName").removeClass("hide");     
                }else{
                    $$("pnlBuilderName").addClass("hide");
                    $$("pnlPropertyProjName").addClass("hide");
                }
            })
            control.view.loadBuilder = function(callback){
                var filter="user_id="+window.shopOnContext.user.user_id;
                var sort=" builder_name";
                control.data.builders.search(filter,sort,function (data) {
                    $$("ddlBuilderName").dataBind(data, "builder_code", "builder_name", "Select");
                    if(callback!=undefined)
                    callback();
                });                  
            }
            control.view.loadProject = function(callback){
                var filter="user_id="+window.shopOnContext.user.user_id;
                var sort=" project_name";
                control.data.projects.search(filter,sort,function (data) {
                    $$("ddlProjectName").dataBind(data, "project_code", "project_name", "Select");
                    if(callback!=undefined)
                    callback();                   
                })                  
            }            
            $$("ddlMainPropType").selectionChange(function () {
                if ($$("ddlMainPropType").selectedValue() != "-1") {
                    var data = {
                        filter: "mpt_no eq " + $$("ddlMainPropType").selectedValue() + ""
                    };
                    control.data.productTypes.withFilter(data,function (data) {
                        $$("ddlPropType").dataBind(data, "ptype_no", "ptype_name", "Select");
                        control.interface.loadAsPerType("prop", "two", "");
                    });
                    
                } else {
                    control.interface.loadAsPerType("prop", "one", "");
                }
            })
            $$("ddlPropType").selectionChange(function () {
                if ($$("ddlPropType").selectedValue() != "-1") {
                    if ($$("ddlPropType").selectedData(0).attr_keys != undefined && $$("ddlPropType").selectedData(0).attr_keys != "" && $$("ddlPropType").selectedData(0).attr_keys != null) {
                        var attrs = [];
                        attrs = $$("ddlPropType").selectedData(0).attr_keys.split(",");
                        control.interface.loadAsPerType("prop", "three", attrs);
                        $$("pnlPropTypeText").html($$("ddlPropType").selectedData(0).ptype_name);
                    }
                } else {
                    control.interface.loadAsPerType("prop", "two", "");
                }
            })



        }

        $("#chkContactbyOwn").click(function () {
            $("#chkContactbyOwn").prop('checked', true);
            $("#chkContactView").prop('checked', false);
            $("#chkContactOnlybyme").prop('checked', false);
            $("#chkContactbyOwnMsg").prop('checked', false);
            $("#chkContactOnlybymeMsg").prop('checked', false);
        })
        $("#chkContactView").click(function () {
            $("#chkContactbyOwn").prop('checked', false);
            $("#chkContactView").prop('checked', true);
            $("#chkContactOnlybyme").prop('checked', false);
            $("#chkContactbyOwnMsg").prop('checked', false);
            $("#chkContactOnlybymeMsg").prop('checked', false);
        })
        $("#chkContactOnlybyme").click(function () {
            $("#chkContactbyOwn").prop('checked', false);
            $("#chkContactView").prop('checked', false);
            $("#chkContactOnlybyme").prop('checked', true);
            $("#chkContactbyOwnMsg").prop('checked', false);
            $("#chkContactOnlybymeMsg").prop('checked', false);
        })
        $("#chkContactbyOwnMsg").click(function () {
            $("#chkContactbyOwn").prop('checked', false);
            $("#chkContactView").prop('checked', false);
            $("#chkContactOnlybyme").prop('checked', false);
            $("#chkContactbyOwnMsg").prop('checked', true);
            $("#chkContactOnlybymeMsg").prop('checked', false);
        })
        $("#chkContactOnlybymeMsg").click(function () {
            $("#chkContactbyOwn").prop('checked', false);
            $("#chkContactView").prop('checked', false);
            $("#chkContactOnlybyme").prop('checked', false);
            $("#chkContactbyOwnMsg").prop('checked', false);
            $("#chkContactOnlybymeMsg").prop('checked', true);
        })
        control.events.btnAgent_click = function () {
            $$("btnAgent").addClass("btn-active");
            $$("btnOwner").removeClass("btn-active");
            $$("btnBuilder").removeClass("btn-active");
        }
        control.events.lnkMyAds_click = function () {
            control.interface.subscribe("myads");
        }
        control.events.btnBuilder_click = function () {
            $$("btnBuilder").addClass("btn-active");
            $$("btnAgent").removeClass("btn-active");
            $$("btnOwner").removeClass("btn-active");
        }
        control.events.btnOwner_click = function () {
            $$("btnOwner").addClass("btn-active");
            $$("btnBuilder").removeClass("btn-active");
            $$("btnAgent").removeClass("btn-active");
        }
        control.events.btnContinueType_click = function () {
            
            if ($$("btnOwner").hasClass('btn-active')) {
                postData.owner_type = 'owner'
                control.interface.showIfOwner();
            } else if ($$("btnAgent").hasClass('btn-active')) {
                postData.owner_type = 'agent'
                control.interface.showIfOwner();
            } else if ($$("btnBuilder").hasClass('btn-active')) {
                postData.owner_type = 'builder'
                control.interface.showIfOwner();
            }

        }
        control.events.btnBackIfOwner_click = function () {
            control.interface.showSelectPerson();
        }

        control.events.btnContinueIfOwner_click = function () {
            if ($$("txtYourName").value().trim() == "") {
                msgPanel("Please enter your name");
                $$("txtYourName").focus();
            } else if ($$("txtYourMobile").value().trim() == "") {
                msgPanel("Please enter mobile number");
                $$("txtYourMobile").focus();
            } else if ($$("txtYourEmail").value().trim() == "") {
                msgPanel("Please enter your email id");
                $$("txtYourEmail").focus();
            } else if ($$("ddlPropWhatisFor").selectedValue() == "Select") {
                msgPanel("Please select what are you looking for");
            } else {
                window.postData.name = $$("txtYourName").value();
                window.postData.mobile = $$("txtYourMobile").value();
                window.postData.email = $$("txtYourEmail").value();
                window.postData.action_type = $$("ddlPropWhatisFor").selectedValue();
                control.interface.showIfOwnerPropDetails();
                control.interface.loadAsPerType("prop", "one","");
            }
            
        }
        control.events.btnBackIfOwnerProp2_click = function () {
            control.interface.showIfOwner();
        }
        control.events.btnBackPropBtnStep3_click = function () {
            control.interface.loadAsPerType("prop", "two", $$("ddlPropType").selectedData(0).attr_keys.split(","));
        }
        control.events.btnBackPropBtnStep4_click = function () {
            control.interface.loadAsPerType("prop", "three", $$("ddlPropType").selectedData(0).attr_keys.split(","));
        }
        control.events.btnContPropBtnStep3_click = function () {
            if ($$("ddlPropWhatisFor").selectedValue() == "Rent" && $$("txtExpectedRent").value().trim() == "") {
                msgPanel("Please fill the expected rent");
                $$("txtExpectedRent").focus();
            } else {
                window.itemAttrData2 = [];
                if ($$("ddlPropWhatisFor").selectedValue() != "Select") {
                    window.itemAttrData2.push({
                        //item_no: window.item_no,
                        org_id: window.shopOnContext.companyProduct.comp_prod_id,
                        attr_key: "action_type",
                        attr_value: $$("ddlPropWhatisFor").selectedValue(),
                        attr_text: $$("ddlPropWhatisFor").selectedValue(),
                    });
                }
                    if ($$("txtExpectedPrice").value() != "") {
                        window.itemAttrData2.push({
                            //item_no: window.item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "expected_price",
                            attr_value: $$("txtExpectedPrice").value(),
                            attr_text: $$("txtExpectedPrice").value(),
                        });
                    }
                    if ($$("txtExpectedRent").value() != "") {
                        window.itemAttrData2.push({
                            //item_no: window.item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "expected_rent",
                            attr_value: $$("txtExpectedRent").value(),
                            attr_text: $$("txtExpectedRent").value(),
                        });
                    }
                    if ($$("ddlPropContStatus").selectedValue() != "Select") {
                        window.itemAttrData2.push({
                            //item_no: window.item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "construction_status",
                            attr_value: $$("ddlPropContStatus").selectedValue(),
                            attr_text: $$("ddlPropContStatus").selectedValue(),
                        });
                    }
                    if ($$("dsPropAvailFrom").getDate() != "") {
                        window.itemAttrData2.push({
                            //item_no: window.item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "avail_from",
                            attr_value: $$("dsPropAvailFrom").getDate(),
                            attr_text: $$("dsPropAvailFrom").getDate(),
                        });
                    }
                    if ($$("ddlPropTransType").selectedValue() != "Select") {
                        window.itemAttrData2.push({
                            //item_no: window.item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "transaction_type",
                            attr_value: $$("ddlPropTransType").selectedValue(),
                            attr_text: $$("ddlPropTransType").selectedValue(),
                        });
                    }
                    
                /*
                    var inputData = [];
                    inputData.push({
                        item_no: window.item_no,
                        action_type:($$("ddlPropWhatisFor").selectedValue()=="Sell" ? "Buy" : "Rent"),
                        price: ($$("txtExpectedRent").value() != "" ? $$("txtExpectedRent").value() : $$("txtExpectedPrice").value()),
                    });
                
                    $.server.webMethodPUT("propon", "propon/items/" + window.item_no, inputData, function (data) {
                        */
                        //$.server.webMethodPOST("propon", "propon/item_attributes/", window.itemAttrData, function (data) {
                            control.interface.loadAsPerType("prop", "four", $$("ddlPropType").selectedData(0).attr_keys.split(","));
                        //})
                   // })
                
            }
            
        }
        control.events.btnOpenPricePlan_click = function () {
            control.controls.pnlPricePlan.open();
            control.controls.pnlPricePlan.title("Choose Pricing");
            control.controls.pnlPricePlan.height("300");
            control.controls.pnlPricePlan.width("60%");
        }
        control.events.btnUploadImages_click = function () {
            control.controls.pnlImageUpload.open();
            control.controls.pnlImageUpload.title("Photos");
            control.controls.pnlImageUpload.height("300");
            control.controls.pnlImageUpload.width("50%");
            
        }
        control.events.btnAddNewProject_click = function(){
            control.controls.pnlAddNewProjectr.open();
            control.controls.pnlAddNewProjectr.title("New Project");
            control.controls.pnlAddNewProjectr.width("30%");
            control.controls.pnlAddNewProjectr.height("300");
        }
        control.events.btnSaveNewProject_click = function(){
            if($$("txtPrjName").value().trim()==""){
                msgPanel("Please enter project name...!");
            }else if($$("txtPrjLoc").value().trim()==""){
                msgPanel("Please enter location...!");
            }else{
                var prjData = [];
                prjData.push({
                    project_code: "##GUID##",
                    project_name: $$("txtPrjName").value(),
                    location: $$("txtPrjLoc").value(),
                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                    user_id: window.shopOnContext.user.user_id,
                });                
                $.server.webMethodPOST("propon", "propon/project/", prjData, function (data) {
                    msgPanel("New project added successfully...!");
                    control.controls.pnlAddNewProjectr.close();
                    control.view.loadProject(function(){
                        $$("ddlProjectName").selectedValue(data[0].project_code);    
                        control.view.clearNewProject();
                    });
                })
            }
        }
        control.view.clearNewProject=function(){
            $$("txtPrjName").value("");
            $$("txtPrjLoc").value("");
        }
        control.events.btnAddNewBuilder_click = function(){
            control.controls.pnlAddNewBuilder.open();
            control.controls.pnlAddNewBuilder.title("New Builder");
            control.controls.pnlAddNewBuilder.width("30%");
            control.controls.pnlAddNewBuilder.height("300");
        }
        control.events.btnSaveNewBuilder_click = function(){
            if($$("txtBuildName").value().trim()==""){
                msgPanel("Please enter builder name...!");
            }else if($$("txtBuildLoc").value().trim()==""){
                msgPanel("Please enter location...!");
            }else{
                var buildData = [];
                buildData.push({
                    builder_code: "##GUID##",
                    builder_name: $$("txtBuildName").value(),
                    location: $$("txtBuildLoc").value(),
                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                    user_id: window.shopOnContext.user.user_id,
                });                
                $.server.webMethodPOST("propon", "propon/builder/", buildData, function (data) {
                    msgPanel("New builder added successfully...!");
                    control.controls.pnlAddNewBuilder.close();
                    control.view.loadBuilder(function(){
                        $$("ddlBuilderName").selectedValue(data[0].builder_code);    
                        control.view.clearNewBuilder();
                    });
                })
            }
        }
        control.view.clearNewBuilder=function(){
            $$("txtBuildName").value("");
            $$("txtBuildLoc").value("");
        }
        control.events.btnPostFreeAdSave_click = function () {
            //herepost
            if ($$("ddlPropWho").selectedValue() == "Select") {
                msgPanel("Please select Who you are..!");
            }
            if ($$("txtYourName").value().trim() == "") {
                msgPanel("Please enter your name");
                $$("txtYourName").focus();
            } else if ($$("txtYourMobile").value().trim() == "") {
                msgPanel("Please enter mobile number");
                $$("txtYourMobile").focus();
            } else if ($$("txtYourEmail").value().trim() == "") {
                msgPanel("Please enter your email id");
                $$("txtYourEmail").focus();
            } else if ($$("ddlPropWhatisFor").selectedValue() == "Select") {
                msgPanel("Please select what are you looking for");
            } else if ($$("txtYourPropName").value().trim() == "") {
                msgPanel("Please enter your property description");
                $$("txtYourPropName").focus();
            }
            //else if ($$("txtProjName").value().trim() == "") {
            //    msgPanel("Please enter your project name");
            //    $$("txtProjName").focus();
            //}
            else if ($$("txtYourPropLoc").value().trim() == "") {
                msgPanel("Please enter your property location");
                $$("txtYourPropLoc").focus();
            } else if($$("ddlPricePlan").selectedValue()==-1 || $$("ddlPricePlan").selectedValue()=="Select"){
                msgPanel("Please select a package ...!");
            }
            else if (!$$("pnlBedRooms").hasClass("hide") && $$("ddlPropTypeBedRooms").selectedValue() == "Select") {
                msgPanel("Please select bed rooms");
            } else if (!$$("pnlPropArea").hasClass("hide") && $$("txtAreaSize").value().trim() == "") {
                msgPanel("Please enter area size");
                $$("txtAreaSize").focus();
            } else if (!$$("pnlPropExpectedPrice").hasClass("hide") && $$("txtExpectedPrice").value().trim() == "") {
                msgPanel("Please enter expected price to continue");
            } else if (!$$("pnlPropExpectedRent").hasClass("hide") && $$("txtExpectedRent").value().trim() == "") {
                msgPanel("Please enter expected rent to continue");
            } else if (!$$("pnlPropConstruStatus").hasClass("hide") && $$("ddlPropContStatus").selectedValue() == "Select") {
                msgPanel("Please select construction status");
            } else if (!$$("pnlPropFurnStatus").hasClass("hide") && $$("ddlPropFurnStatus").selectedValue() == "Select") {
                msgPanel("Please select furnished status");
            } else if (!$$("pnlBathRooms").hasClass("hide") && $$("ddlPropTypeBathRooms").selectedValue() == "Select") {
                msgPanel("Please select no of bath rooms");
            } else if (!$$("pnlPropFloorsAllowed").hasClass("hide") && $$("txtTotalFloorsAllowed").value().trim() == "") {
                msgPanel("Please enter total floors allowed");
            } else if (!$$("pnlPropFloorNumber").hasClass("hide") && $$("txtFloorNumber").value().trim() == "") {
                msgPanel("Please enter total number of floors");
            } else if (!$$("pnlPropAvailFrom").hasClass("hide") && $$("dsPropAvailFrom").getDate() == "") {
                msgPanel("Please select when the property is available from");
            } else if (!$$("pnlPropAvailFor").hasClass("hide") && $$("ddlPropAvailFor").selectedValue() == "Select") {
                msgPanel("Please select available for");
            } else if (!$$("pnlPropOccupancy").hasClass("hide") && $$("ddlPropOccupancy").selectedValue() == "Select") {
                msgPanel("Please select occupancy");
            } else if (!$$("pnlPropTransType").hasClass("hide") && $$("ddlPropTransType").selectedValue() == "Select") {
                msgPanel("Please select transaction type");
            } else if (!$$("pnlPropTotalFloors").hasClass("hide") && $$("txtTotalFloors").value() == "") {
                msgPanel("Please enter total floors available");
                $$("txtTotalFloors").focus();
            } else if (!$$("pnlPropCornerProp").hasClass("hide") && $$("ddlPropCorner").selectedValue() == "Select") {
                msgPanel("Please select corners available or not");
            }
            else {
                var contact_flag="1";
                if($("#chkContactbyOwn").prop('checked')){
                    contact_flag = $("#chkContactbyOwn").val();
                }else if($("#chkContactView").prop('checked')){
                    contact_flag = $("#chkContactView").val();
                }else if($("#chkContactOnlybyme").prop('checked')){
                    contact_flag = $("#chkContactOnlybyme").val();
                }else if($("#chkContactbyOwnMsg").prop('checked')){
                    contact_flag = $("#chkContactbyOwnMsg").val();
                }else if($("#chkContactOnlybymeMsg").prop('checked')){
                    contact_flag = $("#chkContactOnlybymeMsg").val();
                }
                if(!window.isEdit){
                    if($$("ddlPricePlan").selectedValue()=="1"){
                        var ExpDate = new Date();
                        ExpDate.setDate(ExpDate.getDate() + 30);
                        var expiry_date = dbDate(ExpDate);
                    }else{
                        var ExpDate = new Date();
                        ExpDate.setDate(ExpDate.getDate() + 180);
                        var expiry_date = dbDate(ExpDate);
                    }
                }


                var itemData = [];
                itemData.push({
                    action_type: ($$("ddlPropWhatisFor").selectedValue() == "Sell" ? "Buy" : "Rent"),
                    item_name: $$("txtYourPropName").value(),
                    location: $$("txtYourPropLoc").value(),
                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                    name: $$("txtYourName").value(),
                    mobile: $$("txtYourMobile").value(),
                    email: $$("txtYourEmail").value(),
                    ptype_no: $$("ddlPropType").selectedValue(),
                    user_id: window.shopOnContext.user.user_id,
                    owner_type: $$("ddlPropWho").selectedValue(),
                    price_plan_id: $$("ddlPricePlan").selectedValue(),
                    state: 10,
                    project_code: $$("ddlProjectName").selectedValue(),
                    builder_code: $$("ddlBuilderName").selectedValue(),
                    contact_flag: contact_flag,
                    price: ($$("txtMonthlyRent").value() != "" ? $$("txtMonthlyRent").value() : $$("txtExpectedPrice").value()),
                    expiry_date:expiry_date,
                });
                
                if (!window.isEdit) {
                    itemData[0].item_no = "##GUID##";
                    $.server.webMethodPOST("propon", "propon/items/", itemData, function (data) {
                        var itemAttrData = [];
                        $(control.imgData).each(function (i, item) {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "image"+i+1,
                                attr_value: item.file_no,
                                attr_text: item.file_no,
                            });
                        })
                        itemAttrData.push({
                            item_no: data[0].item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "location",
                            attr_value: $$("txtYourPropLoc").value(),
                            attr_text: $$("txtYourPropLoc").value(),
                        });
                        if (!$$("pnlBedRooms").hasClass("hide") && $$("ddlPropTypeBedRooms").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "bed_room",
                                attr_value: $$("ddlPropTypeBedRooms").selectedValue(),
                                attr_text: $$("ddlPropTypeBedRooms").selectedValue() + " BHK",
                                
                            });
                        }
                        if (!$$("pnlPropArea").hasClass("hide") && $$("txtAreaSize").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "area",
                                attr_value: $$("txtAreaSize").value(),
                                attr_text: $$("txtAreaSize").value() + " " + $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                            });
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "area_unit",
                                attr_value: $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                                attr_text: $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropArea").hasClass("hide") && $$("ddlPropWhatisFor").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "action_type",
                                attr_value: $$("ddlPropWhatisFor").selectedValue(),
                                attr_text: $$("ddlPropWhatisFor").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropExpectedPrice").hasClass("hide") && $$("txtExpectedPrice").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "expected_price",
                                attr_value: $$("txtExpectedPrice").value(),
                                attr_text: $$("txtExpectedPrice").value(),
                            });
                        }
                        if (!$$("pnlPropExpectedRent").hasClass("hide") && $$("txtExpectedRent").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "expected_rent",
                                attr_value: $$("txtExpectedRent").value(),
                                attr_text: $$("txtExpectedRent").value(),
                            });
                        }
                        if (!$$("pnlPropConstruStatus").hasClass("hide") && $$("ddlPropContStatus").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "construction_status",
                                attr_value: $$("ddlPropContStatus").selectedValue(),
                                attr_text: $$("ddlPropContStatus").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAvailFrom").hasClass("hide") && $$("dsPropAvailFrom").getDate() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "avail_from",
                                attr_value: $$("dsPropAvailFrom").getDate(),
                                attr_text: $$("dsPropAvailFrom").getDate(),
                            });
                        }
                        if (!$$("pnlPropTransType").hasClass("hide") && $$("ddlPropTransType").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "transaction_type",
                                attr_value: $$("ddlPropTransType").selectedValue(),
                                attr_text: $$("ddlPropTransType").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropFurnStatus").hasClass("hide") && $$("ddlPropFurnStatus").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "furnished_status",
                                attr_value: $$("ddlPropFurnStatus").selectedValue(),
                                attr_text: $$("ddlPropFurnStatus").selectedValue(),
                            });
                        }
                        if (!$$("pnlBathRooms").hasClass("hide") && $$("ddlPropTypeBathRooms").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "bath_rooms",
                                attr_value: $$("ddlPropTypeBathRooms").selectedValue(),
                                attr_text: $$("ddlPropTypeBathRooms").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropFloorNumber").hasClass("hide") && $$("txtFloorNumber").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "floor_number",
                                attr_value: $$("txtFloorNumber").value(),
                                attr_text: $$("txtFloorNumber").value(),
                            });
                        }
                        if (!$$("pnlPropTotalFloors").hasClass("hide") && $$("txtTotalFloors").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "total_floors",
                                attr_value: $$("txtTotalFloors").value(),
                                attr_text: $$("txtTotalFloors").value(),
                            });
                        }
                        if (!$$("pnlPropFloorsAllowed").hasClass("hide") && $$("txtTotalFloorsAllowed").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "floors_allowed",
                                attr_value: $$("txtTotalFloorsAllowed").value(),
                                attr_text: $$("txtTotalFloorsAllowed").value(),
                            });
                        }
                        if (!$$("pnlPropAvailFor").hasClass("hide") && $$("ddlPropAvailFor").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "available_for",
                                attr_value: $$("ddlPropAvailFor").selectedValue(),
                                attr_text: $$("ddlPropAvailFor").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerProp").hasClass("hide") && $$("ddlPropCorner").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_property",
                                attr_value: $$("ddlPropCorner").selectedValue(),
                                attr_text: $$("ddlPropCorner").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropOccupancy").hasClass("hide") && $$("ddlPropOccupancy").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "occupancy",
                                attr_value: $$("ddlPropOccupancy").selectedValue(),
                                attr_text: $$("ddlPropOccupancy").selectedValue(),
                            });
                        }
                        if (!$$("pnlWidthOfentrance").hasClass("hide") && $$("ddlPropWidthOfentrance").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "width_of_entrance",
                                attr_value: $$("ddlPropWidthOfentrance").selectedValue(),
                                attr_text: $$("ddlPropWidthOfentrance").selectedValue(),
                            });
                        }
                        if (!$$("pnlWashRooms").hasClass("hide") && $$("ddlPropWashRooms").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "wash_rooms",
                                attr_value: $$("ddlPropWashRooms").selectedValue(),
                                attr_text: $$("ddlPropWashRooms").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropTenantsYouPref").hasClass("hide") && $$("ddlPropTenantsYouPref").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "tenants_you_prefer",
                                attr_value: $$("ddlPropTenantsYouPref").selectedValue(),
                                attr_text: $$("ddlPropTenantsYouPref").selectedValue(),
                            });
                        }
                        if (!$$("pnlStampDuty").hasClass("hide")) {
                            var tenants_you_prefer = "1";
                            if ($("#chkStampDuty").prop('checked')) {
                                tenants_you_prefer = "1";
                            } else {
                                tenants_you_prefer = "0";
                            }

                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "tenants_you_prefer",
                                attr_value: tenants_you_prefer,
                                attr_text: tenants_you_prefer,
                            });
                        }
                        if (!$$("pnlPropRoadWidth").hasClass("hide") && $$("txtRoadWidth").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "road_width",
                                attr_value: $$("txtRoadWidth").value(),
                                attr_text: $$("txtRoadWidth").value(),
                            });
                        }
                        if (!$$("pnlPropPricePerSeatM").hasClass("hide") && $$("txtPricePerSeatM").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "price_per_seats_monthly",
                                attr_value: $$("txtPricePerSeatM").value(),
                                attr_text: $$("txtPricePerSeatM").value(),
                            });
                        }
                        if (!$$("pnlPropPricePerSeatD").hasClass("hide") && $$("txtPricePerSeatD").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "price_per_seats_daily",
                                attr_value: $$("txtPricePerSeatD").value(),
                                attr_text: $$("txtPricePerSeatD").value(),
                            });
                        }
                        if (!$$("pnlPropPriceIncludes").hasClass("hide")) {
                            var price_includes = "";
                            if ($("#chkPLC").prop('checked')) {
                                price_includes = price_includes+"1";
                            }
                            if ($("#chkCarParking").prop('checked')) {
                                if (price_includes!="") {
                                    price_includes = price_includes + ",2";
                                } else {
                                    price_includes = price_includes + "2";
                                }
                            }
                            if ($("#chkClubMem").prop('checked')) {
                                if (price_includes != "") {
                                    price_includes = price_includes + ",3";
                                } else {
                                    price_includes = price_includes + "3";
                                }
                            }
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "price_includes",
                                attr_value: price_includes,
                                attr_text: price_includes,
                            });
                        }
                        if (!$$("pnlPropGatedColony").hasClass("hide") && $$("ddlPropGatedColony").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_gated_colony",
                                attr_value: $$("ddlPropGatedColony").selectedValue(),
                                attr_text: $$("ddlPropGatedColony").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropBoundWallmade").hasClass("hide") && $$("ddlPropBoundWallmade").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_bound_wall_made",
                                attr_value: $$("ddlPropBoundWallmade").selectedValue(),
                                attr_text: $$("ddlPropBoundWallmade").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPerWashRoom").hasClass("hide") && $$("ddlPropPerWashRoom").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "personal_washroom",
                                attr_value: $$("ddlPropPerWashRoom").selectedValue(),
                                attr_text: $$("ddlPropPerWashRoom").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPerKitchen").hasClass("hide") && $$("ddlPropPerKitchen").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "personal_kitchen",
                                attr_value: $$("ddlPropPerKitchen").selectedValue(),
                                attr_text: $$("ddlPropPerKitchen").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPerBathroom").hasClass("hide") && $$("ddlPropPerBathroom").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "personal_bathroom",
                                attr_value: $$("ddlPropPerBathroom").selectedValue(),
                                attr_text: $$("ddlPropPerBathroom").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropOpeningHours").hasClass("hide") && $$("txtOpeningHours").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "opening_hours",
                                attr_value: $$("txtOpeningHours").value(),
                                attr_text: $$("txtOpeningHours").value(),
                            });
                        }
                        if (!$$("pnlPropNoOfSeats").hasClass("hide") && $$("txtNoOfSeats").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "no_of_seats",
                                attr_value: $$("txtNoOfSeats").value(),
                                attr_text: $$("txtNoOfSeats").value(),
                            });
                        }
                        if (!$$("pnlNoofRoomsPg").hasClass("hide") && $$("ddlPropNoofRoomsPg").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "no_of_rooms_pg",
                                attr_value: $$("ddlPropNoofRoomsPg").selectedValue(),
                                attr_text: $$("ddlPropNoofRoomsPg").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropNearbyBusi").hasClass("hide") && $$("txtNearbyBusi").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "nearby_businesses",
                                attr_value: $$("txtNearbyBusi").value(),
                                attr_text: $$("txtNearbyBusi").value(),
                            });
                        }
                        if (!$$("pnlPropMonthlyRent").hasClass("hide") && $$("txtMonthlyRent").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "monthly_rent",
                                attr_value: $$("txtMonthlyRent").value(),
                                attr_text: $$("txtMonthlyRent").value(),
                            });
                        }
                        if (!$$("pnlPropLockinPeriods").hasClass("hide") && $$("txtLockinPeriods").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "lock_in_periods",
                                attr_value: $$("txtLockinPeriods").value(),
                                attr_text: $$("txtLockinPeriods").value(),
                            });
                        }
                        if (!$$("pnlPropLandZone").hasClass("hide") && $$("ddlPropLandZone").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "land_zone",
                                attr_value: $$("ddlPropLandZone").selectedValue(),
                                attr_text: $$("ddlPropLandZone").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropIsMainRoadFac").hasClass("hide") && $$("ddlPropIsMainRoadFac").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "is_main_road_facing",
                                attr_value: $$("ddlPropIsMainRoadFac").selectedValue(),
                                attr_text: $$("ddlPropIsMainRoadFac").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropIdealBusi").hasClass("hide") && $$("txtIdealBusi").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "ideal_for_businesses",
                                attr_value: $$("txtIdealBusi").value(),
                                attr_text: $$("txtIdealBusi").value(),
                            });
                        }
                        if (!$$("pnlGettingBrokResp").hasClass("hide")) {
                            var getting_response_from_brokers = "1";
                            if ($("#chkGettingBrokResp").prop('checked')) {
                                getting_response_from_brokers = "1";
                            } else {
                                getting_response_from_brokers = "0";
                            }

                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "getting_response_from_brokers",
                                attr_value: getting_response_from_brokers,
                                attr_text: getting_response_from_brokers,
                            });
                        }
                        if (!$$("pnlFurnishingDetails").hasClass("hide")) {
                            var furnishing_details = "";
                            if ($("#chkCupboards").prop('checked')) {
                                furnishing_details = furnishing_details + $("#chkCupboards").val();
                            }
                            if ($("#chkWashingMc").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkWashingMc").val();
                            }
                            if ($("#chkStudyTable").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkStudyTable").val();
                            }
                            if ($("#chkAC").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkAC").val();
                            }
                            if ($("#chkWifi").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkWifi").val();
                            }
                            if ($("#chkGeyser").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkGeyser").val();
                            }
                            if ($("#chkFridge").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkFridge").val();
                            }
                            if ($("#chkCooler").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkCooler").val();
                            }
                            if ($("#chkTV").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkTV").val();
                            }

                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "furnishing_details",
                                attr_value: furnishing_details,
                                attr_text: furnishing_details,
                            });
                        }
                        if (!$$("pnlGettingBrokResp").hasClass("hide")) {
                            var electricity_water_charges_included = "1";
                            if ($("#chkElectricityWater").prop('checked')) {
                                electricity_water_charges_included = "1";
                            } else {
                                electricity_water_charges_included = "0";
                            }

                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "electricity_water_charges_included",
                                attr_value: electricity_water_charges_included,
                                attr_text: electricity_water_charges_included,
                            });
                        }
                        if (!$$("pnlPropSecurityAmt").hasClass("hide") && $$("txtSecurityAmt").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "security_amount",
                                attr_value: $$("txtSecurityAmt").value(),
                                attr_text: $$("txtSecurityAmt").value(),
                            });
                        }
                        if (!$$("pnlPropLaundry").hasClass("hide") && $$("txtLaundry").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "laundry_amount",
                                attr_value: $$("txtLaundry").value(),
                                attr_text: $$("txtLaundry").value(),
                            });
                        }
                        if (!$$("pnlPropOtherChar").hasClass("hide") && $$("txtOtherChar").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "other_charges",
                                attr_value: $$("txtOtherChar").value(),
                                attr_text: $$("txtOtherChar").value(),
                            });
                        }
                        if (!$$("pnlPropElectricityAmt").hasClass("hide") && $$("txtElectricityAmt").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "electricity_amount",
                                attr_value: $$("txtElectricityAmt").value(),
                                attr_text: $$("txtElectricityAmt").value(),
                            });
                        }
                        if (!$$("pnlPropRentout").hasClass("hide") && $$("ddlPropRentout").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "currently_rent_out",
                                attr_value: $$("ddlPropRentout").selectedValue(),
                                attr_text: $$("ddlPropRentout").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropLeasedout").hasClass("hide") && $$("ddlPropLeasedout").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "currently_leased_out",
                                attr_value: $$("ddlPropLeasedout").selectedValue(),
                                attr_text: $$("ddlPropLeasedout").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerShow").hasClass("hide") && $$("ddlPropCornerShow").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_showroom",
                                attr_value: $$("ddlPropCornerShow").selectedValue(),
                                attr_text: $$("ddlPropCornerShow").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerShop").hasClass("hide") && $$("ddlPropCornerProp").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_shop",
                                attr_value: $$("ddlPropCornerProp").selectedValue(),
                                attr_text: $$("ddlPropCornerProp").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerPlot").hasClass("hide")) {
                            var corner_plot = "1";
                            if ($("#chkCornerPlot").prop('checked')) {
                                corner_plot = "1";
                            } else {
                                corner_plot = "0";
                            }

                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_plot",
                                attr_value: corner_plot,
                                attr_text: corner_plot,
                            });
                        }
                        if (!$$("pnlPropCommonArea").hasClass("hide") && $$("ddlPropCommonArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "common_area",
                                attr_value: $$("ddlPropCommonArea").selectedValue(),
                                attr_text: $$("ddlPropCommonArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPlotArea").hasClass("hide") && $$("ddlPropPlotArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_area",
                                attr_value: $$("ddlPropPlotArea").selectedValue(),
                                attr_text: $$("ddlPropPlotArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPlotArea").hasClass("hide") && $$("txtPlotArea").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_area",
                                attr_value: $$("txtPlotArea").value(),
                                attr_text: $$("txtPlotArea").value(),
                            });
                        }
                        if (!$$("pnlPropCoveredArea").hasClass("hide") && $$("ddlPropCoveredArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "covered_area",
                                attr_value: $$("ddlPropCoveredArea").selectedValue(),
                                attr_text: $$("ddlPropCoveredArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCoveredArea").hasClass("hide") && $$("txtCoveredArea").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "covered_area",
                                attr_value: $$("txtCoveredArea").value(),
                                attr_text: $$("txtCoveredArea").value(),
                            });
                        }
                        if (!$$("pnlPropCarpet").hasClass("hide") && $$("ddlPropCoveredArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "carpet_area",
                                attr_value: $$("ddlPropCarpetArea").selectedValue(),
                                attr_text: $$("ddlPropCarpetArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCarpet").hasClass("hide") && $$("txtCarpetArea").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "carpet_area",
                                attr_value: $$("txtCarpetArea").value(),
                                attr_text: $$("txtCarpetArea").value(),
                            });
                        }
                        if (!$$("pnlCafeteria").hasClass("hide") && $$("ddlPropCafeteria").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "cafeteria",
                                attr_value: $$("ddlPropCafeteria").selectedValue(),
                                attr_text: $$("ddlPropCafeteria").selectedValue(),
                            });
                        }
                        if (!$$("pnlCabins").hasClass("hide") && $$("ddlPropCabins").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "cabin_meeting_rooms",
                                attr_value: $$("ddlPropCabins").selectedValue(),
                                attr_text: $$("ddlPropCabins").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropSuperBuiltUpArea").hasClass("hide") && $$("ddlPropSuperBuiltUpArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "super_built_up_area",
                                attr_value: $$("ddlPropSuperBuiltUpArea").selectedValue(),
                                attr_text: $$("ddlPropSuperBuiltUpArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropSuperBuiltUpArea").hasClass("hide") && $$("txtSuperBuiltUpArea").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "super_built_up_area",
                                attr_value: $$("txtSuperBuiltUpArea").value(),
                                attr_text: $$("txtSuperBuiltUpArea").value(),
                            });
                        }
                        if (!$$("pnlPropBuiltUpArea").hasClass("hide") && $$("ddlPropBuiltUpArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "built_up_area",
                                attr_value: $$("ddlPropBuiltUpArea").selectedValue(),
                                attr_text: $$("ddlPropBuiltUpArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropBuiltUpArea").hasClass("hide") && $$("txtBuiltUpArea").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "built_up_area",
                                attr_value: $$("txtBuiltUpArea").value(),
                                attr_text: $$("txtBuiltUpArea").value(),
                            });
                        }
                        if (!$$("pnlMainCharge").hasClass("hide") && $$("ddlMainCharge").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "maintenance_charge_limit",
                                attr_value: $$("ddlMainCharge").selectedValue(),
                                attr_text: $$("ddlMainCharge").selectedValue(),
                            });
                        }
                        if (!$$("pnlMainCharge").hasClass("hide") && $$("txtMainCharge").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "maintenance_charge",
                                attr_value: $$("txtMainCharge").value(),
                                attr_text: $$("txtMainCharge").value(),
                            });
                        }
                        if (!$$("pnlPropBookingAmt").hasClass("hide") && $$("txtBookingAmt").value() != "") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "booking_amount",
                                attr_value: $$("txtBookingAmt").value(),
                                attr_text: $$("txtBookingAmt").value(),
                            });
                        }
                        if (!$$("pnlBalconies").hasClass("hide") && $$("ddlPropBalconies").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "balconies",
                                attr_value: $$("ddlPropBalconies").selectedValue(),
                                attr_text: $$("ddlPropBalconies").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAttachedBath").hasClass("hide") && $$("ddlPropAttachedBath").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "attached_bath_room",
                                attr_value: $$("ddlPropAttachedBath").selectedValue(),
                                attr_text: $$("ddlPropAttachedBath").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAttachedBalco").hasClass("hide") && $$("ddlPropAttachedBalco").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "attached_balcony",
                                attr_value: $$("ddlPropAttachedBalco").selectedValue(),
                                attr_text: $$("ddlPropAttachedBalco").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAssuredReturn").hasClass("hide") && $$("ddlPropAssuredReturn").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "assured_returns",
                                attr_value: $$("ddlPropAssuredReturn").selectedValue(),
                                attr_text: $$("ddlPropAssuredReturn").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAnyContrDone").hasClass("hide") && $$("ddlPropAnyConsDone").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "any_constructions_done",
                                attr_value: $$("ddlPropAnyConsDone").selectedValue(),
                                attr_text: $$("ddlPropAnyConsDone").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAgeofPG").hasClass("hide") && $$("ddlPropAgePG").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "age_of_pg",
                                attr_value: $$("ddlPropAgePG").selectedValue(),
                                attr_text: $$("ddlPropAgePG").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAgeofConstru").hasClass("hide") && $$("ddlPropAgeCont").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: data[0].item_no,
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "age_of_construction",
                                attr_value: $$("ddlPropAgeCont").selectedValue(),
                                attr_text: $$("ddlPropAgeCont").selectedValue(),
                            });
                        }
                        var item_no=data[0].item_no;
                        $.server.webMethodPOST("propon", "propon/item_attributes/", itemAttrData, function (data) {
                            if($$("ddlPricePlan").selectedValue()!="Select" && $$("ddlPricePlan").selectedValue()!="1"){
                                var itemPay = [];
                                itemPay.push({
                                    pay_id:"##GUID##",
                                    item_no: item_no,
                                    amount: 499,
                                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                    state_no: 30,
                                    user_id:window.shopOnContext.user.user_id
                                })
                                $.server.webMethodPOST("propon", "propon/item_payments/", itemPay, function (data) {
                                    msgPanel("Thank you for posting your ad..!");
                                    control.interface.subscribe("myads");
                                    control.interface.clearAllData();
                                })
                            } else {
                                msgPanel("Thank you for posting your ad..!");
                                control.interface.subscribe("myads");
                                control.interface.clearAllData();
                            }

                        })
                    })
                }
                else {
                    itemData[0].item_no = window.item_no;
                    $.server.webMethodPUT("propon", "propon/items/", itemData, function (data) {
                        var itemAttrData = [];
                        var itemNewImg = [];
                        $(control.imgData).each(function (i, item) {
                            if (item.item_attr_no != undefined) {
                                itemAttrData.push({
                                    item_no: window.item_no,
                                    item_attr_no: item.item_attr_no,
                                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                    attr_key: "image" + i + 1,
                                    attr_value: item.file_no,
                                    attr_text: item.file_no,
                                });
                            } else {
                                itemNewImg.push({
                                    item_no: window.item_no,
                                    item_attr_no: item.item_attr_no,
                                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                    attr_key: "image" + i + 1,
                                    attr_value: item.file_no,
                                    attr_text: item.file_no,
                                });
                            }

                        })
                        itemAttrData.push({
                            item_no: window.item_no,
                            item_attr_no: $$("lblPropLocId").value(),
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "location",
                            attr_value: $$("txtYourPropLoc").value(),
                            attr_text: $$("txtYourPropLoc").value(),
                        });
                        if (!$$("pnlBedRooms").hasClass("hide") && $$("ddlPropTypeBedRooms").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPropBedRoomsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "bed_room",
                                attr_value: $$("ddlPropTypeBedRooms").selectedValue(),
                                attr_text: $$("ddlPropTypeBedRooms").selectedValue() + " BHK",

                            });
                        }
                        if (!$$("pnlPropArea").hasClass("hide") && $$("txtAreaSize").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPropAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "area",
                                attr_value: $$("txtAreaSize").value(),
                                attr_text: $$("txtAreaSize").value() + " " + $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                            });
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAreaUnitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "area_unit",
                                attr_value: $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                                attr_text: $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropArea").hasClass("hide") && $$("ddlPropWhatisFor").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPropActionId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "action_type",
                                attr_value: $$("ddlPropWhatisFor").selectedValue(),
                                attr_text: $$("ddlPropWhatisFor").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropExpectedPrice").hasClass("hide") && $$("txtExpectedPrice").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblExpectedPriceId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "expected_price",
                                attr_value: $$("txtExpectedPrice").value(),
                                attr_text: $$("txtExpectedPrice").value(),
                            });
                        }
                        if (!$$("pnlPropExpectedRent").hasClass("hide") && $$("txtExpectedRent").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblExpectedRentId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "expected_rent",
                                attr_value: $$("txtExpectedRent").value(),
                                attr_text: $$("txtExpectedRent").value(),
                            });
                        }
                        if (!$$("pnlPropConstruStatus").hasClass("hide") && $$("ddlPropContStatus").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblConstStatusId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "construction_status",
                                attr_value: $$("ddlPropContStatus").selectedValue(),
                                attr_text: $$("ddlPropContStatus").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAvailFrom").hasClass("hide") && $$("dsPropAvailFrom").getDate() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAvailFromId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "avail_from",
                                attr_value: $$("dsPropAvailFrom").getDate(),
                                attr_text: $$("dsPropAvailFrom").getDate(),
                            });
                        }
                        if (!$$("pnlPropTransType").hasClass("hide") && $$("ddlPropTransType").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblTransTypeId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "transaction_type",
                                attr_value: $$("ddlPropTransType").selectedValue(),
                                attr_text: $$("ddlPropTransType").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropFurnStatus").hasClass("hide") && $$("ddlPropFurnStatus").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblFurnStatusId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "furnished_status",
                                attr_value: $$("ddlPropFurnStatus").selectedValue(),
                                attr_text: $$("ddlPropFurnStatus").selectedValue(),
                            });
                        }
                        if (!$$("pnlBathRooms").hasClass("hide") && $$("ddlPropTypeBathRooms").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblBathroomsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "bath_rooms",
                                attr_value: $$("ddlPropTypeBathRooms").selectedValue(),
                                attr_text: $$("ddlPropTypeBathRooms").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropFloorNumber").hasClass("hide") && $$("txtFloorNumber").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblFloorNoId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "floor_number",
                                attr_value: $$("txtFloorNumber").value(),
                                attr_text: $$("txtFloorNumber").value(),
                            });
                        }
                        if (!$$("pnlPropTotalFloors").hasClass("hide") && $$("txtTotalFloors").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblTotFloorsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "total_floors",
                                attr_value: $$("txtTotalFloors").value(),
                                attr_text: $$("txtTotalFloors").value(),
                            });
                        }
                        if (!$$("pnlPropFloorsAllowed").hasClass("hide") && $$("txtTotalFloorsAllowed").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblFloorsAllowId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "floors_allowed",
                                attr_value: $$("txtTotalFloorsAllowed").value(),
                                attr_text: $$("txtTotalFloorsAllowed").value(),
                            });
                        }
                        if (!$$("pnlPropAvailFor").hasClass("hide") && $$("ddlPropAvailFor").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAvailForId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "available_for",
                                attr_value: $$("ddlPropAvailFor").selectedValue(),
                                attr_text: $$("ddlPropAvailFor").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerProp").hasClass("hide") && $$("ddlPropCorner").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCornerPropId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_property",
                                attr_value: $$("ddlPropCorner").selectedValue(),
                                attr_text: $$("ddlPropCorner").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropOccupancy").hasClass("hide") && $$("ddlPropOccupancy").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblOccupancyId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "occupancy",
                                attr_value: $$("ddlPropOccupancy").selectedValue(),
                                attr_text: $$("ddlPropOccupancy").selectedValue(),
                            });
                        }
                        if (!$$("pnlWidthOfentrance").hasClass("hide") && $$("ddlPropWidthOfentrance").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblWidthOfentranceId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "width_of_entrance",
                                attr_value: $$("ddlPropWidthOfentrance").selectedValue(),
                                attr_text: $$("ddlPropWidthOfentrance").selectedValue(),
                            });
                        }
                        if (!$$("pnlWashRooms").hasClass("hide") && $$("ddlPropWashRooms").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPropWashRoomsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "wash_rooms",
                                attr_value: $$("ddlPropWashRooms").selectedValue(),
                                attr_text: $$("ddlPropWashRooms").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropTenantsYouPref").hasClass("hide") && $$("ddlPropTenantsYouPref").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblTenantsYouPrefId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "tenants_you_prefer",
                                attr_value: $$("ddlPropTenantsYouPref").selectedValue(),
                                attr_text: $$("ddlPropTenantsYouPref").selectedValue(),
                            });
                        }
                        if (!$$("pnlStampDuty").hasClass("hide")) {
                            var tenants_you_prefer = "1";
                            if ($("#chkStampDuty").prop('checked')) {
                                tenants_you_prefer = "1";
                            } else {
                                tenants_you_prefer = "0";
                            }

                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblchkStampDutyId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "tenants_you_prefer",
                                attr_value: tenants_you_prefer,
                                attr_text: tenants_you_prefer,
                            });
                        }
                        if (!$$("pnlPropRoadWidth").hasClass("hide") && $$("txtRoadWidth").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblRoadWidthId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "road_width",
                                attr_value: $$("txtRoadWidth").value(),
                                attr_text: $$("txtRoadWidth").value(),
                            });
                        }
                        if (!$$("pnlPropPricePerSeatM").hasClass("hide") && $$("txtPricePerSeatM").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPricePerSeatMId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "price_per_seats_monthly",
                                attr_value: $$("txtPricePerSeatM").value(),
                                attr_text: $$("txtPricePerSeatM").value(),
                            });
                        }
                        if (!$$("pnlPropPricePerSeatD").hasClass("hide") && $$("txtPricePerSeatD").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPricePerSeatDId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "price_per_seats_daily",
                                attr_value: $$("txtPricePerSeatD").value(),
                                attr_text: $$("txtPricePerSeatD").value(),
                            });
                        }
                        if (!$$("pnlPropPriceIncludes").hasClass("hide")) {
                            var price_includes = "";
                            if ($("#chkPLC").prop('checked')) {
                                price_includes = price_includes + "1";
                            }
                            if ($("#chkCarParking").prop('checked')) {
                                if (price_includes != "") {
                                    price_includes = price_includes + ",2";
                                } else {
                                    price_includes = price_includes + "2";
                                }
                            }
                            if ($("#chkClubMem").prop('checked')) {
                                if (price_includes != "") {
                                    price_includes = price_includes + ",3";
                                } else {
                                    price_includes = price_includes + "3";
                                }
                            }
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPriceIncludesId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "price_includes",
                                attr_value: price_includes,
                                attr_text: price_includes,
                            });
                        }
                        if (!$$("pnlPropGatedColony").hasClass("hide") && $$("ddlPropGatedColony").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblGatedColonyId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_gated_colony",
                                attr_value: $$("ddlPropGatedColony").selectedValue(),
                                attr_text: $$("ddlPropGatedColony").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropBoundWallmade").hasClass("hide") && $$("ddlPropBoundWallmade").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblBoundWallmadeId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_bound_wall_made",
                                attr_value: $$("ddlPropBoundWallmade").selectedValue(),
                                attr_text: $$("ddlPropBoundWallmade").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPerWashRoom").hasClass("hide") && $$("ddlPropPerWashRoom").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPerWashRoomId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "personal_washroom",
                                attr_value: $$("ddlPropPerWashRoom").selectedValue(),
                                attr_text: $$("ddlPropPerWashRoom").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPerKitchen").hasClass("hide") && $$("ddlPropPerKitchen").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPerKitchenId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "personal_kitchen",
                                attr_value: $$("ddlPropPerKitchen").selectedValue(),
                                attr_text: $$("ddlPropPerKitchen").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPerBathroom").hasClass("hide") && $$("ddlPropPerBathroom").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPerBathroomId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "personal_bathroom",
                                attr_value: $$("ddlPropPerBathroom").selectedValue(),
                                attr_text: $$("ddlPropPerBathroom").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropOpeningHours").hasClass("hide") && $$("txtOpeningHours").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblOpeningHoursId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "opening_hours",
                                attr_value: $$("txtOpeningHours").value(),
                                attr_text: $$("txtOpeningHours").value(),
                            });
                        }
                        if (!$$("pnlPropNoOfSeats").hasClass("hide") && $$("txtNoOfSeats").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblNoOfSeatsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "no_of_seats",
                                attr_value: $$("txtNoOfSeats").value(),
                                attr_text: $$("txtNoOfSeats").value(),
                            });
                        }
                        if (!$$("pnlNoofRoomsPg").hasClass("hide") && $$("ddlPropNoofRoomsPg").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblNoofRoomsPgId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "no_of_rooms_pg",
                                attr_value: $$("ddlPropNoofRoomsPg").selectedValue(),
                                attr_text: $$("ddlPropNoofRoomsPg").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropNearbyBusi").hasClass("hide") && $$("txtNearbyBusi").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblNearbyBusiId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "nearby_businesses",
                                attr_value: $$("txtNearbyBusi").value(),
                                attr_text: $$("txtNearbyBusi").value(),
                            });
                        }
                        if (!$$("pnlPropMonthlyRent").hasClass("hide") && $$("txtMonthlyRent").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblMonthlyRentId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "monthly_rent",
                                attr_value: $$("txtMonthlyRent").value(),
                                attr_text: $$("txtMonthlyRent").value(),
                            });
                        }
                        if (!$$("pnlPropLockinPeriods").hasClass("hide") && $$("txtLockinPeriods").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblLockinPeriodsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "lock_in_periods",
                                attr_value: $$("txtLockinPeriods").value(),
                                attr_text: $$("txtLockinPeriods").value(),
                            });
                        }
                        if (!$$("pnlPropLandZone").hasClass("hide") && $$("ddlPropLandZone").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblLandZoneId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "land_zone",
                                attr_value: $$("ddlPropLandZone").selectedValue(),
                                attr_text: $$("ddlPropLandZone").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropIsMainRoadFac").hasClass("hide") && $$("ddlPropIsMainRoadFac").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblIsMainRoadFacId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "is_main_road_facing",
                                attr_value: $$("ddlPropIsMainRoadFac").selectedValue(),
                                attr_text: $$("ddlPropIsMainRoadFac").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropIdealBusi").hasClass("hide") && $$("txtIdealBusi").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblIdealBusiId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "ideal_for_businesses",
                                attr_value: $$("txtIdealBusi").value(),
                                attr_text: $$("txtIdealBusi").value(),
                            });
                        }
                        if (!$$("pnlGettingBrokResp").hasClass("hide")) {
                            var getting_response_from_brokers = "1";
                            if ($("#chkGettingBrokResp").prop('checked')) {
                                getting_response_from_brokers = "1";
                            } else {
                                getting_response_from_brokers = "0";
                            }

                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblchkGettingBrokRespId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "getting_response_from_brokers",
                                attr_value: getting_response_from_brokers,
                                attr_text: getting_response_from_brokers,
                            });
                        }
                        if (!$$("pnlFurnishingDetails").hasClass("hide")) {
                            var furnishing_details = "";
                            if ($("#chkCupboards").prop('checked')) {
                                furnishing_details = furnishing_details + $("#chkCupboards").val();
                            }
                            if ($("#chkWashingMc").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkWashingMc").val();
                            }
                            if ($("#chkStudyTable").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkStudyTable").val();
                            }
                            if ($("#chkAC").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkAC").val();
                            }
                            if ($("#chkWifi").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkWifi").val();
                            }
                            if ($("#chkGeyser").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkGeyser").val();
                            }
                            if ($("#chkFridge").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkFridge").val();
                            }
                            if ($("#chkCooler").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkCooler").val();
                            }
                            if ($("#chkTV").prop('checked')) {
                                furnishing_details = furnishing_details + "," + $("#chkTV").val();
                            }

                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblchkFurnDetId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "furnishing_details",
                                attr_value: furnishing_details,
                                attr_text: furnishing_details,
                            });
                        }
                        if (!$$("pnlGettingBrokResp").hasClass("hide")) {
                            var electricity_water_charges_included = "1";
                            if ($("#chkElectricityWater").prop('checked')) {
                                electricity_water_charges_included = "1";
                            } else {
                                electricity_water_charges_included = "0";
                            }

                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblElectricityWaterId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "electricity_water_charges_included",
                                attr_value: electricity_water_charges_included,
                                attr_text: electricity_water_charges_included,
                            });
                        }
                        if (!$$("pnlPropSecurityAmt").hasClass("hide") && $$("txtSecurityAmt").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblSecurityAmtId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "security_amount",
                                attr_value: $$("txtSecurityAmt").value(),
                                attr_text: $$("txtSecurityAmt").value(),
                            });
                        }
                        if (!$$("pnlPropLaundry").hasClass("hide") && $$("txtLaundry").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblLaundryId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "laundry_amount",
                                attr_value: $$("txtLaundry").value(),
                                attr_text: $$("txtLaundry").value(),
                            });
                        }
                        if (!$$("pnlPropOtherChar").hasClass("hide") && $$("txtOtherChar").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblOtherCharId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "other_charges",
                                attr_value: $$("txtOtherChar").value(),
                                attr_text: $$("txtOtherChar").value(),
                            });
                        }
                        if (!$$("pnlPropElectricityAmt").hasClass("hide") && $$("txtElectricityAmt").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblElectricityAmtId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "electricity_amount",
                                attr_value: $$("txtElectricityAmt").value(),
                                attr_text: $$("txtElectricityAmt").value(),
                            });
                        }
                        if (!$$("pnlPropRentout").hasClass("hide") && $$("ddlPropRentout").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblRentoutId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "currently_rent_out",
                                attr_value: $$("ddlPropRentout").selectedValue(),
                                attr_text: $$("ddlPropRentout").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropLeasedout").hasClass("hide") && $$("ddlPropLeasedout").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblLeasedoutId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "currently_leased_out",
                                attr_value: $$("ddlPropLeasedout").selectedValue(),
                                attr_text: $$("ddlPropLeasedout").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerShow").hasClass("hide") && $$("ddlPropCornerShow").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCornerShowId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_showroom",
                                attr_value: $$("ddlPropCornerShow").selectedValue(),
                                attr_text: $$("ddlPropCornerShow").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerShop").hasClass("hide") && $$("ddlPropCornerProp").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCornerShopId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_shop",
                                attr_value: $$("ddlPropCornerProp").selectedValue(),
                                attr_text: $$("ddlPropCornerProp").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCornerPlot").hasClass("hide")) {
                            var corner_plot = "1";
                            if ($("#chkCornerPlot").prop('checked')) {
                                corner_plot = "1";
                            } else {
                                corner_plot = "0";
                            }

                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCornerPlotId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "corner_plot",
                                attr_value: corner_plot,
                                attr_text: corner_plot,
                            });
                        }
                        if (!$$("pnlPropCommonArea").hasClass("hide") && $$("ddlPropCommonArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCommonAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "common_area",
                                attr_value: $$("ddlPropCommonArea").selectedValue(),
                                attr_text: $$("ddlPropCommonArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPlotArea").hasClass("hide") && $$("ddlPropPlotArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPlotAreaUnitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_area_unit",
                                attr_value: $$("ddlPropPlotArea").selectedValue(),
                                attr_text: $$("ddlPropPlotArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropPlotArea").hasClass("hide") && $$("txtPlotArea").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblPlotAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "plot_area",
                                attr_value: $$("txtPlotArea").value(),
                                attr_text: $$("txtPlotArea").value(),
                            });
                        }
                        if (!$$("pnlPropCoveredArea").hasClass("hide") && $$("ddlPropCoveredArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCoveredAreaUnitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "covered_area_unit",
                                attr_value: $$("ddlPropCoveredArea").selectedValue(),
                                attr_text: $$("ddlPropCoveredArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCoveredArea").hasClass("hide") && $$("txtCoveredArea").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCoveredAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "covered_area",
                                attr_value: $$("txtCoveredArea").value(),
                                attr_text: $$("txtCoveredArea").value(),
                            });
                        }
                        if (!$$("pnlPropCarpet").hasClass("hide") && $$("ddlPropCoveredArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCarpetAreaUnitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "carpet_area_unit",
                                attr_value: $$("ddlPropCarpetArea").selectedValue(),
                                attr_text: $$("ddlPropCarpetArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropCarpet").hasClass("hide") && $$("txtCarpetArea").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCarpetAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "carpet_area",
                                attr_value: $$("txtCarpetArea").value(),
                                attr_text: $$("txtCarpetArea").value(),
                            });
                        }
                        if (!$$("pnlCafeteria").hasClass("hide") && $$("ddlPropCafeteria").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCafeteriaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "cafeteria",
                                attr_value: $$("ddlPropCafeteria").selectedValue(),
                                attr_text: $$("ddlPropCafeteria").selectedValue(),
                            });
                        }
                        if (!$$("pnlCabins").hasClass("hide") && $$("ddlPropCabins").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblCabinsId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "cabin_meeting_rooms",
                                attr_value: $$("ddlPropCabins").selectedValue(),
                                attr_text: $$("ddlPropCabins").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropSuperBuiltUpArea").hasClass("hide") && $$("ddlPropSuperBuiltUpArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblSuperBuiltUpAreaUnitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "super_built_up_area_unit",
                                attr_value: $$("ddlPropSuperBuiltUpArea").selectedValue(),
                                attr_text: $$("ddlPropSuperBuiltUpArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropSuperBuiltUpArea").hasClass("hide") && $$("txtSuperBuiltUpArea").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblSuperBuiltUpAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "super_built_up_area",
                                attr_value: $$("txtSuperBuiltUpArea").value(),
                                attr_text: $$("txtSuperBuiltUpArea").value(),
                            });
                        }
                        if (!$$("pnlPropBuiltUpArea").hasClass("hide") && $$("ddlPropBuiltUpArea").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblBuiltUpAreaId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "built_up_area_unit",
                                attr_value: $$("ddlPropBuiltUpArea").selectedValue(),
                                attr_text: $$("ddlPropBuiltUpArea").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropBuiltUpArea").hasClass("hide") && $$("txtBuiltUpArea").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblBuiltUpAreaUnitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "built_up_area",
                                attr_value: $$("txtBuiltUpArea").value(),
                                attr_text: $$("txtBuiltUpArea").value(),
                            });
                        }
                        if (!$$("pnlMainCharge").hasClass("hide") && $$("ddlMainCharge").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblMainChargeLimitId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "maintenance_charge_limit",
                                attr_value: $$("ddlMainCharge").selectedValue(),
                                attr_text: $$("ddlMainCharge").selectedValue(),
                            });
                        }
                        if (!$$("pnlMainCharge").hasClass("hide") && $$("txtMainCharge").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblMainChargeId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "maintenance_charge",
                                attr_value: $$("txtMainCharge").value(),
                                attr_text: $$("txtMainCharge").value(),
                            });
                        }
                        if (!$$("pnlPropBookingAmt").hasClass("hide") && $$("txtBookingAmt").value() != "") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblBookingAmtId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "booking_amount",
                                attr_value: $$("txtBookingAmt").value(),
                                attr_text: $$("txtBookingAmt").value(),
                            });
                        }
                        if (!$$("pnlBalconies").hasClass("hide") && $$("ddlPropBalconies").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblBalconiesId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "balconies",
                                attr_value: $$("ddlPropBalconies").selectedValue(),
                                attr_text: $$("ddlPropBalconies").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAttachedBath").hasClass("hide") && $$("ddlPropAttachedBath").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAttachedBathroomId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "attached_bath_room",
                                attr_value: $$("ddlPropAttachedBath").selectedValue(),
                                attr_text: $$("ddlPropAttachedBath").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAttachedBalco").hasClass("hide") && $$("ddlPropAttachedBalco").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAttachedBalcoId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "attached_balcony",
                                attr_value: $$("ddlPropAttachedBalco").selectedValue(),
                                attr_text: $$("ddlPropAttachedBalco").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAssuredReturn").hasClass("hide") && $$("ddlPropAssuredReturn").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAssuredReturId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "assured_returns",
                                attr_value: $$("ddlPropAssuredReturn").selectedValue(),
                                attr_text: $$("ddlPropAssuredReturn").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAnyContrDone").hasClass("hide") && $$("ddlPropAnyConsDone").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAnyConstDoneId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "any_constructions_done",
                                attr_value: $$("ddlPropAnyConsDone").selectedValue(),
                                attr_text: $$("ddlPropAnyConsDone").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAgeofPG").hasClass("hide") && $$("ddlPropAgePG").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAgePGId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "age_of_pg",
                                attr_value: $$("ddlPropAgePG").selectedValue(),
                                attr_text: $$("ddlPropAgePG").selectedValue(),
                            });
                        }
                        if (!$$("pnlPropAgeofConstru").hasClass("hide") && $$("ddlPropAgeCont").selectedValue() != "Select") {
                            itemAttrData.push({
                                item_no: window.item_no,
                                item_attr_no: $$("lblAgeConstId").value(),
                                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                                attr_key: "age_of_construction",
                                attr_value: $$("ddlPropAgeCont").selectedValue(),
                                attr_text: $$("ddlPropAgeCont").selectedValue(),
                            });
                        }
                        $.server.webMethodPUT("propon", "propon/item_attributes/", itemAttrData, function (data) {
                            if (itemNewImg.length > 0) {
                                $.server.webMethodPOST("propon", "propon/item_attributes/", itemNewImg, function (data) {
                                    msgPanel("Your Ad details updated..!");
                                    control.interface.subscribe("myads");
                                    control.interface.clearAllData();
                                })
                            } else {
                                msgPanel("Your Ad details updated..!");
                                control.interface.subscribe("myads");
                                control.interface.clearAllData();
                            }
                        })
                    })
                }

            }

        }
        control.interface.clearAllData = function () {
            $$("ddlPropWho").selectedValue("Select");
            $$("txtYourName").value("");
            $$("txtYourMobile").value("");
            $$("txtYourEmail").value("");
            $$("ddlPropWhatisFor").selectedValue("Select");
            $$("txtYourPropName").value("");
            $$("txtYourPropLoc").value("");
            $$("ddlBuilderName").selectedObject.val(-1);
            $$("ddlProjectName").selectedObject.val(-1);
            $$("pnlBuilderName").addClass("hide");
            $$("pnlPropertyProjName").addClass("hide");
            $$("ddlMainPropType").selectedObject.val("-1");
            $$("ddlPropType").selectedObject.val("-1");
            $$("ddlPropTypeBedRooms").selectedValue("Select");
            $$("txtAreaSize").value("");
            $$("ddlPropTypeAreaSizeUnit").selectedValue("Select");
            $$("txtExpectedPrice").value("");
            $$("txtExpectedRent").value("");
            $$("ddlPropContStatus").selectedValue("Select");
            $$("ddlPropFurnStatus").selectedValue("Select");
            $$("ddlPropTypeBathRooms").selectedValue("Select");
            $$("txtFloorNumber").value("");
            $$("dsPropAvailFrom").setDate("");
            $$("ddlPropAvailFor").selectedValue("Select");
            $$("ddlPropOccupancy").selectedValue("Select");
            $$("ddlPropTransType").selectedValue("Select");
            $$("txtTotalFloors").value("");
            $$("ddlPropCorner").selectedValue("Select");

            $$("ddlPropWidthOfentrance").selectedValue("Select");
            $$("txtTotalFloors").value("");
            $$("ddlPropWashRooms").selectedValue("Select");
            $$("ddlPropTenantsYouPref").selectedValue("Select");
            $("#chkStampDuty").prop("checked", false);
            $$("txtRoadWidth").value("");
            $$("txtPricePerSqFt").value("");
            $$("txtPricePerSeatM").value("");
            $$("txtPricePerSeatD").value("");
            $("#chkPLC").prop("checked", false);
            $("#chkCarParking").prop("checked", false);
            $("#chkClubMem").prop("checked", false);
            $$("ddlPropWashRooms").selectedValue("Select");
            $$("ddlPropBoundWallmade").selectedValue("Select");
            $$("ddlPropPerWashRoom").selectedValue("Select");
            $$("ddlPropPerKitchen").selectedValue("Select");
            $$("ddlPropPerBathroom").selectedValue("Select");
            $$("txtOpeningHours").value("");
            $$("txtNoOfSeats").value("");
            $$("ddlPropNoofRoomsPg").selectedValue("Select");
            $$("txtNearbyBusi").value("");
            $$("txtMonthlyRent").value("");
            $$("txtLockinPeriods").value("");
            $$("ddlPropLandZone").selectedValue("Select");
            $$("ddlPropIsMainRoadFac").selectedValue("Select");
            $$("txtIdealBusi").value("");
            $("#chkGettingBrokResp").prop("checked", false);
            $("#chkCupboards").prop("checked", false);
            $("#chkWashingMc").prop("checked", false);
            $("#chkStudyTable").prop("checked", false);
            $("#chkAC").prop("checked", false);
            $("#chkWifi").prop("checked", false);
            $("#chkFridge").prop("checked", false);
            $("#chkCooler").prop("checked", false);
            $("#chkTV").prop("checked", false);
            $("#chkGeyser").prop("checked", false);
            $("#chkElectricityWater").prop("checked", false);
            $$("txtSecurityAmt").value("");
            $$("txtLaundry").value("");
            $$("txtOtherChar").value("");
            $$("txtElectricityAmt").value("");
            $$("ddlPropRentout").selectedValue("Select");
            $$("ddlPropLeasedout").selectedValue("Select");
            $$("ddlPropCornerShow").selectedValue("Select");
            $$("ddlPropCornerProp").selectedValue("Select");
            $("#chkCornerPlot").prop("checked", false);
            $$("ddlPropCommonArea").selectedValue("Select");
            $$("txtPlotArea").value("");
            $$("ddlPropPlotArea").selectedValue("Select");
            $$("txtCoveredArea").value("");
            $$("ddlPropCoveredArea").selectedValue("Select");
            $$("txtCarpetArea").value("");
            $$("ddlPropCarpetArea").selectedValue("Select");
            $$("ddlPropCafeteria").selectedValue("Select");
            $$("ddlPropCabins").selectedValue("Select");
            $$("txtSuperBuiltUpArea").value("");
            $$("ddlPropSuperBuiltUpArea").selectedValue("Select");
            $$("txtBuiltUpArea").value("");
            $$("ddlPropBuiltUpArea").selectedValue("Select");
            $$("txtMainCharge").value("");
            $$("ddlMainCharge").selectedValue("Select");
            $$("txtBookingAmt").value("");
            $$("ddlPropBalconies").selectedValue("Select");
            $$("ddlPropAttachedBath").selectedValue("Select");
            $$("ddlPropAttachedBalco").selectedValue("Select");
            $$("ddlPropAssuredReturn").selectedValue("Select");
            $$("ddlPropAnyConsDone").selectedValue("Select");
            $$("ddlPropAgePG").selectedValue("Select");
            $$("ddlPropAgeCont").selectedValue("Select");
            $("#chkContactbyOwn").prop('checked', false);
            $("#chkContactView").prop('checked', false);
            $("#chkContactOnlybyme").prop('checked', false);
            $("#chkContactbyOwnMsg").prop('checked', false);
            $("#chkContactOnlybymeMsg").prop('checked', false);
        }
        control.events.btnContinueIfOwnerProp2_click = function () {
            if ($$("txtYourPropName").value().trim() == "") {
                msgPanel("Please enter your property description");
                $$("txtYourPropName").focus();
            } else if ($$("txtYourPropLoc").value().trim() == "") {
                msgPanel("Please enter your property location");
                $$("txtYourPropLoc").focus();
            } else if (!$$("pnlPropArea").hasClass("hide") && $$("txtAreaSize").value().trim() == "") {
                msgPanel("Please enter area size");
                $$("txtAreaSize").focus();
            } else {
                window.postData.item_name = $$("txtYourPropName").value();
                window.postData.location = $$("txtYourPropLoc").value();
                
                window.itemData =[]
                window.itemData.push({
                    item_no: "##GUID##",
                    action_type:($$("ddlPropWhatisFor").selectedValue()=="Sell" ? "Buy" : "Rent"),
                    item_name: window.postData.item_name,
                    location: window.postData.location,
                    org_id:window.shopOnContext.companyProduct.comp_prod_id,
                    name:window.postData.name,
                    mobile:window.postData.mobile,
                    email: window.postData.email,
                    ptype_no: $$("ddlPropType").selectedValue(),
                    user_id: window.shopOnContext.user.user_id,
                    owner_type:$$("ddlPropWho").selectedValue()
                });
                
                //$.server.webMethodPOST("propon", "propon/items/", window.itemData, function (data) {
                    window.itemAttrData1 = [];
                    //window.item_no = data[0].item_no;
                    window.itemAttrData1.push({
                        //item_no: data[0].item_no,
                        org_id: window.shopOnContext.companyProduct.comp_prod_id,
                        attr_key: "location",
                        attr_value: $$("txtYourPropLoc").value(),
                        attr_text: $$("txtYourPropLoc").value(),
                    });
                    if ($$("ddlPropTypeBedRooms").selectedValue() != "Select") {
                        window.itemAttrData1.push({
                            //item_no: data[0].item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "bed_room",
                            attr_value: $$("ddlPropTypeBedRooms").selectedValue(),
                            attr_text: $$("ddlPropTypeBedRooms").selectedValue() + " BHK",
                        });
                    }
                    if ($$("txtAreaSize").value() != "") {
                        window.itemAttrData1.push({
                            //item_no: data[0].item_no,
                            org_id: window.shopOnContext.companyProduct.comp_prod_id,
                            attr_key: "area",
                            attr_value: $$("txtAreaSize").value(),
                            attr_text: $$("txtAreaSize").value() + " " + $$("ddlPropTypeAreaSizeUnit").selectedValue(),
                        });
                    }
                    //$.server.webMethodPOST("propon", "propon/item_attributes/", window.itemAttrData, function (data) {
                        control.interface.loadAsPerType("prop", "three", $$("ddlPropType").selectedData(0).attr_keys.split(","));
                    //})
                //})
                
            }

        }
        control.events.htmlResidential_click = function () {
            $("[controlid=chkResidential]").prop('checked', true);
            $("[controlid=chkCommercial]").prop('checked', false);

            control.interface.loadAsPerType("residential", "one");
        }
        control.events.htmlCommercial_click = function () {
            $("[controlid=chkCommercial]").prop('checked', true);
            $("[controlid=chkResidential]").prop('checked', false);

            control.interface.loadAsPerType("commercial", "one");
        }

        control.events.btnOwnerResMore_click = function () {
            
            if($(".option-arrow").hasClass('glyphicon-chevron-down')){
                $(".option-arrow").addClass('glyphicon-chevron-up');
                $(".option-arrow").removeClass('glyphicon-chevron-down');
                $(".dropdown-content").css("display", "block");
            } else {
                $(".option-arrow").addClass('glyphicon-chevron-down');
                $(".option-arrow").removeClass('glyphicon-chevron-up');
                $(".dropdown-content").css("display", "none");
            }
            
        }

        control.interface.loadAsPerType = function (type,step, attr_key) {
            if (type == "prop") {
                //
                $$("pnlPropWho").addClass("hide");
                $$("pnlPersonName").addClass("hide");
                $$("pnlPersonMobile").addClass("hide");
                $$("pnlPersonEmail").addClass("hide");
                $$("pnlPropWhatisFor").addClass("hide");
                //one
                $$("pnlPropertyDetails").addClass("hide");
                $$("pnlImageUpload").addClass("hide");
                $$("pnlPropertyDesc").addClass("hide");
                //$$("pnlPropertyProjName").addClass("hide");
                $$("pnlPropertyLoc").addClass("hide");
                $$("pnlMainPropertyType").addClass("hide");
                $$("pnlPropertyType").addClass("hide");
                //$$("pnlBuilderName").addClass("hide");
                //two
                $$("pnlBedRooms").addClass("hide");
                $$("pnlPropArea").addClass("hide");
                //three
                $$("pnlPropWhatisFor").addClass("hide");
                $$("pnlPropExpectedPrice").addClass("hide");
                $$("pnlPropExpectedRent").addClass("hide");
                $$("pnlPropConstruStatus").addClass("hide");
                $$("pnlPropAvailFrom").addClass("hide");
                $$("pnlPropTransType").addClass("hide");
                //four
                $$("pnlPropFurnStatus").addClass("hide");
                $$("pnlBathRooms").addClass("hide");
                $$("pnlPropTotalFloors").addClass("hide");
                $$("pnlPropFloorsAllowed").addClass("hide");
                $$("pnlPropAvailFor").addClass("hide");
                $$("pnlPropCornerProp").addClass("hide");
                $$("pnlPropFloorNumber").addClass("hide");
                $$("pnlPropOccupancy").addClass("hide");
                $$("pnlWidthOfentrance").addClass("hide");
                $$("pnlWashRooms").addClass("hide");
                $$("pnlPropTenantsYouPref").addClass("hide");
                $$("pnlStampDuty").addClass("hide");
                $$("pnlPropRoadWidth").addClass("hide");
                $$("pnlPropPricePerSqFt").addClass("hide");
                $$("pnlPropPricePerSeatM").addClass("hide");
                $$("pnlPropPricePerSeatD").addClass("hide");
                $$("pnlPropPriceIncludes").addClass("hide");
                $$("pnlPropGatedColony").addClass("hide");
                $$("pnlPropBoundWallmade").addClass("hide");
                $$("pnlPropPerWashRoom").addClass("hide");
                $$("pnlPropPerKitchen").addClass("hide");
                $$("pnlPropPerBathroom").addClass("hide");
                $$("pnlPropOpeningHours").addClass("hide");
                $$("pnlPropNoOfSeats").addClass("hide");
                $$("pnlNoofRoomsPg").addClass("hide");
                $$("pnlPropNearbyBusi").addClass("hide");
                $$("pnlPropNearbyBusi").addClass("hide");
                $$("pnlPropMonthlyRent").addClass("hide");
                $$("pnlPropLockinPeriods").addClass("hide");
                $$("pnlPropLandZone").addClass("hide");
                $$("pnlPropIsMainRoadFac").addClass("hide");
                $$("pnlPropIdealBusi").addClass("hide");
                $$("pnlGettingBrokResp").addClass("hide");
                $$("pnlFurnishingDetails").addClass("hide");
                $$("pnlPropElectricityWater").addClass("hide");
                $$("pnlPropSecurityAmt").addClass("hide");
                $$("pnlPropLaundry").addClass("hide");
                $$("pnlPropOtherChar").addClass("hide");
                $$("pnlPropElectricityAmt").addClass("hide");
                $$("pnlPropRentout").addClass("hide");
                $$("pnlPropLeasedout").addClass("hide");
                $$("pnlPropCornerShow").addClass("hide");
                $$("pnlPropCornerShop").addClass("hide");
                $$("pnlPropCornerPlot").addClass("hide");
                $$("pnlPropCommonArea").addClass("hide");
                $$("pnlPropPlotArea").addClass("hide");
                $$("pnlPropCoveredArea").addClass("hide");
                $$("pnlPropCarpet").addClass("hide");
                $$("pnlCafeteria").addClass("hide");
                $$("pnlCabins").addClass("hide");
                $$("pnlPropSuperBuiltUpArea").addClass("hide");
                $$("pnlPropBuiltUpArea").addClass("hide");
                $$("pnlMainCharge").addClass("hide");
                $$("pnlPropBookingAmt").addClass("hide");
                $$("pnlBalconies").addClass("hide");
                $$("pnlPropAttachedBath").addClass("hide");
                $$("pnlPropAttachedBalco").addClass("hide");
                $$("pnlPropAssuredReturn").addClass("hide");
                $$("pnlPropAnyContrDone").addClass("hide");
                $$("pnlPropAgeofPG").addClass("hide");
                $$("pnlPropAgeofConstru").addClass("hide");
                //buttons
                //$$("pnlPropBtnStep2").addClass("hide");
                //$$("pnlPropBtnStep3").addClass("hide");
                $$("pnlPropBtnStep4").addClass("hide");
                
                $$("pnlPropPricePlan").addClass("hide");
                //showing for start looking
                $$("pnlPropWho").removeClass("hide");
                $$("pnlPersonName").removeClass("hide");
                $$("pnlPersonMobile").removeClass("hide");
                $$("pnlPersonEmail").removeClass("hide");
                $$("pnlPropWhatisFor").removeClass("hide");

                if (step == "one") {
                    $$("pnlPropertyDetails").removeClass("hide");
                    $$("pnlPropWho").removeClass("hide");
                    $$("pnlPersonName").removeClass("hide");
                    $$("pnlPersonMobile").removeClass("hide");
                    $$("pnlPersonEmail").removeClass("hide");
                    $$("pnlPropWhatisFor").removeClass("hide");
                    $$("pnlPropertyDesc").removeClass("hide");
                    //$$("pnlPropertyProjName").removeClass("hide");
                    $$("pnlPropertyLoc").removeClass("hide");
                    $$("pnlMainPropertyType").removeClass("hide");
                    $$("pnlPropPricePlan").removeClass("hide");
                    if(window.isEdit){
                        $$("ddlPricePlan").disable(true);
                    }else{
                        $$("ddlPricePlan").disable(false);
                    }
                    
                }
                if (step == "two") {
                    $$("pnlPropertyDetails").removeClass("hide");
                    $$("pnlPropWho").removeClass("hide");
                    $$("pnlPersonName").removeClass("hide");
                    $$("pnlPersonMobile").removeClass("hide");
                    $$("pnlPersonEmail").removeClass("hide");
                    $$("pnlPropWhatisFor").removeClass("hide");
                    $$("pnlPropertyDesc").removeClass("hide");
                    //$$("pnlPropertyProjName").removeClass("hide");
                    $$("pnlPropertyLoc").removeClass("hide");
                    $$("pnlMainPropertyType").removeClass("hide");
                    $$("pnlPropertyType").removeClass("hide");
                    $$("pnlPropPricePlan").removeClass("hide");
                    if(window.isEdit){
                        $$("ddlPricePlan").disable(true);
                    }else{
                        $$("ddlPricePlan").disable(false);
                    }                   

                }
            else if (step == "three") {
                $$("pnlPropertyDetails").removeClass("hide");
                $$("pnlPropWho").removeClass("hide");
                $$("pnlPersonName").removeClass("hide");
                $$("pnlPersonMobile").removeClass("hide");
                $$("pnlPersonEmail").removeClass("hide");
                $$("pnlPropWhatisFor").removeClass("hide");
                $$("pnlPropertyDesc").removeClass("hide");
                //$$("pnlPropertyProjName").removeClass("hide");
                $$("pnlPropertyLoc").removeClass("hide");
                $$("pnlMainPropertyType").removeClass("hide");
                $$("pnlPropertyType").removeClass("hide");
                $$("pnlPropPricePlan").removeClass("hide");
                if(window.isEdit){
                    $$("ddlPricePlan").disable(true);
                }else{
                    $$("ddlPricePlan").disable(false);
                }                
                $$("pnlImageUpload").removeClass("hide");

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "bed_room"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlBedRooms").removeClass("hide");
                } else {
                    $$("pnlBedRooms").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropArea").removeClass("hide");
                } else {
                    $$("pnlPropArea").addClass("hide");
                }

                //four
                

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "expected_price"
                })
                if (isAttrExsist.length > 0 && $$("ddlPropWhatisFor").selectedValue() == "Sell") {
                    $$("pnlPropExpectedPrice").removeClass("hide");
                } else {
                    $$("pnlPropExpectedPrice").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "expected_rent"
                })
                if (isAttrExsist.length > 0 && $$("ddlPropWhatisFor").selectedValue() == "Rent") {
                    $$("pnlPropExpectedRent").removeClass("hide");
                } else {
                    $$("pnlPropExpectedRent").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "construction_status"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropConstruStatus").removeClass("hide");
                } else {
                    $$("pnlPropConstruStatus").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "avail_from"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAvailFrom").removeClass("hide");
                } else {
                    $$("pnlPropAvailFrom").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "transaction_type"//new property or resale
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropTransType").removeClass("hide");
                } else {
                    $$("pnlPropTransType").addClass("hide");
                }

                //five

                $$("pnlPropBtnStep4").removeClass("hide");

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "furnished_status"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropFurnStatus").removeClass("hide");
                } else {
                    $$("pnlPropFurnStatus").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "bath_rooms"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlBathRooms").removeClass("hide");
                } else {
                    $$("pnlBathRooms").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "total_floors"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropTotalFloors").removeClass("hide");
                } else {
                    $$("pnlPropTotalFloors").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "floors_allowed"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropFloorsAllowed").removeClass("hide");
                } else {
                    $$("pnlPropFloorsAllowed").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "available_for"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAvailFor").removeClass("hide");
                } else {
                    $$("pnlPropAvailFor").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "corner_property"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCornerProp").removeClass("hide");
                } else {
                    $$("pnlPropCornerProp").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "floor_number"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropFloorNumber").removeClass("hide");
                } else {
                    $$("pnlPropFloorNumber").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "occupancy"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropOccupancy").removeClass("hide");
                } else {
                    $$("pnlPropOccupancy").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "age_of_construction"
                })
                if (isAttrExsist.length > 0 && $$("ddlPropWhatisFor").selectedValue() == "Rent") {
                    $$("pnlPropAgeofConstru").removeClass("hide");
                } else {
                    $$("pnlPropAgeofConstru").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "age_of_pg"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAgeofPG").removeClass("hide");
                } else {
                    $$("pnlPropAgeofPG").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "any_constructions_done"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAnyContrDone").removeClass("hide");
                } else {
                    $$("pnlPropAnyContrDone").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "assured_returns"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAssuredReturn").removeClass("hide");
                } else {
                    $$("pnlPropAssuredReturn").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "attached_balcony"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAttachedBalco").removeClass("hide");
                } else {
                    $$("pnlPropAttachedBalco").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "attached_bath_room"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAttachedBath").removeClass("hide");
                } else {
                    $$("pnlPropAttachedBath").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "balconies"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlBalconies").removeClass("hide");
                } else {
                    $$("pnlBalconies").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "booking_amount"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropBookingAmt").removeClass("hide");
                } else {
                    $$("pnlPropBookingAmt").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "maintenance_charge"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlMainCharge").removeClass("hide");
                } else {
                    $$("pnlMainCharge").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "built_up_area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropBuiltUpArea").removeClass("hide");
                } else {
                    $$("pnlPropBuiltUpArea").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "super_built_up_area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropSuperBuiltUpArea").removeClass("hide");
                } else {
                    $$("pnlPropSuperBuiltUpArea").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "cabin_meeting_rooms"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlCabins").removeClass("hide");
                } else {
                    $$("pnlCabins").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "cafeteria"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlCafeteria").removeClass("hide");
                } else {
                    $$("pnlCafeteria").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "carpet_area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCarpet").removeClass("hide");
                } else {
                    $$("pnlPropCarpet").addClass("hide");
                }


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "covered_area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCoveredArea").removeClass("hide");
                } else {
                    $$("pnlPropCoveredArea").addClass("hide");
                }


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "plot_area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPlotArea").removeClass("hide");
                } else {
                    $$("pnlPropPlotArea").addClass("hide");
                }


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "common_area"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCommonArea").removeClass("hide");
                } else {
                    $$("pnlPropCommonArea").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "corner_plot"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCornerPlot").removeClass("hide");
                } else {
                    $$("pnlPropCornerPlot").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "corner_shop"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCornerShop").removeClass("hide");
                } else {
                    $$("pnlPropCornerShop").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "corner_showroom"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCornerShow").removeClass("hide");
                } else {
                    $$("pnlPropCornerShow").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "currently_leased_out"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropLeasedout").removeClass("hide");
                } else {
                    $$("pnlPropLeasedout").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "currently_rent_out"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropRentout").removeClass("hide");
                } else {
                    $$("pnlPropRentout").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "electricity_amount"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropElectricityAmt").removeClass("hide");
                } else {
                    $$("pnlPropElectricityAmt").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "other_charges"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropOtherChar").removeClass("hide");
                } else {
                    $$("pnlPropOtherChar").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "laundry_amount"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropLaundry").removeClass("hide");
                } else {
                    $$("pnlPropLaundry").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "security_amount"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropSecurityAmt").removeClass("hide");
                } else {
                    $$("pnlPropSecurityAmt").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "electricity_water_charges_included"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropElectricityWater").removeClass("hide");
                } else {
                    $$("pnlPropElectricityWater").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "furnishing_details"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlFurnishingDetails").removeClass("hide");
                } else {
                    $$("pnlFurnishingDetails").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "getting_response_from_brokers"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlGettingBrokResp").removeClass("hide");
                } else {
                    $$("pnlGettingBrokResp").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "ideal_for_businesses"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropIdealBusi").removeClass("hide");
                } else {
                    $$("pnlPropIdealBusi").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "is_main_road_facing"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropIsMainRoadFac").removeClass("hide");
                } else {
                    $$("pnlPropIsMainRoadFac").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "land_zone"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropLandZone").removeClass("hide");
                } else {
                    $$("pnlPropLandZone").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "lock_in_periods"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropLockinPeriods").removeClass("hide");
                } else {
                    $$("pnlPropLockinPeriods").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "monthly_rent"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropMonthlyRent").removeClass("hide");
                } else {
                    $$("pnlPropMonthlyRent").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "nearby_businesses"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropNearbyBusi").removeClass("hide");
                } else {
                    $$("pnlPropNearbyBusi").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "notice_period"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropNearbyBusi").removeClass("hide");
                } else {
                    $$("pnlPropNearbyBusi").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "no_of_rooms_pg"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlNoofRoomsPg").removeClass("hide");
                } else {
                    $$("pnlNoofRoomsPg").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "no_of_seats"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropNoOfSeats").removeClass("hide");
                } else {
                    $$("pnlPropNoOfSeats").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "opening_hours"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropOpeningHours").removeClass("hide");
                } else {
                    $$("pnlPropOpeningHours").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "personal_bathroom"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPerBathroom").removeClass("hide");
                } else {
                    $$("pnlPropPerBathroom").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "personal_kitchen"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPerKitchen").removeClass("hide");
                } else {
                    $$("pnlPropPerKitchen").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "personal_washroom"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPerWashRoom").removeClass("hide");
                } else {
                    $$("pnlPropPerWashRoom").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "plot_bound_wall_made"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropBoundWallmade").removeClass("hide");
                } else {
                    $$("pnlPropBoundWallmade").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "plot_gated_colony"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropGatedColony").removeClass("hide");
                } else {
                    $$("pnlPropGatedColony").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "price_includes"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPriceIncludes").removeClass("hide");
                } else {
                    $$("pnlPropPriceIncludes").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "price_per_seats_daily"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPricePerSeatD").removeClass("hide");
                } else {
                    $$("pnlPropPricePerSeatD").addClass("hide");
                }


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "price_per_seats_monthly"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPricePerSeatM").removeClass("hide");
                } else {
                    $$("pnlPropPricePerSeatM").addClass("hide");
                }


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "price_per_sq_ft"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropPricePerSqFt").removeClass("hide");
                } else {
                    $$("pnlPropPricePerSqFt").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "road_width"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropRoadWidth").removeClass("hide");
                } else {
                    $$("pnlPropRoadWidth").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "stamp_duty_registration_charges_excluded"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlStampDuty").removeClass("hide");
                } else {
                    $$("pnlStampDuty").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "tenants_you_prefer"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropTenantsYouPref").removeClass("hide");
                } else {
                    $$("pnlPropTenantsYouPref").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "wash_rooms"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlWashRooms").removeClass("hide");
                } else {
                    $$("pnlWashRooms").addClass("hide");
                }


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "width_of_entrance"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlWidthOfentrance").removeClass("hide");
                } else {
                    $$("pnlWidthOfentrance").addClass("hide");
                }

            }

            else if (step == "four") {

                $$("pnlPropBtnStep3").removeClass("hide");

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "expected_price"
                })
                if (isAttrExsist.length > 0 && $$("ddlPropWhatisFor").selectedValue()=="Sell") {
                    $$("pnlPropExpectedPrice").removeClass("hide");
                } else {
                    $$("pnlPropExpectedPrice").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "expected_rent"
                })
                if (isAttrExsist.length > 0 && $$("ddlPropWhatisFor").selectedValue() == "Rent") {
                    $$("pnlPropExpectedRent").removeClass("hide");
                } else {
                    $$("pnlPropExpectedRent").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "construction_status"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropConstruStatus").removeClass("hide");
                } else {
                    $$("pnlPropConstruStatus").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "avail_from"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAvailFrom").removeClass("hide");
                } else {
                    $$("pnlPropAvailFrom").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "transaction_type"//new property or resale
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropTransType").removeClass("hide");
                } else {
                    $$("pnlPropTransType").addClass("hide");
                }

                
            }
            else if (step == "five") {

                $$("pnlPropBtnStep4").removeClass("hide");


                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "furnished_status"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropFurnStatus").removeClass("hide");
                } else {
                    $$("pnlPropFurnStatus").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "bath_rooms"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlBathRooms").removeClass("hide");
                } else {
                    $$("pnlBathRooms").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "total_floors"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropTotalFloors").removeClass("hide");
                } else {
                    $$("pnlPropTotalFloors").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "floors_allowed"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropFloorsAllowed").removeClass("hide");
                } else {
                    $$("pnlPropFloorsAllowed").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "available_for"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropAvailFor").removeClass("hide");
                } else {
                    $$("pnlPropAvailFor").addClass("hide");
                }
                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "corner_property"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropCornerProp").removeClass("hide");
                } else {
                    $$("pnlPropCornerProp").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "floor_number"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropFloorNumber").removeClass("hide");
                } else {
                    $$("pnlPropFloorNumber").addClass("hide");
                }

                var isAttrExsist = attr_key.filter(function (find) {
                    return find == "occupancy"
                })
                if (isAttrExsist.length > 0) {
                    $$("pnlPropOccupancy").removeClass("hide");
                } else {
                    $$("pnlPropOccupancy").addClass("hide");
                }

            }
        }
          
        }


        
        

    })
}

$.fn.compProdSelection = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        control.interface.subscribe = null;

        control.interface.main = function () {// $(control.selectedObject).removeClass("hide");
            // if (shopOnContext.companyProduct.comp_id) {
            //     setTimeout(function () {
            //         $$("grdCompProdSelection").selectionChanged(null, shopOnContext.companyProduct);
            //         $.server.webMethodGET("eshopon", "shopon/settings?filter_expression=comp_id=" + window.shopOnContext.companyProduct.comp_prod_id
            //             //+ " and sett_key in ('DEFAULT_STORE_NO','DEFAULT_REGISTER_NO','SHOPON_COMP_ID','SHOPON_STORE_NO','SHOPON_AUTH_TOKEN','ENABLE_IMAGE_DOWNLOAD_PATH')"
            //             , function (data) {
            //                 //window.settings = {};
            //                 $(data).each(function (i, item) {
            //                     if (item.value_1.toUpperCase() == "TRUE")
            //                         window.settings[item.sett_key] = true;
            //                     else if (item.value_1.toUpperCase() == "FALSE")
            //                         window.settings[item.sett_key] = false;
            //                     else
            //                         window.settings[item.sett_key] = item.value_1;
            //                 })
            //                 if (window.settings.initialSearch == "All") {
            //                     control.interface.itemInitialSearch(1);
            //                 }
            //             });

            //     }, 50);

            // }
        }
        control.interface.hide = function () {
            $(control.selectedObject).addClass("hide");
        }
        control.events.page_load = function () {
            
                $$("txtSimpleList1").dataBind({
                    getData: function (text, fn) {
    
                        fn([{value:"2",label:"aaaaaaaa"},
                        {value:"3",label:"aa111aaaaaa"},{value:"233",label:"aaaa4444aaaa"}
                        ,{value:"4",label:"aaa333aaaaa"}]);
    
                    }
                });

                $$("txtSimpleList2").dataBind({
                    getData: function (text, fn) {
    
                        fn([{value:"2",label:"aaaaaaaa"},
                        {value:"3",label:"aa111aaaaaa"},{value:"233",label:"aaaa4444aaaa"}
                        ,{value:"4",label:"aaa333aaaaa"}]);
    
                    }
                });

                $$("txtSimpleList3").dataBind({
                    getData: function (text, fn) {
    
                        fn([{value:"2",label:"aaaaaaaa"},
                        {value:"3",label:"aa111aaaaaa"},{value:"233",label:"aaaa4444aaaa"}
                        ,{value:"4",label:"aaa333aaaaa"}]);
    
                    }
                });

                $$("txtSimpleList4").dataBind({
                    getData: function (text, fn) {
    
                        fn([{value:"2",label:"aaaaaaaa"},
                        {value:"3",label:"aa111aaaaaa"},{value:"233",label:"aaaa4444aaaa"}
                        ,{value:"4",label:"aaa333aaaaa"}]);
    
                    }
                });
        
  
            //Design the grid
            $$("grdCompProdSelection").width("100%");
            $$("grdCompProdSelection").height("auto");
            $$("grdCompProdSelection").setTemplate({
                selection: "Single",
                columns: [{
                    'name': "comp_prod_name",
                    'width': "100%",
                    'dataField': "comp_prod_name,comp_prod_image",
                    itemTemplate: ' <div class="comp_prod_row"><a><span>  {comp_prod_name}</span> </a></div>'
                }]
            });
            $$("grdCompProdSelection").selectionChanged = function (row, item) {
                control.interface.subscribe("compProdSelected", item);
                setTimeout(function () {
                    if (localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id) != "" && localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id) != null) {
                        window.shopOnContext.isLoggedIn = true;

                            window.shopOnContext.user = JSON.parse(localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id));
                            control.interface.subscribe("loginSuccess", localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id));
                            control.interface.subscribe("compProdSelected");


                        //navigate("ucCompProdSelection");
                    }

                }, 1000)
            }
            //Get the company product where online app purchase is made.
            $.server.webMethodGET("cloud-open", "cloud/companyproduct?start_record=0&end_record=10&filter_expression=cpt.prod_id in (37) and cpt.app_flag=1", function (data) {
                //Bind the data and hide header for better design
                $$("grdCompProdSelection").dataBind(data);
                gridToRepeater($$("grdCompProdSelection"));
            }, function (err) {
                msgPanel(err);
            });

        }
    });
}


$.fn.header = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        control.interface.subscribe = null;

        control.interface.main = function (isLoggedIn) {

            //             control.data.categories.search(function (data) {
            //                 control.view.sideMenu(data);
            //             });
            $(control.selectedObject).removeClass("hide");
            $$("pnlMobilMenuBar").addClass("desktop-hide");
            setTimeout(function () {
                if (localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id) != "" && localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id) != null) {
                    window.shopOnContext.isLoggedIn = true;

                    window.shopOnContext.user = JSON.parse(localStorage.getItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id));
                        $$("lnkUserName").removeClass("hide");
                        $$("lnkMy").removeClass("hide");
                        $$("pnlLoggedIn").removeClass("hide");
                        $$("lnkLogout").removeClass("hide");
                        $$("lnkUserName").html(window.shopOnContext.user.full_name + '<i class="fa fa-caret-down"></i>');
                        $$("lblMyName").html(window.shopOnContext.user.full_name);
                        $$("btnLoginIcon").addClass("hide");
                        $$("btnLoginOKIcon").removeClass("hide");
                        $$("lnkLogin").addClass("hide");
                        $("#myTopnav").removeClass("hide");
                } else {
                    $$("lnkLogin").removeClass("hide");
                    $$("lnkUserName").addClass("hide");
                    $$("lnkLogout").addClass("hide");
                    $$("lnkMy").addClass("hide");
                    $$("pnlLoggedIn").addClass("hide");
                    $$("lnkUserName").html("");
                    $$("lblMyName").html("");
                    $$("btnLoginIcon").addClass("hide");
                    $$("btnLoginOKIcon").addClass("hide");
                    $("#myTopnav").addClass("hide");

                }

            }, 1000)
            

            /*if (window.shopOnContext.isLoggedIn == true) {

                $$("Hello").removeClass("hide");
                $$("lnkUserName").html(window.shopOnContext.user.full_name).removeClass("hide");
                $$("btnLogout").removeClass("hide");
                $$("btnLogin").addClass("hide");
                $$("btnSignup").addClass("hide");
                $$("btnLoginIcon").addClass("hide");
                $$("btnLoginOKIcon").removeClass("hide");
            } else {
                $$("Hello").addClass("hide");
                $$("btnLogout").addClass("hide");
                $$("btnLogin").removeClass("hide");
                $$("btnSignup").removeClass("hide");
                $$("btnLoginIcon").removeClass("hide");
                $$("btnLoginOKIcon").addClass("hide");

            }*/

        }
        control.events = {
            btnSearch_click: function () {
                if (control.interface.subscribe)
                    control.interface.subscribe("propertySearch", {
                        location: control.view.searchLocation(),
                        actionType: control.view.searchActionType(),
                        prodType: control.view.searchProdTypes(),
                        price: control.view.searchPrice()
                    });
            },
            btnBuy_click: function () {
                control.interface.subscribe("propertySearch", {
                    location: "",
                    actionType: "BUY",
                    prodType: "",
                    price: ""
                });
                closeNav();
            },
            btnSell_click: function () {
                if (window.shopOnContext.isLoggedIn) {
                    control.interface.subscribe("postfreead");
                    window.isEdit = false;
                }
                else {
                    control.interface.subscribe("login");
                }
                closeNav();
            },
            btnFreeAd_click: function () {
                if (window.shopOnContext.isLoggedIn) {
                    control.interface.subscribe("postfreead");
                    window.isEdit = false;
                }
                else {
                    control.interface.subscribe("login");
                }
            },
        btnRent_click: function () {
            control.interface.subscribe("propertySearch", {
                location: "",
                actionType: "RENT",
                prodType: "",
                price: ""
            });
            closeNav();
        },
        btnBranchCourse_click : function () {
                control.interface.subscribe("propertySearch", {
                    location: "",
                    actionType: "RENT",
                    prodType: "",
                    price: ""
                });
        },
        btnPostAd_click : function () {
            if(window.shopOnContext.isLoggedIn){
                control.interface.subscribe("postfreead");
                window.isEdit = false;
            }
            else {
                control.interface.subscribe("login");
            }
        },
        lblHome_click: function () {
            control.interface.subscribe("home", null);
            closeNav();
        },
        lblAppName_click : function () {
            control.interface.subscribe("home", null);
        },
        btnBatchStudent_click : function () {
            if (window.shopOnContext.isLoggedIn) {
                control.interface.subscribe("postfreead");
                window.isEdit = false;
            }
            else {
                control.interface.subscribe("login");
            }
        },
        btnStudents_click : function () {
            control.interface.subscribe("students", "");
        },
        btnStaffs_click : function () {
            control.interface.subscribe("staffs", "");
        },
        btnFees_click : function () {
            control.interface.subscribe("fees", "");
        },

        btnHome_click : function () {
            //control.interface.showHome();
            control.interface.subscribe("propertySearch", {
                location: "",
                actionType: "BUY",
                prodType: "",
                price: ""
            });
        },
        lnkLogin_click: function () {
            control.interface.subscribe("login");
        },
        btnLogout_click : function () {
            location.reload();
            localStorage.setItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id, "");
            localStorage.setItem("propon_login_data_" + window.shopOnContext.companyProduct.comp_prod_id, "");
            window.shopOnContext.user = [];
        },
        btnMyAds1_click : function () {
            control.events.btnMyAds_click();
        },
        btnMyAccount1_click: function () {
            control.events.btnMyAccount_click();
        },
        btnMessaging1_click: function () {
            control.events.btnMessaging_click();
        },
        btnMyAds_click : function () {
            if (window.shopOnContext.isLoggedIn == true) {
                control.interface.subscribe("myads");
            }
            else {
                msgPanel("Please login to view your ads");
            }
            closeNav();
        },
        btnMyAccount_click: function () {
            if (window.shopOnContext.isLoggedIn == true) {
                control.interface.subscribe("myaccount");
            }
            else {
                msgPanel("Please login to access your account");
            }
            closeNav();
        },
            btnMessaging_click: function () {
                if (window.shopOnContext.isLoggedIn == true) {
                    control.interface.subscribe("messaging");
                }
                else {
                    msgPanel("Please login to view messaging");
                }
                closeNav();
            }

        }
        
        control.cache = {};
        control.view = {
            sideMenu: function (data) {
                if (typeof (data) !== "undefined") {
                    control.cache.sideMenu = data;
                    var contentHTML = " <a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>";
                    for (var d in data) {
                        d = data[d];
                        if (d.cat_no_par == null)
                            contentHTML = contentHTML + "<a href='#'>" + d.cat_name + "</a>";
                    }
                    $($$("mnSideMenu")).html(contentHTML);
                }
                return control.cache.sideMenu;
            }
        };
        control.data = {
            categories: {
                search: function (fn) {
                    $.server.webMethodGET("propon", "propon/categories", function (data) {
                        fn(data);
                    });
                }
            }
        }

    });
}

$.fn.login = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        // control.userAPI = new UserAPI();
        //control.customerAPI = new CustomerAPI();
        control.interface.subscribe = null;
        // $(".img__btn").click(function(){
        //     $(".cont").toggleClass("s--signup");
        // })
        control.interface.main = function () {
            $(control.selectedObject).removeClass("hide");
            $(control.selectedObject).find(".login-panel").removeClass("hide");
            $(control.selectedObject).find(".signup-panel").addClass("hide");
        }

        control.interface.showForgotPwd = function () {//$(control.selectedObject).removeClass("hide");
            //$(control.selectedObject).find(".signup-panel").removeClass("hide");
            //$(control.selectedObject).find(".login-panel").removeClass("hide");
            //$(control.selectedObject).find(".forgot-password-panel").addClass("hide");
        }
        control.interface.showSignUp = function () {
            $(control.selectedObject).removeClass("hide");
            $(control.selectedObject).find(".login-panel").addClass("hide");
            $(control.selectedObject).find(".signup-panel").removeClass("hide");
        }
        control.interface.hide = function () {
            $(control.selectedObject).addClass("hide");
        }

        control.interface.loginSuccess = null;
        control.events.btnForgotPwd_click = function () {
            control.interface.showForgotPwd();
        }
        control.events.btnVerify_click = function(){
            
            var filter=" user_id="+window.shopOnContext.user.user_id+" and otp="+$$("txtOTP").value();
            //$.server.webMethodGET("iam", "iam/user/" , function (permissions) {
            $.server.webMethodGET("iam", "iam/usersearch?start_record=0&end_record=100&filter_expression=" + encodeURIComponent(filter)+"&comp_id="+window.shopOnContext.companyProduct.comp_id , function (user) {
                if(user.length>0){
                    var userData={
                        full_name:window.shopOnContext.user.full_name,
                        user_id:user[0].user_id,
                        active:1
                    }
                    $.server.webMethodPUT("iam","iam/users/" + userData.user_id, userData, function (data) {
                        $$("pnlVerifyOTP").addClass("hide");
                        $$("pnlSigninPwd").removeClass("hide");
                        $$("pnlSigninuser").removeClass("hide");
                        $$("pnlVerifyOTPButton").addClass("hide");
                        $$("pnlSignInButton").removeClass("hide");
                        localStorage.setItem("propon_login_data_" + window.shopOnContext.companyProduct.comp_prod_id, JSON.stringify(user[0]));
                        localStorage.setItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id, JSON.stringify(user[0]));
                        control.interface.subscribe("loginSuccess", user[0]);
                    })
                }else{
                    msgPanel("Incorrect OTP please check...!");
                }
            })
        }
        control.events.btnSignIn_click = function () {
            if($$("txtEmail").val().trim()==""){
                msgPanel("Please enter your registered mobile number or email id");
            }else if($$("txtPassword").val().trim()==""){
                msgPanel("Please enter your password");
            }
            else{
                var email=false,mobile=false;
                var email=(isNaN($$("txtEmail").val()));
                var mobile=(!isNaN($$("txtEmail").val()));
                var data={
                    "password": $$("txtPassword").val(),
                    "comp_id": window.shopOnContext.companyProduct.comp_id
                }
                if(!mobile){
                    data.email_id=$$("txtEmail").val();
                }else{
                    data.phone_no=$$("txtEmail").val();
                }
                //Verify User is vali for Cloud Registered Company
                $.server.webMethodPOST("iam", "iam/tokens/", data, function (user) {
                    window.userData=user[0];
                    //Verify User has access to the ShopOn Product. 
                    $.server.webMethodGET("iam", "iam/user-permissions/" + user[0].user_id + "?user_id=" + user[0].user_id + "&obj_id=37&obj_type=Product::CompProd&comp_prod_id=" + window.shopOnContext.companyProduct.comp_prod_id, function (permissions) {
                        
                        //Check atleast has access to one store
                        if (permissions.length > 0) {
                            window.shopOnContext.isLoggedIn = true;
                            window.shopOnContext.user = user[0];
                            window.shopOnContext.user.store_no = "";
                            //Setting empty for now.Or else api throwing error
                            window.shopOnContext.user.comp_id = window.shopOnContext.companyProduct.comp_prod_id;
                            $.server.webMethodGET("propon", "propon/customer?filter=user_id=" + window.shopOnContext.user.user_id, function (cust) {
                                window.shopOnContext.user.cust_no = cust[0].cust_no == null ? "" : cust[0].cust_no;
                                window.shopOnContext.deliveryAddress.full_name = cust[0].first_name == null ? "" : cust[0].first_name + " " + cust[0].last_name == null ? "" : cust[0].last_name;
                                window.shopOnContext.deliveryAddress.email = cust[0].email == null ? "" : cust[0].email;
                                window.shopOnContext.deliveryAddress.contactNo = cust[0].phone_no == null ? "" : cust[0].phone_no;
                                window.shopOnContext.deliveryAddress.addrLine1 = cust[0].address1 == null ? "" : cust[0].address1;
                                window.shopOnContext.deliveryAddress.addrLine2 = cust[0].address2 == null ? "" : cust[0].address2;
                                window.shopOnContext.deliveryAddress.addrLine3 = "";
                                window.shopOnContext.deliveryAddress.city = cust[0].city == null ? "" : cust[0].city;
                                window.shopOnContext.deliveryAddress.pincode = cust[0].zip_code == null ? "" : cust[0].zip_code;
                                window.shopOnContext.deliveryAddress.state = cust[0].state == null ? "" : cust[0].state;
                                $.server.webMethodGET("iam", "iam/user-permissions/" + user[0].user_id + "?user_id=" + user[0].user_id + "&obj_id=37&obj_type=Product::Previlege", function (permissions) {
                                    if (permissions.length > 0) {
                                        user[0].isAdmin = true;
                                    } else {
                                        user[0].isAdmin = false;
                                    }
                                    if(window.userData.active=="0"){
                                        var otp=Math.floor(100000 + Math.random() * 900000);
                                        var ajaxInfo = {
                                            "appName": "eShopOn",
                                            "senderNumber": "WOTOLT",
                                            "companyId": "WT-WOTOLTD",
                                            "receiverNumber": "+919894692492",
                                            "message": otp+" is your OTP for Tech Homes Registration",
                                        };
                                        var userData={
                                            full_name:window.shopOnContext.user.full_name,
                                            user_id:user[0].user_id,
                                            otp:otp,
                                            active:"0"
                                        }
                                        $.server.webMethodPUT("iam","iam/users/" + userData.user_id, userData, function (data) {
                                            sendSMS(ajaxInfo, function (response) {
                                                $$("pnlVerifyOTP").removeClass("hide");
                                                $$("pnlVerifyOTPButton").removeClass("hide");
                                                $$("pnlSigninPwd").addClass("hide");
                                                $$("pnlSigninuser").addClass("hide");                                                
                                                $$("pnlSignInButton").addClass("hide");                                                
                                            })
                                        })
                                    }else{
                                        $$("pnlVerifyOTP").addClass("hide");
                                        $$("pnlSigninPwd").removeClass("hide");
                                        $$("pnlSigninuser").removeClass("hide");
                                        $$("pnlVerifyOTPButton").addClass("hide");
                                        $$("pnlSignInButton").removeClass("hide");
                                    localStorage.setItem("propon_login_data_" + window.shopOnContext.companyProduct.comp_prod_id, JSON.stringify(window.shopOnContext.user));
                                    localStorage.setItem("propon_user_data_" + window.shopOnContext.companyProduct.comp_prod_id, JSON.stringify(user[0]));
                                    control.interface.subscribe("loginSuccess", user[0]);
                                    }
                                })
                                
                            })
                        } else {
    
                            msgPanel("You do not have permission.Please contact Admin.");
                        }
    
                    });
    
                    //Get All User Privileges.
    
                }, function (err) {
                    msgPanel(err.message);
                });                
            }


        }

        control.events.lnkSignUp_click = function () {
            // $(control.selectedObject).find(".login-panel").addClass("hide");
            // $(control.selectedObject).find(".signup-panel").removeClass("hide");
            $(".cont").toggleClass("s--signup");
            $$("pnlVerifyOTP").addClass("hide");
            $$("pnlVerifyOTPButton").addClass("hide");
            $$("pnlSignInButton").removeClass("hide");
            $$("pnlSigninPwd").removeClass("hide");
            $$("pnlSigninuser").removeClass("hide");
        }

        control.events.btnSignUp_click = function () {
            if ($$("txtFullName").val().trim() == "") {
                msgPanel("Enter your name here...!");
                $$("txtFullName").focus();
            } 
            // else if (($$("txtUserName").val().trim()) == "") {
            //     msgPanel("Enter username here...!");
            //     $$("txtUserName").focus();
            // } 
            else if ($$("txtMobileNo").val().trim() == "" && $$("txtEmailId").val() == "") {
                msgPanel("Enter your Mobile number or e-mail ID...!");
            } else if (!isInt($$("txtMobileNo").val()) || $$("txtMobileNo").val().length < 10) {
                $$("txtMobileNo").focus();
                msgPanel("Enter valid mobile number...!");
            } else if ($$("txtEmailId").val() != "" && !ValidateEmail($$("txtEmailId").val())) {
                msgPanel("Check your e-mail id...!");
                $$("txtEmailId").focus();
            } else if ($$("txtSignPassword").val().trim() == "") {
                msgPanel("Enter password for your account here...!");
                $$("txtSignPassword").focus();
            } else if ($$("txtConfirmPwd").val().trim() == "") {
                msgPanel("Confirm your password...!");
                $$("txtConfirmPwd").focus();
            } else {
                var userData = {
                    full_name: $$("txtFullName").val(),
                    //user_name: $$("txtUserName").val(),
                    phone_no: $$("txtMobileNo").val(),
                    email_id: $$("txtEmailId").val(),
                    password: $$("txtSignPassword").val(),
                    comp_id: window.shopOnContext.companyProduct.comp_id,
                };
                var custData=[];
                custData.push({
                    first_name: $$("txtFullName").val(),
                    phone_no: ($$("txtMobileNo").val() == "") ? "" : "+91" + $$("txtMobileNo").val(),
                    email: $$("txtEmailId").val(),
                    comp_id: window.shopOnContext.companyProduct.comp_prod_id,
                    org_id: window.shopOnContext.companyProduct.comp_prod_id
                });
                 var otp=Math.floor(100000 + Math.random() * 900000);
                // var ajaxInfo = {
                //     "appName": "eShopOn",
                //     "senderNumber": "WOTOLT",
                //     "companyId": "WT-WOTOLTD",
                //     "receiverNumber": "+919894692492",
                //     "message": otp+" is your OTP for Tech Homes Registration",
                // };
                 userData.otp=otp;
                 userData.active=0;
                //     sendSMS(ajaxInfo, function (response) {
                $.server.webMethodPOST("iam", "iam/users/", userData, function (data) {
                    
                    custData[0].user_id = data[0].user_id;
                    var groupData = {
                        comp_prod_id: window.shopOnContext.companyProduct.comp_prod_id,
                        group_id: 64,
                        user_id: data[0].user_id
                    }
                    $.server.webMethodPOST("iam", "iam/user-groups/", groupData, function (data) {
                        $.server.webMethodPOST("propon", "propon/customer/", custData, function (data) {
                            /*
                            if(window.userData.active=="0"){
                                $$("pnlVerifyOTP").removeClass("hide");
                                $$("pnlVerifyOTPButton").removeClass("hide");
                            }else{
                                $$("pnlVerifyOTP").addClass("hide");
                                $$("pnlVerifyOTPButton").addClass("hide");
                                */
                                msgPanel("User Account created sucessfully...!");
                                //On success back to sign in
                                $(control.selectedObject).find(".login-panel").removeClass("hide");
                                $(control.selectedObject).find(".signup-panel").addClass("hide");
                                
                                control.events.lnkSignUp_click();
                            //}
                        }, function (error) {
                            msgPanel(error.message);
                        })
                    });
                }, function (error) {
                    if (error.message.indexOf('Duplicate entry') > -1 && error.message.indexOf('phone_no') > -1)
                        msgPanel("Phone No already exists...!");
                    else if (error.message.indexOf('Duplicate entry') > -1 && error.message.indexOf('email_id') > -1)
                        msgPanel("Email ID already exists...!");
                    else
                        msgPanel(error.message);
                });
            // })
            }

        }

        control.events.lnkSignIn_click = function () {
            // $(control.selectedObject).find(".login-panel").removeClass("hide");
            // $(control.selectedObject).find(".signup-panel").addClass("hide");
            $(".cont").toggleClass("s--signup");
        }
        control.events.btnCancel_click = function () {
            control.interface.showLogout();
        }
    });
}
$.fn.splash = function () {
    return $.pageController.getControl(this, function (control, $$, e) { });
}
$.fn.messaging = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        control.interface.show = function () {
            $(control.selectedObject).removeClass("hide");
            control.interface.loadConversations();
        }
        control.interface.hide = function () {
            $(control.selectedObject).addClass("hide");
        }
        control.interface.loadConversations = function () {
            /*
            var filter = "user_id=" + window.shopOnContext.user.user_id;
            var ref = "product_types"
            */
            var filter = "(msg_from =" + window.shopOnContext.user.user_id + " or msg_to =" + window.shopOnContext.user.user_id + ")";
            //var sort = "item_messages_t.upd_date";
            var ref = "items";
            control.data.messages.distinctItem(filter, "", ref, function (data) {
                control.view.myMessaging(data);
            })
        }
        control.events.btnSendMsg_click = function () {
            if ($$("txtTypeMessage").value().trim() == "") {
                msgPanel("please type something to send");
                $$("txtTypeMessage").focus();
            } else {
                var data = [];
                data.push({
                    msg_id: "##GUID##",
                    item_no: window.CurrentChatItem,
                    message: $$("txtTypeMessage").value(),
                    state_no: 10,
                    org_id: window.shopOnContext.companyProduct.comp_prod_id,
                    user_id: window.shopOnContext.user.user_id,
                    msg_from: window.shopOnContext.user.user_id,
                    msg_to: window.CurrentChatUser,
                    date_time: dbDate(new Date()),
                });
                $.server.webMethodPOST("propon", "propon/item_messages/", data, function (data) {
                    console.log("message sent");
                    $$("txtTypeMessage").value("");
                    $$("grdMessagedUsers").selectionChanged(window.CurrentChatRow, window.itemList);
                })
            }


        }
        control.view = {
            myMessaging: function (data) {
                $$("grdMyConversations").width("200px");
                $$("grdMyConversations").height("auto");
                $$("grdMyConversations").setTemplate({
                    selection: "Single",
                    columns: [{
                            'name': "conversation", 'width': "100%", 'dataField': "image,item_no,item_name,last_chat_time,last_chat_msg", editable: false,
                            itemTemplate: '<div class="row"><div class="col-lg-4"><div class="product-list" style="height: 40px;width: 40px;"><div><div id="idProduct"><div class="prd-img"  style="height: 40px;background-image:url({image})"></div></div></div></div></div> <div class="col-lg-8"><span class="col-lg-6 left-name">{item_name}</span><span class="col-lg-6 right-time">{last_chat_time}</span><span class="col-lg-12  msg-text">{last_chat_msg}</span></div></div>'
                        }]
                })
                $$("grdMyConversations").selectionChanged = function (row, item) {
                    window.AdPostedUser = item.user_id;
                    var filter = "item_messages_t.item_no='" + item.item_no + "' and user_id !=" + window.shopOnContext.user.user_id + " and (msg_from =" + window.shopOnContext.user.user_id + " or msg_to =" + window.shopOnContext.user.user_id + ")";
                    var sort = "item_messages_t.upd_date";
                    //var ref = "customer";
                    control.data.messages.distinctUser(filter, "", "", function (data) {
                        control.view.messagedUser(data);
                        if (window.shopOnContext.user.user_id == item.user_id) {
                            $$("pnlUserConvList").removeClass("hide");
                        }else {
                            $$("pnlUserConvList").addClass("hide");
                            var filter = "item_no='" + item.item_no + "' and ((msg_from =" + window.shopOnContext.user.user_id + " and msg_to= " + window.AdPostedUser + ") or (msg_from =" + window.AdPostedUser + " and msg_to= " + window.shopOnContext.user.user_id + "))";
                            var sort = "upd_date";

                            control.data.messages.search(filter, sort, function (data) {
                                window.CurrentChatUser = item.user_id;
                                window.CurrentChatItem = item.item_no;
                                window.CurrentChatRow = row;
                                control.view.openConversation(data);
                                $$("lblConvPerson").html(item.name);
                                $$("lblPropImg").css("background-image", "url(" + item.image + ")");
                                window.itemList = item;
                            });

                        }
                    })

                
                }
                $$("grdMyConversations").beforeRowBound = function (row, item) {
                    if (item.image == undefined || item.image == "" || item.image != null) {
                        item.image = "img/alter-img.png";
                    } else {
                        item.image = encodeURI(item.image);
                    }
                }
                $$("grdMyConversations").rowBound = function (row, item) {
                    
                }

                $$("grdMyConversations").dataBind(data);
                $$("grdMyConversations").selectedObject.find(".grid_header").addClass("hide");
                $$("grdMyConversations").selectedObject.find(".grid_data").addClass("row");
                $$("grdMyConversations").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-12");
                $$("grdMyConversations").selectedObject.css("border", "none");
                $$("grdMyConversations").selectedObject.find(".grid_data").css("overflow", "hidden");

            },
            messagedUser: function (data) {
                $$("grdMessagedUsers").width("200px");
                $$("grdMessagedUsers").height("auto");
                $$("grdMessagedUsers").setTemplate({
                    selection: "Single",

                    columns: [
                        {
                            'name': "conversation", 'width': "100%", 'dataField': "image,user_id,name,item_no", editable: false,
                            itemTemplate: '<div class="row"><div class="col-lg-4"><div class="product-list" style="height: 40px;width: 40px;"><div><div id="idProduct"><div class="prd-img"  style="height: 40px;background-image:url({image})"></div></div></div></div></div> <div class="col-lg-8"><span class="col-lg-6 left-name">{user_id}</span></div></div>'
                        }

                    ]

                })
                $$("grdMessagedUsers").selectionChanged = function (row, item) {
                    var filter = "item_no='" + item.item_no + "' and ((msg_from =" + item.user_id + " and msg_to= " + window.AdPostedUser + ") or (msg_from =" + window.AdPostedUser + " and msg_to= " + item.user_id + "))";
                    var sort = "upd_date";

                    control.data.messages.search(filter, sort, function (data) {
                        window.CurrentChatUser = item.user_id;
                        window.CurrentChatItem = item.item_no;
                        window.CurrentChatRow = row;
                        control.view.openConversation(data);
                        if (item.image == undefined || item.image == "" || item.image != null) {
                            item.image = "img/alter-img.png";
                        } else {
                            item.image = encodeURI(item.image);
                        }
                        $$("lblConvPerson").html(item.name);
                        $$("lblPropImg").css("background-image", "url(" + item.image + ")");
                        window.itemList = item;
                    });

                }
                $$("grdMessagedUsers").beforeRowBound = function (row, item) {
                    if (item.image == undefined || item.image == "" || item.image != null) {
                        item.image = "img/alter-img.png";
                    } else {
                        item.image = encodeURI(item.image);
                    }
                }
                $$("grdMessagedUsers").rowBound = function (row, item) {

                }

                $$("grdMessagedUsers").dataBind(data);
                $$("grdMessagedUsers").selectedObject.find(".grid_header").addClass("hide");
                $$("grdMessagedUsers").selectedObject.find(".grid_data").addClass("row");
                $$("grdMessagedUsers").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-12");
                $$("grdMessagedUsers").selectedObject.css("border", "none");
                $$("grdMessagedUsers").selectedObject.find(".grid_data").css("overflow", "hidden");

            },
            openConversation: function (data) {
                $$("grdOpenedConversation").width("100%");
                $$("grdOpenedConversation").height("auto");
                $$("grdOpenedConversation").setTemplate({
                    selection: "Single",

                    columns: [
                        {
                            'name': "conversation", 'width': "100%", 'dataField': "message,date", editable: false,
                            itemTemplate: '<div id="idChatPanel" class="row" style="border-radius: 30px;"><div class="col-lg-12"><span class="chat-text-date" >{date}</span></div><div class="col-lg-12 chat-text-single"><p class="right" >{message}</p></div></div>'
                        }

                    ]

                })
                $$("grdOpenedConversation").beforeRowBound = function (row, item) {
                    if (item.msg_from == window.shopOnContext.user.user_id) {
                        $(row).find("#idChatPanel").addClass("right");
                        $(row).find("#idChatPanel p").addClass("right");
                        $(row).find("#idChatPanel span").addClass("right");

                        $(row).find("#idChatPanel").removeClass("left");
                        $(row).find("#idChatPanel p").removeClass("left");
                        $(row).find("#idChatPanel span").removeClass("left");

                        $(row).find("#idChatPanel").css("background", "aliceblue");
                    } else{
                        $(row).find("#idChatPanel").addClass("left");
                        $(row).find("#idChatPanel p").addClass("left");
                        $(row).find("#idChatPanel span").addClass("left");

                        $(row).find("#idChatPanel").removeClass("right");
                        $(row).find("#idChatPanel p").removeClass("right");
                        $(row).find("#idChatPanel span").removeClass("right");

                        $(row).find("#idChatPanel").css("background", "antiquewhite");
                    }
                }
                $$("grdOpenedConversation").rowBound = function (row, item) {

                }

                $$("grdOpenedConversation").dataBind(data);
                $$("grdOpenedConversation").selectedObject.find(".grid_header").addClass("hide");
                $$("grdOpenedConversation").selectedObject.find(".grid_data").addClass("row");
                $$("grdOpenedConversation").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-12");
                $$("grdOpenedConversation").selectedObject.css("border", "none");
                $$("grdOpenedConversation").selectedObject.find(".grid_data").css("overflow", "hidden");

            },
        }

        control.data = {
            items: {
                search: function (filter, ref, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter + "&ref=" + ref, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                withFilter: function (filter, ref, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter + "&refs=" + ref, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                filterAttributes: function (filter, attrFilter, attrKeys, fn) {
                    $.server.webMethodGET("propon", "propon/search/items/filter-attributes?attr_keys=" + attrKeys + "&filter=" + filter + "&attr_filter=" + attrFilter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            payments: {
                search: function (filter, sort, fn) {
                    $.server.webMethodGET("propon", "propon/item_payments?filter=" + filter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            viewers: {
                search: function (filter, sort, ref, fn) {
                    $.server.webMethodGET("propon", "propon/item_viewers?filter=" + filter + "&sort=" + sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            messages: {
                search: function (filter, sort,  fn) {
                    $.server.webMethodGET("propon", "propon/item_messages?filter=" + encodeURIComponent(filter) + "&sort=" + sort , function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                distinctUser: function (filter, sort,ref, fn) {
                    $.server.webMethodGET("propon", "propon/item_messages?select=distinct item_messages_t.user_id,item_messages_t.item_no&filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                distinctItem: function (filter, sort,ref, fn) {
                    $.server.webMethodGET("propon", "propon/item_messages?select=distinct item_name,item_messages_t.item_no,items_t.user_id&filter=" + filter + "&ref=" + ref, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            }
        }
    })
}
$.fn.orders = function () {
    return $.pageController.getControl(this,function(control,$$,e){
        control.interface.show = function () {
            $(control.selectedObject).removeClass("hide");
            control.interface.loadMyAds();
        }
        control.interface.hide = function () {
            $(control.selectedObject).addClass("hide");
        }
        control.interface.loadMyAds = function () {
            
                
            var filter = "concat(ifnull(item_no,''),ifnull(items_t.ptype_no,''),ifnull(location,''),ifnull(item_name,''),ifnull(price,'')) like '%" + $$("txtMyAdsSearch").value() + "%'";
                if (!window.shopOnContext.user.isAdmin) {
                    filter = filter+ "and user_id=" + window.shopOnContext.user.user_id;
                }

                var ref = "product_types"
                var sort = "items_t.upd_date desc"
                control.data.items.withFilter(encodeURIComponent(filter), ref, sort, function (data) {
                    control.view.myAds(data);
                })

            /*} else {
                var filter = "user_id=" + window.shopOnContext.user.user_id;
                var ref = "product_types"
                var sort = "items_t.upd_date desc"
                control.data.items.withFilter(encodeURIComponent(filter), ref, sort, function (data) {
                    control.view.myAds(data);
                })
            }*/

        }
        control.events.page_load = function () {
            $("[controlid=txtMyAdsSearch]").on('keydown', function (e) {
                if (e.keyCode == 13) {
                    control.interface.loadMyAds();
                    //var filter=""
                    //filter = "concat(ifnull(item_no,''),ifnull(ptype_no,''),ifnull(location,''),ifnull(item_name,''),ifnull(price,'')) like '%" + $$("txtMyAdsSearch").value() + "%'";
                    //if (!window.isAdmin)
                    //    filter = filter + " and user_id=" + window.shopOnContext.user.user_id

                    //filter = encodeURIComponent(filter);
                    //var ref = "product_types"
                    //control.data.items.search(filter, ref, function (data) {
                    //    control.view.myAds(data);
                    //})
                }
            })

        }
        control.events.btnNewPostAd_click = function () {
            control.interface.subscribe("postfreead");
            window.isEdit = false;
        }
        control.events.btnPostAdPayment_click = function () {
            //$$("mdlMyPayments").title("Payments");
            //$$("mdlMyPayments").open();
            //$$("mdlMyPayments").height("300");
            //$$("mdlMyPayments").width("60%");
            control.controls.mdlMyPayments.open();
            control.controls.mdlMyPayments.title("Payments");
            control.controls.mdlMyPayments.height("300");
            control.controls.mdlMyPayments.width("60%");
            if (window.pay_item_no!="")
                var filter = " item_payments_t.item_no='"+ window.pay_item_no +"' and  item_payments_t.user_id=" + window.shopOnContext.user.user_id;
            else
                var filter = "item_payments_t.user_id=" + window.shopOnContext.user.user_id;

            var sort = "item_payments_t.upd_date desc";
            control.data.payments.search(filter, sort,"items", function (data) {
                control.view.myAdsPayments(data);
            });
            
        }
        control.events.btnBuyFreePlan_click = function () {
            var item = control.pricePlan;
            var itemPay = [];
            itemPay.push({
                pay_id: "##GUID##",
                item_no: item.item_no,
                amount: 0,
                org_id: window.shopOnContext.companyProduct.comp_prod_id,
                state_no: 10,
                user_id: window.shopOnContext.user.user_id
            })
            $.server.webMethodPOST("propon", "propon/item_payments/", itemPay, function (data) {
                var data = [];
                var ExpDate = new Date();
                ExpDate.setDate(ExpDate.getDate() + 30);
                var expiry_date = dbDate(ExpDate);

                data.push({
                    item_no: item.item_no,
                    expiry_date: expiry_date
                });
                $.server.webMethodPUT("propon", "propon/items/", data, function (data) {
                    msgPanel("Your Ad Renewal request is sent successfully..!");
                })
            })
        }
        control.events.btnBuyPremiumPlan_click = function () {
            var filter ="";
            filter = filter+" state_no='30' and user_id="+window.shopOnContext.user.user_id+ " and item_no='"+control.pricePlan.item_no+"'" ;
            var sort=" upd_date desc"
            control.data.payments.search(filter,sort,function(data){
                if(data.length<=0){
                    var item = control.pricePlan;
                    var itemPay = [];
                    itemPay.push({
                        pay_id: "##GUID##",
                        item_no: item.item_no,
                        amount: 499,
                        org_id: window.shopOnContext.companyProduct.comp_prod_id,
                        state_no: 10,
                        user_id: window.shopOnContext.user.user_id
                    })
                    $.server.webMethodPOST("propon", "propon/item_payments/", itemPay, function (data) {
                        var data = [];
                        var ExpDate = new Date();
                        ExpDate.setDate(ExpDate.getDate() + 180);
                        var expiry_date = dbDate(ExpDate);
    
                        data.push({
                            item_no: item.item_no,
                            expiry_date: expiry_date
                        });
                        $.server.webMethodPUT("propon", "propon/items/", data, function (data) {
                            msgPanel("Your Ad Renewal request is sent successfully ..!");
                            control.controls.pnlPricePlan.close();
                            control.events.btnPostAdPayment_click();
                        })
                })                    
                }else{
                    msgPanel("You already have a pending payment..!");
                    control.controls.pnlPricePlan.close();
                    control.events.btnPostAdPayment_click();
                }

        })
        }
        control.view = {
            myAds: function (data) {
                $$("grdMyAds").width("1156px");
                $$("grdMyAds").height("auto");
                $$("grdMyAds").setTemplate({
                    selection: "Single",
                    dataLocation: "InMemory",
                    columns: [
                        { 'name': "Prop Ref No", 'width': "200px", 'dataField': "item_no" },
                        { 'name': "Prop Name", 'width': "150px", 'dataField': "item_name" },
                        { 'name': "Property Type", 'width': "100px", 'dataField': "ptype_name" },
                        { 'name': "Price", 'width': "100px", 'dataField': "price" },
                        { 'name': "Posted Date", 'width': "100px", 'dataField': "upd_date" },
                        { 'name': "Location", 'width': "100px", 'dataField': "location" },
                        { 'name': "Expiry Date", 'width': "80px", 'dataField': "expiry_date" },
                        //{ 'name': "Type", 'width': "150px", 'dataField': "action_type" },
                        //{ 'name': "Person Type", 'width': "150px", 'dataField': "owner_type" },
                        //{ 'name': "Name", 'width': "150px", 'dataField': "name" },
                        {
                            'name': "", 'width': "200px", 'dataField': "actions", editable: false,
                            itemTemplate: '<button type="button" id="idEditAd" class="btn-icon" action="edit" title="Edit this Ad"><i class="fa fa-pencil-square-o" style="font-size:17px;"></i></button><button id="idApproveAd" type="button" class="btn-icon" title="Approve this Ad"><i class="fa fa-check" style="font-size:17px;"></i></button><button type="button" id="idAdViewers" class="btn-icon" title="See Ad Viewers"><i class="fa fa-eye" style="font-size:17px;"></i></button><button type="button" id="idRenewAd" class="btn-icon" title="Renew your Ad"><i class="fa fa-refresh" style="font-size:17px;"></i></button><button type="button" id="idViewAdMsgs" class="btn-icon" title="View Messages for this Ad"><i class="fa fa-envelope" style="font-size:17px;"></i></button><button type="button" id="idRemoveAd" class="btn-icon" title="Activate / De-activate this Ad"><i class="fa fa-toggle-on" style="font-size:17px;"></i></button>'
                        }

                    ]
                })
                $$("grdMyAds").selectionChanged = function (row, item) {
                    window.pay_item_no = item.item_no;
                }
                $$("grdMyAds").beforeRowBound = function (row, item) {
                    if (window.shopOnContext.user.isAdmin) {
                        $(row).find("#idRemoveAd").removeClass("hide");
                        if (item.state == "30") {
                            $(row).find("#idApproveAd").css('background-color', '#3dcc69');
                        }
                    } else {
                        $(row).find("#idApproveAd").addClass("hide");
                    }
                    // if (item.state == "70") { // if deactivated
                    //     $(row).find("#idRemoveAd").css('background-color', '#ef3f36');// change to red
                    // }
                    if (item.state == "70") {// if deactivated
                        $(row).find("#idRemoveAd .fa").removeClass('fa-toggle-on');
                        $(row).find("#idRemoveAd .fa").addClass('fa-toggle-off');
                        $(row).find("#idRemoveAd").removeClass("hide");
                        $(row).find("#idRemoveAd").css('background-color', '#ef3f36');// change to red
                    } else if (item.state == "30") {
                        $(row).find("#idRemoveAd .fa").addClass('fa-toggle-on');
                        $(row).find("#idRemoveAd").removeClass("hide");
                        $(row).find("#idRemoveAd .fa").removeClass('fa-toggle-off');
                        $(row).find("#idRemoveAd").css('background-color', '#18a33b');// change to green
                    } else {
                        $(row).find("#idRemoveAd").addClass("hide");
                    }
                }
                $$("grdMyAds").rowBound = function (row, item) {
                    $(row).find('#idEditAd').click(function () {
                        control.interface.editPostFreeAd("editAd",item);
                    })
                    $(row).find('#idAdViewers').click(function () {
                        control.controls.mdlMyAdViewers.open();
                        control.controls.mdlMyAdViewers.title("Viewers");
                        control.controls.mdlMyAdViewers.height("300");
                        control.controls.mdlMyAdViewers.width("60%");


                        var filter = "item_no='" + item.item_no+"'";
                        var sort = "upd_date desc";
                        var ref = ""
                        control.data.viewers.search(filter,function (data) {
                            control.view.myAdsViewers(data);
                        });
                    })
                    $(row).find('#idViewAdMsgs').click(function () {
                        control.controls.mdlMyAdMessages.open();
                        control.controls.mdlMyAdMessages.title("Messages");
                        control.controls.mdlMyAdMessages.height("300");
                        control.controls.mdlMyAdMessages.width("60%");


                        var filter = "item_messages_t.item_no='" + item.item_no + "'";
                        var sort = "item_messages_t.upd_date desc";
                        var ref =  "items";
                        control.data.messages.search(filter, sort, ref, function (data) {
                            control.view.myAdsMessages(data);
                        });
                    })
                    $(row).find('#idApproveAd').click(function () {
                        if (confirm("Are you sure want to do this?")) {
                            if (item.state == "30" && item.state != "70") {
                                msgPanel("This Ad is already approved...!");
                            } else {
                                var data = [];
                                data.push({
                                    item_no: item.item_no,
                                    state: 30
                                });
                                $.server.webMethodPUT("propon", "propon/items/", data, function (data) {
                                    msgPanel("This Ad is Approved successfully...!");
                                    control.interface.loadMyAds();
                                })
                            }
                        }

                    })
                    $(row).find('#idRemoveAd').click(function () {
                        var data = [];
                        data.push({
                            item_no: item.item_no,
                            state: (item.state == "70")? "30":"70"
                        });
                        $.server.webMethodPUT("propon", "propon/items/", data, function (data) {
                            control.interface.loadMyAds();
                            if (item.state == "70"){
                                msgPanel("This Ad is Activated successfully...!");
                                $(row).find("#idRemoveAd .fa").addClass('fa-toggle-on');
                                $(row).find("#idRemoveAd .fa").removeClass('fa-toggle-off');
                            }
                            else {
                                msgPanel("This Ad is De-activated successfully...!");
                                $(row).find("#idRemoveAd .fa").removeClass('fa-toggle-on');
                                $(row).find("#idRemoveAd .fa").addClass('fa-toggle-off');
                            }
                        })
                    })
                    $(row).find('#idRenewAd').click(function () {
                        control.pricePlan = item;
                        control.controls.pnlPricePlan.open();
                        control.controls.pnlPricePlan.title("Choose Plan");
                        control.controls.pnlPricePlan.height("auto");
                        control.controls.pnlPricePlan.width("60%");
                    })
                }
                window.pay_item_no = "";
                $$("grdMyAds").dataBind(data);
                if (window.matchMedia('(max-width: 600px)').matches) {
                    $$("grdMyAds").width("100%");
                    $$("grdMyAds").selectedObject.find(".grid_header").addClass("hide");
                    $$("grdMyAds").selectedObject.find(".grid_data").addClass("row");
                    $$("grdMyAds").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-12");
                    $$("grdMyAds").selectedObject.find(".grid_row").addClass("div-background");
                    $$("grdMyAds").selectedObject.css("border", "none");
                    $$("grdMyAds").selectedObject.find(".grid_data").css("overflow", "hidden");
                }

            },
            myAdsPayments: function (data) {
                $$("grdMyAdsPayments").width("100%");
                $$("grdMyAdsPayments").height("auto");
                $$("grdMyAdsPayments").setTemplate({
                    selection: "Single",

                    columns: [
                        { 'name': "Prop Ref No", 'width': "200px", 'dataField': "item_no" },
                        { 'name': "Property", 'width': "200px", 'dataField': "item_name" },
                        { 'name': "Amount", 'width': "80px", 'dataField': "amount" },
                        { 'name': "Status", 'width': "80px", 'dataField': "state_no" },
                        {
                            'name': "Action", 'width': "120px", 'dataField': "actions", editable: false,
                            itemTemplate: '<button type="button" id="idPayAd" class="btn-icon" title="Pay Now">Pay Now</button>'
                        }

                    ]
                })
                $$("grdMyAdsPayments").beforeRowBound = function (row, item) {
                    if(item.status=="Paid")
                        $(row).find('#idPayForAd').addClass("hide");
                    else
                        $(row).find('#idPayForAd').removeClass("hide");
                }
                $$("grdMyAdsPayments").rowBound = function (row, item) {
                    $(row).find('#idPayForAd').click(function () {
                        
                    })

                }
                $$("grdMyAdsPayments").dataBind(data);

            },
            myAdsViewers: function (data) {
                $$("grdMyAdViewers").width("100%");
                $$("grdMyAdViewers").height("auto");
                $$("grdMyAdViewers").setTemplate({
                    selection: "Single",

                    columns: [
                        { 'name': "Prop Ref No", 'width': "200px", 'dataField': "item_no" },
                        { 'name': "Viewer", 'width': "80px", 'dataField': "user_id" },
                        { 'name': "Name", 'width': "100px", 'dataField': "name" },
                        { 'name': "Email", 'width': "100px", 'dataField': "email" },
                        { 'name': "Mobile", 'width': "80px", 'dataField': "mobile" },
                        //{ 'name': "View Type", 'width': "80px", 'dataField': "view_type" },
                        //{
                        //    'name': "Action", 'width': "120px", 'dataField': "actions", editable: false,
                        //    itemTemplate: '<button type="button" id="idPayForAd" class="btn-icon" title="Pay Now"><i class="fa fa-pencil-square-o" style="font-size:17px;"></i></button>'
                        //}

                    ]
                })
                $$("grdMyAdViewers").beforeRowBound = function (row, item) {

                }
                $$("grdMyAdViewers").rowBound = function (row, item) {


                }
                $$("grdMyAdViewers").dataBind(data);
                if (window.matchMedia('(max-width: 600px)').matches) {
                    $$("grdMyAdViewers").width("100%");
                    $$("grdMyAdViewers").selectedObject.find(".grid_header").addClass("hide");
                    $$("grdMyAdViewers").selectedObject.find(".grid_data").addClass("row");
                    $$("grdMyAdViewers").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-12");
                    $$("grdMyAdViewers").selectedObject.find(".grid_row").addClass("div-background");
                    $$("grdMyAdViewers").selectedObject.css("border", "none");
                    $$("grdMyAdViewers").selectedObject.find(".grid_data").css("overflow", "hidden");
                }
            },
            myAdsMessages: function (data) {
                $$("grdMyAdMessages").width("100%");
                $$("grdMyAdMessages").height("auto");
                $$("grdMyAdMessages").setTemplate({
                    selection: "Single",

                    columns: [
                        { 'name': "Property", 'width': "200px", 'dataField': "item_name" },
                        { 'name': "Message", 'width': "200px", 'dataField': "message" },
                        { 'name': "From", 'width': "80px", 'dataField': "msg_from" },
                        { 'name': "To", 'width': "80px", 'dataField': "msg_to" },
                        { 'name': "Date", 'width': "100px", 'dataField': "date" },

                    ]
                })
                $$("grdMyAdMessages").beforeRowBound = function (row, item) {

                }
                $$("grdMyAdMessages").rowBound = function (row, item) {


                }
                $$("grdMyAdMessages").dataBind(data);
                if (window.matchMedia('(max-width: 600px)').matches) {
                    $$("grdMyAdMessages").width("100%");
                    $$("grdMyAdMessages").selectedObject.find(".grid_header").addClass("hide");
                    $$("grdMyAdMessages").selectedObject.find(".grid_data").addClass("row");
                    $$("grdMyAdMessages").selectedObject.find(".grid_row").addClass("col-xs-12 col-lg-12");
                    $$("grdMyAdMessages").selectedObject.find(".grid_row").addClass("div-background");
                    $$("grdMyAdMessages").selectedObject.css("border", "none");
                    $$("grdMyAdMessages").selectedObject.find(".grid_data").css("overflow", "hidden");
                }
            }
        }
        control.data = {
            items: {
                search: function (filter,ref,fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter + "&refs="+ref, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                withFilter: function (filter, ref, sort, fn) {
                    $.server.webMethodGET("propon", "propon/items?filter=" + filter + "&ref=" + ref+"&sort="+sort, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
                filterAttributes: function (filter, attrFilter, attrKeys, fn) {
                    $.server.webMethodGET("propon", "propon/search/items/filter-attributes?attr_keys=" + attrKeys + "&filter=" + filter + "&attr_filter=" + attrFilter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            payments: {
                search: function (filter, sort,ref, fn) {
                    if(ref!="" && ref !=undefined ){
                        $.server.webMethodGET("propon", "propon/item_payments?filter=" + filter + "&sort=" + sort+"&ref="+ref, function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    }else{
                        $.server.webMethodGET("propon", "propon/item_payments?filter=" + filter + "&sort=" + sort, function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });                        
                    }

                },
            },
            viewers: {
                search: function (filter,fn) {
                    $.server.webMethodGET("propon", "propon/item_viewers?filter=" + filter, function (data) {
                        $(data).each(function (i, item) {
                            item.label = item.attr_text;
                            item.value = item.attr_value;
                        });
                        fn(data);
                    });
                },
            },
            messages: {
                search: function (filter, sort, ref, fn) {
                    if (ref != "" && ref != undefined) {
                        $.server.webMethodGET("propon", "propon/item_messages?filter=" + filter + "&sort=" + sort+"&ref="+ref, function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    } else {
                        $.server.webMethodGET("propon", "propon/item_messages?filter=" + filter + "&sort=" + sort , function (data) {
                            $(data).each(function (i, item) {
                                item.label = item.attr_text;
                                item.value = item.attr_value;
                            });
                            fn(data);
                        });
                    }

                },
            }
        }
    })
}
$.fn.account = function () {
    return $.pageController.getControl(this, function (control, $$, e) {
        control.interface.show = function () {
            $(control.selectedObject).removeClass("hide");
            $(control.selectedObject).find(".pnlProfileInfo").removeClass("hide");
            $(control.selectedObject).find(".pnlManageAddress").addClass("hide");
            control.showCustomerDetail();
        }
        control.interface.hide = function () {
            $(control.selectedObject).addClass("hide");
        }
        control.events = {
            lnkProfileInfo_click : function () {
                $(control.selectedObject).find(".pnlProfileInfo").removeClass("hide");
                $(control.selectedObject).find(".pnlManageAddress").addClass("hide");
            },
            lnkManageaddress_click : function () {
                $(control.selectedObject).find(".pnlProfileInfo").addClass("hide");
                $(control.selectedObject).find(".pnlManageAddress").removeClass("hide");
                control.showCustAddressDetails();
            },
            btnSaveAddr_click: function () {
                var custData = [];
                custData.push({
                    cust_no: window.shopOnContext.user.cust_no,
                    first_name: $$("txtAddFullName").val(),
                    address1: $$("txtAddAddress1").val(),
                    city: $$("txtAddCity").val(),
                    state: $$("txtAddState").val(),
                    zip_code: $$("txtAddZipCode").val(),
                    email: $$("txtAddEmail").val()
                });
                $.server.webMethodPUT("propon", "propon/customer/", custData, function (data) {


                    msgPanel("Account  details updated successfully...!");
                })
            },
            btnSave_click : function () {
                var userData = {
                    user_id:window.shopOnContext.user.user_id,
                    full_name: $$("txtActFirstName").val(),
                    phone_no: $$("txtActMobile").val(),
                    email_id: $$("txtActEmail").val(),
                };
                var custData = [];
                custData.push({
                    cust_no: window.shopOnContext.user.cust_no,
                    first_name: $$("txtActFirstName").val(),
                    last_name: $$("txtActLastName").val(),
                    phone_no: ($$("txtActMobile").val() == "") ? "" : $$("txtActMobile").val(),
                    email: $$("txtActEmail").val()
                });
                if ($("#chkActGenMale").prop("checked")) {
                    userData.gender = "male";
                    custData[0].gender = "male";
                }
                else if ($("#chkActGenFeMale").prop("checked")) {
                    userData.gender = "female";
                    custData[0].gender = "female";
                }

                $.server.webMethodPUT("iam","iam/users/" + userData.user_id, userData, function (data) {
                    $.server.webMethodPUT("propon", "propon/customer/", custData, function (data) {
                        msgPanel("Account  details updated successfully...!");
                    })
                })
        }
        }
        control.showCustAddressDetails = function () {
            $.server.webMethodGET("propon", "propon/customer?filter=cust_no=" + window.shopOnContext.user.cust_no,
                function (custData) {
                    if (custData.length > 0) {
                        var item = custData[0]
                        $$("txtAddFullName").val(item.first_name);
                        $$("txtAddEmail").val(item.email);
                        $$("txtAddAddress1").val(item.address1);
                        $$("txtAddCity").val(item.city);
                        $$("txtAddState").val(item.state);
                        $$("txtAddZipCode").val(item.zip_code);
                    }
                })
        }
        control.showCustomerDetail = function () {
            $.server.webMethodGET("propon", "propon/customer?filter=cust_no=" + window.shopOnContext.user.cust_no,
                function (custData) {
                    if (custData.length > 0) {
                        var item = custData[0]
                        $$("txtActFirstName").val(item.first_name);
                        $$("txtActLastName").val(item.last_name);
                        if (item.gender == "male") {
                            $("#chkActGenMale").prop("checked", true);
                        }
                        else if (item.gender == "female") {
                            $("#chkActGenFeMale").prop("checked", true);
                        }
                        $$("txtActMobile").val(item.phone_no);
                        $$("txtActEmail").val(item.email);
                    }
                })
        }
    })
}
function gridToRepeater(grd) {
    grd.selectedObject.find(".grid_header").addClass("hide");
    grd.selectedObject.find(".grid_data").addClass("row");
    grd.selectedObject.find(".grid_row").addClass("col-xs-4");
    grd.selectedObject.css("border", "none");
    grd.selectedObject.find(".grid_data").css("overflow", "auto");
}

function gridToList(grd) {
    // grd.selectedObject.find(".grid_header").addClass("hide");
    grd.selectedObject.css("border", "none");
    grd.selectedObject.find(".grid_row").css("border-bottom", "solid 1px whitesmoke");
    grd.selectedObject.find(".grid_row").css("outline", "none");
    grd.selectedObject.find(".grid_row").css("border-bottom");
}
function msgPanel(msg) {
    if (window.shopOnContext.isAndroid)
        window.plugins.toast.showShortCenter(msg);
    else
        alert(msg);
}


function swipe(id) {
    var largeImage = id;
    //largeImage.style.display = 'block';
    //largeImage.style.width = 200 + "px";
    //largeImage.style.height = 200 + "px";
    var url = largeImage.getAttribute('src');
    //window.open(url, 'Image', 'width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
    window.open(url +
    encodeURIComponent( // Escape for URL formatting
        '<!DOCTYPE html>' +
        '<html lang="en">' +
        '<head><title>Embedded Window</title></head>' +
        '<body><h1>42</h1></body>' +
        '</html>'
    )
);
}


var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}
