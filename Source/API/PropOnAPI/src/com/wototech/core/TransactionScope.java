/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import java.sql.Connection;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 *
 * @author Ramkumar
 */
public class TransactionScope {
    public static ThreadLocal<Deque<DBConnection>> currentConnection = null;
    
    private boolean seperateTransaction=false;
    public TransactionScope(){
    }
   // public TransactionScope(boolean seperateTransaction){
      //  this.seperateTransaction=seperateTransaction;
  //  }
    public  void beginTransaction(){ 
        //Create a stack one time per thread8
        if(currentConnection==null)
            currentConnection= new ThreadLocal<Deque<DBConnection>> ();

        if(currentConnection.get()==null)
         currentConnection.set(new ArrayDeque<DBConnection>());
        
        DBConnection db = null;
        if(currentConnection.get().size()==0)
            db=new DBConnection(true);
        else
            db=currentConnection.get().peek();
        
        
        currentConnection.get().push(db);
        
                    
    }
    public  void commit(){
        
        if(currentConnection.get().size()==1)
            currentConnection.get().peek().commit();
        currentConnection.get().pop();    
        
        
    }
    public  void rollback(){
        if(currentConnection.get().size()==1)
            currentConnection.get().peek().rollback();
        currentConnection.get().pop();  
       
      
    }
}


//   DB  1tr,1tr        tr1, tr2rentConnection.