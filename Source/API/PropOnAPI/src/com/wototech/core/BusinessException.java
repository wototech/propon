/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

/**
 *
 * @author BALU
 */
public class BusinessException extends RuntimeException{

    String message=null;
    public BusinessException(Exception e){
        super(e);
        message=e.getMessage();
    }
    public BusinessException(String message){
       this.message =message;
    }

    @Override
    public String getMessage() {
        return message; //To change body of generated methods, choose Tools | Templates.
    }
    
    
   
    
    public String getUserMessage() {        
            return message;
    }
        
}
