/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Ramkumar
 */
public class Security {
    public static void Authorize(String authToken) {
        if (authToken == null) {
            throw new UserAccessException("MissingToken");
        }
        try {
            

            URL url = new URL(DBConnection.getConfig("IAMTokenContextURL").replace("{auth_token}", authToken));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		
		int responseCode = con.getResponseCode();
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
                
                JSONObject jsonObj =new JSONObject( "{\"data\":" + response  + "}");
                if(!  ((JSONArray)jsonObj.get("data")).getJSONObject(0).has("user_id"))
                 throw new UserAccessException("InvalidToken");
           
           
        } catch (Exception e) {
             e.printStackTrace();
            throw new UserAccessException("InvalidToken");
        }

        //throw new UserAccessException("InvalidToken");
    }

}
