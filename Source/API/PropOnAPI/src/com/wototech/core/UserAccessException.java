package com.wototech.core;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class UserAccessException extends WebApplicationException {
    private static final long serialVersionUID = 1L;

    public UserAccessException() {
        this("UnAuthorised");
    }

    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param message the String that is the entity of the 404 response.
     */
    public UserAccessException(String errorType) {	
        super(Response.status(Status.UNAUTHORIZED).entity(getJSONString(errorType)).type("application/json").build());
    }

    public static String getJSONString(String errorType) {
    	
    	String jsonString="";
    	if(errorType.equals("MissingToken"))
    		jsonString = "{\"message\": \"No API Key Found\"}";
    	if(errorType.equals("InvalidToken"))
    		jsonString = "{\"message\": \"API Key is Invalid\"}";
    	
    	return jsonString;
    }
}