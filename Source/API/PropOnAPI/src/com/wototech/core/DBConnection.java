/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

// <editor-fold desc="Imports">
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.json.XML;
import org.json.*;

// </editor-fold>

public class DBConnection {

    // <editor-fold desc="Declaration & Variables">
    private Connection con;
    private Statement cs;
    private boolean _isClosed = false;
    private boolean _isTransactionEnabled = false;
    private int randno=0;
public static HashMap poolList=new HashMap() ;
    
    public DBConnection() {
        this(false);
    }

 public JSONArray executeNonQuery(String query) {
        try {
            cs = con.createStatement();
            cs.execute(query);

            return null;
        } catch (java.sql.SQLException ex) {
            throw new DataAccessException(ex.getMessage());
        }
    }
    public JSONArray executeQuery(String query) {
        try {
            
            cs = con.createStatement();           
            ResultSet rs = cs.executeQuery(query);
            

            //Load the table schema
            JSONArray dt = new JSONArray();
            while (rs.next()) {
                JSONObject dr = new JSONObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String data=rs.getString(rs.getMetaData().getColumnName(i));
                    if(data!=null)
                        dr.put(rs.getMetaData().getColumnName(i), data);
                    else
                       dr.put(rs.getMetaData().getColumnName(i), JSONObject.NULL);     
                }
                dt.put(dr);
            }

            rs.close();
            return dt;
        } catch (java.sql.SQLException ex) {
            throw new DataAccessException(ex.getMessage());
        }
    }




    //Create a DB Connection with transaction scope or without transaction scope
    public DBConnection(boolean enableTransaction) {
        _isTransactionEnabled = enableTransaction;
        try {
            con = GetConnection();
            randno = (new Random()).nextInt();
            poolList.put("randno" ,"open");
            if (_isTransactionEnabled == true) {
                con.setAutoCommit(false);
            }
        } catch (Exception ex) {
            throw new DataAccessException("Database is Down." + ex.getMessage());
        }
    }

    public void commit() {
        close(true);
    }

    public void rollback() {
        close(false);
    }
    
    public JSONArray Execute(String methodName,JSONObject methodParam) {
        try {
            
            String spName=methodName.split("\\.")[0];
            
           
            if(methodName.startsWith("#"))
            {   
                spName="RestProxy";
                cs = con.prepareCall("{call " + getConfig("Schema")+ ".`" + spName + "`(?,?)}");
                ((CallableStatement)cs).setString(1, methodName);
            }
            else
            {   
                cs = con.prepareCall("{call " + getConfig("Schema")+ ".`" + spName + "`(?,?)}");
                ((CallableStatement)cs).setString(1, methodName.split("\\.")[1]);
            }
            ((CallableStatement)cs).setString(2, XML.toString(methodParam)  );
            
            
            ResultSet rs = ((CallableStatement)cs).executeQuery();
            

            //Load the table schema
            JSONArray dt = new JSONArray();
            while (rs.next()) {
                JSONObject dr = new JSONObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    String data=rs.getString(rs.getMetaData().getColumnName(i));
                    if(data!=null)
                        dr.put(rs.getMetaData().getColumnName(i), data);
                    else
                       dr.put(rs.getMetaData().getColumnName(i), JSONObject.NULL);     
                }
                dt.put(dr);
            }

            rs.close();
            return dt;
        } catch (java.sql.SQLException ex) {
            throw new DataAccessException(ex.getMessage());
        }
    }

   /* public String convertToDBParamFormat(JSONArray methodParam){
       
        Iterator<JSONObject> iterator = methodParam.iterator();  
        String cmdText = "";      
        String sep = "";  
        while (iterator.hasNext()) {
                if (cmdText != "")
                    sep = ",";
                JSONObject jo=(JSONObject) iterator.next();
                if(((String) jo.get("type")).equals("Text") ){
                    cmdText = cmdText + sep + jo.get("name") + ";'" + jo.get("value") + "'";
                }else{
                    cmdText = cmdText + sep + jo.get("name") + ";" + jo.get("value");
                }
        }
         
        return cmdText;
    } */
    
    private static Connection GetConnection() {

        String url =  getConfig("DBURL") + getConfig("Schema");
        String user = getConfig("UserName");
        String password = getConfig("Password");
        String dateTime="?useUnicode=true&characterEncoding=UTF-8&useLegacyDatetimeCode=false&serverTimezone=Asia/Calcutta";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(url+dateTime, user, password);
            //return DriverManager.getConnection(url, user, password);
        } catch (Exception ex) {
            throw new DataAccessException("Database is Down." + ex.getMessage());
        }
    }
    
    public static String getConfig(String name) {
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            return (String) env.lookup(name);
        } catch (Exception e) {
            throw new DataAccessException("Problem with DB configuration.Please check web.xml" + e.getMessage());
        }
    }

    
    public void close(boolean commit) {
        //Close the statement
        closeStatement();
        //Close the Connection
        try {
            if (con != null) {
                if (!con.isClosed()) {
                    if (_isTransactionEnabled == true) {
                        if (commit) {
                            con.commit();
                        } else {
                            con.rollback();
                        }
                    }
                    con.close();
                     poolList.put("randno" ,"close");
                    
                }
                con = null;
            }
        } catch (java.sql.SQLException se) {
            throw new DataAccessException(se.getMessage());
        }
        _isClosed = true;
    }

    private void closeStatement() {
        //Close the statement
        try {
            if (cs != null) {
                if (!cs.isClosed()) {
                    cs.close();
                }
                cs = null;
            }
        } catch (java.sql.SQLException se) {

        }

    }
    
    protected void finalize() throws Throwable {
        if (!_isClosed) {
            this.close(false);
        }
        super.finalize();
    }

    
    
  
    // </editor-fold>
}



