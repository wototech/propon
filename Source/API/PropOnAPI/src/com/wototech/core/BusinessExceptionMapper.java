/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import com.sun.jersey.api.NotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.json.JSONObject;

@Provider
public class BusinessExceptionMapper
        implements ExceptionMapper<BusinessException> {

    final String html
            = "<p style='font-size:20px;font-family:Sans-serif'>"
            + "Service is unavailable message."
            + "</p>"
            + "<a href='http://www.stackoverflow.com'>"
            + "Check out Stackoverflow</a>";
    @Context
    private HttpServletRequest request;
    @Context
    private UriInfo uriInfo;

    @Override
    public Response toResponse(BusinessException e) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("{\"exceptionType\":\"BusinessException\",\"message\":\""+e.getMessage()+"\"}").build();
    }
}

