/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import java.util.*;

/**
 *
 * @author Ramkumar
 */
public class JDMap {
  
    
    //Get Resource Fields
    public static String[] getRF(String resourceName){
        return maps.get(resourceName);
    }
    //Get Database table Fields
    public static String[] getTF(String resourceName,String tableName){
        
        List<String> ls=null;
        for(String val: maps.get(resourceName)){
            if(ls==null)
                ls=new ArrayList<>();
             else{
                if(val.split("::")[1].split("\\.")[0].equals(tableName))
                    ls.add( val.split("::")[1].split("\\.")[1]);
            }
        }
        String[] stockArr = new String[ls.size()];
        stockArr = ls.toArray(stockArr);

         return  stockArr;
    }
    //Get Mandatory Resource Fields
    public static String[] getMRF(String resourceName){
         return maps.get(resourceName);
    }
    //Get Mandatory DB Fields
    public static String[] getMTF(String resourceName){
         return maps.get(resourceName);
    }
    
    //Map REsource NAme to DB colmn name
    public static String getTCN(String resourceName,String tableName,String key){
        String search= key + "::" + tableName;
        for(String val: maps.get(resourceName)){
            if(val.indexOf(search)>-1)
                return val.split("::")[1].split("\\.")[1];
        }
        return "";
    }
    
     //Get Mandatory DB Fields
    public static String getParentTable(String resourceName){
         return maps.get(resourceName)[0].split(",")[0].split("::")[1];
    }
     public static String getParentKey(String resourceName){
         return maps.get(resourceName)[0].split(",")[0].split("::")[2];
    }
    public static HashMap<String,String> getChildTables(String resourceName){
        
        HashMap<String,String> cmaps=new  HashMap<String,String> ();
      
        for(String val: maps.get(resourceName)[0].split(",")){
            if(! (val.split("::")[0] .equals( "root")))
                cmaps.put( val.split("::")[0].split("\\.")[1], val.split("::")[1]);
        }
        
        

         return  cmaps;
    }
    
 
    public static String[] invoiceResource = new String [] {
        "root::bill_t::bill_no,root.invoice_items::bill_item_t,root.payments::bill_pay_t",
     
        
        "root.invoice_date::bill_t.bill_date::*",
        "root.invoice_type::bill_type::*",
        "root.total::bill_t.total::*",
        "root.state_no::bill_t.state_no::",
        "root.comp_id::bill_t.comp_id::*",
        "root.sub_total::bill_t.discount::*",
        "root.discount::bill_t.tax::*",
        "root.discount::bill_t.sub_total::*",
      
        "root.invoice_items.bill_no::bill_item_t.bill_no::*",
 "root.invoice_items.item_no::bill_item_t.item_no::*",
 "root.invoice_items.var_no::bill_item_t.var_no::*",
 "root.invoice_items.qty::bill_item_t.qty::*",
 "root.invoice_items.price::bill_item_t.price::*",
 "root.invoice_items.taxable_value::bill_item_t.taxable_value::*",
 "root.invoice_items.tax_per::bill_item_t.tax_per::*",
 "root.invoice_items.discount::bill_item_t.discount::*",
 "root.invoice_items.total_price::bill_item_t.total_price::*",
        /*
        "root.tax::bill_t.total::*",          
        "root.invoice_no::bill_t.total::*",
        "root.invoice_type::bill_t.total::*",
        "root.invoice_date::bill_t.total::*",
        "root.invoice_ref_no::bill_t.total::*",
        "root.order_no::bill_t.total::*",
        "root.due_date::bill_t.total::*",
        "root.discount::bill_t.total::*",
        "root.tax::bill_t.total::*",
        "root.round_off::bill_t.total::*",
        "root.total::bill_t.total::*", 
        "root.tot_qty_words::bill_t.total::*",
        "root.sales_tax_no::bill_t.total::*",
        "root.state_no::bill_t.total::*",
        "root.pay_mode::bill_t.total::*",
        "root.sales_executive::bill_t.total::*",
        "root.comp_id::bill_t.total::*",
        "root.party_no::bill_t.total::*",
        "root.party_tax_area::bill_t.total::*",
        "root.party_baddress::bill_t.total::*",
        "root.party_saddress::bill_t.total::*"*/
    };
    
    public static HashMap<String,String[]> maps;
    static {
        maps=new HashMap<String,String[]> ();   
        maps.put("invoice", invoiceResource);
    }
    
    
}
