/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;


import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Ramkumar
 */
public class Util {
    
    
    public static final double R = 6372.8; // In kilometers

    public static String getConfig(String name) {
        try {
            javax.naming.Context env = (javax.naming.Context) new InitialContext().lookup("java:comp/env");
            return (String) env.lookup(name);
        } catch (NamingException e) {

        }
        return null;

    }

    public static void ValidatePositiveNumber(String data, String errorMessage) {
        Double cost;
        try {
            cost = Double.parseDouble(data);
        } catch (Exception e) {
            throw new BusinessException(errorMessage);
        }
        
        if (cost < 0) {
            throw new BusinessException(errorMessage);
        }

    }

    public static void ValidateEmpty(String data, String errorMessage) {
        if (data == null || data.equals("")) {
            throw new BusinessException(errorMessage);
        }
    }

    public static boolean IsNullOrEmpty(String data) {
        return data != null && !data.equals("");
    }
    
    public static void ValidateDataExists(String packageName,JSONObject inputData, String errorMessage) {
       JSONArray result=DB.Execute("Stock.checkCombinationExists", inputData );
       if(result.length()>0)
           throw new BusinessException(errorMessage);
    }
   
         
    public static double haversineDistance(double lat1, double lon1, double lat2, double lon2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
 
        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

}
