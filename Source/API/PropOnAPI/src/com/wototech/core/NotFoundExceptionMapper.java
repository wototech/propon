/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import com.sun.jersey.api.NotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.json.JSONObject;

@Provider
public class NotFoundExceptionMapper
        implements ExceptionMapper<NotFoundException> {

    final String html
            = "<p style='font-size:20px;font-family:Sans-serif'>"
            + "Service is unavailable message."
            + "</p>"
            + "<a href='http://www.stackoverflow.com'>"
            + "Check out Stackoverflow</a>";
    @Context
    private HttpServletRequest request;
    @Context
    private UriInfo uriInfo;

    @Override
    public Response toResponse(NotFoundException e) {
        try {
            if (request.getMethod().equals("GET") || request.getMethod().equals("DELEE")) {
                return DB.ExecuteAsResponse("#" + request.getRequestURI(), DB.AllInput(uriInfo));
            } else if (request.getMethod().equals("POST") || request.getMethod().equals("PUT")) {

                String json = "";

                try {
                    // How to cache getInputStream: http://stackoverflow.com/a/17129256/356408
                    InputStream is = request.getInputStream();
                    // Read an InputStream elegantly: http://stackoverflow.com/a/5445161/356408
                    Scanner s = new Scanner(is, "UTF-8").useDelimiter("\\A");
                    json = s.hasNext() ? s.next() : json;
                } catch (Exception ex) {
                    // Ignore exceptions around getting the entity
                }

                /* StringBuilder sb = new StringBuilder();
                BufferedReader br;
              
                try {
                    br = request.getReader();
                    String str;
                    while ((str = br.readLine()) != null) {
                        sb.append(str);
                    }
                    json = sb.toString();
                } catch (IOException ex) {
                    Logger.getLogger(NotFoundExceptionMapper.class.getName()).log(Level.SEVERE, null, ex);
                }*/
                return DB.ExecuteAsResponse("#" + request.getRequestURI(), new JSONObject(json));

            } else {
                return Response.status(Status.OK)
                        .entity(html).build();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return Response.status(Status.OK)
                    .entity(ex.toString()).build();
        }

    }
}
