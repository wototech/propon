/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.*;


/**
 *
 * @author Ramkumar
 */
public    class ParameterList{
        JSONArray joArray=new JSONArray();
        public String ConvertDate(String paramType){
            Date d=null;
            try{
                  d= new SimpleDateFormat("dd-MM-yyyy").parse(paramType);
            }catch(Exception e){}
            if(d!=null){
                SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
                return sm.format(d);
            }else
                return paramType;
        }
        public void add(String paramName,String paramValue,String paramType){
            if(paramType=="Date")
            {    paramValue=ConvertDate(paramValue);
                 paramType="Text";
            }
            JSONObject jo=new JSONObject();
            jo.put("name",paramName);
            jo.put("value",paramValue);
            jo.put("type",paramType);
            joArray.put(jo);
        }
        public void add(String paramName,String paramValue){
            add( paramName, paramValue, "Number");
        }
        public JSONArray toJSONArray(){
            return joArray;
        }
    }