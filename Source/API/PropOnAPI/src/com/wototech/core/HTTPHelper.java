/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import static com.wototech.core.DB.log;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

/**
 *
 * @author Ramkumar
 */
public class HTTPHelper {
     public static JSONObject GET(String authToken,String uri) {
        try {

            URL url = new URL(DBConnection.getConfig("IAMTokenContextURL")+ uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            
            if(!authToken.equals(""))
                con.setRequestProperty("auth-token", authToken);
 
                
            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return new JSONObject("{\"data\":" + response + "}");

        } catch (Exception e) {
           throw new RuntimeException();
        }
    }

    public static JSONObject POST(String authToken,String app,String uri, String JSON) {
        try {
            java.time.Instant start = java.time.Instant.now();
             URL url =null;
              
            if(app.equals("Finfacts")){
                url = new URL(DBConnection.getConfig("FinfactsURL")+ uri);
                //url = new URL("http://104.251.218.116:9080/FinFactsShopOn4DevRestAPI/api/v1/"+ uri);
                //url = new URL("http://104.251.218.116:9080/FinFactsShopOn4ProdRestAPI/api/v1/"+ uri);
                //rl = new URL("http://localhost:8080/FinFactsShopOnRestAPI/api/v1/"+ uri);   
            }
            else
                url = new URL(DBConnection.getConfig("IAMTokenContextURL")+ uri);
                  
             HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            
            if(!authToken.equals(""))
                con.setRequestProperty("auth-token", authToken);
                //con.setRequestProperty("auth-token", authToken);
            
            //String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(JSON);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

             java.time.Instant end = java.time.Instant.now();
            log(uri + " Time taken : " + java.time.Duration.between(start, end).toMillis());
            return new JSONObject("{\"data\":" + response + "}");


        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    
    public static JSONObject GETSource(String authToken,String uri,String location) {
        try {
            URL url =null;
            if(location.equals("Source")){
                url = new URL("http://localhost:8080/ShopOnSourceRestAPI/api/v1/"+ uri);
            }
            else{
                url = new URL("http://localhost:8080/ShopOnSourceRestAPI/api/v1/"+ uri);
            }
            HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
            
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            
            if(!authToken.equals(""))
                con.setRequestProperty("auth-token", authToken);
 
                
            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return new JSONObject("{\"data\":" + response + "}");

        } catch (Exception e) {
           throw new RuntimeException();
        }
    }
    

    public static JSONObject POSTSource(String authToken,String app,String uri, String JSON) {
        try {
             URL url =null;
              
            if(app.equals("Finfacts"))
                url = new URL("http://localhost:8080/FinfactsShopOnRestAPI/api/v1/"+ uri);
            else if(app.equals("Destination"))
                url = new URL("http://localhost:8084/ShopOnRestAPI/api/v1/"+ uri);
            else
                url = new URL(DBConnection.getConfig("IAMTokenContextURL")+ uri);
                  
             HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            
            if(!authToken.equals(""))
                con.setRequestProperty("auth-token", authToken);
            
            //String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(JSON);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return new JSONObject("{\"data\":" + response + "}");


        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
