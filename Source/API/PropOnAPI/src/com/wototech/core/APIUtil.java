/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Ramkumar
 */
public class APIUtil {
    public static JSONObject getResourceMap(String resourceName){
        JSONArray resMapArray =  DB.executeQuery("select * from resource_map_t where resource_name='" +resourceName+ "'");
        JSONObject jo=new JSONObject ();
        for (int i = 0; i < resMapArray.length(); i++) {
            JSONObject resMap= resMapArray.getJSONObject(i);
            
            String resName=resMap.getString("object_from").replace("root.", "");
            if(!jo.has(resName))
                jo.put(resName, new JSONArray());
            
            jo.getJSONArray(resName).put(resMap);
        }
         
        return jo;
    }
  
    public static String insertResource(JSONArray postDataArray,String resourceName){
        StringBuilder query=new StringBuilder();
        String primaryKey=null;
        
        //Get the resource Map
        JSONObject resMap = getResourceMap(resourceName);
         
        //Loop all objects
        for (int i = 0; i < postDataArray .length(); i++) {
            JSONObject postData= postDataArray.getJSONObject(i);           
            JSONObject mappedPostData=new JSONObject();
            String tableName="";
            
            
            List<String> fields=new ArrayList<String>();
            //Loop the columns in root of resource mapping
            for (int j = 0; j < resMap.getJSONArray("root") .length(); j++) {
                JSONObject jo= resMap.getJSONArray("root").getJSONObject(j);
                tableName= jo.getString("object_to");
                fields.add(jo.getString("column_to"));
                //Map the colmn names
                if(jo.getString("is_key").equals("1"))
                {   
                    primaryKey=java.util.UUID.randomUUID().toString();
                    mappedPostData.put(jo.getString("column_to"), primaryKey);            
                }
                else if(jo.getString("is_key").equals("2"))
                {   
                    //primaryKey=java.util.UUID.randomUUID().toString();
                    //mappedPostData.put(jo.getString("column_to"), primaryKey);            
                }
                else if(jo.getString("is_key").equals("0"))
                    mappedPostData.put(jo.getString("column_to"), postData.getString(jo.getString("column_from")));
             
            }
            String[] stockArr = new String[fields.size()];
            stockArr = fields.toArray(stockArr);

            //Frame insert Query
            query.append( frameInsertQuery(mappedPostData,tableName,stockArr));
            
            if(primaryKey== null){
                query.append("select LAST_INSERT_ID() into @parentKey;");
                primaryKey="@parentKey";
            }
            Iterator<String> keys = resMap.keys();
            while(keys.hasNext()) {
                String key=keys.next();
                if(!key.equals("root"))
                {
                    for (int j = 0; j < postData.getJSONArray(key) .length(); j++) {  
                        JSONObject postDataSecondLevel= postData.getJSONArray(key).getJSONObject(j);
                        mappedPostData=new JSONObject();
                        fields=new ArrayList<String>();
                        for (int k = 0; k < resMap.getJSONArray(key) .length(); k++) {
                            JSONObject jo= resMap.getJSONArray(key).getJSONObject(k);
                            tableName= jo.getString("object_to");
                            fields.add(jo.getString("column_to"));
                            if(jo.getString("column_from").equals("{parentKey}"))
                                mappedPostData.put(jo.getString("column_to"), primaryKey);
                            else
                                mappedPostData.put(jo.getString("column_to"), postDataSecondLevel.getString(jo.getString("column_from")));
                        }
                        stockArr = new String[fields.size()];
                        stockArr = fields.toArray(stockArr);

                        //Frame insert Query
                        query.append( frameInsertQuery(mappedPostData,tableName,stockArr));
                    
                        
                    }
                  
            
                }
            }
 
        }
       //return query.toString();
       DB.executeNonQuery("insert into invoice_item_t (var_no,qty,invoice_guid) values('4343','10','546de377-1dd5-4395-a771-75c434ef4c3a');\n" +
"insert into invoice_item_t (var_no,qty,invoice_guid) values('4343','10','546de377-1dd5-4395-a771-75c434ef4c3a');");
       return "{invoice_guid:'"+primaryKey+"'}";
                 
       
    }
    
    
    
    
    
    
    
    
    public static void validateMandatory(JSONObject postData,String[] fields){
        for(String f:fields){
             if(postData.isNull(f))
                throw new BusinessException("Mandatory field " + f + " is missing." );
        }
    }
     public static void validateAllowed(JSONObject data,String[] fields){
        JSONObject newData=new JSONObject();
        Iterator<String> keys = data.keys();
        while(keys.hasNext()) {
            String key=keys.next();
            if(!java.util.Arrays.asList(fields).contains(key))
                throw new BusinessException("Field " + key + " is not supported." );
        }
    }
     public static void setDefault(JSONObject data,String field,String value){
        //Field missing or Field vale is null
        if(!data.has(field) || data.isNull(field))
            data.put(field, value);
    }   
    //Useful for post and not for put
     public static JSONObject getWithoutNull(JSONObject data){
  
        JSONObject newData=new JSONObject();
        Iterator<String> keys = data.keys();
        while(keys.hasNext()) {
            String key=keys.next();
            if(!data.isNull(key))
                newData.put(key,data.get(key));
        }
          return newData;
    }
     public static JSONObject selectFields(JSONObject data,String[] fields){
  
        JSONObject newData=new JSONObject();
        Iterator<String> keys = data.keys();
        while(keys.hasNext()) {
            String key=keys.next();
            if(java.util.Arrays.asList(fields).contains(key))
                newData.put(key,data.get(key));
        }
        return newData;
    }
    public static  JSONObject setDefaultPostValues(JSONObject data){
        
        data = getWithoutNull(data); //Clear all null values for post. So that db will auto insert null
        
        setDefault(data, "discount", "0");
        setDefault(data, "tax", "0");
        setDefault(data, "round_off", "0");
        setDefault(data, "pay_mode", "Cash");
        
        
        setDefault(data, "store_no", "Cash"); //Take from settings
        setDefault(data, "reg_no", "Cash"); //Take from settings
        setDefault(data, "sales_tax_no", "0"); //0 or -1 means no tax
        setDefault(data, "tot_qty_words", "0"); //convert to words in server       

        return data;
    }
    
    public static  void validatePostData(JSONObject postData){
        

             
        
        //Verify Main Invoice Quantity is not exceeded for Credit and Debit invoice.
        //Verify Returned Items are reduced while calculating Main Invoice Quantity.
        if(postData.get("invoice_type").toString().equals("SalesCredit") || postData.get("invoice_type").toString().equals("SalesDebit") || postData.get("invoice_type").toString().equals("PrchaseCredit") || postData.get("invoice_type").toString().equals("PurchaseDebit")){
            //Read from Main Invoice and verify quantity is not grater than master invoice
            throw new BusinessException("Invalid Invoice Type.");    
        }
        
        
    }
    
     public   static  String frameInsertQuery(JSONObject jo,String tableName,String[] fields){
             return frameInsertQuery( jo,tableName,fields,"");
         }
         
    public  static         String frameInsertQuery(JSONObject jo,String tableName,String[] fields,String parentKey){
                    StringBuilder columns = new StringBuilder();
                    StringBuilder values = new StringBuilder();
                    String sep="";
                    for(String field:fields){
                        if(jo.has(field)){
                            columns.append(sep + field);
                            if(jo.get(field).equals("@parentKey"))
                                values.append(sep  + jo.get(field) );
                            else
                                values.append(sep + "'" + jo.get(field) + "'");
                            if(sep.equals(""))
                                sep=",";
                        }
                       
                    }
                    return "insert into "+tableName+" (" + columns.toString()  + ") values(" + values.toString().replace("'@parentKey'",parentKey ) + ");";
               }
               
}
