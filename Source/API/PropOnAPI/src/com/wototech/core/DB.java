package com.wototech.core;

import java.util.Iterator;
import javax.ws.rs.core.HttpHeaders;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.joda.time.LocalDateTime;

import org.json.*;

public class DB {
    
     public static JSONArray executeQuery(String query){
          if(TransactionScope.currentConnection==null || TransactionScope.currentConnection.get() ==null || TransactionScope.currentConnection.get().size()==0){
                 

                DBConnection db = new DBConnection(false);   //uncomment this for prod
                JSONArray data = (JSONArray) db.executeQuery(query);               
                db.close(true);

                return data;
            }else{
                java.time.Instant start = java.time.Instant.now();
                JSONArray data = (JSONArray) TransactionScope.currentConnection.get().peek().executeQuery(query);
                java.time.Instant end = java.time.Instant.now();
                return data;
            }
    }
      public static JSONArray executeNonQuery(String query){
          if(TransactionScope.currentConnection==null || TransactionScope.currentConnection.get() ==null || TransactionScope.currentConnection.get().size()==0){
                 

                DBConnection db = new DBConnection(false);   //uncomment this for prod
                JSONArray data = (JSONArray) db.executeNonQuery(query);               
                db.close(true);

                return data;
            }else{
                java.time.Instant start = java.time.Instant.now();
                JSONArray data = (JSONArray) TransactionScope.currentConnection.get().peek().executeNonQuery(query);
                java.time.Instant end = java.time.Instant.now();
                return data;
            }
    }
     
     
     
   public static  void log(String methodName, String methodParams,String message){
       /*if( (Boolean.parseBoolean(DBConnection.getConfig("EnableLog"))) ){
            JSONObject data = new JSONObject();
            data.put("method_name", methodName);
            data.put("method_param", methodParams);
            data.put("message", message);
            data.put("log_date", LocalDateTime.now());
            DB.Execute("LogAPI.postValue", data);
       }*/
       
    }
    public static  void log(String message){
      //if( (Boolean.parseBoolean(DBConnection.getConfig("EnableLog"))) ){
            JSONObject data = new JSONObject();
            data.put("message", message);
            DB.Execute("LogAPI.postValue", data);
       //}
       
    }
   
   
    //public static  DBConnection dbStatic = new DBConnection(false);   //comment this for prod
    public static JSONArray Execute(String methodName, JSONObject methodParam) {
        
            if(TransactionScope.currentConnection==null || TransactionScope.currentConnection.get() ==null || TransactionScope.currentConnection.get().size()==0){
                 
                java.time.Instant start = java.time.Instant.now();
                DBConnection db = new DBConnection(false);   //uncomment this for prod
                JSONArray data = (JSONArray) db.Execute(methodName, methodParam);
                
                db.close(true);
                
                java.time.Instant end = java.time.Instant.now();
                if(!methodName.equals("LogAPI.postValue"))
                  log(methodName + " Time taken : " + java.time.Duration.between(start, end).toMillis());
                return data;
            }else{
                java.time.Instant start = java.time.Instant.now();
                JSONArray data = (JSONArray) TransactionScope.currentConnection.get().peek().Execute(methodName, methodParam);
                java.time.Instant end = java.time.Instant.now();
                if(!methodName.equals("LogAPI.postValue"))
                  log(methodName + " Time taken : " + java.time.Duration.between(start, end).toMillis());
                return data;
            }
            
        
    }

    public static Response ExecuteAsResponse(String methodName, JSONObject methodParam) {
        return Response.status(200).entity(DB.Execute(methodName, methodParam).toString()).build();
    }
   
     public static JSONObject AllInput(UriInfo uriInfo,String compId) {
         JSONObject inputParam = new JSONObject();
             
        try {
            //get all queryParam
            MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
            Iterator<String> it = pathParams.keySet().iterator();
            while (it.hasNext()) {
                String theKey = (String) it.next();
                inputParam.put(theKey, pathParams.getFirst(theKey));
            }
        } catch (Exception ex) {
        }

        try {
            //get all queryParam
            MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
            Iterator<String> it = queryParams.keySet().iterator();
            while (it.hasNext()) {
                String theKey = (String) it.next();
                inputParam.put(theKey, queryParams.getFirst(theKey));
            }
        } catch (Exception ex) {
        }
        inputParam.put("comp_id", compId);
        return inputParam;
     }
     
     public static JSONObject AllInput(UriInfo uriInfo, HttpHeaders httpHeaders) {
         JSONObject input = AllInput(uriInfo);
         input.put("comp_id", httpHeaders.getRequestHeader("comp_id").get(0));
         
         input.put("store_no", httpHeaders.getRequestHeader("store_no").get(0));
         input.put("langcode", httpHeaders.getRequestHeader("langcode").get(0));
         return input;
         
     }
    public static JSONObject AllInput(UriInfo uriInfo) {

        JSONObject inputParam = new JSONObject();
        
       

        try {
      
            //get all queryParam
            MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
            Iterator<String> it = pathParams.keySet().iterator();
            while (it.hasNext()) {
                String theKey = (String) it.next();
                inputParam.put(theKey, pathParams.getFirst(theKey));
            }
        } catch (Exception ex) {
        }

        try {
            //get all queryParam
            MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
            Iterator<String> it = queryParams.keySet().iterator();
            while (it.hasNext()) {
                String theKey = (String) it.next();
                inputParam.put(theKey, queryParams.getFirst(theKey));
            }
        } catch (Exception ex) {
        }

        return inputParam;
    }

}
