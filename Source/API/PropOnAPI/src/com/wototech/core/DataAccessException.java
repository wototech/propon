/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.core;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author BALU
 */
/*
public class DataAccessException extends RuntimeException{

    String message=null;
    public DataAccessException(Exception e){
        super(e);
        message=e.getMessage();
    }
    public DataAccessException(String message){
       this.message =message;
    }
    
   
    
    public String getUserMessage() {        
            return message;
    }
        
}*/
public class DataAccessException extends WebApplicationException {
    private static final long serialVersionUID = 1L;

    public DataAccessException() {
        this("UnAuthorised");
    }

    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param message the String that is the entity of the 404 response.
     */
    private String errType="";
    public DataAccessException(String errorType) {
        
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(getJSONString(errorType)).type("application/json").build());
        errType=errorType;
    }

    @Override
    public String toString() {
        return errType; //To change body of generated methods, choose Tools | Templates.
    }
    

    public static String getJSONString(String errorType) {
    	
    	String jsonString="";
    	if(errorType.equals("MissingToken"))
    		jsonString = "{\"message\": \"No API Key Found\"}";
    	if(errorType.equals("InvalidToken"))
    		jsonString = "{\"message\": \"API Key is Invalid\"}";
    	
    	return "{\"message\": \""+errorType+"\"}";
    }
}