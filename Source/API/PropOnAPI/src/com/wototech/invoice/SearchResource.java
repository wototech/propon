/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.invoice;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.wototech.core.DB;
import com.wototech.core.Security;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

import javax.ws.rs.*;

/**
 *
 * @author wototech
 */
@Path("/propon/search")

public class SearchResource {

	SearchRepository repository= new SearchRepository();
	
	@GET
	@Path("/items/")
	public Response search(
			@HeaderParam("auth-token") String authToken,
			@HeaderParam("comp_id") String orgId,
			@QueryParam("start") String start,
			@QueryParam("end") String end,
			@QueryParam("sort") String sort,
			@QueryParam("filter") String filter,
			@QueryParam("select") String select,
			@QueryParam("attr_filter") String attrFilter) {
		
		if(filter!=null) {
			filter=filter.replace("eq", "=");
			filter=filter.replace("lt", "<");
		}
			

		return   Response.status(200).entity(repository.searchItem(orgId,start,  end,  sort,  filter,  select,  attrFilter).toString()).build();
	}
	
	@GET
	@Path("/items/filter-attributes/")
	public Response filter(
			@HeaderParam("auth-token") String authToken,
			@HeaderParam("comp_id") String orgId,			
			@QueryParam("filter") String filter,
			@QueryParam("attr_keys") String attrKeys,
			@QueryParam("attr_filter") String attrFilter) {
		
		if(filter!=null)
			filter=filter.replace("eq", "=");

		return   Response.status(200).entity(repository.searchItemFilterAttributes(orgId, attrKeys, filter, attrFilter).toString()).build();
	}

}
