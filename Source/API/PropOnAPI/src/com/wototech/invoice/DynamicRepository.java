package com.wototech.invoice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;

import com.wototech.core.DB;
import com.wototech.core.DBConnection;
import com.wototech.core.TransactionScope;

public class DynamicRepository implements IRepository {

	String resToTable(String resourceName) {
		return resourceName.replace("-", "_") + "_t";
	}

	
	

	ArrayList<String> buildInsertQuery(String uuid,String tableName, JSONObject jsonObject) {

		
		
		ArrayList<String> queryList = new ArrayList<String>();
		String qry = "insert into " + tableName + "(upd_date";

		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONArray) {
				JSONArray jsonArray = (JSONArray) jsonObject.get(key);
				for (int n = 0; n < jsonArray.length(); n++) {
					JSONObject childOject = (JSONObject) jsonArray.get(n);
					if (childOject.has("org_id"))
						childOject.put("org_id", jsonObject.get("org_id"));
					queryList.add(buildInsertQuery(uuid,key + "_t", childOject).get(0));
				}
			} else if (!(jsonObject.get(key) instanceof JSONObject)) {
				qry = qry + "," + key;
			}
		}

		qry = qry + ") values(sysdate()";

		keys = jsonObject.keys();

		while (keys.hasNext()) {
			String key = keys.next();

			if (!(jsonObject.get(key) instanceof JSONObject) && !(jsonObject.get(key) instanceof JSONArray)) {
				if(key.equals("id_base")) {
					qry = qry + ",YEAR(NOW())";
				}else if(key.equals("id_run")) {
					qry = qry + ",(SELECT ifnull(MAX(id_run),0)+1 FROM ( select * from " +tableName+ " ) bill WHERE id_base = YEAR(NOW())) ";
				}else {
					jsonObject.put(key, jsonObject.get(key).toString().replaceAll("##GUID##",uuid) );
					qry = qry + ",'" + jsonObject.get(key).toString()+ "'";
				}
			}
		}

		qry = qry + ");";
		queryList.add(0, qry);
		return queryList;
	}

	ArrayList<String> buildUpdateQuery(String tableName, JSONObject jsonObject) {

		ArrayList<String> queryList = new ArrayList<String>();
		String qry = "update " + tableName + " set upd_date=sysdate()";
		String whereQry = "";
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONArray) {
				JSONArray jsonArray = (JSONArray) jsonObject.get(key);
				for (int n = 0; n < jsonArray.length(); n++) {
					JSONObject childOject = (JSONObject) jsonArray.get(n);
					if (childOject.has("org_id"))
						childOject.put("org_id", jsonObject.get("org_id"));
					queryList.add(buildUpdateQuery(key + "_t", childOject).get(0));
				}
			} else if (!(jsonObject.get(key) instanceof JSONObject)) {
				if (getPK(tableName).contains(key))
					whereQry = whereQry + (whereQry == "" ? "" : " and ") + key + "='" + jsonObject.get(key) + "'";
				else
					qry = qry + "," + key + "='" + jsonObject.get(key) + "'";
			}
		}

		qry = qry + " where " + whereQry;
		queryList.add(0, qry);
		return queryList;
	} 

	String buildSearchQuery(String orgId, String tableName, String start, String end, String sort, String filter,
			String select, String groupBy, String pack,String ref, String resourceId ) {
		if (select == null)
			select = "*";

		String packTables = tableName;
		if (pack != null) {

			for (String packTable : pack.split(",")) {
				packTables = packTables + " inner join " + packTable + " on ";
				String sep = "";
				for (String pk : getPK(tableName)) {
					packTables = packTables + sep + tableName + "." + pk + " = " + packTable + "." + pk;
					sep = " and ";
				}

			}
		}
		if (ref != null) {

			for (String packTable : ref.split(",")) {
				packTables = packTables + " inner join " + packTable + " on ";
				String sep = "";
				for (String pk : getPK(packTable)) {
					packTables = packTables + sep + tableName + "." + pk + " = " + packTable + "." + pk;
					sep = " and ";
				}

			}
		}

		String qry = "select " + select + " from " + packTables + " where " + tableName + ".org_id=" + orgId;
		if (filter != null)
			qry = qry + " and " + filter;
		if (resourceId != null && !resourceId.isEmpty())
			qry = qry + " and " + getPK(tableName).get(0)+"='"+resourceId+"'";
		if (sort != null)
			qry = qry + " order by " + sort;
		if (groupBy != null)
			qry = qry + " group by " + groupBy;
		if (start != null)
			qry = qry + " limit " + start + "," + (Integer.parseInt(end) - Integer.parseInt(start) + 1);

		return qry;

	}

	private ArrayList<String> getPK(String tableName) {
		if (cachePrimaryKeys == null)
			cachePrimaryKeys = getPrimaryKeys();
		return cachePrimaryKeys.get(tableName);
	}

	private HashMap<String, ArrayList<String>> cachePrimaryKeys = null;

	public HashMap<String, ArrayList<String>> getPrimaryKeys() {
		HashMap<String, ArrayList<String>> tempPrimaryKeys = new HashMap<String, ArrayList<String>>();

		JSONArray jsonArray = DB.executeQuery("SELECT t.table_name,k.column_name, k.ordinal_position\r\n"
				+ "		   		FROM information_schema.table_constraints t\r\n"
				+ "		   		JOIN information_schema.key_column_usage k\r\n"
				+ "		   		USING(constraint_name,table_schema,table_name)\r\n"
				+ "		   		WHERE t.constraint_type='PRIMARY KEY'\r\n"
				+ "		   		  AND upper(t.table_schema)=upper('" + DBConnection. getConfig("Schema")+"')");

		for (int n = 0; n < jsonArray.length(); n++) {
			JSONObject jsonObject = (JSONObject) jsonArray.get(n);
			String tableName = ((String) jsonObject.get("TABLE_NAME")).toLowerCase();
			if (!tempPrimaryKeys.containsKey(tableName))
				tempPrimaryKeys.put(tableName, new ArrayList<String>());

			ArrayList<String> arList = tempPrimaryKeys.get(tableName);
			arList.add((String) jsonObject.get("COLUMN_NAME"));
		}
		return tempPrimaryKeys;
	}

	public static String getPriaryKeyWhereClause(String tableName) {
		String qry = "";
		JSONArray jsonArray = DB.executeQuery("SELECT k.column_name\r\n"
				+ "FROM information_schema.table_constraints t\r\n" + "JOIN information_schema.key_column_usage k\r\n"
				+ "USING(constraint_name,table_schema,table_name)\r\n" + "WHERE t.constraint_type='PRIMARY KEY'\r\n"
				+ "  AND t.table_schema='cognizance_dev'\r\n" + "  AND t.table_name='batches_t'");
		for (int n = 0; n < jsonArray.length(); n++) {
			JSONObject jsonObject = (JSONObject) jsonArray.get(n);
			Iterator<String> keys = jsonObject.keys();
			String sep = "";
			while (keys.hasNext()) {
				String key = keys.next();
				qry = qry + sep + key + "='" + jsonObject.get(key) + "'";
				sep = ",";
			}
		}
		return qry;
	}
	public void test() {}

	public JSONArray insert(String orgId, String resourceName, JSONArray jsonArray) {

		TransactionScope transScope = new TransactionScope();
		transScope.beginTransaction();
		try {

			for (int n = 0; n < jsonArray.length(); n++) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(n);
				jsonObject.put("state_no", "30");
				// One company cannot update another company
				if (jsonObject.has("org_id"))
					jsonObject.put("org_id", orgId);

				String uuid=UUID.randomUUID().toString();
				ArrayList<String> queryList = buildInsertQuery(uuid,resToTable(resourceName), jsonObject);
				for (String query : queryList) {
					DB.executeNonQuery(query);
				}
			}

			transScope.commit();
		} catch (Exception e) {
			transScope.rollback();
			throw e;
		}
		return jsonArray;
	}

	public JSONArray update(String orgId, String resourceName, JSONArray jsonArray) {

		TransactionScope transScope = new TransactionScope();
		transScope.beginTransaction();
		try {

			for (int n = 0; n < jsonArray.length(); n++) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(n);
				jsonObject.put("state_no", "30");
				// One company cannot update another company
				if (jsonObject.has("org_id"))
					jsonObject.put("org_id", orgId);

				ArrayList<String> queryList = buildUpdateQuery(resToTable(resourceName), jsonObject);
				for (String query : queryList) {
					DB.executeNonQuery(query);
				}
			}

			transScope.commit();
		} catch (Exception e) {
			transScope.rollback();
			throw e;
		}
		return jsonArray;
	}

	@Override
	public JSONArray search(String orgId, String resourceName, String start, String end, String sort, String filter,
			String select, String groubBy, String pack,String ref,String resourceId) {
		// TODO Auto-generated method stub
		String packTables = null;
		if (pack != null) {
			packTables = "";
			for (String packRes : pack.split(",")) {
				packTables = packTables + (packTables == "" ? "" : ",") + packRes + "_t";
			}	
		}
		
		String refTables = null;
		if (ref != null) {
			refTables = "";
			for (String refRes : ref.split(",")) {
				refTables = refTables + (refTables == "" ? "" : ",") + refRes + "_t";
			}	
		}
		return DB.executeQuery(buildSearchQuery(orgId, resToTable(resourceName), start, end, sort, filter, select,
				groubBy, packTables,refTables,resourceId));
	}

}
