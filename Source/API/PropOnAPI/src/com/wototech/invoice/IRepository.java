package com.wototech.invoice;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IRepository {
	JSONArray insert(String orgId,String resourceName,JSONArray jsonObject);
	JSONArray search(String orgId, String resourceName, String start, String end, String sort, String filter, String select, String groubBy,String pack,String ref,String resourceId);
}
