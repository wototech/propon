/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wototech.invoice;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import com.sun.xml.internal.messaging.saaj.util.Base64;
import com.wototech.core.DB;
import com.wototech.core.Security;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.ws.rs.*;

/**
 *
 * @author wototech
 */
@Path("/propon")

public class DynamicResource {

	DynamicRepository repository= new DynamicRepository();
	@POST
	@Path("/{resourceName}/")
	public Response insert(String json, @HeaderParam("auth-token") String authToken,@HeaderParam("comp_id") String orgId
			,@PathParam("resourceName") String resourceName) {
		
		 
		
		// Security.Authorize(authToken);
		return   Response.status(200).entity(repository.insert(orgId,resourceName, new JSONArray(json)).toString()).build();
	}
	
	@PUT
	@Path("/{resourceName}/")
	public Response update(String json, @HeaderParam("auth-token") String authToken,@HeaderParam("comp_id") String orgId
			,@PathParam("resourceName") String resourceName) {
		
		// Security.Authorize(authToken);
		return   Response.status(200).entity(repository.update(orgId,resourceName, new JSONArray(json)).toString()).build();
	}
	
	@GET
	@Path("/{resourceName}/")
	public Response search(String json, @HeaderParam("auth-token") String authToken,@HeaderParam("comp_id") String orgId
			,@PathParam("resourceName") String resourceName,
			@QueryParam("start") String start,@QueryParam("end") String end,
			@QueryParam("sort") String sort,@QueryParam("filter") String filter,
			@QueryParam("select") String select,@QueryParam("group") String group,@QueryParam("pack") String pack,@QueryParam("ref") String ref) {
		if(filter!=null)
		filter=filter.replace("eq", "=");
		// Security.Authorize(authToken);
		return   Response.status(200).entity(repository.search(orgId,resourceName,start,  end,  sort,  filter,  select,  group,pack,ref,"").toString()).build();
	}
	

	@GET
	@Path("/static/{orgId}/{fileId}")
	@Produces({ "image/jpg"})
	public Response search1(@PathParam("orgId") String orgId,@PathParam("fileId") String fileId) {
		JSONObject jo=(JSONObject)repository.search(orgId,"files",null,  null,  null,  "file_no = '"+fileId+"'",  null,  null,null,null).get(0);
		
		
		
		String base64Image = jo.getString("file_data").split(",")[1];
		byte[] imageBytes1 = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
		ResponseBuilder response = Response.ok(imageBytes1);	
        return response.build();
		
	}

}
