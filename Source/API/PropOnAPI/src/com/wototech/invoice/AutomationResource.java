package com.wototech.invoice;


import com.wototech.core.APIUtil;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


import org.json.JSONObject;

import com.wototech.core.DB;
import com.wototech.core.Security;
import com.wototech.core.UserAccessException;
import org.json.JSONArray;
import org.json.*;
 
@Path("/cognizance/automation/")
public class AutomationResource {
    
       
        //SALE INVOICE
        @GET
	@Path("/test-suite/0001/init/{orgId}")
	public Response createcomp999(@Context UriInfo uriInfo,@PathParam("orgId") String orgId) {
            
        	delete(orgId);
            return Response.status(200).entity("success").build();
		
	}
        
        void delete(String orgId) {
        	 
        	  
              
              
              DB.executeNonQuery("delete from course_subjects_t where org_id="+orgId);
              DB.executeNonQuery("delete from course_exams_t where org_id="+orgId);
              DB.executeNonQuery("delete from course_fees_t where org_id="+orgId);
              DB.executeNonQuery("delete from course_rules_t where org_id="+orgId);
              DB.executeNonQuery("delete from course_batches_t where org_id="+orgId);
              
              DB.executeNonQuery("delete from bills_t where org_id="+orgId);
              DB.executeNonQuery("delete from bill_items_t where org_id="+orgId);
              DB.executeNonQuery("delete from bill_payments_t where org_id="+orgId);
              DB.executeNonQuery("delete from student_results_t where org_id="+orgId);
              
              DB.executeNonQuery("delete from student_batches_t where org_id="+orgId);
              DB.executeNonQuery("delete from sections_t where org_id="+orgId);
              DB.executeNonQuery("delete from batches_t where org_id="+orgId);
              DB.executeNonQuery("delete from courses_t where org_id="+orgId);
              DB.executeNonQuery("delete from students_t where org_id="+orgId);
              DB.executeNonQuery("delete from fee_types_t where org_id="+orgId);
              DB.executeNonQuery("delete from branches_t where org_id="+orgId);
        }
    
        @GET
	@Path("/{sales_item_no}/attribtes")
	@Consumes(MediaType.APPLICATION_JSON) 
	public Response getInvoice(@Context UriInfo uriInfo,@HeaderParam("auth-token") String authToken) {		
            //Security.Authorize(authToken);
            return DB.ExecuteAsResponse("Invoice.getInvoice", DB.AllInput(uriInfo) );
	}
        
     
        
      
        
        
       
}
