package com.wototech.invoice;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;

import com.wototech.core.DB;
import com.wototech.core.TransactionScope;

public class SearchRepository {


	public JSONArray searchItem(String orgId, String start, String end, String sort, String filter,
			String select, String attrFilter) {
		
		String attrFinalQry="";
		String finalPagingQry="";
		String finalSortQry="";
		String finalFilterQry="";
		
		
		    
		if(start!=null && !start.equals("")) 
			finalPagingQry=" LIMIT "+start+", " +(Integer.parseInt(end)-Integer.parseInt(start));	
		
		if(filter!=null && !filter.equals("")) {
			finalFilterQry=" and " + filter;	
		}
			
		if(sort!=null && !sort.equals("") ) 
			finalSortQry=" order by " + sort;
		if(attrFilter!=null && !attrFilter.equals("")) {
			attrFinalQry="";		
			for(String attrQry:attrFilter.split(";")) {
				String[] attrQryData=attrQry.split(":");
				attrFinalQry=attrFinalQry + " and exists (select 1 from item_attributes_t iat where items.item_no=iat.item_no and attr_key='"+attrQryData[0]+"' and attr_value in ("+attrQryData[1]+"))";
			}
		}
		
		
		
		String qry="select * from items_t items_temp inner join item_attributes_t itemAttributes " + 
				" on items_temp.item_no=itemAttributes.item_no and items_temp.org_id="+orgId+" and itemAttributes.org_id= " + orgId +
				 " and  items_temp.item_no in (select item_no from ( SELECT  item_no FROM items_t items where items.org_id="+orgId+" "+ finalFilterQry + attrFinalQry + finalSortQry + finalPagingQry + ") ff)";
				
		JSONArray jsonArray = DB.executeQuery(qry);
		JSONArray newJsonArray = new JSONArray();
		HashMap< String, JSONObject> dict=new HashMap<String, JSONObject>();
		for (int n = 0; n < jsonArray.length(); n++) {
			JSONObject childOject = (JSONObject) jsonArray.get(n);
			
			JSONObject newChildOject =null;
			if(dict.containsKey(childOject.get("item_no")))
				newChildOject=dict.get(childOject.get("item_no"));
			else
			{
				  
		            
				newChildOject=new JSONObject();
				newChildOject.put("item_no", childOject.get("item_no"));
				newChildOject.put("item_name", childOject.get("item_name"));
				newChildOject.put("ptype_no", childOject.get("ptype_no"));
				newChildOject.put("location", childOject.get("location"));
				newChildOject.put("user_id", childOject.get("user_id"));
				newChildOject.put("price", childOject.get("price"));
				newChildOject.put("price_plan_id", childOject.get("price_plan_id"));
				newChildOject.put("addr1", childOject.get("addr1"));
				newChildOject.put("addr2", childOject.get("addr2"));
				newChildOject.put("addr3", childOject.get("addr3"));
				newChildOject.put("area", childOject.get("area"));
				newChildOject.put("city", childOject.get("city"));
				newChildOject.put("landmark", childOject.get("landmark"));
				newChildOject.put("business", childOject.get("business"));
				newChildOject.put("state", childOject.get("state"));
				newChildOject.put("project", childOject.get("project"));
				newChildOject.put("action_type", childOject.get("action_type"));
				newChildOject.put("owner_type", childOject.get("owner_type"));
				newChildOject.put("name", childOject.get("name"));
				newChildOject.put("email", childOject.get("email"));
				newChildOject.put("mobile", childOject.get("mobile"));
				newChildOject.put("popularity", childOject.get("popularity"));
             

				
				newChildOject.put("attributes", new JSONObject());
				dict.put((String)childOject.get("item_no"), newChildOject);			
			}
			
			
			JSONObject newAttrObject =(JSONObject)newChildOject.get("attributes");
			newAttrObject.put((String)childOject.get("attr_key"), childOject.get("attr_value"));
			newAttrObject.put((String)childOject.get("attr_key") + "_text", childOject.get("attr_text"));

		}
		
		 Iterator it =dict.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        newJsonArray .put( pair.getValue());
		        it.remove(); // avoids a ConcurrentModificationException
		    }
		
		
		return newJsonArray;
	}

	public JSONArray searchItemFilterAttributes(String orgId, String attrKeys, String filter,String attrFilter) {
		
		String attrFinalQry="";
		String finalFilterQry="";
		
		if(filter!=null && !filter.equals("")) {
			finalFilterQry=" and " + filter;	
		}
		
			
		if(attrFilter!=null && !attrFilter.equals("")) {
			attrFinalQry="";		
			for(String attrQry:attrFilter.split(";")) {
				String[] attrQryData=attrQry.split(":");
				attrFinalQry=attrFinalQry + " and exists (select 1 from item_attributes_t iat where items.item_no=iat.item_no and attr_key='"+attrQryData[0]+"' and attr_value in ("+attrQryData[1]+"))";
			}
		}
		
		
		
		String qry="select distinct attr_key,attr_value,attr_text  from items_t items_temp inner join item_attributes_t itemAttributes " + 
				" on items_temp.item_no=itemAttributes.item_no and items_temp.org_id="+orgId+" and itemAttributes.org_id= " + orgId +
				 " and  items_temp.item_no in (select item_no from ( SELECT  item_no FROM items_t items where items.org_id="+orgId+" "+ finalFilterQry + attrFinalQry+ ") ff) order by attr_key";
		
		
		
		
	
		return DB.executeQuery(qry);
	}

}
