﻿//Hold State,Cache,Constants etc.Single global variable where all connect specific objects should be kept.
//Keep direct child in UPPERCASE.Level lower than that should follow normal js conventions
var CONNECT = new Object();

//MAINPAGE will be given control after core intitialisation
CONNECT.MAINPAGE = null;

//Cache objects
CONNECT.CACHE = new Object();

//UI Controls and UserContorls related objects
CONNECT.UICONTROLS = new Object();

//Connect Constants
CONNECT.CONSTANTS = new Object();

CONNECT.CONSTANTS.DateFormat = "yy-mm-dd";



//Connect Messages
CONNECT.MESSAGES = new Object();
CONNECT.MESSAGES.InvalidDateFormat = "Invalid Date Format.";
CONNECT.MESSAGES.InvalidAccessMessage = "User is not Authenticated";


$(document).ready(function () {

    coreInitGlobalErrorHandler(); //Handle Authentication
    coreInitDependencyManager(); //Support to load dependent JS files
    coreInitPageController(); //Controlling page and usercontrols
    coreInitLogger(); //Add support fsor logging
    coreInitWebMethod(); //Initialise WebMethod Calling Support
    coreInitUtilities(); //Initialise utility methods in Common.js  
    coreInitLoadPage(); //Give control to current page

});


function isInt(value) {
    var x = parseFloat(value);
    //return !isNaN(value) && (x | 0) === x;
    return !isNaN(value);
}
// To Validate Email Address
function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};
// alert box
function ShowDialogBox(title, content, btn1text, btn2text, parameterList) {
    var btn1css;
    var btn2css;

    if (btn1text == '') {
        btn1css = "hidecss";
    } else {
        btn1css = "showcss";
    }

    if (btn2text == '') {
        btn2css = "hidecss";
    } else {
        btn2css = "showcss";
    }
    $("#lblMessage").html(content);

    $("#dialog").dialog({
        resizable: false,
        title: title,
        modal: true,
        width: '400px',
        height: 'auto',
        bgiframe: false,
        hide: { effect: 'scale', duration: 400 },

        buttons: [
                        {
                            text: btn1text,
                            "class": btn1css,
                            "controlid": "btnOk",
                            click: function () {

                                $("#dialog").dialog('close');
                                return true;

                            }
                        },
                        {
                            text: btn2text,
                            "class": btn2css,
                            "controlid": "btnCancel",
                            click: function () {
                                $("#dialog").dialog('close');
                                return false;
                            }
                        }
        ]
    });
}

//confirm dialogue
function ConfirmDialog(title, content, btn1text, btn2text, parameterList) {
    $('<div></div>').appendTo('body')
                    .html('<div><h6>' + content + '?</h6></div>')
                    .dialog({
                        resizable: false,
                        title: title,
                        modal: true,
                        width: '400px',
                        height: 'auto',
                        bgiframe: false,
                        buttons: {
                            Yes: function () {
                                // $(obj).removeAttr('onclick');                                
                                // $(obj).parents('.Parent').remove();

                                $('body').append('<h1>Confirm Dialog Result: <i>Yes</i></h1>');

                                $(this).dialog("close");
                            },
                            No: function () {
                                $('body').append('<h1>Confirm Dialog Result: <i>No</i></h1>');

                                $(this).dialog("close");
                            }
                        },
                        close: function (event, ui) {
                            $(this).remove();
                        }
                    });
};
function coreInitGlobalErrorHandler() {
    $(document).ajaxError(function (event, jqxhr) {
        //InvalidOperation is for XCAG Timeout.
        if (jqxhr.responseText != undefined) {
            var excep = JSON.parse(jqxhr.responseText);
            if (excep.exceptionType == "UserAccessException") {
                // alert("User is not Authenticated");
                window.location.href = "/" + appConfig.root + "/login/view/login-page/login-page.html";
            }
            throw excep;
        }
    });


}

function coreInitPageController() {
    $.pageController = new Object();
    $.pageController.initControlAndEvents = function (page) {
        //Load the contents      .TODO Change to method in future.  
        if ($(page.container).attr("controlHTML")) {
            jQuery.ajax({
                url: $(page.container).attr("controlHTML"),
                success: function (result) {
                    $(page.container).html(result);
                },
                async: false
            });
        }



        if (page.events.page_init)
            page.events.page_init();

        var ucName = $(page.container).attr("control").split(".")[1]; //Get the cotrol name


        //Get all child controls with control attribute and belongs to the current user controls.If a usercontrol is inside current user control then we wont get elements inside the nested user control.
        $(page.container).find("[control^='" + ucName + ".']").each(function () {
            try {
                var controlName = $(this).attr("control").split(".")[1];
                var controlId = $(this).attr("controlId");
                var controlLocation = $(this).attr("controllocation");
                try {
                    if (typeof controlLocation != "undefined")
                        $.require(controlLocation);
                    page.controls[controlId] = $(this)[controlName](); // ex $(this).grid();  //Call default constructor
                } catch (e) {


                    CONNECT.UICONTROLS.error[controlId] = e;
                    page.controls[controlId] = $(this);
                }
                var eventAttr = $(this).attr("event");
                if (eventAttr) {
                    // var events = JSON.parse(eventAttr);
                    $(eventAttr.split(",")).each(function (j, eventName) {
                        page.controls[controlId][eventName.split(":")[0]](page["events"][eventName.split(":")[1]]); //ex $(this).click(page.events.new_click); 
                    });
                }


            } catch (err) {
                $.logger.push(err);
            }
        });

        page.view.controls = page.controls;

        if (page.events.page_load)
            page.events.page_load();
    }
    $.pageController.getPageContext = function (obj) {
        var uniqueId = $(obj).attr("controlid"); // TODO : Generate really unique id by appending parent uersontrol ids
        if (CONNECT.UICONTROLS[uniqueId]) //Fix if control is recreated.
            CONNECT.UICONTROLS[uniqueId].selectedObject = $(obj);
        return {
            getPage: function () {
                //Acquire Unique Storage
                if (!CONNECT.UICONTROLS[uniqueId]) {
                    CONNECT.UICONTROLS[uniqueId] = new Object();
                    CONNECT.UICONTROLS[uniqueId].interface = new Object();


                    CONNECT.UICONTROLS[uniqueId].state = "new"; //new->template->data->destoryed.
                    CONNECT.UICONTROLS[uniqueId].uniqueId = uniqueId;
                    CONNECT.UICONTROLS[uniqueId].selectedObject = $(obj);
                    CONNECT.UICONTROLS[uniqueId].container = $(obj);
                    CONNECT.UICONTROLS[uniqueId].controls = new Object();

                    CONNECT.UICONTROLS.error = {};
                }
                return CONNECT.UICONTROLS[uniqueId];
            },
        };
    }
    $.pageController.getControlContext = function (obj) {
        var sel = obj;
        var str = [];
        while ($(sel)[0].tagName != "BODY") {
            var parentControlName = $(sel).attr("control").split(".")[0];
            if (parentControlName == "page")
                break;

            var parentControl = $(sel).closest("[control$='." + parentControlName + "']");
            str.push($(parentControl).attr("controlid"));
            sel = parentControl;
        }

        var uniqueId = str.reverse().join("_") + "_" + $(obj).attr("controlId"); // TODO : Generate really unique id by appending parent uersontrol ids
        if (CONNECT.UICONTROLS[uniqueId])
            CONNECT.UICONTROLS[uniqueId].selectedObject = $(obj);
        return {
            getControl: function () {
                //Acquire Unique Storage
                if (!CONNECT.UICONTROLS[uniqueId]) {
                    CONNECT.UICONTROLS[uniqueId] = new Object();
                    CONNECT.UICONTROLS[uniqueId].interface = new Object();
                    CONNECT.UICONTROLS[uniqueId].interface.selectedObject = $(obj);


                    CONNECT.UICONTROLS[uniqueId].state = "new"; //new->template->data->destoryed.
                    CONNECT.UICONTROLS[uniqueId].uniqueId = uniqueId;
                    CONNECT.UICONTROLS[uniqueId].selectedObject = $(obj);
                    $(obj).attr("uniqueId", uniqueId);
                    CONNECT.UICONTROLS[uniqueId].container = $(obj);
                    CONNECT.UICONTROLS[uniqueId].controls = new Object();
                }
                return CONNECT.UICONTROLS[uniqueId];
            },
        };

    }
    $.pageController.getPage = function (container, defineControl) {

        var context = $.pageController.getPageContext(container);
        var page = context.getPage();
        if (typeof page.controlName === "undefined") { //Do the intitialisation only one time.
            //Automatically set the control name.
            page.controlName = $(container).attr("control").split(".")[1];

            page.view = {};
            page.events = {};


            page.require = function () {
                for (var i = 0; i < arguments.length; i++) {
                    $.require(arguments[i]);
                }
            }

            page.template = function (url) {
                var self = this;
                $.ajax({
                    url: url,
                    success: function (result) {
                        page.selectedObject.html(result);
                    },
                    async: false,
                    cache: false,
                    dataType: 'HTML'
                });

            }
            if (window.mobile)
                page.viewMode = "Mobile";
            else
                page.viewMode = "Desktop";

            defineControl(page, function (ctrl) {
                return page.controls[ctrl];

            }, page.events);
            $.pageController.initControlAndEvents(page);
        }
        return page.interface;
    }
    $.pageController.getControl = function (container, defineControl) {
        var context = $.pageController.getControlContext(container);
        var page = context.getControl();
        if (typeof page.controlName === "undefined") { //Do the intitialisation only one time.
            page.controlName = $(container).attr("control").split(".")[1];

            page.view = {};
            page.events = {};

            page.require = function () {
                for (var i = 0; i < arguments.length; i++) {
                    $.require(arguments[i]);
                }
            }

            page.template = function (url) {
                var self = this;
                $.ajax({
                    url: url,
                    success: function (result) {
                        page.selectedObject.html(result);
                    },
                    async: false,
                    cache: false,
                    dataType: 'HTML'
                });
            }

            page.attachCommonroperties = function () {
                // Show the textbox
                page.interface.show = function () {
                    $(page.selectedObject).show();
                }
                // Hide the textbox
                page.interface.hide = function () {
                    $(page.selectedObject).hide();
                }
                //Set Style properties
                page.interface.css = function (key, value) {
                    $(page.selectedObject).css(key, value);
                }
                //Set Style properties through CSS class
                page.interface.addClass = function () {
                    //TODO
                }
                //Set Style properties through CSS class
                page.interface.removeClass = function () {
                    //TODO
                }
                page.interface.disable = function (val) {
                    if (val == true) {
                        $(page.selectedObject).attr('disabled', 'disabled');
                        $(page.selectedObject).addClass("disable_style");

                    } else {
                        $(page.selectedObject).removeAttr('disabled');
                        $(page.selectedObject).removeClass("disable_style");
                    }

                }



            }
            if (window.mobile)
                page.viewMode = "Mobile";
            else
                page.viewMode = "Desktop";
            page.attachCommonroperties();
            defineControl(page, function (ctrl) {
                return page.controls[ctrl];

            }, page.events);

            $.pageController.initControlAndEvents(page);
        }
        return page.interface;
    }
}

function coreInitLogger() {
    $.logger = new Array();
}

function coreInitDependencyManager() {
    $.require = function (file) {
        var loaded = false;
        var head = document.getElementsByTagName("head")[0];
        var script = document.createElement('script');
        script.src = file;
        script.type = 'text/javascript';
        //real browsers
        script.onload = function () {
            loaded = true;
        };
        //Internet explorer
        script.onreadystatechange = function () {
            if (this.readyState == 'complete') {
                loaded = true;
            }
        }
        head.appendChild(script);

        while (!loaded) {
            $.loadJS(file); //Dirty wait. TODO add logic to skip after 5 seconds
        }
    }
}

function coreInitWebMethod() {

    $.server = new Object();
    $.server.xhrPool = new Array();
    $.server.ASYNCACHE = new Object();
    $.server.ASYNCOUNTER = 0;

    //Interupt Ajax Request on Demand
    $.server.isBusy = function () {
        return $.server.xhrPool.length > 0;
    }
    $.server.abortAll = function () {
        $($.server.xhrPool).each(function (i, jqXhr) { //  cycle through list of recorded connectionDataService/DataService.jsp
            try {
                jqXhr.abort(); //  aborts connection       
            } catch (e) {

            }
        });
        $.server.xhrPool = new Array();
        $.server.requestStart = null;
    }
    ///finfacts/company/1
    var API_CATALOG = {};
    API_CATALOG["finfacts"] = { url: "http://localhost:8084/FinFactsRestAPI/api/v1", auth_token: "" };
    API_CATALOG["shopon"] = { url: "http://localhost:8084/ShopOnRestAPI/api/v1", auth_token: "" };
    API_CATALOG["rac"] = { url: "http://localhost:8084/IAMRestAPI/api/v1", auth_token: "" };

    $.server.webMethodGET = function (methodName, callback, errorCallback) {
        var mainurl = "api/v1/";


        var result = "";

        $.ajax({
            type: "GET",
            url: mainurl + methodName,
            async: true,
            xhrFields: {
                withCredentials: true
            },
            headers: {
                "auth-token": "91dd9bc2-894f-11e7-8df1-0767ee757e1a"

            },
            success: function (items) {
                items = JSON.parse(items);
                var cnt = $.util.queryString("id", this.url);
                var obj = $.server.ASYNCACHE[cnt];
                result = items;
                delete $.server.ASYNCACHE[cnt];
                if (typeof callback !== "undefined")
                    callback(result, obj);
            },
            error: function (err) {
                if (typeof AsyncError !== "undefined")
                    AsyncError(err.responseJSON);
                var cnt = $.util.queryString("id", this.url);
                delete $.server.ASYNCACHE[cnt];
                if (typeof error !== "undefined")
                    error(JSON.parse(err.responseText));
            }
        });
        $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;

    }

    $.server.webMethodPOST = function (methodName, inputData, callback, errorCallback) {
        var mainurl = "api/v1/";   
        var result = "";

        $.ajax({
            type: "POST",
            url: mainurl + methodName,
            async: true,
            xhrFields: {
                withCredentials: true
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(inputData),
            headers: {
                "auth-token": "91dd9bc2-894f-11e7-8df1-0767ee757e1a"

            },
            success: function (items) {
                items = items;
                var cnt = $.util.queryString("id", this.url);
                var obj = $.server.ASYNCACHE[cnt];
                result = items;
                delete $.server.ASYNCACHE[cnt];
                if (typeof callback !== "undefined")
                    callback(result, obj);
            },
            error: function (err) {
                if (typeof AsyncError !== "undefined")
                    AsyncError(err.responseJSON);
                var cnt = $.util.queryString("id", this.url);
                delete $.server.ASYNCACHE[cnt];
                if (typeof error !== "undefined")
                    error(JSON.parse(err.responseText));
            }
        });
        $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;

    }

    $.server.webMethodPUT = function (methodName, inputData, callback, errorCallback) {

        var result = "";

        $.ajax({
            type: "PUT",
            //url: "http://localhost:8084/CargoRestAPI/api/v1/" + methodName,
            url: "http://104.251.218.116:9080/CargoRestAPI/api/v1/" + methodName,
            async: true,
            xhrFields: {
                withCredentials: true
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(inputData),
            headers: {
                "auth-token": "91dd9bc2-894f-11e7-8df1-0767ee757e1a"

            },
            success: function (items) {
                items = items;
                var cnt = $.util.queryString("id", this.url);
                var obj = $.server.ASYNCACHE[cnt];
                result = items;
                delete $.server.ASYNCACHE[cnt];
                if (typeof callback !== "undefined")
                    callback(result, obj);
            },
            error: function (err) {
                if (typeof AsyncError !== "undefined")
                    AsyncError(err.responseJSON);
                var cnt = $.util.queryString("id", this.url);
                delete $.server.ASYNCACHE[cnt];
                if (typeof error !== "undefined")
                    error(JSON.parse(err.responseText));
            }
        });
        $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;

    }

    $.server.webMethodDELETE = function (methodName, inputData, callback, error, context) {
        //$.server.webMethodDELETE = function (methodName, inputData, callback, errorCallback) {
        var result = "";

        $.ajax({
            type: "DELETE",
            //url: "http://localhost:8084/CargoRestAPI/api/v1/" + methodName,
            url: "http://104.251.218.116:9080/CargoRestAPI/api/v1/" + methodName,
            async: true,
            xhrFields: {
                withCredentials: true
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(inputData),
            headers: {
                "auth-token": "91dd9bc2-894f-11e7-8df1-0767ee757e1a"

            },
            success: function (items) {
                items = items;
                var cnt = $.util.queryString("id", this.url);
                var obj = $.server.ASYNCACHE[cnt];
                result = items;
                delete $.server.ASYNCACHE[cnt];
                if (typeof callback !== "undefined")
                    callback(result, obj);
            },
            error: function (err) {
                if (typeof AsyncError !== "undefined")
                    AsyncError(err.responseJSON);
                var cnt = $.util.queryString("id", this.url);
                delete $.server.ASYNCACHE[cnt];
                if (typeof error !== "undefined")
                    error(JSON.parse(err.responseText));
            }
        });
        $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;

    }


    //WebMethod to interact with Web Serice and NamedQuery in DB
    $.server.webMethod = function (methodName, inputData, callback, error, context) {

        if (androidApp == true) {
            var result = "";
            $.server.ASYNCACHE[$.server.ASYNCOUNTER] = context;
            $.ajax({
                type: "POST",


                //  url: "http://localhost:8084/ShopOn/DataService.jsp" + "?id=" + $.server.ASYNCOUNTER,
                //url: "http://104.251.218.116:9080/PharmaShopOnWeb/DataService.jsp" + "?user_id=1&id=" + $.server.ASYNCOUNTER,
                url: "http://104.251.218.116:8080/SelfieTVWeb/DataService.jsp" + "?user_id=1&id=" + $.server.ASYNCOUNTER,
                //url: "http://104.251.218.116:8080/ShopOnWebSplit/DataService.jsp" + "?user_id=1&id=" + $.server.ASYNCOUNTER,
                //  url: "../../Usability/Controller/" + methodName.split(".")[0] + ".aspx/" + methodName.split(".")[1] + "?id=" + $.server.ASYNCOUNTER,
                // contentType: "application/json; charset=utf-8",ShopOnWebSplit
                data: {
                    'user_id': '1',
                    methodName: methodName,
                    methodParam: inputData
                },
                xhrFields: {
                    withCredentials: true
                },
                //dataType: "json",
                async: true, crossDomain: true,
                success: function (items) {
                    items = JSON.parse(items);
                    var cnt = $.util.queryString("id", this.url);
                    var obj = $.server.ASYNCACHE[cnt];
                    result = items;
                    delete $.server.ASYNCACHE[cnt];
                    if (typeof callback !== "undefined")
                        callback(result, obj);
                },
                error: function (err) {
                    if (typeof AsyncError !== "undefined")
                        AsyncError(err.responseJSON);
                    var cnt = $.util.queryString("id", this.url);
                    delete $.server.ASYNCACHE[cnt];
                    if (typeof error !== "undefined")
                        error(JSON.parse(err.responseText));


                }
            });
            $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;
        } else {
            var result = "";
            $.server.ASYNCACHE[$.server.ASYNCOUNTER] = context;
            $.ajax({
                type: "POST",

                // url: "http://localhost:8084/ShopOn/DataService.jsp" + "?id=" + $.server.ASYNCOUNTER,
                //   url: "http://104.251.218.116:9080/PharmaShopOnWeb/DataService.jsp" + "?id=" + $.server.ASYNCOUNTER,
                url: "http://104.251.218.116:8080/SelfieTVWeb/DataService.jsp" + "?id=" + $.server.ASYNCOUNTER,
                //url: "http://104.251.218.116:8080/ShopOnWebSplit/DataService.jsp" + "?id=" + $.server.ASYNCOUNTER,
                //  url: "../../Usability/Controller/" + methodName.split(".")[0] + ".aspx/" + methodName.split(".")[1] + "?id=" + $.server.ASYNCOUNTER,
                // contentType: "application/json; charset=utf-8",ShopOnWebSplit
                data: {
                    methodName: methodName,
                    methodParam: inputData
                },
                xhrFields: {
                    withCredentials: true
                },
                //dataType: "json",
                async: true,
                crossDomain: true,
                success: function (items) {
                    items = JSON.parse(items);
                    var cnt = $.util.queryString("id", this.url);
                    var obj = $.server.ASYNCACHE[cnt];
                    result = items;
                    delete $.server.ASYNCACHE[cnt];
                    if (typeof callback !== "undefined")
                        callback(result, obj);
                },
                error: function (err) {
                    if (typeof AsyncError !== "undefined")
                        AsyncError(err.responseJSON);
                    var cnt = $.util.queryString("id", this.url);
                    delete $.server.ASYNCACHE[cnt];
                    if (typeof error !== "undefined")
                        error(JSON.parse(err.responseText));


                }
            });
            $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;


        }


    }
    $.server.webMethodNQ = function (methodName, inputData, callback, error, context) {
        var result = "";
        var postData = new Object();
        postData.methodName = methodName;
        postData.methodParams = JSON.stringify(inputData);
        $.server.ASYNCACHE[$.server.ASYNCOUNTER] = context;
        $.ajax({
            type: "POST",
            url: "../Controller/DataReader.aspx/GetConnectData?id=" + $.server.ASYNCOUNTER,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            xhrFields: {
                withCredentials: true
            },
            success: function (items) {
                var cnt = $.util.queryString("id", this.url);
                var obj = $.server.ASYNCACHE[cnt];
                result = items.d;
                delete $.server.ASYNCACHE[cnt];
                if (typeof callback !== "undefined")
                    callback(result, obj);
            },
            error: function (err) {
                //  if (AsyncError)
                //   AsyncError(err.responseJSON);
                var cnt = $.util.queryString("id", this.url);
                delete $.server.ASYNCACHE[cnt];
                if (typeof error !== "undefined")
                    error(JSON.parse(err.responseText));
            }
        });
        $.server.ASYNCOUNTER = $.server.ASYNCOUNTER + 1;
    }
    $.server.webMethodSync = function (methodName, inputData, callback) {

        var result = "";
        // $.server.ASYNCACHE[$.server.ASYNCOUNTER] = context;
        $.ajax({
            type: "POST",

            // url: "http://localhost:8084/ShopOn/DataService.jsp" + "?id=" + $.server.ASYNCOUNTER,
            //  url: "http://104.251.218.116:9080/PharmaShopOnWeb/DataService.jsp" + "?id=" ,
            url: "http://104.251.218.116:8080/SelfieTVWeb/DataService.jsp" + "?id=",
            //url: "http://104.251.218.116:8080/ShopOnWebSplit/DataService.jsp" + "?id=",
            //  url: "../../Usability/Controller/" + methodName.split(".")[0] + ".aspx/" + methodName.split(".")[1] + "?id=" + $.server.ASYNCOUNTER,
            // contentType: "application/json; charset=utf-8",ShopOnWebSplit
            data: {
                methodName: methodName,
                methodParam: inputData
            },
            xhrFields: {
                withCredentials: true
            },
            //dataType: "json",
            async: false,
            crossDomain: true,
            success: function (items) {
                items = JSON.parse(items);
                var cnt = $.util.queryString("id", this.url);
                var obj = $.server.ASYNCACHE[cnt];
                result = items;
                delete $.server.ASYNCACHE[cnt];
                if (typeof callback !== "undefined")
                    callback(result, obj);
            },
            error: function (err) {
                if (typeof AsyncError !== "undefined")
                    AsyncError(err.responseJSON);
                var cnt = $.util.queryString("id", this.url);
                delete $.server.ASYNCACHE[cnt];
                if (typeof error !== "undefined")
                    error(JSON.parse(err.responseText));


            }
        });



    }
    $.server.webMethodNQSync = function (methodName, inputData) {
        var result = null;

        var postData = new Object();
        postData.methodName = methodName;
        postData.methodParams = JSON.stringify(inputData);

        $.ajax({
            type: "POST",
            url: "../Controller/DataReader.aspx/GetConnectData",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            dataType: "json",
            async: false,
            xhrFields: {
                withCredentials: true
            },
            success: function (items) {
                result = items.d;
            },
            failure: function () {
                alert("Error");
            }
        });
        return result;
    }

    //Track each ajax request to server.
    $.ajaxSetup({
        beforeSend: function (jqXhr) {
            if (!$.server.isBusy())
                $.server.requestStart = new Date();
            $.server.xhrPool.push(jqXhr);
        },
        complete: function (jqXhr) {
            var i = $.server.xhrPool.indexOf(jqXhr); //  get index for current connection completed
            if (i > -1) $.server.xhrPool.splice(i, 1); //  removes from list by index
            if (!$.server.isBusy())
                $.server.requestStart = null;
        }
    });

}

function coreInitUtilities() {
    loadUtilityFunction();
}

function coreInitLoadPage() {
    //After framework is intialised, load page control
    if (typeof $("body").attr("control") !== "undefined")
        CONNECT.MAINPAGE = $("body")[$("body").attr("control").split(".")[1]]();


}



function Map() {
    var cmdText = "";
    var sep = "";
    return {
        add: function (key, value, type) {
            if (cmdText != "")
                sep = ",";
            if (type == "Text")
                cmdText = cmdText + sep + key + ";'" + value + "'";
            else if (type == "Date") {
                var dd = value.substring(0, 2);
                var mm = value.substring(3, 5);
                var yyyy = value.substring(6, 10);


                var tt = value.substring(11, 19);
                if (tt.length == 8)
                    cmdText = cmdText + sep + key + ";'" + yyyy + "-" + mm + "-" + dd + " " + tt + "'";
                else
                    cmdText = cmdText + sep + key + ";'" + yyyy + "-" + mm + "-" + dd + "'";


            } else
                cmdText = cmdText + sep + key + ";" + value;
        },
        toString: function () {
            return cmdText;
        }

    };
}

function dbDate(value) {

    var dd = value.substring(0, 2);
    var mm = value.substring(3, 5);
    var yyyy = value.substring(6, 10);
    return yyyy + "-" + mm + "-" + dd;
}


var nvl = function (data, val) {
    if (typeof data == "undefined" || data == null)
        return val;
    else
        return data;

};


function getFullCode(codeStringValue) {
    codeStringValue = pad(codeStringValue, 12);
    var chetVal = 0, nechetVal = 0;
    var codeToParse = codeStringValue;

    for (var index = 0; index < 6; index++) {
        chetVal += parseInt(codeToParse.substring(index * 2 + 1, index * 2 + 2));
        nechetVal += parseInt(codeToParse.substring(index * 2, index * 2 + 1));
    }

    chetVal *= 3;
    var controlNumber = 10 - (chetVal + nechetVal) % 10;
    if (controlNumber == 10) controlNumber = 0;

    codeToParse = codeToParse + "" + (controlNumber);

    return codeToParse;

}
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function ReportTesting() {

    var accountInfo = {
        "Phone": "030-0074321",
        "ContactName": "Maria Anders",
        "Fax": "030-0076545",
        "Address": "Obere Str. 57",
        "CustomerID": "ALFKI",
        "CompanyName": "Alfreds Futterkiste",
        "Country": "Germany",
        "City": "Berlin",
        "ContactTitle": "Sales Representative",
        "Orders": [
       {
           "ShipPostalCode": 51100,
           "ShippedDate": null,
           "OrderDate": null,
           "OrderID": 10248,
           "Freight": 32.38,
           "RequiredDate": null,
           "ShipCity": "Reims",
           "ShipCountry": "France",
           "EmployeeID": 5,
           "ShipVia": 3,
           "CustomerID": "VINET",
           "ShipAddress": "59 rue de l'Abbaye",
           "ShipName": "Vins et alcools Chevalier"
       },
       {
           "ShipPostalCode": "05454-876",
           "ShippedDate": null,
           "OrderDate": null,
           "OrderID": 10250,
           "Freight": 65.83,
           "ShipRegion": "RJ",
           "RequiredDate": null,
           "ShipCity": "Rio de Janeiro",
           "ShipCountry": "Brazil",
           "EmployeeID": 4,
           "ShipVia": 2,
           "CustomerID": "HANAR",
           "ShipAddress": "Rua do Pao 67",
           "ShipName": "Hanari Carnes"
       }
        ]
    };

    //  GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "PDF");

    GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "EXCEL");
    //   GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "WORD");
    //GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "PPT");

    // GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "PPT");

    // GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "CSV");

    // GeneratePrint("ShopOnDev", "Wood.jrxml", accountInfo, "HTML");

    //  GenerateReport(
    // "ShopOnDev", "/balance-sheet/balance-sheet.jrxml", "Finfacts", "report/balance-sheet", "{per_id:12}", "PDF");
}



function GeneratePrint(appName, templateURI, jsonString, exportType, callback) {
    // page.controls.grdSOItems.allData()


    $.ajax({
        type: "POST",
        //  url: "http://104.251.218.116:8080/woto-utility-rest/rest/sendEmail/sales-order",
        url: "http://104.251.218.116:8080/woto-utility-rest/rest/communication/print",
        //CONTEXT.SOEmailURL,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/octet-stream',
            'app-name': appName,
            'template-uri': templateURI,
            'export-type': exportType,

        },
        crossDomain: false,
        // data: JSON.stringify(jsonString),
        data: JSON.stringify({ root: jsonString }),
        contentType: "application/octet-stream; charset=utf-8",
        // dataType: 'pdf',
        success: function (responseData, status, xhr) {
            //  var win = window.open('', '_blank');
            if (exportType == "PDF") {
                // window.open("data:application/pdf;base64, " + responseData);


                var uri = 'data:text/pdf;base64,' + responseData;
                if (callback)
                    callback(responseData);

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "data.pdf";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
            else if (exportType == "HTML") {
                //  $('#output').html(responseData);
                var uri = 'data:text/html;base64,' + responseData;

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "data.html";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
                //window.open("data:application/html;base64, " + responseData);

            }
            else if (exportType == "CSV") {

                var uri = 'data:text/csv;base64,' + responseData;

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "data.csv";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
                //window.open("data:application/csv;charset=UTF-8 " + responseData);

            }
            else if (exportType == "EXCEL") {
                var uri = 'data:text/xls;base64,' + responseData;

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "data.xls";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);


            }

            else if (exportType == "WORD") {
                var uri = 'data:text/docx;base64,' + responseData;

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "data.docx";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
                //   window.open("data:application/docx;charset=UTF-8 " + responseData);

            }

            else if (exportType == "PPT") {
                var uri = 'data:application/pptx;base64,' + responseData;

                var downloadLink = document.createElement("a");
                downloadLink.href = uri;
                downloadLink.download = "data.pptx";

                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
                //      window.open("data:application/pptx;charset=UTF-8 " + responseData);

            }
            //   win.location.href = responseData;

            /*    console.log(responseData);
                window.open("data:application/pdf;base64, " + responseData);
    
                         alert("REport generated Successfully...");*/
        },
        error: function (request, status, error) {
            console.log(request.responseText);

            alert("Error in report");
        }
    });
}



var $$$ = function (uniqueId) {
    var ctrl = CONNECT.UICONTROLS[uniqueId];
    if (ctrl.controlName == "textBox") {
        return {
            value: function (data) {
                return ctrl.interface.val(data);
            },
        };
    } else if (ctrl.controlName == "primaryWriteButton" || ctrl.controlName == "primaryReadButton" || ctrl.controlName == "secondaryButton") {
        return {
            click: function () {
                return ctrl.interface.click();
            }
        };

    } else if (ctrl.controlName == "autoCompleteList") {
        return {
            search: function (term) {
                ctrl.interface.search(term);
            },
        };
    }
    else if (ctrl.controlName == "dropDownList") {
        return {
            value: function (data) {
                ctrl.interface.selectedValue(data);
            },
            index: function (value) {
                ctrl.interface.selectedIndex(value);
            },
            data: function (value) {
                ctrl.interface.selectedData(value);
            },
            append: function (value, text) {
                ctrl.interface.selectedValueAppend(value, text);
            },
            getValue: function (text) {
                ctrl.interface.getValue(text);
            },
        };
    }
    else if (ctrl.controlName == "grid") {

        //action('{ "item_no": 54 }', "edit");
        //value('{ "item_no": 54 }', "input[dataField=pcaName]", "Hellow")
        //$$$("purchaseOrder_grdPOResult").value('{"po_no":67}',"[datafield=vendor_name]")
        //select('{ "item_no": 54 }');
        //$$$("purchaseOrder_grdPOResult").select('{"po_no":67}')
        return {
            value: function (filter, selector, value) {
                if (typeof value != "undefined") {

                    var row = $(ctrl.interface.getRow(JSON.parse(filter))[0]);
                    row.find(selector).val(value).trigger('change');
                } else {
                    var row = $(ctrl.interface.getRow(JSON.parse(filter))[0]);
                    return $(row.find(selector)).first().text().trim();
                }
            },
            select: function (filter) {
                var rowId = $(ctrl.interface.getRow(JSON.parse(filter))[0]).attr("row_id");
                ctrl.interface.selectRow(rowId);
            },
            action: function (filter, actionName) {
                var row = $(ctrl.interface.getRow(JSON.parse(filter))[0]);
                row.find("[action=" + actionName + "]").click();
            }

        };
    }

    else if (ctrl.controlName == "chkBox") {
        return {
            value: function (data) {
                ctrl.interface.selectedValue(data);


            },
        };
    }

    else if (ctrl.controlName == "dateSelector") {
        return {
            setDate: function (date) {
                ctrl.interface.setDate(date);
            },
            getDate: function () {
                ctrl.interface.getDate();
            },
            disable: function (val) {
                ctrl.interface.disable(val);
            },
        };
    }
    else if (ctrl.controlName == "dateTimePicker") {
        return {
            setDate: function (date) {
                ctrl.interface.setDate(date);
            },
            getDate: function () {
                ctrl.interface.getDate();
            },
        };
    }
    else if (ctrl.controlName == "actionButton") {
        return {
            click: function (handler) {
                ctrl.interface.click(handler);
            },
        };
    }
    else if (ctrl.controlName == "tabButton") {
        return {
            click: function (handler) {
                ctrl.interface.click(handler);
            },
        };
    }
    else if (ctrl.controlName == "label") {
        return {
            value: function (val) {
                ctrl.interface.value(val);
            },
            click: function () {
                ctrl.interface.selectedObject.click();
            },
        };
    }
    else if (ctrl.controlName == "labelValue") {
        return {
            value: function (val) {
                ctrl.interface.value(val);
            },
        };
    }
    else if (ctrl.controlName == "hidden") {
        return {
            value: function (data) {
                ctrl.interface.value(data);
            },
        };
    }


}



var lang = {};


function R(value) {
    return lang[CONTEXT.CurrentLanguage][value];
}



